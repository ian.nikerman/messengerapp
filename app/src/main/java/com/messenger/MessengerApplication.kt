package com.messenger

import android.app.Application
import androidx.viewbinding.BuildConfig
import com.messenger.di.appModule
import com.messenger.di.networkModule
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.google.GoogleEmojiProvider
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MessengerApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        setupKoin()

        EmojiManager.install(GoogleEmojiProvider())

    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@MessengerApplication)
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            modules(appModule, networkModule)
        }
    }
}
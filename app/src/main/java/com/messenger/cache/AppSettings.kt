package com.messenger.cache

import android.content.SharedPreferences

class AppSettings(private val sharedPreferences: SharedPreferences) {

    var displayName: String?
        get() = sharedPreferences.getString(SETTING_DISPLAY_NAME, null)
        set(displayName) = sharedPreferences.edit().putString(SETTING_DISPLAY_NAME, displayName)
            .apply()

    var isSecureCallsMode: Boolean
        get() = sharedPreferences.getBoolean(SETTING_SECURE_CALLS, false)
        set(isSecureCallsMode) = sharedPreferences.edit()
            .putBoolean(SETTING_SECURE_CALLS, isSecureCallsMode).apply()

    var isSemiSecureCallsMode: Boolean
        get() = sharedPreferences.getBoolean(SETTING_SEMI_SECURE_CALLS, false)
        set(isSemiSecureCallsMode) = sharedPreferences.edit()
            .putBoolean(SETTING_SEMI_SECURE_CALLS, isSemiSecureCallsMode).apply()

    var isPrivateMode: Boolean
        get() = sharedPreferences.getBoolean(SETTING_PRIVATE_MODE, false)
        set(isPrivateMode) = sharedPreferences.edit()
            .putBoolean(SETTING_PRIVATE_MODE, isPrivateMode).apply()

    var isMicrophoneAvailable: Boolean
        get() = sharedPreferences.getBoolean(SETTING_MICROPHONE_AVAILABILITY, false)
        set(isMicrophoneAvailable) = sharedPreferences.edit()
            .putBoolean(SETTING_MICROPHONE_AVAILABILITY, isMicrophoneAvailable).apply()

    var isNotificationWindowEnabled: Boolean
        get() = sharedPreferences.getBoolean(SETTING_NOTIFICATION_WINDOW, false)
        set(isNotificationWindowEnabled) = sharedPreferences.edit()
            .putBoolean(SETTING_NOTIFICATION_WINDOW, isNotificationWindowEnabled).apply()

    var incomingRingtone: String?
        get() = sharedPreferences.getString(SETTING_INCOMING_MESSAGE_RINGTONE, null)
        set(isExtendedMode) = sharedPreferences.edit()
            .putString(SETTING_INCOMING_MESSAGE_RINGTONE, isExtendedMode).apply()

    var outGoingRingtone: String?
        get() = sharedPreferences.getString(SETTING_OUTGOING_MESSAGE_RINGTONE, null)
        set(outGoingRingtone) = sharedPreferences.edit()
            .putString(SETTING_OUTGOING_MESSAGE_RINGTONE, outGoingRingtone).apply()

    var isAutoDownLoadFilesMode: Boolean
        get() = sharedPreferences.getBoolean(SETTING_AUTO_FILES_DOWNLOAD, false)
        set(isAutoDownLoadFilesMode) = sharedPreferences.edit()
            .putBoolean(SETTING_AUTO_FILES_DOWNLOAD, isAutoDownLoadFilesMode).apply()

    var isDarkMode: Boolean
        get() = sharedPreferences.getBoolean(SETTING_DARK_MODE, false)
        set(isDarkMode) = sharedPreferences.edit().putBoolean(SETTING_DARK_MODE, isDarkMode).apply()

    var tlMessagesExpireTime: String?
        get() = sharedPreferences.getString(SETTING_TIME_LIMITED_EXPIRE_TIME, null)
        set(tlMessagesExpireTime) = sharedPreferences.edit()
            .putString(SETTING_TIME_LIMITED_EXPIRE_TIME, tlMessagesExpireTime).apply()

    var isRemoveTLMessagesMode: Boolean
        get() = sharedPreferences.getBoolean(SETTING_REMOVE_TIME_LIMITED_MESSAGES, false)
        set(isRemoveTLMessagesMode) = sharedPreferences.edit()
            .putBoolean(SETTING_REMOVE_TIME_LIMITED_MESSAGES, isRemoveTLMessagesMode).apply()

    var isSendTLMessagesMode: Boolean
        get() = sharedPreferences.getBoolean(SETTING_SEND_TIME_LIMITED_MESSAGES, false)
        set(isSendTLMessagesMode) = sharedPreferences.edit()
            .putBoolean(SETTING_SEND_TIME_LIMITED_MESSAGES, isSendTLMessagesMode).apply()

    var isFirstSetup: Boolean
        get() = sharedPreferences.getBoolean(FIRST_SETUP_KEY, true)
        set(isFirstSetup) = sharedPreferences.edit().putBoolean(FIRST_SETUP_KEY, isFirstSetup)
            .apply()

    var setupStage: String?
        get() = sharedPreferences.getString(SETUP_STAGE_KEY, null)
        set(setupStage) = sharedPreferences.edit().putString(SETUP_STAGE_KEY, setupStage).apply()

    fun clearCache() {
        sharedPreferences.edit().clear().apply()
    }

    companion object {
        private const val SETTING_DISPLAY_NAME = "setting_display_name"
        private const val SETTING_SECURE_CALLS = "setting_secure_calls"
        private const val SETTING_SEMI_SECURE_CALLS = "setting_semi_secure_calls"
        private const val SETTING_PRIVATE_MODE = "setting_private_mode"
        private const val SETTING_MICROPHONE_AVAILABILITY = "setting_microphone_availability"
        private const val SETTING_NOTIFICATION_WINDOW = "setting_notification_window"
        private const val SETTING_INCOMING_MESSAGE_RINGTONE = "setting_incoming_message_ringtone"
        private const val SETTING_OUTGOING_MESSAGE_RINGTONE = "setting_outgoing_message_ringtone"
        private const val SETTING_AUTO_FILES_DOWNLOAD = "setting_auto_files_download"
        private const val SETTING_DARK_MODE = "setting_dark_mode"
        private const val SETTING_TIME_LIMITED_EXPIRE_TIME = "setting_time_limited_expire_time"
        private const val SETTING_REMOVE_TIME_LIMITED_MESSAGES =
            "setting_remove_time_limited_expire_messages"
        private const val SETTING_SEND_TIME_LIMITED_MESSAGES = "setting_send_time_limited_messages"
        private const val FIRST_SETUP_KEY: String = "FIRST_SETUP_KEY"
        private const val SETUP_STAGE_KEY: String = "SETUP_STAGE_KEY"
    }
}

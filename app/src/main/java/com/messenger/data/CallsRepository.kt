package com.messenger.data

import android.net.Uri
import com.google.gson.GsonBuilder
import com.messenger.models.CacheCalls
import com.messenger.models.CacheChats
import com.messenger.models.CallData
import com.messenger.models.CallStatus
import com.messenger.models.ChatMessage
import com.messenger.models.ConnectionStatus
import com.messenger.models.ContentType
import com.messenger.models.ConversationData
import com.messenger.models.MessageData
import com.messenger.models.MessageStatus
import com.messenger.models.SenderType
import com.messenger.network.ApplicationCache
import com.messenger.utils.extensions.UriDeserializer
import com.messenger.utils.extensions.UriSerializer
import com.messenger.utils.getUTCMilliseconds
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class CallsRepository(
    private val applicationCache: ApplicationCache
) {

    companion object {
        private val TAG = CallsRepository::class.java.simpleName
        private var calls = emptyList<CallData>()
        private var callDuration = 0L
        private var callId: Long? = null
        private var currentRoomId: Long? = null
        private var connectionStatus: ConnectionStatus? = null
        private var chat = emptyList<ChatMessage>()
    }

    /** CALLS */

    suspend fun fetchCalls(
        isUpdate: Boolean = false
    ): List<CallData> = withContext(Dispatchers.IO) {
        when {
            calls.isEmpty() -> {
                calls = fetchCallsFromCache()
                if (calls.isEmpty()) {
                    calls = fetchCallsFromServer()
                    saveCallsToCache()
                }
            }

            isUpdate -> {
                calls = fetchCallsFromServer()
                saveCallsToCache()
            }
        }
        calls
    }

    suspend fun deleteCalls(selectedItems: List<Long>): List<CallData> =
        withContext(Dispatchers.IO) {
            calls = calls.filterNot { selectedItems.contains(it.id) }
            saveCallsToCache()
            calls

        }

    suspend fun removeCalls(): List<CallData> = withContext(Dispatchers.IO) {
        calls = emptyList()
        saveCallsToCache()
        calls
    }

    private suspend fun fetchCallsFromServer(): List<CallData> {
        return withContext(Dispatchers.IO) {
            try {
                emptyList()
            } catch (ex: Exception) {
                emptyList()
            }
        }
    }

    private suspend fun saveCallsToCache() {
        withContext(Dispatchers.IO) {
            when {
                calls.isNotEmpty() -> {
                    try {
                        sortCalls()
                        val cacheCalls = CacheCalls(calls = calls)
                        val gson = GsonBuilder().registerTypeAdapter(
                            Uri::class.java, UriSerializer()
                        ).create()
                        val jsonUpdate = gson.toJson(cacheCalls)
                        applicationCache.calls = jsonUpdate
                    } catch (e: java.lang.Exception) {
                        Timber.tag(TAG).d("saveChatsToCache parse exception: $e")
                    }
                }

                else -> {
                    applicationCache.calls = ""
                }
            }
        }
    }

    private suspend fun fetchCallsFromCache(
    ): List<CallData> = withContext(Dispatchers.IO) {
        val cacheCalls = applicationCache.calls
        when {
            cacheCalls.isNullOrEmpty() -> emptyList()
            else -> {
                try {
                    val gson = GsonBuilder().registerTypeAdapter(
                        Uri::class.java, UriDeserializer()
                    ).create()
                    val json: String = cacheCalls
                    val calls = gson.fromJson(json, CacheCalls::class.java)
                    calls.calls
                } catch (e: java.lang.Exception) {
                    Timber.tag(TAG).d("fetchCallsFromCache parse exception: $e")
                    emptyList()
                }
            }
        }
    }

    private fun sortCalls() {
        calls = calls.sortedByDescending { it.createdAt }
    }

    /** CALL */

    private fun resetCallData() {
        callId = null
        connectionStatus = null
        callDuration = 0L
    }

    private fun getCallStatus() = when (connectionStatus) {
        ConnectionStatus.CONNECTING -> CallStatus.OUTGOING_CALL_CANCELLED
        else -> CallStatus.OUTGOING_CALL
    }

    fun updateCallDuration(): Long {
        callDuration += 1000
        return callDuration
    }

    fun updateConnectionStatus(status: ConnectionStatus) {
        connectionStatus = status
    }

    fun getConnectionStatus(): ConnectionStatus? {
        return connectionStatus
    }

    suspend fun prepareCall(conversationData: ConversationData): Boolean =
        withContext(Dispatchers.IO) {
            updateConnectionStatus(ConnectionStatus.CONNECTING)
            callId = calls.size.toLong()
            currentRoomId = conversationData.id
            callId?.let {
                val callData = CallData(
                    id = it,
                    conversationData = conversationData,
                    createdAt = getUTCMilliseconds(),
                    duration = callDuration,
                    callStatus = getCallStatus(),
                    isSelected = false
                )
                calls = calls.plus(callData)
                saveCallsToCache()
            }
            true
        }

    suspend fun makeCall(): Boolean = withContext(Dispatchers.IO) {
        updateConnectionStatus(ConnectionStatus.CONNECTED)
        calls = calls.map {
            it.copy(
                callStatus = if (it.id == callId) getCallStatus() else it.callStatus,
                duration = if (it.id == callId) callDuration else it.duration
            )
        }
        saveCallsToCache()
        true
    }

    suspend fun endCall(): Boolean = withContext(Dispatchers.IO) {
        calls = calls.map {
            it.copy(
                callStatus = if (it.id == callId) getCallStatus() else it.callStatus,
                duration = if (it.id == callId) callDuration else it.duration
            )
        }
        saveCallsToCache()
        saveCallsToChatCache()
        resetCallData()
        true
    }

    private suspend fun saveCallsToChatCache() {
        withContext(Dispatchers.IO) {
            fetchChat()
            val index = chat.size.toLong()
            val call = calls.find { it.id == callId }
            call?.let {
                val chatMessage = createChatMessage(index, call)
                val isSameDay = chat.isNotEmpty()
                if (!isSameDay) {
                    chat = chat.plus(
                        ChatMessage(
                            id = index,
                            message = "",
                            senderType = SenderType.DATE,
                            createdAt = chatMessage.createdAt,
                            type = MessageStatus.UI,
                            messageData = null,
                            isSelected = false,
                        )
                    ).plus(
                        chatMessage.copy(
                            id = index + 1
                        )
                    )
                } else {
                    chat = chat.plus(
                        chatMessage.copy(
                            id = index
                        )
                    )
                }
                currentRoomId?.let {
                    saveChatToCache(it)
                }
            }
        }
    }

    private fun createChatMessage(
        index: Long, call: CallData
    ) = ChatMessage(
        id = index,
        message = "",
        senderType = SenderType.ME,
        createdAt = call.createdAt,
        type = MessageStatus.SENT,
        messageData = MessageData(id = call.id, contentType = ContentType.CALL, callData = call),
        isSelected = false,
    )

    private suspend fun fetchChat(): List<ChatMessage> = withContext(Dispatchers.IO) {
        currentRoomId?.let {
            chat = fetchChatFromCache(it)
            if (chat.isEmpty()) {
                chat = fetchChatFromServer(it)
                saveChatToCache(it)
            }
        }
        chat
    }

    private suspend fun fetchChatFromServer(roomId: Long): List<ChatMessage> {
        return withContext(Dispatchers.IO) {
            try {
                emptyList()
            } catch (ex: Exception) {
                emptyList()
            }
        }
    }

    private suspend fun fetchChatFromCache(roomId: Long): List<ChatMessage> =
        withContext(Dispatchers.IO) {
            val cacheChats = applicationCache.chats
            when {
                cacheChats.isNullOrEmpty() -> emptyList()
                else -> {
                    try {
                        val gson = GsonBuilder().registerTypeAdapter(
                            Uri::class.java, UriDeserializer()
                        ).create()
                        val json: String = cacheChats
                        val chats = gson.fromJson(json, CacheChats::class.java)
                        chats.chats[roomId]?.sortedBy { it.id } ?: emptyList()
                    } catch (e: java.lang.Exception) {
                        Timber.tag(TAG).d("fetchChatFromCache parse exception: $e")
                        emptyList()
                    }
                }
            }
        }

    private suspend fun saveChatToCache(roomId: Long) {
        withContext(Dispatchers.IO) {
            val cacheContacts = applicationCache.chats
            try {
                when {
                    cacheContacts.isNullOrEmpty() -> {
                        val updatedCacheChats = CacheChats(chats = mapOf(Pair(roomId, chat)))
                        val updatedGSon = GsonBuilder().registerTypeAdapter(
                            Uri::class.java, UriSerializer()
                        ).create()
                        val jsonUpdate = updatedGSon.toJson(updatedCacheChats)
                        applicationCache.chats = jsonUpdate
                    }

                    else -> {
                        val gson = GsonBuilder().registerTypeAdapter(
                            Uri::class.java, UriDeserializer()
                        ).create()
                        val json: String = applicationCache.chats ?: ""
                        var cacheChats: CacheChats = gson.fromJson(json, CacheChats::class.java)

                        cacheChats =
                            cacheChats.copy(chats = cacheChats.chats.filter { it.key != roomId }
                                .plus(Pair(roomId, chat)))

                        val updatedCacheChats = CacheChats(chats = cacheChats.chats)
                        val updatedGSon = GsonBuilder().registerTypeAdapter(
                            Uri::class.java, UriSerializer()
                        ).create()
                        val jsonUpdate = updatedGSon.toJson(updatedCacheChats)
                        applicationCache.chats = jsonUpdate
                    }
                }
            } catch (e: java.lang.Exception) {
                Timber.tag(TAG).d("saveChatToCache exception: $e")
            }
        }
    }
}
package com.messenger.data

import android.net.Uri
import timber.log.Timber
import com.google.gson.GsonBuilder
import com.messenger.models.CacheChats
import com.messenger.models.ChatMessage
import com.messenger.models.MessageStatus
import com.messenger.models.SenderType
import com.messenger.network.ApplicationCache
import com.messenger.utils.extensions.UriDeserializer
import com.messenger.utils.extensions.UriSerializer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ChatRepository(
    private val applicationCache: ApplicationCache
) {

    companion object {
        private val TAG = ChatRepository::class.java.simpleName
        private var chat = emptyList<ChatMessage>()
        private var currentRoomId: Long? = null
    }

    suspend fun fetchChat(roomId: Long): List<ChatMessage> = withContext(Dispatchers.IO) {
        when {
            currentRoomId != roomId || chat.isEmpty() -> {
                chat = fetchChatFromCache(roomId)
                if (chat.isEmpty()) {
                    chat = fetchChatFromServer(roomId)
                    saveChatToCache(roomId)
                }
            }
        }
        chat
    }

    suspend fun sendMessage(
        roomId: Long, chatMessage: ChatMessage, isSameDay: Boolean
    ): List<ChatMessage> = withContext(Dispatchers.IO) {
        if (!isSameDay) {
            chat = chat.plus(
                ChatMessage(
                    id = (chat.size).toLong(),
                    message = "",
                    senderType = SenderType.DATE,
                    createdAt = chatMessage.createdAt,
                    type = MessageStatus.UI,
                    messageData = null,
                    isSelected = false,
                )
            ).plus(
                chatMessage.copy(
                    id = (chat.size).toLong(), type = MessageStatus.SENT
                )
            )
        } else {
            chat = chat.plus(
                chatMessage.copy(
                    id = (chat.size).toLong(), type = MessageStatus.SENT
                )
            )
        }
        saveChatToCache(roomId)
        chat
    }

    private suspend fun fetchChatFromServer(roomId: Long): List<ChatMessage> {
        return withContext(Dispatchers.IO) {
            try {
                emptyList()
            } catch (ex: Exception) {
                emptyList()
            }
        }
    }

    private suspend fun fetchChatFromCache(roomId: Long): List<ChatMessage> =
        withContext(Dispatchers.IO) {
            val cacheChats = applicationCache.chats
            when {
                cacheChats.isNullOrEmpty() -> emptyList()
                else -> {
                    try {
                        val gson = GsonBuilder().registerTypeAdapter(
                            Uri::class.java, UriDeserializer()
                        ).create()
                        val json: String = cacheChats
                        val chats = gson.fromJson(json, CacheChats::class.java)
                        chats.chats[roomId]?.sortedBy { it.id } ?: emptyList()
                    } catch (e: java.lang.Exception) {
                        Timber.tag(TAG).d("fetchChatFromCache parse exception: $e")
                        emptyList()
                    }
                }
            }
        }

    private suspend fun saveChatToCache(roomId: Long) {
        withContext(Dispatchers.IO) {
            val cacheContacts = applicationCache.chats
            try {
                when {
                    cacheContacts.isNullOrEmpty() -> {
                        val updatedCacheChats = CacheChats(chats = mapOf(Pair(roomId, chat)))
                        val updatedGSon = GsonBuilder().registerTypeAdapter(
                            Uri::class.java, UriSerializer()
                        ).create()
                        val jsonUpdate = updatedGSon.toJson(updatedCacheChats)
                        applicationCache.chats = jsonUpdate
                    }

                    else -> {
                        val gson = GsonBuilder().registerTypeAdapter(
                            Uri::class.java, UriDeserializer()
                        ).create()
                        val json: String = applicationCache.chats ?: ""
                        var cacheChats: CacheChats = gson.fromJson(json, CacheChats::class.java)

                        cacheChats =
                            cacheChats.copy(chats = cacheChats.chats.filter { it.key != roomId }
                                .plus(Pair(roomId, chat)))

                        val updatedCacheChats = CacheChats(chats = cacheChats.chats)
                        val updatedGSon = GsonBuilder().registerTypeAdapter(
                            Uri::class.java, UriSerializer()
                        ).create()
                        val jsonUpdate = updatedGSon.toJson(updatedCacheChats)
                        applicationCache.chats = jsonUpdate
                    }
                }
            } catch (e: java.lang.Exception) {
                Timber.tag(TAG).d("saveChatToCache exception: $e")
            }
        }
    }

    private fun sortChat() {
        chat = chat.sortedByDescending { it.createdAt }
    }
}
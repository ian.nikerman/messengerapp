package com.messenger.data

import android.net.Uri
import timber.log.Timber
import com.google.gson.GsonBuilder
import com.messenger.models.CacheChatsDetails
import com.messenger.models.ChatSettingsTab
import com.messenger.models.ContactData
import com.messenger.models.ContentType
import com.messenger.models.ConversationData
import com.messenger.models.ConversationType
import com.messenger.models.GroupRequest
import com.messenger.network.ApplicationCache
import com.messenger.utils.extensions.UriDeserializer
import com.messenger.utils.extensions.UriSerializer
import com.messenger.utils.getRandomColor
import com.messenger.utils.getUTCMilliseconds
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.random.Random

class ChatsRepository(
    private val applicationCache: ApplicationCache
) {

    companion object {
        private val TAG = ChatsRepository::class.java.simpleName
        private var chats = emptyList<ConversationData>()
    }

    suspend fun fetchChats(isUpdate: Boolean = false): List<ConversationData> =
        withContext(Dispatchers.IO) {
            when {
                chats.isEmpty() -> {
                    chats = fetchChatsFromCache()
                    if (chats.isEmpty()) {
                        chats = fetchChatsFromServer()
                        saveChatsToCache()
                    }
                }

                isUpdate -> {
                    chats = fetchChatsFromServer()
                    saveChatsToCache()
                }
            }
            chats
        }

    private suspend fun fetchChatsFromServer(): List<ConversationData> {
        return withContext(Dispatchers.IO) {
            try {
                emptyList()
            } catch (ex: Exception) {
                emptyList()
            }
        }
    }

    private suspend fun saveChatsToCache() {
        withContext(Dispatchers.IO) {
            if (chats.isNotEmpty()) {
                try {
                    sortChats()
                    val cacheChats = CacheChatsDetails(chats = chats)
                    val gson = GsonBuilder().registerTypeAdapter(
                        Uri::class.java, UriSerializer()
                    ).create()
                    val jsonUpdate = gson.toJson(cacheChats)
                    applicationCache.chatsDetails = jsonUpdate
                } catch (e: java.lang.Exception) {
                    Timber.tag(TAG).d("saveChatsToCache parse exception: $e")
                }
            } else {
                applicationCache.chatsDetails = ""
            }
        }
    }

    private suspend fun fetchChatsFromCache(
    ): List<ConversationData> = withContext(Dispatchers.IO) {
        val cacheChats = applicationCache.chatsDetails
        when {
            cacheChats.isNullOrEmpty() -> emptyList()
            else -> {
                try {
                    val gson = GsonBuilder().registerTypeAdapter(
                        Uri::class.java, UriDeserializer()
                    ).create()
                    val json: String = cacheChats
                    val chats = gson.fromJson(json, CacheChatsDetails::class.java)
                    chats.chats
                } catch (e: java.lang.Exception) {
                    Timber.tag(TAG).d("fetchChatsFromCache parse exception: $e")
                    emptyList()
                }
            }
        }
    }

    suspend fun fetchChatsByQuery(
        query: String? = null
    ): List<ConversationData> {
        return withContext(Dispatchers.IO) {
            if (chats.isEmpty()) {
                chats
            }
            chats.filter {
                it.name.contains(
                    query ?: "", false
                ) || it.members.filter { member -> member.isMe != true }.any { member ->
                    member.name.contains(
                        query ?: "", false
                    )
                } || it.description?.contains(
                    query ?: "", false
                ) ?: false
            }
        }
    }

    suspend fun deleteChats(selectedItems: List<Long>): List<ConversationData> {
        return withContext(Dispatchers.IO) {
            chats = chats.filterNot { selectedItems.contains(it.id) }
            saveChatsToCache()
            chats
        }
    }

    suspend fun muteChats(selectedItems: List<Long>): List<ConversationData> {
        return withContext(Dispatchers.IO) {
            chats =
                chats.map { it.copy(isMute = if (selectedItems.contains(it.id)) true else it.isMute) }
            saveChatsToCache()
            chats
        }
    }

    suspend fun unMuteChats(selectedItems: List<Long>): List<ConversationData> {
        return withContext(Dispatchers.IO) {
            chats =
                chats.map { it.copy(isMute = if (selectedItems.contains(it.id)) false else it.isMute) }
            saveChatsToCache()
            chats
        }
    }

    suspend fun pinChats(selectedItems: List<Long>): List<ConversationData> {
        return withContext(Dispatchers.IO) {
            chats =
                chats.map { it.copy(isPinned = if (selectedItems.contains(it.id)) true else it.isPinned) }
            saveChatsToCache()
            chats
        }
    }

    suspend fun unpinChats(selectedItems: List<Long>): List<ConversationData> {
        return withContext(Dispatchers.IO) {
            chats =
                chats.map { it.copy(isPinned = if (selectedItems.contains(it.id)) false else it.isPinned) }
            saveChatsToCache()
            chats
        }
    }

    suspend fun fetchChatPrimaryDataById(roomId: Long): ConversationData? {
        return withContext(Dispatchers.IO) {
            chats.firstOrNull { it.id == roomId }
        }
    }

    suspend fun createChat(contact: ContactData): ConversationData {
        return withContext(Dispatchers.IO) {
            val conversationItem = ConversationData(
                id = contact.id,
                name = contact.name,
                icon = contact.avatar,
                description = "",
                color = contact.color,
                admin = null,
                isMute = false,
                isPinned = false,
                inviteLink = "https://k.me/${contact.username}",
                members = listOf(contact),
                type = ConversationType.TYPE_ENCRYPTED_CONVERSATION,
                unreadMessages = 0,
                updatedAt = getUTCMilliseconds()
            )
            chats = chats.plus(conversationItem)
            saveChatsToCache()
            conversationItem
        }
    }

    suspend fun createGroup(groupRequest: GroupRequest): Long {
        return withContext(Dispatchers.IO) {
            val roomId = Random.nextLong(
                chats.size.toLong() * 2 + Random.nextLong(1000),
                chats.size.toLong() * 3 + Random.nextLong(1000)
            )
            chats = chats.plus(
                ConversationData(
                    id = roomId,
                    name = groupRequest.name,
                    icon = groupRequest.icon,
                    description = groupRequest.description,
                    color = getRandomColor(),
                    admin = groupRequest.admin,
                    isMute = false,
                    isPinned = false,
                    inviteLink = "https://k.me/${groupRequest.name}",
                    members = groupRequest.members,
                    type = ConversationType.TYPE_SECURE_GROUP,
                    unreadMessages = 0,
                    updatedAt = getUTCMilliseconds()
                )
            )
            saveChatsToCache()
            roomId
        }
    }

    private fun sortChats() {
        chats = chats.sortedByDescending { it.updatedAt }.sortedByDescending { it.isPinned }
    }

    suspend fun getChatInfoTabs(): List<ChatSettingsTab> {
        return withContext(Dispatchers.IO) {
            listOf(
                ChatSettingsTab(
                    id = 0, contentType = ContentType.CONTACT, isSelected = true
                ), ChatSettingsTab(
                    id = 0, contentType = ContentType.MEDIA, isSelected = false
                ), ChatSettingsTab(
                    id = 1, contentType = ContentType.FILE, isSelected = false
                ), ChatSettingsTab(
                    id = 2, contentType = ContentType.LINK, isSelected = false
                ), ChatSettingsTab(
                    id = 3, contentType = ContentType.AUDIO, isSelected = false
                ), ChatSettingsTab(
                    id = 4, contentType = ContentType.VOICE, isSelected = false
                ), ChatSettingsTab(
                    id = 5, contentType = ContentType.GIF, isSelected = false
                ), ChatSettingsTab(
                    id = 6, contentType = ContentType.GROUP, isSelected = false
                )
            )
        }
    }
}
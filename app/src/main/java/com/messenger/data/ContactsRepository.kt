package com.messenger.data

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.provider.ContactsContract
import androidx.annotation.RequiresPermission
import com.google.gson.GsonBuilder
import com.messenger.models.CacheContacts
import com.messenger.models.ContactData
import com.messenger.models.PhoneType
import com.messenger.network.ApplicationCache
import com.messenger.utils.extensions.UriDeserializer
import com.messenger.utils.extensions.UriSerializer
import com.messenger.utils.getRandomColor
import com.messenger.utils.hasPermission
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*

class ContactsRepository(
    private val applicationCache: ApplicationCache, private val contentResolver: ContentResolver
) {

    companion object {
        private val TAG = ChatsRepository::class.java.simpleName
        private val CONTACT_PROJECTION = arrayOf(
            ContactsContract.Contacts._ID,
            ContactsContract.Contacts.LOOKUP_KEY,
            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY,
            ContactsContract.Contacts.HAS_PHONE_NUMBER,
            ContactsContract.Contacts.STARRED
        )
        private var contacts: List<ContactData> = ArrayList()
    }

    suspend fun fetchContacts(context: Context, isUpdate: Boolean): List<ContactData> =
        withContext(Dispatchers.IO) {
            when {
                contacts.isEmpty() -> {
                    contacts = fetchContactsFromCache()
                    if (contacts.isEmpty()) {
                        fetchContactsFromDevice(context)

                    }
                }

                isUpdate -> {
                    fetchContactsFromDevice(context)
                }
            }
            contacts
        }

    @SuppressLint("MissingPermission")
    private suspend fun fetchContactsFromDevice(context: Context) {
        withContext(Dispatchers.IO) {
            if (context.hasPermission(Manifest.permission.READ_CONTACTS)
            ) {
                var updatedContacts = retrieveAllContacts()
                updatedContacts =
                    updatedContacts.filter { contacts.none { contact -> it.id == contact.id } }
                contacts = contacts.plus(updatedContacts)
                contacts = contacts.sortedBy { it.name }
                saveContactsToCache()
            }
        }
    }

    suspend fun fetchContactById(contactId: Long): ContactData? {
        return withContext(Dispatchers.IO) { contacts.firstOrNull { it.id == contactId } }
    }

    suspend fun fetchFavourites(): List<ContactData> {
        return withContext(Dispatchers.IO) { contacts.filter { it.isFavourite } }
    }

    private suspend fun saveContactsToCache() {
        withContext(Dispatchers.IO) {
            if (contacts.isNotEmpty()) {
                try {
                    val cacheContacts = CacheContacts(contacts = contacts)
                    val gson = GsonBuilder().registerTypeAdapter(
                        Uri::class.java, UriSerializer()
                    ).create()
                    val jsonUpdate = gson.toJson(cacheContacts)
                    applicationCache.contacts = jsonUpdate
                } catch (e: java.lang.Exception) {
                    Timber.tag(TAG).d("saveContactsToCache exception: $e")
                }
            }
        }
    }

    private suspend fun fetchContactsFromCache(
    ): List<ContactData> = withContext(Dispatchers.IO) {
        val cacheContacts = applicationCache.contacts
        when {
            cacheContacts.isNullOrEmpty() -> emptyList()
            else -> {
                try {
                    val gson = GsonBuilder().registerTypeAdapter(
                        Uri::class.java, UriDeserializer()
                    ).create()
                    val json: String = cacheContacts
                    val contacts = gson.fromJson(json, CacheContacts::class.java)
                    contacts.contacts
                } catch (e: java.lang.Exception) {
                    Timber.tag(TAG).d("fetchContactsFromCache parse exception: $e")
                    emptyList()
                }
            }
        }
    }

    @RequiresPermission(Manifest.permission.READ_CONTACTS)
    private fun retrieveAllContacts(
        searchPattern: String = ""
    ): List<ContactData> {
        val result: MutableList<ContactData> = mutableListOf()

        try {
            contentResolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                CONTACT_PROJECTION,
                if (searchPattern.isNotBlank()) "${ContactsContract.Contacts.DISPLAY_NAME_PRIMARY} LIKE '%?%'" else null,
                if (searchPattern.isNotBlank()) arrayOf(searchPattern) else null,
                ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " ASC"
            )?.let { cursor ->
                if (cursor.moveToFirst()) {
                    do {
                        val contactId: Long?
                        val name: String?
                        val phonesCount: Int?
                        val isFavorite: Boolean
                        cursor.getColumnIndex(CONTACT_PROJECTION[0])
                            .let { contactId = cursor.getLong(it) }
                        cursor.getColumnIndex(CONTACT_PROJECTION[2]).let {
                            name = cursor.getString(it)
                        }
                        cursor.getColumnIndex(CONTACT_PROJECTION[3]).let {
                            phonesCount = cursor.getString(it).toInt()
                        }
                        cursor.getColumnIndex(CONTACT_PROJECTION[4]).let {
                            isFavorite = cursor.getInt(it) == 1
                        }

                        contactId?.let { id ->
                            val phoneNumber: List<String> = if ((phonesCount ?: 0) > 0) {
                                retrievePhoneNumber(id)
                            } else mutableListOf()
                            val avatar = retrieveAvatar(id)

                            if (phoneNumber.isNotEmpty()) {
                                result.add(
                                    ContactData(
                                        id = id,
                                        name = name ?: "Unknown name",
                                        bio = "",
                                        phoneType = PhoneType.MOBILE,
                                        username = if (name.isNullOrEmpty()) "unknown_$contactId" else "@" + name.replace(
                                            ".", ""
                                        ).lowercase() + "$contactId",
                                        isFavourite = isFavorite,
                                        phoneNumber = phoneNumber,
                                        avatar = avatar,
                                        isSelected = false,
                                        isMe = false,
                                        color = getRandomColor()
                                    )
                                )
                            }
                        }
                    } while (cursor.moveToNext())
                }
                cursor.close()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return result
    }

    private fun retrievePhoneNumber(contactId: Long): List<String> {
        val result: MutableList<String> = mutableListOf()
        try {
            contentResolver.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                "${ContactsContract.CommonDataKinds.Phone.CONTACT_ID} =?",
                arrayOf(contactId.toString()),
                null
            )?.use { cursor ->
                if (cursor.moveToFirst()) {
                    do {
                        cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER).let {
                            result.add(cursor.getString(it))
                        }
                    } while (cursor.moveToNext())
                }
                cursor.close()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return result
    }

    private fun retrieveAvatar(contactId: Long): Uri? {
        return contentResolver.query(
            ContactsContract.Data.CONTENT_URI,
            null,
            "${ContactsContract.Data.CONTACT_ID} =? AND ${ContactsContract.Data.MIMETYPE} = '${ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE}'",
            arrayOf(contactId.toString()),
            null
        )?.use {
            if (it.moveToFirst()) {
                val contactUri = ContentUris.withAppendedId(
                    ContactsContract.Contacts.CONTENT_URI, contactId
                )
                Uri.withAppendedPath(
                    contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY
                )
            } else null
        }
    }

    suspend fun removeFavourites(): List<ContactData> = withContext(Dispatchers.IO) {
        contacts = contacts.map { it.copy(isFavourite = false) }
        saveContactsToCache()
        contacts
    }

    suspend fun deleteFavourites(selectedItems: List<Long>): List<ContactData> =
        withContext(Dispatchers.IO) {
            contacts =
                contacts.map { it.copy(isFavourite = if (selectedItems.contains(it.id)) false else it.isFavourite) }
            saveContactsToCache()
            contacts
        }

    suspend fun checkFavouriteContact(itemId: Long): List<ContactData> =
        withContext(Dispatchers.IO) {
            contacts =
                contacts.map { if (it.id == itemId) it.copy(isFavourite = !it.isFavourite) else it }
            saveContactsToCache()
            contacts
        }

    suspend fun fetchContactsByQuery(query: String): List<ContactData> =
        withContext(Dispatchers.IO) {
            if (query.isEmpty()) {
                emptyList<ContactData>()
            }
            val filteredContacts = contacts.filter {
                it.name.lowercase(Locale.getDefault())
                    .contains(query.lowercase(Locale.getDefault())) || it.username.lowercase(Locale.getDefault())
                    .contains(query.lowercase(Locale.getDefault())) || it.phoneNumber.toString()
                    .contains(query)
            }
            filteredContacts
        }
}
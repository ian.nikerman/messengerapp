package com.messenger.data

import android.content.ContentResolver
import android.net.Uri
import android.provider.MediaStore
import com.messenger.models.GalleryDataType
import com.messenger.models.GalleryMediaData
import com.messenger.models.ResponseState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import timber.log.Timber

class ContentRepository(
    private val contentResolver: ContentResolver
) {

    /** fetch images or files **/
    suspend fun loadMedia(isImages: Boolean): Flow<ResponseState<MutableList<GalleryMediaData>>> {
        val mediaType =
            if (isImages) MediaStore.Images.Media::class.java else MediaStore.Files::class.java
        Timber.tag(TAG).d(LOG_LOAD_IMAGES)
        return flow {
            val data = loadMediaFromStorage(isImages, mediaType)
            emit(ResponseState.success(data))
        }.flowOn(Dispatchers.IO)
    }

    /** load media from storage **/
    private fun loadMediaFromStorage(
        isImages: Boolean,
        mediaType: Class<*>
    ): MutableList<GalleryMediaData> {
        Timber.tag(TAG).d(LOG_LOAD_FROM_STORAGE)
        val listOfAllMedia: MutableList<GalleryMediaData> = ArrayList()
        val uri: Uri = if (isImages) MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        else MediaStore.Files.getContentUri("external")

        val projection = arrayOf(
            MediaStore.MediaColumns._ID,
            MediaStore.MediaColumns.VOLUME_NAME,
            MediaStore.MediaColumns.SIZE
        )

        contentResolver.query(uri, projection, null, null, null)?.use { cursor ->
            val columnIndexID = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns._ID)
            val columnDisplayName =
                cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.VOLUME_NAME)
            val columnSize = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.SIZE)

            while (cursor.moveToNext()) {
                val id = cursor.getLong(columnIndexID)
                val displayName = cursor.getString(columnDisplayName)
                val size = cursor.getLong(columnSize)
                val uriMedia = Uri.withAppendedPath(uri, id.toString())

                listOfAllMedia.add(getGalleryMediaData(id, uriMedia, displayName, size, isImages))
            }
        }

        return listOfAllMedia.reversed().toMutableList()
    }

    private fun getGalleryMediaData(
        id: Long,
        uri: Uri,
        displayName: String,
        size: Long,
        isImages: Boolean
    ) = GalleryMediaData(
        id = id,
        uri = uri,
        title = displayName.takeIf { isImages },
        duration = null,
        size = size.takeIf { !isImages },
        isSelected = false,
        selectCounter = null,
        type = if (isImages) GalleryDataType.IMAGE else GalleryDataType.FILE
    )

    companion object {
        private val TAG = ContentRepository::class.java.simpleName
        const val LOG_LOAD_IMAGES = "loadImages called"
        const val LOG_LOAD_FROM_STORAGE = "loadImagesFromStorage called"
    }
}
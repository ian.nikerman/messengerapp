package com.messenger.data

import android.annotation.SuppressLint
import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Looper
import com.google.android.gms.location.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import timber.log.Timber
import java.io.IOException
import java.util.Locale

class LocationRepository(
    private val context: Context, private val client: FusedLocationProviderClient
) {

    private lateinit var callBack: LocationCallback

    companion object {
        private val TAG = LocationRepository::class.java.simpleName
        private const val MAX_RESULTS = 1
        private const val UPDATE_INTERVAL_SECS = 5L
        private const val UPDATE_DISTANCE_MIN = 200f
    }

    @SuppressLint("MissingPermission")
    suspend fun getLocations(): Flow<Location?> = callbackFlow {
        Timber.tag(TAG).d("getLocations")
        val locationRequest =
            LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, UPDATE_INTERVAL_SECS).apply {
                setMinUpdateDistanceMeters(UPDATE_DISTANCE_MIN)
                setGranularity(Granularity.GRANULARITY_PERMISSION_LEVEL)
                setWaitForAccurateLocation(true)
            }.build()

        callBack = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                Timber.tag(TAG).d("getLocations success")
                val location = locationResult.lastLocation
                trySend(location)
            }
        }

        client.requestLocationUpdates(locationRequest, callBack, Looper.getMainLooper())
        awaitClose { client.removeLocationUpdates(callBack) }
    }

    fun stopLocationUpdates() {
        if (::callBack.isInitialized) {
            client.removeLocationUpdates(callBack)
        }
    }

    fun getCity(location: Location): String {
        try {
            val geoCoder = Geocoder(context, Locale.getDefault())
            val addresses =
                geoCoder.getFromLocation(location.latitude, location.longitude, MAX_RESULTS)
            Timber.tag(TAG).d("getCity succeeded: $addresses")
            if (!addresses.isNullOrEmpty()) {
                val returnAddress =
                    addresses.firstOrNull { !it.locality.isNullOrEmpty() || !it.subAdminArea.isNullOrEmpty() || !it.adminArea.isNullOrEmpty() }
                returnAddress?.let { address ->
                    var cityAddress = "${getLocality(address)}${getSubAdminArea(address)}${
                        getAdminArea(
                            address
                        )
                    }${getCountryName(address)}"
                    if (cityAddress.startsWith(",")) {
                        cityAddress = cityAddress.substringAfter(",")
                    }
                    return when {
                        cityAddress.isNotEmpty() -> cityAddress
                        else -> {
                            getFeatureName(address)
                        }
                    }
                }
            }
        } catch (ex: IOException) {
            Timber.tag(TAG).d("getCity failed: $ex")
        }
        return ""
    }

    fun getStreet(location: Location): String {
        val geoCoder = Geocoder(context, Locale.getDefault())

        try {
            val addresses =
                geoCoder.getFromLocation(location.latitude, location.longitude, MAX_RESULTS)
            Timber.tag(TAG).d("getStreet succeeded: $addresses")
            if (!addresses.isNullOrEmpty()) {
                val returnAddress = addresses.firstOrNull { !it.thoroughfare.isNullOrEmpty() }
                val streetName = returnAddress?.thoroughfare ?: ""
                val homeNumber = returnAddress?.subThoroughfare ?: ""
                return "$streetName $homeNumber"
            }
        } catch (ex: IOException) {
            Timber.tag(TAG).d("getStreet failed: $ex")
        }
        return ""
    }

    private fun getLocality(returnAddress: Address) =
        if (!returnAddress.locality.isNullOrEmpty()) returnAddress.locality else ""

    private fun getFeatureName(returnAddress: Address) =
        if (!returnAddress.featureName.isNullOrEmpty()) returnAddress.featureName else ""

    private fun getSubAdminArea(returnAddress: Address) =
        if (!returnAddress.subAdminArea.isNullOrEmpty()) ", ${returnAddress.subAdminArea}" else ""

    private fun getAdminArea(returnAddress: Address) =
        if (!returnAddress.adminArea.isNullOrEmpty()) ", ${returnAddress.adminArea}" else ""

    private fun getCountryName(returnAddress: Address) =
        if (!returnAddress.countryName.isNullOrEmpty()) ", ${returnAddress.countryName}" else ""

}
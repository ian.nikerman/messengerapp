package com.messenger.data

import android.content.res.Resources
import com.messenger.R
import com.messenger.models.CategoryAttach
import com.messenger.models.CategoryAttachType

class MetaRepository(private val resources: Resources) {

    companion object {
        private val TAG = MetaRepository::class.java.simpleName
    }

    fun fetchAttachCategoriesData(): List<CategoryAttach> {
        return listOf(
            CategoryAttach(
                id = 0,
                name = resources.getString(R.string.camera),
                headerTitle = resources.getString(R.string.camera),
                type = CategoryAttachType.CAMERA,
                icon = R.drawable.ic_btn_camera
            ), CategoryAttach(
                id = 1,
                name = resources.getString(R.string.gallery),
                headerTitle = resources.getString(R.string.gallery),
                type = CategoryAttachType.GALLERY,
                icon = R.drawable.ic_btn_gallery
            ), CategoryAttach(
                id = 2,
                name = resources.getString(R.string.file),
                headerTitle = resources.getString(R.string.set_file),
                type = CategoryAttachType.FILE,
                icon = R.drawable.ic_btn_file
            ), CategoryAttach(
                id = 3,
                name = resources.getString(R.string.location),
                headerTitle = resources.getString(R.string.set_location),
                type = CategoryAttachType.LOCATION,
                icon = R.drawable.ic_btn_location
            ), CategoryAttach(
                id = 4,
                name = resources.getString(R.string.contacts),
                headerTitle = resources.getString(R.string.choose_contacts),
                type = CategoryAttachType.CONTACT,
                icon = R.drawable.ic_btn_contact
            ), CategoryAttach(
                id = 5,
                name = resources.getString(R.string.audio),
                headerTitle = resources.getString(R.string.set_audio),
                type = CategoryAttachType.MUSIC,
                icon = R.drawable.ic_btn_music
            )
        )
    }
}
package com.messenger.data

import android.net.Uri
import timber.log.Timber
import com.google.gson.GsonBuilder
import com.messenger.models.CacheProfile
import com.messenger.models.ContactData
import com.messenger.models.PhoneType
import com.messenger.network.ApplicationCache
import com.messenger.utils.extensions.UriDeserializer
import com.messenger.utils.extensions.UriSerializer
import com.messenger.utils.getRandomColor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ProfileRepository(
    private val applicationCache: ApplicationCache
) {

    companion object {
        private val TAG = ProfileRepository::class.java.simpleName
        private var profile: ContactData? = null
    }

    suspend fun fetchProfile(isUpdate: Boolean = false): ContactData? =
        withContext(Dispatchers.IO) {
            when {
                profile == null -> {
                    profile = fetchProfileFromCache()
                    if (profile == null) {
                        profile = fetchProfileFromServer()
                        saveProfileToCache()
                    }
                }

                isUpdate -> {
                    profile = fetchProfileFromServer()
                    saveProfileToCache()
                }
            }
            profile
        }

    private fun fetchProfileFromServer(): ContactData {
        return ContactData(
            id = 999,
            isMe = true,
            name = "Yuri Orlov",
            phoneType = PhoneType.MOBILE,
            bio = "Android Developer",
            username = "powerman5000",
            isFavourite = false,
            phoneNumber = listOf("555-44-333"),
            color = getRandomColor(),
            avatar = Uri.parse("https://medias.spotern.com/spots/w640/10/10666-1532336916.jpg")
        )
    }

    private suspend fun saveProfileToCache() {
        withContext(Dispatchers.IO) {
            profile?.let {
                try {
                    val cacheContacts = CacheProfile(profile = it)
                    val gson = GsonBuilder().registerTypeAdapter(
                        Uri::class.java, UriSerializer()
                    ).create()
                    val jsonUpdate = gson.toJson(cacheContacts)
                    applicationCache.profile = jsonUpdate
                } catch (e: java.lang.Exception) {
                    Timber.tag(TAG).d("saveProfileToCache parse exception: $e")
                }
            }
        }
    }

    private suspend fun fetchProfileFromCache(
    ): ContactData? = withContext(Dispatchers.IO) {
        val cacheProfile = applicationCache.profile
        val profile = when {
            cacheProfile.isNullOrEmpty() -> null
            else -> {
                try {
                    val gson = GsonBuilder().registerTypeAdapter(
                        Uri::class.java, UriDeserializer()
                    ).create()
                    val json: String = cacheProfile
                    gson.fromJson(json, ContactData::class.java)
                } catch (e: java.lang.Exception) {
                    Timber.tag(TAG).d("fetchProfileFromCache parse exception: $e")
                }
                null
            }
        }
        profile
    }
}
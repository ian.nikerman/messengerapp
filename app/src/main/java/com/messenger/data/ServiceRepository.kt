package com.messenger.data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ServiceRepository {

    /** set owner pin **/
    suspend fun setOwnerPin(
        code: String
    ): Boolean {
        return withContext(Dispatchers.IO) {
            true
        }
    }

    /** set chameleon pin **/
    suspend fun setChameleonPassword(
        code: String
    ): Boolean {
        return withContext(Dispatchers.IO) {
            true
        }
    }

    /** check that entered code equals owner pin **/
    suspend fun equalsOwnerPIN(code: String): Boolean {
        return withContext(Dispatchers.IO) {
            true
        }
    }

    /** connect to WiFi **/
    suspend fun connectToWifi(
        networkSSID: String, password: String
    ): Boolean {
        return withContext(Dispatchers.IO) {
            true
        }
    }

    /** enable WiFi **/
    suspend fun enableWiFi(
    ): Boolean {
        return withContext(Dispatchers.IO) {
            true
        }
    }

    /** start scanning WiFi **/
    suspend fun startWifiScan(
    ) {
        return withContext(Dispatchers.IO) {

        }
    }

    /** get/set for chameleon status **/
    var chameleonStatus: Boolean
        get() = true
        set(chameleonStatus) {
//            service.chameleon = chameleonStatus
        }

    /** get/set for panic status **/
    var panicStatus: Boolean
        get() = true
        set(panicStatus) {
//            service.panic = panicStatus
        }
}
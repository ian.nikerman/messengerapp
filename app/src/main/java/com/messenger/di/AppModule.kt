package com.messenger.di

import com.messenger.data.CallsRepository
import com.messenger.data.ChatRepository
import com.messenger.data.ChatsRepository
import com.messenger.data.ProfileRepository
import com.messenger.features.main.MainViewModel
import com.messenger.features.main.call.CallViewModel
import com.messenger.features.main.callDetails.CallDetailsViewModel
import com.messenger.features.main.camera.CameraViewModel
import com.messenger.features.main.chat.ChatViewModel
import com.messenger.features.main.chat.attachFile.AttachViewModel
import com.messenger.features.main.chatSettings.ChatSettingsViewModel
import com.messenger.features.main.contacts.ContactsViewModel
import com.messenger.features.main.mainPager.MainPagerViewModel
import com.messenger.features.main.newContact.NewContactViewModel
import com.messenger.features.main.newGroup.NewGroupViewModel
import com.messenger.features.main.newGroup.groupMembers.GroupMembersViewModel
import com.messenger.features.main.newGroup.groupName.GroupNameViewModel
import com.messenger.features.main.search.SearchViewModel
import com.messenger.features.main.settings.SettingsViewModel
import com.messenger.features.main.settings.subSettings.profile.ProfileViewModel
import com.messenger.features.onBoarding.OnBoardingViewModel
import com.messenger.features.onBoarding.agreement.AgreementViewModel
import com.messenger.features.onBoarding.emailCode.EnterEmailCodeViewModel
import com.messenger.features.onBoarding.enterDomain.EnterDomainViewModel
import com.messenger.features.onBoarding.enterEmail.EnterEmailViewModel
import com.messenger.features.onBoarding.enterPhone.EnterPhoneViewModel
import com.messenger.features.onBoarding.phoneCode.EnterPhoneCodeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single { ProfileRepository(get()) }
    single { ChatsRepository(get()) }
    single { ChatRepository(get()) }
    single { CallsRepository(get()) }

    // onBoarding
    viewModel { OnBoardingViewModel(get()) }
    viewModel { AgreementViewModel(get()) }
    viewModel { EnterDomainViewModel() }
    viewModel { EnterEmailViewModel() }
    viewModel { EnterEmailCodeViewModel() }
    viewModel { EnterPhoneCodeViewModel(get()) }
    viewModel { EnterPhoneViewModel(get(), get()) }

    // main
    viewModel { MainViewModel(get(), get()) }
    viewModel { MainPagerViewModel(get(), get(), get(), get()) }

    // settings
    viewModel { SettingsViewModel() }
    viewModel { ProfileViewModel(get(), get(), get(), get()) }

    // search
    viewModel { SearchViewModel(get(), get()) }

    // contacts
    viewModel { ContactsViewModel(get()) }
    viewModel { NewContactViewModel(get()) }

    // group
    viewModel { NewGroupViewModel(get(), get()) }
    viewModel { GroupMembersViewModel(get()) }
    viewModel { GroupNameViewModel() }

    // call
    viewModel { CallViewModel(get(), get(), get()) }
    viewModel { CallDetailsViewModel(get(), get(), get(), get()) }

    // chat
    viewModel { ChatViewModel(get(), get(), get()) }
    viewModel { AttachViewModel(get(), get(), get(), get()) }
    viewModel { ChatSettingsViewModel(get(), get(), get()) }

    // camera
    viewModel { CameraViewModel() }
}

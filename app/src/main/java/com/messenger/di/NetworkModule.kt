package com.messenger.di

import android.content.ContentResolver
import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.telephony.TelephonyManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.messenger.cache.AppSettings
import com.messenger.data.ContactsRepository
import com.messenger.data.ContentRepository
import com.messenger.data.LocationRepository
import com.messenger.data.MetaRepository
import com.messenger.network.ApiService
import com.messenger.network.ApiSettings
import com.messenger.network.ApplicationCache
import com.messenger.network.AuthInterceptor
import com.messenger.utils.Constants.Companion.BASE_SERVER_URL
import com.messenger.utils.Constants.Companion.PREFS_KEY
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {

    single { provideSharedPreferences(get()) }
    single { provideContentResolver(get()) }
    single { provideResources(get()) }
    single { provideFusedLocationProviderClient(get()) }
    single { provideTelephonyManager(get()) }
    single { provideDefaultOkhttpClient(get()) }
    single { provideRetrofit(get()) }
    single { provideApiService(get()) }

    factory { AppSettings(get()) }
    factory { ApiSettings(get()) }
    factory { ApplicationCache(get()) }
    factory { MetaRepository(get()) }
    factory { LocationRepository(get(), get()) }
    factory { ContactsRepository(get(), get()) }
    factory { ContentRepository(get()) }
}

fun provideSharedPreferences(context: Context): SharedPreferences {
    return context.getSharedPreferences(
        PREFS_KEY, Context.MODE_PRIVATE
    )
}

fun provideContentResolver(context: Context): ContentResolver {
    return context.contentResolver
}

fun provideResources(context: Context): Resources {
    return context.resources as Resources
}

fun provideFusedLocationProviderClient(context: Context): FusedLocationProviderClient {
    return LocationServices.getFusedLocationProviderClient(context)
}

fun provideTelephonyManager(context: Context): TelephonyManager {
    return context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
}

fun provideDefaultOkhttpClient(apiSettings: ApiSettings): OkHttpClient {
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY

    val authInterceptor = AuthInterceptor(apiSettings)

    val httpClient = OkHttpClient.Builder().addInterceptor(logging).addInterceptor(authInterceptor)
    return httpClient.build()
}

fun provideRetrofit(client: OkHttpClient): Retrofit {

    val gson = GsonBuilder().setLenient().create()

    return Retrofit.Builder().baseUrl(BASE_SERVER_URL).client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(CoroutineCallAdapterFactory()).build()
}

fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(
    ApiService::class.java
)

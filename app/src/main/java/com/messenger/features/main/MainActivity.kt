package com.messenger.features.main

import android.content.Intent
import android.os.Bundle
import timber.log.Timber
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import com.messenger.R
import com.messenger.databinding.ActivityMainBinding
import com.messenger.features.onBoarding.OnBoardingActivity
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()
    private lateinit var binding: ActivityMainBinding

    private companion object {
        private val TAG = MainActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Timber.tag(TAG).d("onCreate")
        initObservers()
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")

        viewModel.fragNavEvent.observe(this) { event ->
            handleNavigationEvent(event)
        }
        viewModel.intentEvent.observe(this) { event ->
            handleIntentEvent(event)
        }
        viewModel.activationStatusData.observe(this) { data ->
            handleActivationStatusData(data)
        }

        viewModel.checkActivation(this)
    }

    private fun handleIntentEvent(event: LiveEvent<MainViewModel.IntentEvent?>?) {
        event?.getContentIfNotHandled()?.let {
            when (it) {
                MainViewModel.IntentEvent.EVENT_FINISH -> finish()
                MainViewModel.IntentEvent.EVENT_BACK -> super.onBackPressed()
            }
        }
    }

    private fun handleActivationStatusData(data: LiveEvent<MainViewModel.ActivationStatus>?) {
        data?.getContentIfNotHandled()?.let {
            Timber.tag(TAG).d("handleActivationStatusData")
            when (it) {
                MainViewModel.ActivationStatus.NOT_ACTIVATED -> {
                    openOnBoarding()
                }

                else -> {}
            }
        }
    }

    private fun openOnBoarding() {
        Timber.tag(TAG).d("openOnBoarding")
        startActivity(Intent(this, OnBoardingActivity::class.java))
        finish()
    }

    private fun handleNavigationEvent(event: LiveEvent<NavDirections?>?) {
        event?.getContentIfNotHandled()?.let {
            Timber.tag(TAG).d("handleNavigationEvent:$event")
            try {
                findNavController(R.id.navigationContainer).navigate(it)
            } catch (ex: java.lang.Exception) {
                Timber.tag(TAG).d("handleNavigationEvent Error: ${ex.message}")
            }
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        Timber.tag(TAG).d("onBackPressed")
        if (onBackPressedDispatcher.hasEnabledCallbacks()) {
            super.onBackPressed()
        } else {
            when (findNavController(R.id.navigationContainer).currentDestination?.id) {
                R.id.mainPagerFragment -> finish()
                else -> super.onBackPressed()
            }
        }
    }
}
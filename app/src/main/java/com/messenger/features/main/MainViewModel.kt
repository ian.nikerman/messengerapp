package com.messenger.features.main

import android.Manifest
import android.content.Context
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.messenger.data.ContactsRepository
import com.messenger.features.main.chat.ChatFragment
import com.messenger.features.main.chat.ChatFragmentDirections
import com.messenger.features.main.chatSettings.ChatSettingsFragment
import com.messenger.features.main.chatSettings.ChatSettingsFragmentDirections
import com.messenger.features.main.contacts.ContactsFragment
import com.messenger.features.main.contacts.ContactsFragmentDirections
import com.messenger.features.main.mainPager.MainPagerFragment
import com.messenger.features.main.mainPager.MainPagerFragmentDirections
import com.messenger.features.main.search.SearchFragment
import com.messenger.features.main.search.SearchFragmentDirections
import com.messenger.features.main.splash.FragmentSplashDirections
import com.messenger.network.ApiSettings
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.hasPermission
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

class MainViewModel(
    private val apiSettings: ApiSettings,
    private val contactsRepository: ContactsRepository
) : ViewModel() {

    companion object {
        private val TAG = MainViewModel::class.java.simpleName
    }

    enum class ErrorEvent {
        ERROR_LOADING_DATA
    }

    val errorEvent = LiveEventData<ErrorEvent>()

    enum class IntentEvent {
        EVENT_FINISH, EVENT_BACK
    }

    enum class ActivationStatus {
        ACTIVATED, NOT_ACTIVATED
    }

    val activationStatusData = LiveEventData<ActivationStatus>()

    val fragNavEvent = LiveEventData<NavDirections?>()
    val intentEvent = LiveEventData<IntentEvent?>()

    fun checkActivation(context: Context) {
        if (apiSettings.token.isNullOrEmpty()) {
            activationStatusData.postRawValue(ActivationStatus.NOT_ACTIVATED)
        } else {
            getContacts(context)
        }
    }

    private fun getContacts(context: Context) {
        Timber.tag(TAG).d("getContacts")
        viewModelScope.launch(Dispatchers.IO) {
            try {
                if (context.hasPermission(Manifest.permission.READ_CONTACTS)
                ) {
                    contactsRepository.fetchContacts(context, true)
                }
                openChats()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_DATA)
            }
        }
    }

    // SCREENS NAVIGATION

    private fun openChats() {
        Timber.tag(TAG).d("openChats")
        fragNavEvent.postRawValue(
            FragmentSplashDirections.actionSplashFragmentToMainPagerFragment()
        )
    }

    fun openContacts(callingFragment: Fragment?) {
        Timber.tag(TAG).d("openContacts")
        val action: NavDirections? = when (callingFragment) {
            is MainPagerFragment -> {
                MainPagerFragmentDirections.actionMainPagerFragmentToContactsFragment()
            }

            else -> null
        }
        action.let {
            fragNavEvent.postRawValue(action)
        }
    }

    fun openChat(callingFragment: Fragment?, roomId: Long) {
        Timber.tag(TAG).d("openChats")
        val action: NavDirections? = when (callingFragment) {
            is MainPagerFragment -> {
                MainPagerFragmentDirections.actionMainPagerFragmentToChatFragment(roomId)
            }

            is ContactsFragment -> {
                ContactsFragmentDirections.actionContactsFragmentToChatFragment(roomId)
            }

            is SearchFragment -> {
                SearchFragmentDirections.actionSearchFragmentToChatFragment(roomId)
            }

            is ChatSettingsFragment -> {
                ChatSettingsFragmentDirections.actionChatSettingsFragmentToChatFragment(roomId)
            }

            else -> null

        }
        action.let {
            fragNavEvent.postRawValue(action)
        }
    }


    fun openCallDetails(callingFragment: Fragment?, roomId: Long) {
        Timber.tag(TAG).d("openChats")
        val action: NavDirections? = when (callingFragment) {
            is MainPagerFragment -> {
                MainPagerFragmentDirections.actionMainPagerFragmentToCallDetailsFragment(roomId)
            }

            else -> null

        }
        action.let {
            fragNavEvent.postRawValue(action)
        }
    }

    fun openCall(callingFragment: Fragment?, roomId: Long) {
        Timber.tag(TAG).d("openCall")
        val action: NavDirections? = when (callingFragment) {
            is MainPagerFragment -> {
                MainPagerFragmentDirections.actionMainPagerFragmentToCallFragment(roomId)
            }

            is ChatFragment -> {
                ChatFragmentDirections.actionChatFragmentToCallFragment(roomId)
            }

            is ChatSettingsFragment -> {
                ChatSettingsFragmentDirections.actionChatSettingsFragmentToCallFragment(roomId)
            }

            else -> null

        }
        action.let {
            fragNavEvent.postRawValue(action)
        }
    }

    fun openChatSettings(roomId: Long) {
        Timber.tag(TAG).d("openChatSettings")
        fragNavEvent.postRawValue(
            ChatFragmentDirections.actionChatFragmentToChatSettingsFragment(roomId)
        )
    }

    fun openSearch() {
        Timber.tag(TAG).d("openSearch")
        fragNavEvent.postRawValue(
            MainPagerFragmentDirections.actionMainPagerFragmentToSearchFragment()
        )
    }

    fun openNewContact(callingFragment: Fragment?) {
        Timber.tag(TAG).d("openNewContact")
        val action: NavDirections? = when (callingFragment) {
            is MainPagerFragment -> {
                MainPagerFragmentDirections.actionMainPagerFragmentToAddContactFragment()
            }

            is ContactsFragment -> {
                ContactsFragmentDirections.actionContactsFragmentToAddContactFragment()
            }

            else -> null

        }
        action.let {
            fragNavEvent.postRawValue(action)
        }
    }

    fun openAttachChat() {
        Timber.tag(TAG).d("openAttachChatFragment")
        fragNavEvent.postRawValue(
            ChatFragmentDirections.actionChatFragmentToFragmentAttach()
        )
    }

    fun finishApp() {
        intentEvent.postRawValue(IntentEvent.EVENT_FINISH)
    }
}
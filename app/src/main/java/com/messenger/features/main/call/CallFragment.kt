package com.messenger.features.main.call

import android.os.Bundle
import android.text.format.DateUtils
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.messenger.R
import com.messenger.databinding.FragmentCallBinding
import com.messenger.features.main.MainViewModel
import com.messenger.features.main.chat.ChatFragmentArgs
import com.messenger.features.views.HeaderView
import com.messenger.models.ConversationData
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class CallFragment : Fragment() {

    private val sharedViewModel by activityViewModel<MainViewModel>()
    private val viewModel: CallViewModel by viewModel()
    private var binding: FragmentCallBinding? = null

    companion object {

        private val TAG = CallFragment::class.java.simpleName

    }

    private val headerListener = object : HeaderView.HeaderViewListener {
        override fun onBackClick() {
            Timber.tag(TAG).d("onBackClick")

        }
    }

    private val onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            if (viewModel.isCallFinished()) {
                isEnabled = false
                requireActivity().onBackPressedDispatcher.onBackPressed()
            } else {
                viewModel.endCall()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCallBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(requireView(), savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            headerView.setListener(headerListener)

            callButton.setOnClickListener { viewModel.endCall() }
            callButton.setOnClickListener { viewModel.endCall() }
            speakerButton.setOnClickListener { }
            videoButton.setOnClickListener { }
            muteButton.setOnClickListener { }
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.chatPrimaryData.observe(viewLifecycleOwner) { data ->
            handleChatPrimaryData(data)
        }
        viewModel.connectionState.observe(viewLifecycleOwner) { state ->
            handleConnectionState(state)
        }
        viewModel.durationData.observe(viewLifecycleOwner) { data ->
            handleDurationData(data)
        }
        viewModel.errorEvent.observe(viewLifecycleOwner) { event ->
            handleErrorEvent(event)
        }
        viewModel.makeCall(roomId = handleRoomId())
    }

    private fun handleDurationData(data: Long?) {
        data?.let {
            binding?.callStatus?.text = DateUtils.formatElapsedTime(it / 1000)
        }
    }

    private fun handleChatPrimaryData(data: ConversationData?) {
        data?.let {
            binding?.apply {
                callName.text = data.name
            }
        }
    }

    private fun handleConnectionState(state: CallViewModel.ConnectionState?) {
        state?.let {
            when (it) {
                CallViewModel.ConnectionState.STATE_CONNECTING -> {
                    binding?.connectionLevel?.isVisible = false
                    binding?.callStatus?.text = getString(R.string.connecting)
                }

                CallViewModel.ConnectionState.STATE_CALLING -> {
                    binding?.connectionLevel?.isVisible = false
                    binding?.callStatus?.text = getString(R.string.calling)
                }

                CallViewModel.ConnectionState.STATE_CONNECTED -> {
                    binding?.connectionLevel?.isVisible = true
                }

                CallViewModel.ConnectionState.STATE_FINISHING -> {
                    binding?.connectionLevel?.isVisible = false
                    binding?.callStatus?.text = getString(R.string.finished)
                }

                CallViewModel.ConnectionState.STATE_CLOSE -> {
                    requireActivity().onBackPressedDispatcher.onBackPressed()
                }
            }
        }
    }

    private fun handleRoomId(): Long {
        val navArgs: ChatFragmentArgs by navArgs()
        return navArgs.roomId
    }

    private fun handleErrorEvent(event: LiveEvent<CallViewModel.ErrorEvent>?) {
        event?.getContentIfNotHandled()?.let {
            when (it) {
                CallViewModel.ErrorEvent.ERROR_DISCONNECTED -> {
                    requireActivity().onBackPressedDispatcher.onBackPressed()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner, onBackPressedCallback
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}

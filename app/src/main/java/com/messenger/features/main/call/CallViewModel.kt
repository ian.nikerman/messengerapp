package com.messenger.features.main.call

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.messenger.data.CallsRepository
import com.messenger.data.ChatsRepository
import com.messenger.data.ContactsRepository
import com.messenger.models.ConnectionStatus
import com.messenger.models.ConversationData
import com.messenger.utils.extensions.LiveEventData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber

class CallViewModel(
    private val callRepository: CallsRepository,
    private val contactsRepository: ContactsRepository,
    private val chatsRepository: ChatsRepository
) : ViewModel() {

    companion object {
        private val TAG = CallViewModel::class.java.simpleName
    }

    private val job = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.Default + job)

    enum class ErrorEvent {
        ERROR_DISCONNECTED
    }

    val errorEvent = LiveEventData<ErrorEvent>()

    enum class ConnectionState {
        STATE_CONNECTING, STATE_CALLING, STATE_CONNECTED, STATE_FINISHING, STATE_CLOSE
    }

    val connectionState = MutableLiveData<ConnectionState>()

    val chatPrimaryData = MutableLiveData<ConversationData?>()
    val durationData = MutableLiveData<Long?>()

    fun makeCall(roomId: Long) {
        Timber.tag(TAG).d("makeCall")
        connectionState.postValue(ConnectionState.STATE_CONNECTING)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                var chatData = chatsRepository.fetchChatPrimaryDataById(roomId)
                when (chatData) {
                    null -> {
                        val contact = contactsRepository.fetchContactById(roomId)
                        contact?.let {
                            chatData = chatsRepository.createChat(it)
                            chatPrimaryData.postValue(chatData)
                        }
                    }

                    else -> chatPrimaryData.postValue(chatData)
                }
                chatData?.let {
                    callRepository.updateConnectionStatus(ConnectionStatus.CONNECTING)
                    callRepository.prepareCall(it)
                    delay(4_000L)
                    connectionState.postValue(ConnectionState.STATE_CALLING)
                    delay(3_000L)
                    when (callRepository.makeCall()) {
                        true -> {
                            connectionState.postValue(ConnectionState.STATE_CONNECTED)
                            startTimer()
                        }

                        else -> connectionState.postValue(ConnectionState.STATE_CONNECTING)
                    }
                }
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_DISCONNECTED)
            }
        }
    }

    fun endCall() {
        Timber.tag(TAG).d("endCall")
        connectionState.postValue(ConnectionState.STATE_FINISHING)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                cancelTimer()
                callRepository.endCall()
                connectionState.postValue(ConnectionState.STATE_CLOSE)
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_DISCONNECTED)
            }
        }
    }

    private fun startCoroutineTimer(
        delayMillis: Long = 0, repeatMillis: Long = 0, action: () -> Unit
    ) = scope.launch(Dispatchers.IO) {
        delay(delayMillis)
        if (repeatMillis > 0) {
            while (true) {
                action()
                delay(repeatMillis)
            }
        } else {
            action()
        }
    }

    private val timer: Job = startCoroutineTimer(delayMillis = 0, repeatMillis = 1000) {
        Timber.tag(TAG).d("Background - tick")
        scope.launch(Dispatchers.Main) {
            Timber.tag(TAG).d("Main thread - tick")
            when (callRepository.getConnectionStatus()) {
                ConnectionStatus.CONNECTED -> durationData.postValue(callRepository.updateCallDuration())
                else -> durationData.postValue(null)
            }
        }
    }

    private fun startTimer() {
        timer.start()
    }

    private fun cancelTimer() {
        timer.cancel()
    }

    fun isCallFinished(): Boolean {
        return callRepository.getConnectionStatus() == null
    }
}
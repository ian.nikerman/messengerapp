package com.messenger.features.main.callDetails

import android.content.res.Resources
import timber.log.Timber
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.messenger.R
import com.messenger.data.ChatRepository
import com.messenger.data.ChatsRepository
import com.messenger.data.ContactsRepository
import com.messenger.models.ChatInfoItem
import com.messenger.models.ChatInfoType
import com.messenger.models.ChatMessage
import com.messenger.models.ChatSettingsTab
import com.messenger.models.ContentData
import com.messenger.models.ContentType
import com.messenger.models.ConversationData
import com.messenger.models.ConversationType
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CallDetailsViewModel(
    private val resources: Resources,
    private val contactsRepository: ContactsRepository,
    private val chatsRepository: ChatsRepository,
    private val chatRepository: ChatRepository
) : ViewModel() {

    companion object {

        private val TAG = CallDetailsViewModel::class.java.simpleName

    }

    enum class ErrorEvent {
        ERROR_LOADING_DATA
    }

    val errorEvent = LiveEventData<ErrorEvent>()
    val progressEvent = ProgressData()

    sealed class ChatInfoDetails {
        data class MainData(
            val mainData: ConversationData, val infoList: List<ChatInfoItem>?
        ) : ChatInfoDetails()

        data class ContentItemData(
            val tabs: List<ChatSettingsTab>,
            val members: List<ContentData>,
            val media: List<ContentData>,
            val files: List<ContentData>,
            val links: List<ContentData>,
            val audios: List<ContentData>,
            val voice: List<ContentData>,
            val gifs: List<ContentData>,
            val groups: List<ContentData>
        ) : ChatInfoDetails()
    }

    val chatMainData = MutableLiveData<ChatInfoDetails.MainData?>()
    val contentData = MutableLiveData<ChatInfoDetails.ContentItemData?>()

    fun getChat(roomId: Long) {
        Timber.tag(TAG).d("getChat")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                var chatData = chatsRepository.fetchChatPrimaryDataById(roomId)
                when (chatData) {
                    null -> {
                        val contact = contactsRepository.fetchContactById(roomId)
                        contact?.let {
                            chatData = chatsRepository.createChat(it)
                            handleMainData(chatData)
                        }
                    }

                    else -> handleMainData(chatData)
                }
                val tabsDataList = chatsRepository.getChatInfoTabs()
                val messages = chatRepository.fetchChat(roomId)
                handleChatExtraData(tabsDataList, chatData, messages)
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_DATA)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    private fun handleMainData(chatData: ConversationData?) {
        chatData?.let {
            chatMainData.postValue(
                ChatInfoDetails.MainData(
                    mainData = chatData, infoList = getChatUser(chatData)
                )
            )
        }
    }

    private fun getChatUser(chatData: ConversationData): List<ChatInfoItem> = when (chatData.type) {
        ConversationType.TYPE_ENCRYPTED_CONVERSATION -> {
            val user = chatData.members.first { it.isMe != true }
            listOf(
                ChatInfoItem(
                    id = 0,
                    title = user.phoneNumber.first(),
                    subtitle = resources.getString(R.string.mobile),
                    contactData = user,
                    type = ChatInfoType.PHONE
                ), ChatInfoItem(
                    id = 1,
                    title = user.username,
                    subtitle = resources.getString(R.string.username),
                    contactData = user,
                    type = ChatInfoType.USERNAME
                )
            )
        }

        ConversationType.TYPE_SECURE_GROUP -> {
            listOf(
                ChatInfoItem(
                    id = 0,
                    title = chatData.inviteLink,
                    subtitle = resources.getString(R.string.invite_link),
                    contactData = null,
                    type = ChatInfoType.INVITE_LINK
                )
            )
        }
    }

    private fun handleChatExtraData(
        tabsData: List<ChatSettingsTab>, chatData: ConversationData?, messages: List<ChatMessage>
    ) {
        val members = getMembersDataFromChat(chatData)
        contentData.postValue(
            ChatInfoDetails.ContentItemData(
                tabs = tabsData,
                members = members,
                media = getDataFromChat(messages, ContentType.MEDIA),
                files = getDataFromChat(messages, ContentType.FILE),
                links = getDataFromChat(messages, ContentType.LINK),
                audios = getDataFromChat(messages, ContentType.AUDIO),
                voice = getDataFromChat(messages, ContentType.VOICE),
                gifs = getDataFromChat(messages, ContentType.GIF),
                groups = getDataFromChat(messages, ContentType.GROUP)
            )
        )
        Timber.tag(TAG).d("$members")
    }

    private fun getMembersDataFromChat(chatData: ConversationData?): List<ContentData> {
        return when (chatData?.type) {
            ConversationType.TYPE_SECURE_GROUP -> chatData.members.map {
                ContentData(
                    id = it.id, contactData = it, contentType = ContentType.CONTACT
                )
            }

            else -> emptyList()
        }
    }

    private fun getDataFromChat(
        messages: List<ChatMessage>, contentType: ContentType
    ): List<ContentData> =
        messages.asSequence().filter { it.messageData != null }.map { it.messageData }
            .filter { it?.contentType == contentType }.filterNotNull()
            .map { ContentData(id = it.id, messageData = it, contentType = contentType) }.toList()
}
package com.messenger.features.main.camera

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CameraViewModel : ViewModel() {

    enum class ScreenState {
        PHOTO, VIDEO
    }

    var screenStateData: MutableLiveData<ScreenState> = MutableLiveData()
    var selectedImageData: MutableLiveData<Uri?> = MutableLiveData()

    init {
        setCameraState()
    }

    private fun setCameraState() {
        screenStateData.postValue(ScreenState.PHOTO)
        resetImage()
    }

    fun setGalleryState() {
        screenStateData.postValue(ScreenState.VIDEO)
        resetImage()
    }

    fun saveCameraImageBitmap(uri: Uri) {
        selectedImageData.postValue(uri)
    }

    fun resetImage() {
        selectedImageData.postValue(null)
    }
}
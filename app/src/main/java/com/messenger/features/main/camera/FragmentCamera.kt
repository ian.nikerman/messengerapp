package com.messenger.features.main.camera

import android.Manifest
import android.net.Uri
import android.os.Bundle
import android.util.Size
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.Camera
import androidx.camera.core.CameraSelector
import androidx.camera.core.FocusMeteringAction
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import com.messenger.R
import com.messenger.databinding.FragmentCameraBinding
import com.messenger.features.main.chat.ChatViewModel
import com.messenger.utils.getUTCMilliseconds
import com.messenger.utils.hasPermission
import com.messenger.utils.isCameraPermissionDenied
import com.messenger.utils.showCameraRationaleDialog
import com.messenger.utils.showImage
import com.messenger.utils.showImageAnimated
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class FragmentCamera : DialogFragment() {

    private val chatViewModel: ChatViewModel by viewModel()
    private val viewModel: CameraViewModel by viewModel()
    private var binding: FragmentCameraBinding? = null
    private var cameraProvider: ProcessCameraProvider? = null

    companion object {
        private lateinit var outputDirectory: File
        private lateinit var cameraExecutor: ExecutorService
        private var imageCapture: ImageCapture? = null
        private var preview: Preview? = null
        private var camera: Camera? = null
        private var lensFacing = CameraSelector.LENS_FACING_BACK
        private fun createFile(baseFolder: File) =
            File(baseFolder, getUTCMilliseconds().toString() + ".jpg")
    }

    private val permissionCameraRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            when {
                granted -> checkCameraPermissions()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_FRAME, R.style.FullScreenDialogStyle)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCameraBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        outputDirectory = getOutputDirectory()
        cameraExecutor = Executors.newSingleThreadExecutor()

        initObservers()
    }

    override fun onResume() {
        super.onResume()
        changeNavigationBarColor()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        cameraExecutor.shutdown()
        binding = null
    }

    private fun initObservers() {
        viewModel.screenStateData.observe(
            viewLifecycleOwner
        ) { screenStateData -> handleScreenStateData(screenStateData) }
        viewModel.selectedImageData.observe(
            viewLifecycleOwner
        ) { imagesLiveData -> handleSelectedImage(imagesLiveData) }
//        mainViewModel.appliedImage.observe(viewLifecycleOwner) { appliedImage ->
//            handleAppliedImage(appliedImage)
//        }
    }

    private fun handleScreenStateData(screenStateData: CameraViewModel.ScreenState?) {
        screenStateData?.let {
            when (it) {
                CameraViewModel.ScreenState.PHOTO -> switchToPhotoCameraState()
                CameraViewModel.ScreenState.VIDEO -> switchToVideoCameraState()
            }
        }
    }

    private fun handleAppliedImage(appliedImage: Uri?) {
        appliedImage?.let {
            viewModel.saveCameraImageBitmap(it)
        }
    }

    private fun handleSelectedImage(fileUri: Uri?) {
        if (fileUri == null) {
            onImageReset()
        }
        fileUri?.let {
            onImageHandled(it)
        }
    }

    private fun onImageHandled(fileUri: Uri) {
        binding?.apply {
            imageControlLayout.shutterBtn.isVisible = false
            imagePreview.isVisible = true
            imagePreview.showImageAnimated(fileUri)
            imageControlLayout.parentLayout.isVisible = true
            imageControlLayout.cancelImageBtn.isVisible = true
            imageControlLayout.saveButton.isVisible = true
            imageControlLayout.saveButton.setOnClickListener {
                chatViewModel.setSelectedPhoto(
                    fileUri
                )
            }
        }
    }

    private fun onImageReset() {
        binding?.apply {
            imageControlLayout.cancelImageBtn.isVisible = false
            imageControlLayout.saveButton.isVisible = false
            imagePreview.isVisible = false
            imagePreview.resetZoom()
            imagePreview.showImage(null)
        }
    }

    private fun switchToPhotoCameraState() {
        binding?.apply {
            cameraView.isVisible = true
            cameraTapFocusView.isVisible = true
            imageControlLayout.shutterBtn.isVisible = true
            imageControlLayout.parentLayout.isVisible = true
            imageControlLayout.cancelImageBtn.setOnClickListener {
                onCancelImage()
            }
            checkCameraPermissions()
        }
    }

    private fun checkCameraPermissions() {
        when {
            requireContext().hasPermission(Manifest.permission.CAMERA) -> {
                setupCamera()
            }

            requireActivity().isCameraPermissionDenied() -> showCameraRationaleDialog(
                requireContext(), permissionCameraRequest
            )

            else -> permissionCameraRequest.launch(Manifest.permission.CAMERA)
        }
    }

    private fun switchToVideoCameraState() {
        binding?.apply {
            cameraTapFocusView.isVisible = false
            cameraView.isVisible = false
            imageControlLayout.shutterBtn.isVisible = false
            imageControlLayout.parentLayout.isVisible = false
            imageControlLayout.cancelImageBtn.setOnClickListener {
                imageControlLayout.parentLayout.isVisible = false
                viewModel.resetImage()
            }
        }
    }

    private fun setupCamera() {
        updateCameraUi()

        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFuture.addListener({
            cameraProvider = cameraProviderFuture.get()
            bindCameraUseCases()
        }, ContextCompat.getMainExecutor(requireContext()))
    }

    private fun bindCameraUseCases() {
        val cameraProvider =
            cameraProvider ?: throw IllegalStateException("Camera initialization failed.")

        val cameraSelector = CameraSelector.Builder().requireLensFacing(lensFacing).build()

        preview = Preview.Builder().build().also {
            it.setSurfaceProvider(binding?.cameraView?.surfaceProvider)
        }

        imageCapture =
            ImageCapture.Builder().setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                .setTargetResolution(Size(1200, 1600)).build()

        cameraProvider.unbindAll()

        try {
            camera = cameraProvider.bindToLifecycle(
                this, cameraSelector, preview, imageCapture
            )
            setupZoomAndTapToFocus()
        } catch (exc: Exception) {
//            Timber.tag(TAG).d( "Use case binding failed", exc)
        }
    }

    private fun setupZoomAndTapToFocus() {
        binding?.apply {
            cameraTapFocusView.setOnTouchListener { view, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        val factory = cameraView.meteringPointFactory
                        val point = factory.createPoint(event.x, event.y)
                        val action = FocusMeteringAction.Builder(point, FocusMeteringAction.FLAG_AF)
                            .setAutoCancelDuration(5, TimeUnit.SECONDS).build()
                        camera?.cameraControl?.startFocusAndMetering(action)
                    }
                }
                view.performClick()
                true
            }
        }
    }

    private fun updateCameraUi() {
        binding?.apply {
            imageControlLayout.shutterBtn.setOnClickListener {
                imageCapture?.let { imageCapture ->

                    val photoFile = createFile(outputDirectory)
                    val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

                    imageCapture.takePicture(
                        outputOptions,
                        cameraExecutor,
                        object : ImageCapture.OnImageSavedCallback {
                            override fun onError(exc: ImageCaptureException) {

                            }

                            override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                                val savedUri = output.savedUri ?: Uri.fromFile(photoFile)
                                viewModel.saveCameraImageBitmap(savedUri)
                            }
                        })
                }
            }
        }
    }

    private fun onCancelImage() {
        binding?.apply {
            imageControlLayout.shutterBtn.isVisible = true
        }
        viewModel.resetImage()
    }

    private fun getOutputDirectory(): File {
        val mediaDir = requireActivity().externalMediaDirs.firstOrNull()?.let {
            File(it, getString(R.string.app_name)).apply { mkdirs() }
        }
        return if (mediaDir != null && mediaDir.exists()) mediaDir else requireActivity().filesDir
    }

    private fun changeNavigationBarColor() {
        activity?.window?.navigationBarColor =
            ContextCompat.getColor(requireContext(), R.color.blue)
    }
}

package com.messenger.features.main.chat

import android.os.Bundle
import android.text.Editable
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.messenger.databinding.FragmentChatBinding
import com.messenger.features.main.MainViewModel
import com.messenger.features.main.chat.adapter.ChatAdapter
import com.messenger.features.main.chat.attachFile.AttachViewModel
import com.messenger.features.views.HeaderView
import com.messenger.models.ChatMessage
import com.messenger.models.ConversationData
import com.messenger.utils.closeKeyboard
import com.messenger.utils.extensions.AfterTextChangedWatcher
import com.messenger.utils.extensions.LiveEvent
import com.vanniktech.emoji.EmojiPopup
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChatFragment : Fragment() {

    private val sharedViewModel by activityViewModel<MainViewModel>()
    private val viewModel: ChatViewModel by viewModel()
    private val attachViewModel: AttachViewModel by viewModel()
    private var binding: FragmentChatBinding? = null
    private val navArgs: ChatFragmentArgs by navArgs()

    private companion object {

        private val TAG = ChatFragment::class.java.simpleName
        private lateinit var chatAdapter: ChatAdapter

    }

    private var itemTouchCallback: ItemTouchHelper.SimpleCallback =
        object : ItemTouchHelper.SimpleCallback(
            0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {}
        }

    private val chatListener = object : ChatAdapter.ChatListener {
        override fun onItemClick(itemId: Int) {
            Timber.tag(TAG).d("onItemClick")

        }

        override fun onItemLongClick(itemId: Int) {
            Timber.tag(TAG).d("onItemLongClick")

        }
    }

    private val headerListener = object : HeaderView.HeaderViewListener {
        override fun onBackClick() {
            Timber.tag(TAG).d("onBackClick")
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        override fun onCallClick() {
            Timber.tag(TAG).d("onCallClick")
            sharedViewModel.openCall(callingFragment = this@ChatFragment, roomId = handleRoomId())
        }

        override fun onMoreClick() {
            Timber.tag(TAG).d("onMoreClick")
            viewModel.showMoreOptions()
        }

        override fun onContactClick() {
            Timber.tag(TAG).d("onContactClick")
            sharedViewModel.openChatSettings(roomId = handleRoomId())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(requireView(), savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")

        initUi()
        initObservers()
    }

    private fun initUi() {
        initChatAdapter()

        binding?.apply {
            headerView.setListener(headerListener)
            timerButton.setOnClickListener { viewModel.showSetTimer() }
            emojiButton.setOnClickListener {
                closeKeyboard(requireContext(), chatInputView)
                val mEmojiPopup = EmojiPopup(root, chatInputView)
                mEmojiPopup.toggle()
            }
            attachButton.setOnClickListener { sharedViewModel.openAttachChat() }
            audioButton.setOnClickListener { viewModel.startRecordMic() }
            sendBtn.setOnClickListener {
                viewModel.confirmMessage()
                chatInputView.text = Editable.Factory.getInstance().newEditable("")
            }

            chatInputView.addTextChangedListener(object : AfterTextChangedWatcher() {
                override fun afterTextChanged(s: Editable?) {
                    viewModel.setTypedText(s?.toString() ?: "")
                }
            })
        }
    }

    private fun initChatAdapter() {
        Timber.tag(TAG).d("initAdapter")
        binding?.recyclerView?.apply {
            chatAdapter = ChatAdapter(
                chatListener
            )
            adapter = chatAdapter
            val itemTouchHelper = ItemTouchHelper(itemTouchCallback)
            itemTouchHelper.attachToRecyclerView(this)
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.chatPrimaryData.observe(viewLifecycleOwner) { data ->
            handleChatPrimaryData(data)
        }
        viewModel.emptyDataState.observe(viewLifecycleOwner) { state ->
            handleEmptyDataState(state)
        }
        viewModel.messagesData.observe(viewLifecycleOwner) { data ->
            handleChatsData(data)
        }
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
        viewModel.errorEvent.observe(viewLifecycleOwner) { event ->
            handleErrorEvent(event)
        }
        viewModel.actionButtonState.observe(viewLifecycleOwner) { state ->
            handleActionButtonState(state)
        }
    }

    private fun handleEmptyDataState(state: ChatViewModel.EmptyDataState?) {
        state?.let {
            when (it) {
                ChatViewModel.EmptyDataState.STATE_DATA -> binding?.emptyDataView?.isVisible = false
                ChatViewModel.EmptyDataState.STATE_DATA_EMPTY -> binding?.emptyDataView?.isVisible =
                    true
            }
        }
    }

    private fun handleActionButtonState(state: ChatViewModel.ActionButtonState?) {
        state?.let {
            when (it) {
                ChatViewModel.ActionButtonState.SEND_BUTTON -> showSendButton()
                ChatViewModel.ActionButtonState.MIC_BUTTON -> showMicButton()
            }
        }
    }

    private fun showSendButton() {
        binding?.apply {
            audioButton.isVisible = false
            sendBtn.isVisible = true
        }
    }

    private fun showMicButton() {
        binding?.apply {
            sendBtn.isVisible = false
            audioButton.isVisible = true
        }
    }

    private fun handleChatPrimaryData(data: ConversationData?) {
        data?.let {
            binding?.headerView?.setHeaderChatData(it)
        }
    }

    private fun handleRoomId(): Long {
        return navArgs.roomId
    }

    private fun handleErrorEvent(event: LiveEvent<ChatViewModel.ErrorEvent>?) {
        event?.getContentIfNotHandled()?.let {

        }
    }

    private fun handleProgressEvent(isLoading: Boolean) {
        binding?.progress?.setProgressEvent(isLoading)
    }

    private fun handleChatsData(data: List<ChatMessage>?) {
        data?.let {
            Timber.tag(TAG).d("handleNetworkData: ${it.size}")
            chatAdapter.submitList(it)
            if (it.isNotEmpty()) {
                binding?.recyclerView?.smoothScrollToPosition(it.size - 1)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getChat(roomId = handleRoomId())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}

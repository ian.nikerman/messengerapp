package com.messenger.features.main.chat

import android.net.Uri
import timber.log.Timber
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.messenger.data.ChatRepository
import com.messenger.data.ChatsRepository
import com.messenger.data.ContactsRepository
import com.messenger.models.ChatMessage
import com.messenger.models.ConversationData
import com.messenger.models.MessageStatus
import com.messenger.models.SenderType
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import com.messenger.utils.getUTCMilliseconds
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ChatViewModel(
    private val contactsRepository: ContactsRepository,
    private val chatsRepository: ChatsRepository,
    private val chatRepository: ChatRepository
) : ViewModel() {

    companion object {

        private val TAG = ChatViewModel::class.java.simpleName
        private var chatRoomId: Long? = null
        private var chatMessageRequest: ChatMessage? = null
        private var messages: List<ChatMessage>? = null

    }

    enum class ErrorEvent {
        ERROR_LOADING_DATA
    }

    enum class EmptyDataState {
        STATE_DATA, STATE_DATA_EMPTY
    }

    val emptyDataState = MutableLiveData<EmptyDataState>()

    val errorEvent = LiveEventData<ErrorEvent>()

    enum class ActionButtonState {
        SEND_BUTTON, MIC_BUTTON
    }

    val actionButtonState = MutableLiveData<ActionButtonState>()
    val chatPrimaryData = MutableLiveData<ConversationData?>()
    val messagesData = MutableLiveData<List<ChatMessage>?>()
    val progressEvent = ProgressData()

    init {
        setTypedText("")
    }

    fun getChat(roomId: Long) {
        Timber.tag(TAG).d("getChat")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                chatRoomId = roomId
                var chatData = chatsRepository.fetchChatPrimaryDataById(roomId)
                when (chatData) {
                    null -> {
                        val contact = contactsRepository.fetchContactById(roomId)
                        contact?.let {
                            chatData = chatsRepository.createChat(it)
                            chatPrimaryData.postValue(chatData)
                        }
                    }

                    else -> chatPrimaryData.postValue(chatData)
                }
                messages = chatRepository.fetchChat(roomId)
                checkDataState()
                messagesData.postValue(messages)
                chatMessageRequest = createChatMessageRequest()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_DATA)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    private fun sendMessage(messageRequest: ChatMessage) {
        Timber.tag(TAG).d("sendMessage")
//        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                chatRoomId?.let { id ->
                    val isSameDay = !messages.isNullOrEmpty()
                    messages = chatRepository.sendMessage(id, messageRequest, isSameDay)
                    checkDataState()
                    messagesData.postValue(messages)
                }
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_DATA)
            } finally {
//                progressEvent.endProgress()
            }
        }
    }

    private fun checkDataState() {
        emptyDataState.postValue(if (messages.isNullOrEmpty()) EmptyDataState.STATE_DATA_EMPTY else EmptyDataState.STATE_DATA)
    }

    fun confirmMessage() {
        chatMessageRequest?.let {
            sendMessage(it)
        }
    }

    private fun createChatMessageRequest(): ChatMessage {
        return ChatMessage(
            id = messages?.lastIndex?.toLong() ?: 0,
            message = "",
            senderType = SenderType.ME,
            createdAt = getUTCMilliseconds(),
            type = MessageStatus.SENDING,
            messageData = null,
            isSelected = false,
        )
    }

    fun showEmojiView() {

    }

    fun showSetTimer() {

    }

    fun startRecordMic() {

    }

    fun setTypedText(typedText: String) {
        chatMessageRequest = chatMessageRequest?.copy(message = typedText)
        when {
            typedText.isEmpty() -> actionButtonState.postValue(ActionButtonState.MIC_BUTTON)
            else -> actionButtonState.postValue(ActionButtonState.SEND_BUTTON)
        }
    }

    fun showMoreOptions() {

    }

    fun saveCameraImageBitmap(uri: Uri) {
//        chatMessageRequest = chatMessageRequest?.copy(message = typedText)
    }

    fun setSelectedPhoto(fileUri: Uri) {
//        chatMessageRequest = chatMessageRequest?.copy(message = typedText)
    }
}
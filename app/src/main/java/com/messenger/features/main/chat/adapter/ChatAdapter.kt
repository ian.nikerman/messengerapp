package com.messenger.features.main.chat.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.messenger.R
import com.messenger.databinding.ItemMessageBinding
import com.messenger.models.CallData
import com.messenger.models.CallStatus
import com.messenger.models.ChatMessage
import com.messenger.models.ContentType
import com.messenger.models.MessageStatus
import com.messenger.models.SenderType
import com.messenger.utils.getChatDateFormatDate
import com.messenger.utils.getDurationFromMilliseconds
import com.messenger.utils.utcToTimeFormatted

class ChatAdapter(
    private val listener: ChatListener
) : ListAdapter<ChatMessage, RecyclerView.ViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemMessageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as ViewHolder).bind(listener, item)
    }

    class ViewHolder(private val itemBinding: ItemMessageBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(listener: ChatListener, data: ChatMessage) {
            when (data.senderType) {
                SenderType.DATE -> setDate(data)
                SenderType.CONTACT -> setContactMessage(listener, data)
                SenderType.ME -> {
                    if (data.messageData?.contentType == ContentType.CALL) {
                        setMyCall(listener, data)
                    } else {
                        setMyMessage(listener, data)
                    }
                }
            }
        }

        private fun setDate(data: ChatMessage) {
            itemBinding.apply {
                layoutContactMessage.root.isVisible = false
                layoutMyMessage.root.isVisible = false
                layoutMyCall.root.isVisible = false
                layoutConversationDate.root.isVisible = true

                data.createdAt.let {
                    layoutConversationDate.chatDate.text = getChatDateFormatDate(root.context, it)
                }
            }
        }

        private fun ImageView.setConversationType(data: CallData) {
            val resId = when (data.callStatus) {
                CallStatus.INCOMING_CALL -> R.drawable.ic_call_received
                CallStatus.INCOMING_CALL_CANCELLED -> R.drawable.ic_call_missed
                CallStatus.INCOMING_CALL_MISSED -> R.drawable.ic_call_missed
                CallStatus.OUTGOING_CALL -> R.drawable.ic_call_made
                CallStatus.OUTGOING_CALL_CANCELLED -> R.drawable.ic_call_missed_outgoing
                CallStatus.OUTGOING_CALL_MISSED -> R.drawable.ic_call_missed_outgoing
            }
            setImageResource(resId)
        }

        private fun TextView.setConversationType(data: CallData) {
            val status = when (data.callStatus) {
                CallStatus.INCOMING_CALL -> context.getString(R.string.incoming_call)
                CallStatus.INCOMING_CALL_CANCELLED -> context.getString(R.string.cancelled_call)
                CallStatus.INCOMING_CALL_MISSED -> context.getString(R.string.missed_call)
                CallStatus.OUTGOING_CALL -> context.getString(R.string.outgoing_call)
                CallStatus.OUTGOING_CALL_CANCELLED -> context.getString(R.string.cancelled_call)
                CallStatus.OUTGOING_CALL_MISSED -> context.getString(R.string.missed_call)
            }
            text = status
        }

        private fun setMyMessage(listener: ChatListener, data: ChatMessage) {
            itemBinding.apply {
                layoutContactMessage.root.isVisible = false
                layoutMyMessage.root.isVisible = true
                layoutMyCall.root.isVisible = false
                layoutConversationDate.root.isVisible = false

                when (data.type) {
                    MessageStatus.READ -> layoutMyMessage.chatMessageStatus.setImageResource(R.drawable.ic_status_read)
                    MessageStatus.RECEIVED -> layoutMyMessage.chatMessageStatus.setImageResource(R.drawable.ic_status_received)
                    MessageStatus.SENT -> layoutMyMessage.chatMessageStatus.setImageResource(R.drawable.ic_status_sent)
                    else -> {}
                }

                layoutMyMessage.chatMyMessage.text = data.message
                data.createdAt.let {
                    layoutMyMessage.chatMyMessageDate.text = utcToTimeFormatted(it)
                }
            }
        }

        private fun setMyCall(listener: ChatListener, data: ChatMessage) {
            itemBinding.apply {
                layoutContactMessage.root.isVisible = false
                layoutMyMessage.root.isVisible = false
                layoutMyCall.root.isVisible = true
                layoutConversationDate.root.isVisible = false

                layoutMyCall.apply {
                    data.messageData?.callData?.let { data ->
                        callStatus.setConversationType(data)
                        chatMyCall.setConversationType(data)

                        if (data.duration != 0L) {
                            (utcToTimeFormatted(data.createdAt) + ", ${
                                getDurationFromMilliseconds(
                                    data.duration
                                )
                            }").also { createdAt.text = it }
                        } else {
                            createdAt.text = utcToTimeFormatted(data.createdAt)
                        }

                    }
                }
            }
        }

        private fun setContactMessage(listener: ChatListener, data: ChatMessage) {
            itemBinding.apply {
                layoutContactMessage.root.isVisible = true
                layoutMyMessage.root.isVisible = false
                layoutConversationDate.root.isVisible = false

                layoutContactMessage.chatMessage.text = data.message
                data.createdAt.let {
                    layoutContactMessage.chatMessageDate.text = utcToTimeFormatted(it)
                }
            }
        }
    }

    interface ChatListener {
        fun onItemClick(itemId: Int)
        fun onItemLongClick(itemId: Int)
    }
}

private class DiffCallback : DiffUtil.ItemCallback<ChatMessage>() {
    override fun areItemsTheSame(oldItem: ChatMessage, newItem: ChatMessage): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: ChatMessage, newItem: ChatMessage
    ): Boolean {
        return oldItem == newItem
    }
}
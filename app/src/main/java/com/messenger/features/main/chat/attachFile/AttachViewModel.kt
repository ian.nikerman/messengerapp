package com.messenger.features.main.chat.attachFile

import android.Manifest
import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.messenger.R
import com.messenger.data.ContactsRepository
import com.messenger.data.ContentRepository
import com.messenger.data.LocationRepository
import com.messenger.data.MetaRepository
import com.messenger.models.CategoryAttach
import com.messenger.models.CategoryAttachType
import com.messenger.models.ContactData
import com.messenger.models.DetailsType
import com.messenger.models.DetailsViewData
import com.messenger.models.GalleryDataType
import com.messenger.models.GalleryMediaData
import com.messenger.models.LocationState
import com.messenger.models.ResponseState
import com.messenger.models.Status
import com.messenger.models.TreasureMapData
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import com.messenger.utils.hasPermission
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import timber.log.Timber

class AttachViewModel(
    private val contentRepository: ContentRepository,
    private val metaRepository: MetaRepository,
    private val locationRepository: LocationRepository,
    private val contactsRepository: ContactsRepository
) : ViewModel() {

    companion object {

        private val TAG = AttachViewModel::class.java.simpleName
        private var galleryItems: MutableList<GalleryMediaData> = ArrayList()
        private var selectedGalleryItems: List<GalleryMediaData> = emptyList()
        private var selectedFIleItems: List<GalleryMediaData> = emptyList()
        private var contacts: List<ContactData> = ArrayList()
        private var categories: List<CategoryAttach> = emptyList()
        private var isLocationUpdatesEnabled = false

        private fun initDetailsData() =
            MutableStateFlow(ResponseState(Status.LOADING, emptyList<TreasureMapData>(), ""))
    }

    enum class ErrorEvent {
        ERROR_LOADING_DATA, ERROR_LOADING_ATTACH_CATEGORIES, ERROR_GET_LOCATION
    }

    val errorEvent = LiveEventData<ErrorEvent>()
    val attachCategoriesData = MutableLiveData<List<CategoryAttach>?>()
    val attachHeaderData = MutableLiveData<CategoryAttach?>()
    var capturedImageData: MutableLiveData<Uri?> = MutableLiveData()
    var contentData: MutableLiveData<AttachMessageData> = MutableLiveData()
    val fragNavEvent = LiveEventData<Int?>()
    val sendMessageData = MutableLiveData<Int?>()
    val progressEvent = ProgressData()

    private val _mapDetailsData = initDetailsData()
    val mapDetailsData = _mapDetailsData.asSharedFlow()

    sealed class AttachMessageData {

        data class GalleryData(
            val galleryList: List<GalleryMediaData>
        ) : AttachMessageData()

        data class FilesData(
            val files: List<GalleryMediaData>
        ) : AttachMessageData()

        data class LocationData(
            val state: String?
        ) : AttachMessageData()

        data class ContactsData(
            val contacts: List<ContactData>
        ) : AttachMessageData()

        data class MusicData(
            val audioList: List<String>
        ) : AttachMessageData()

    }

    init {
        getCategoriesList()
    }

    fun changeSelectedCategory(type: CategoryAttachType, isUpdateContent: Boolean? = true) {
        categories = categories.map { it.copy(isSelected = it.type == type) }
        attachHeaderData.postValue(categories.first { it.type == type })
        attachCategoriesData.postValue(categories)
        if (isUpdateContent == true) {
            updateAttachContentFragment(type)
        }
    }

    private fun updateAttachContentFragment(type: CategoryAttachType) {
        val action: Int = when (type) {
            CategoryAttachType.CAMERA -> R.id.attachCameraSheet
            CategoryAttachType.GALLERY -> R.id.attachGallerySheet
            CategoryAttachType.FILE -> R.id.attachFileSheet
            CategoryAttachType.LOCATION -> R.id.attachLocationSheet
            CategoryAttachType.CONTACT -> R.id.attachContactSheet
            CategoryAttachType.MUSIC -> R.id.attachAudioSheet
        }
        action.let {
            fragNavEvent.postRawValue(action)
        }
    }

    private fun getCategoriesList() {
        Timber.tag(TAG).d("getCategoriesList")
        viewModelScope.launch(Dispatchers.IO) {
            try {
                categories = metaRepository.fetchAttachCategoriesData()
                changeSelectedCategory(CategoryAttachType.CAMERA, false)
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_ATTACH_CATEGORIES)
            }
        }
    }

    fun showGalleryPickerData() {
        Timber.tag(TAG).d("showGalleryPickerData")
        viewModelScope.launch(Dispatchers.IO) {
            contentRepository.loadMedia(true).catch {
                Timber.tag(TAG).d("showGalleryPickerData failed: ${it.message.toString()}")
            }.collect { response ->
                Timber.tag(TAG).d("showGalleryPickerData succeeded: ${response.data}")
                response.data?.let {
                    galleryItems = it
                    contentData.postValue(AttachMessageData.GalleryData(galleryList = galleryItems))
                }
            }
        }
    }

    fun showFilePickerData() {
        Timber.tag(TAG).d("showFilePickerData")
        viewModelScope.launch(Dispatchers.IO) {
            contentRepository.loadMedia(false).catch {
                Timber.tag(TAG).d("showFilePickerData failed: ${it.message.toString()}")
            }.collect { response ->
                Timber.tag(TAG).d("showFilePickerData succeeded: ${response.data}")
                response.data?.let {
                    galleryItems = it
                    contentData.postValue(AttachMessageData.FilesData(files = galleryItems))
                }
            }
        }
    }

    /** Show location details UI for accepted location permissions */
    fun onPermissionAccepted() {
        if (!isLocationUpdatesEnabled) {
            return
        }
        viewModelScope.launch(Dispatchers.IO) {
            contentData.postValue(AttachMessageData.LocationData(state = ""))
            _mapDetailsData.value = ResponseState(
                Status.SUCCESS, listOf(
                    TreasureMapData.ProgressData(
                        showProgress = true
                    ), TreasureMapData.LocationData(
                        locationState = LocationState.LOCATION_AVAILABLE,
                        showOnMap = false,
                        currentLocation = null
                    ), TreasureMapData.MapDetailsData(
                        detailsType = DetailsType.SEARCHING, detailsViewData = null
                    )
                ), ""
            )
            fetchLocation()
        }
    }

    /** Show location details UI for manual location */
    private fun onManualLocationClick() {
        viewModelScope.launch(Dispatchers.IO) {
            _mapDetailsData.value = ResponseState(
                Status.SUCCESS, listOf(
                    TreasureMapData.ProgressData(
                        showProgress = false
                    ), TreasureMapData.LocationData(
                        locationState = LocationState.LOCATION_MANUAL,
                        showOnMap = false,
                        currentLocation = null
                    )
                ), ""
            )
        }
    }

    /** Fetch current fine location */
    private fun fetchLocation() {
        viewModelScope.launch(Dispatchers.IO) {
            locationRepository.getLocations().catch {
                errorEvent.postRawValue(ErrorEvent.ERROR_GET_LOCATION)
            }.collect { location ->
                if (isLocationUpdatesEnabled) {
                    location?.let {
                        handleLocation(it, true)
                    }
                }
            }
        }
    }

    /** Handle current center map location */
    private fun handleCenterMapLocation(targetLatitude: Double, targetLongitude: Double) {
        viewModelScope.launch(Dispatchers.IO) {
            val location = Location(LocationManager.GPS_PROVIDER)
            location.latitude = targetLatitude
            location.longitude = targetLongitude
            handleLocation(location, false)
        }
    }

    /** Handle location details */
    private fun handleLocation(location: Location, showOnMap: Boolean) {
        val city = locationRepository.getCity(location)
        val street = locationRepository.getStreet(location)

        _mapDetailsData.value = ResponseState(
            Status.SUCCESS, listOf(
                TreasureMapData.ProgressData(
                    showProgress = false
                ), TreasureMapData.LocationData(
                    locationState = if (isLocationUpdatesEnabled) LocationState.LOCATION_AVAILABLE else LocationState.LOCATION_MANUAL,
                    showOnMap = showOnMap,
                    currentLocation = location
                ), TreasureMapData.MapDetailsData(
                    detailsType = DetailsType.INFO, detailsViewData = DetailsViewData(
                        location = location,
                        city = city,
                        street = street,
                    )
                )
            ), ""
        )
    }

    /** Trigger for fine location updates */
    private fun setLocationUpdatesEnabled(enabled: Boolean) {
        isLocationUpdatesEnabled = enabled
    }

    /** Map callbacks */

    fun onCameraIdle(targetLatitude: Double, targetLongitude: Double) {
        if (!isLocationUpdatesEnabled) {
            handleCenterMapLocation(targetLatitude, targetLongitude)
        }
    }

    fun setMapMovedByUser() {
        if (isLocationUpdatesEnabled) {
            onManualLocationClick()
        }
        stopLocationUpdates()
        setLocationUpdatesEnabled(false)
    }

    /** Navigation callbacks */

    fun onFindMeClick() {
        setLocationUpdatesEnabled(true)
    }

    fun stopLocationUpdates() {
        locationRepository.stopLocationUpdates()
    }

    fun showContactPickerData(
        context: Context
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            if (contacts.isEmpty()) {
                if (context.hasPermission(Manifest.permission.READ_CONTACTS)
                ) {
                    contentData.postValue(AttachMessageData.ContactsData(contacts = contacts))
                }
                contacts = contactsRepository.fetchContacts(context, false)
            }
            contentData.postValue(AttachMessageData.ContactsData(contacts = contacts))
        }
    }

    fun showAudioPickerData() {
        contentData.postValue(AttachMessageData.MusicData(audioList = emptyList()))
    }

    fun changeGalleryItemSelection(data: GalleryMediaData) {
        when {
            selectedGalleryItems.any { it.id == data.id } -> {
                selectedGalleryItems.firstOrNull { it.id == data.id }?.let {
                    selectedGalleryItems = selectedGalleryItems.minus(it)
                }
            }

            else -> {
                selectedGalleryItems =
                    selectedGalleryItems.plus(data.copy(selectCounter = selectedGalleryItems.size + 1))
            }
        }

        val updatedGalleryList = galleryItems.map { item ->
            GalleryMediaData(
                id = item.id,
                uri = item.uri,
                title = null,
                size = null,
                duration = null,
                isSelected = selectedGalleryItems.any { it.id == item.id },
                selectCounter = selectedGalleryItems.firstOrNull { it.id == item.id }?.selectCounter,
                type = GalleryDataType.IMAGE
            )
        }

        attachCategoriesData.postValue(if (selectedGalleryItems.isEmpty()) categories else null)
        sendMessageData.postValue(selectedGalleryItems.size)
        contentData.postValue(AttachMessageData.GalleryData(galleryList = updatedGalleryList))
    }

    fun changeFileItemSelection(data: GalleryMediaData) {
        when {
            selectedFIleItems.any { it.id == data.id } -> {
                selectedFIleItems.firstOrNull { it.id == data.id }?.let {
                    selectedFIleItems = selectedFIleItems.minus(it)
                }
            }

            else -> {
                selectedFIleItems =
                    selectedFIleItems.plus(data.copy(selectCounter = selectedFIleItems.size + 1))
            }
        }

        val updatedFilesList = galleryItems.map { item ->
            item.copy(
                isSelected = selectedFIleItems.any { it.id == item.id },
                selectCounter = selectedFIleItems.firstOrNull { it.id == item.id }?.selectCounter,
                type = GalleryDataType.FILE
            )
        }

        attachCategoriesData.postValue(if (selectedFIleItems.isEmpty()) categories else null)
        sendMessageData.postValue(selectedFIleItems.size)
        contentData.postValue(AttachMessageData.FilesData(files = updatedFilesList))
    }

    fun setContactsQuery(typedText: String) {
        viewModelScope.launch(Dispatchers.IO) {
            contentData.postValue(AttachMessageData.ContactsData(contacts = contacts.filter {
                it.name.contains(typedText) || it.phoneNumber.contains(typedText) || it.username.contains(
                    typedText
                )
            }))
        }
    }

    fun setAudioListQuery(s: String) {

    }

    fun saveCameraImageBitmap(uri: Uri) {
        capturedImageData.postValue(uri)
    }

    fun resetImage() {
        capturedImageData.postValue(null)
    }
}
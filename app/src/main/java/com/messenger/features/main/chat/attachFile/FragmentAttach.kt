package com.messenger.features.main.chat.attachFile

import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.messenger.R
import com.messenger.databinding.FragmentChatAttachBinding
import com.messenger.features.main.MainViewModel
import com.messenger.features.main.chat.attachFile.adapters.CategoriesAdapter
import com.messenger.features.views.HeaderView
import com.messenger.models.CategoryAttach
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.viewModel

class FragmentAttach : Fragment() {

    private var binding: FragmentChatAttachBinding? = null
    private val viewModel: AttachViewModel by viewModel()
    private lateinit var localController: NavController

    companion object {
        private val TAG = FragmentAttach::class.java.simpleName
        private lateinit var categoriesAdapter: CategoriesAdapter
    }

    private val headerListener = object : HeaderView.HeaderViewListener {
        override fun onBackClick() {
            Timber.tag(TAG).d("onBackClick")
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
    }

    private val categoryListener = object : CategoriesAdapter.CategoryListener {
        override fun onCategoryClick(category: CategoryAttach) {
            Timber.tag(TAG).d("onCategoryClick")
            viewModel.changeSelectedCategory(category.type)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatAttachBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val localNavHost =
            childFragmentManager.findFragmentById(R.id.childNavigationContainer) as NavHostFragment
        localController = localNavHost.navController

        initUi()
        initObservers()
    }

    private fun initObservers() {
        viewModel.attachCategoriesData.observe(viewLifecycleOwner) { data ->
            handleCategoriesData(data)
        }
        viewModel.attachHeaderData.observe(viewLifecycleOwner) { data ->
            handleAttachHeaderData(data)
        }
        viewModel.fragNavEvent.observe(viewLifecycleOwner) { event ->
            handleAttachNavigationEvent(event)
        }
        viewModel.sendMessageData.observe(viewLifecycleOwner) { data ->
            handleSendMessageState(data)
        }
    }

    private fun handleAttachNavigationEvent(event: LiveEvent<Int?>?) {
        event?.getContentIfNotHandled()?.let {
            Timber.tag(TAG).d("handleAttachNavigationEvent:$event")
            try {
                localController.popBackStack()
                localController.navigate(it)
            } catch (ex: java.lang.Exception) {
                Timber.tag(TAG).d("handleAttachNavigationEvent Error: ${ex.message}")
            }
        }
    }

    private fun handleAttachHeaderData(data: CategoryAttach?) {
        data?.let {
            Timber.tag(TAG).d("handleAttachHeaderData")
            binding?.headerView?.setHeaderChatAttachData(it)
        }
    }

    private fun initUi() {
        binding?.apply {
            headerView.setListener(headerListener)
            initCategoriesAdapter()
        }
    }

    private fun initCategoriesAdapter() {
        Timber.tag(TAG).d("initCategoriesAdapter")
        binding?.attachCategoriesRecyclerView?.apply {
            categoriesAdapter = CategoriesAdapter(categoryListener)
            adapter = categoriesAdapter
        }
    }

    private fun handleSendMessageState(attachCounter: Int?) {
        Timber.tag(TAG).d("handleSendMessageState")
        when (attachCounter) {
            0 -> {
                binding?.apply {
                    sendMessageLayout.isVisible = false
                    selectorCounter.isVisible = false
                    selectorFrame.isVisible = false
                }
            }

            else -> {
                binding?.apply {
                    sendMessageLayout.isVisible = true
                    selectorCounter.isVisible = true
                    selectorFrame.isVisible = true
                    selectorCounter.text = attachCounter?.toString()
                }
            }
        }
    }

    private fun handleCategoriesData(data: List<CategoryAttach>?) {
        when (data) {
            null -> {
                Timber.tag(TAG).d("handleCategoriesData null")
                binding?.attachView?.isVisible = false
            }

            else -> {
                Timber.tag(TAG).d("handleCategoriesData")
                binding?.attachView?.isVisible = true
                categoriesAdapter.submitList(data)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
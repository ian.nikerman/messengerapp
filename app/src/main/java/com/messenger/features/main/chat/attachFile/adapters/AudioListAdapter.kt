package com.messenger.features.main.chat.attachFile.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.messenger.databinding.ItemSearchListBinding
import com.messenger.models.ContactData
import com.messenger.utils.showPlaceholderImage

class AudioListAdapter(
    private val listener: SearchListListener
) : ListAdapter<ContactData, RecyclerView.ViewHolder>(AudioListDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemSearchListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as ViewHolder).bind(listener, item)
    }

    class ViewHolder(private val itemBinding: ItemSearchListBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(listener: SearchListListener, data: ContactData) {
            itemBinding.apply {
                imageView.setOnClickListener { listener.onGalleryImageClick(data) }
                imageView.showPlaceholderImage(data.avatar)
                title.text = data.name
                subTitle.text = data.phoneNumber.first()
            }
        }
    }

    interface SearchListListener {
        fun onGalleryImageClick(data: ContactData)
    }
}

private class AudioListDiffCallback : DiffUtil.ItemCallback<ContactData>() {
    override fun areItemsTheSame(oldItem: ContactData, newItem: ContactData): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: ContactData, newItem: ContactData
    ): Boolean {
        return oldItem == newItem
    }
}
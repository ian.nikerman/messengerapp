package com.messenger.features.main.chat.attachFile.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.messenger.R
import com.messenger.databinding.ItemAttachCategoryBinding
import com.messenger.models.CategoryAttach
import com.messenger.utils.showPlaceholderCircleImage

class CategoriesAdapter(
    private val listener: CategoryListener
) : ListAdapter<CategoryAttach, RecyclerView.ViewHolder>(CategoriesDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemAttachCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as ViewHolder).bind(listener, item)
    }

    class ViewHolder(private val itemBinding: ItemAttachCategoryBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(listener: CategoryListener, data: CategoryAttach) {
            itemBinding.apply {

                itemImage.showPlaceholderCircleImage(data.icon)
                checkSelectedState(data.isSelected == true)
                itemTitle.text = data.name

                itemLayout.setOnClickListener {
                    if (data.isSelected == false) {
                        listener.onCategoryClick(data)
                    }
                }
            }
        }

        private fun checkSelectedState(isSelected: Boolean) {
            when {
                isSelected -> {
                    itemBinding.apply {
                        itemSelector.isVisible = true
                        itemBinding.itemTitle.setTextAppearance(R.style.CategoryAttachSelectedStyle)
                    }
                }

                else -> {
                    itemBinding.apply {
                        itemSelector.isVisible = false
                        itemBinding.itemTitle.setTextAppearance(R.style.CategoryAttachStyle)
                    }
                }
            }
        }
    }

    interface CategoryListener {
        fun onCategoryClick(category: CategoryAttach)
    }
}

private class CategoriesDiffCallback : DiffUtil.ItemCallback<CategoryAttach>() {
    override fun areItemsTheSame(oldItem: CategoryAttach, newItem: CategoryAttach): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: CategoryAttach, newItem: CategoryAttach
    ): Boolean {
        return oldItem == newItem
    }
}
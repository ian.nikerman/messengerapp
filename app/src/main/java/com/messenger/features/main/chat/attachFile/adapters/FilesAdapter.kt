package com.messenger.features.main.chat.attachFile.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.messenger.databinding.ItemFileBinding
import com.messenger.models.GalleryMediaData
import com.messenger.utils.formatFileSize
import com.messenger.utils.showPlaceholderImageCorneredAnimated

class FilesAdapter(
    private val listener: FilesListener
) : ListAdapter<GalleryMediaData, RecyclerView.ViewHolder>(FilesDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemFileBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as ViewHolder).bind(listener, item)
    }

    class ViewHolder(private val itemBinding: ItemFileBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(listener: FilesListener, data: GalleryMediaData) {
            itemBinding.apply {
                fileLayout.setOnClickListener { listener.onGalleryImageClick(data) }
                imageView.showPlaceholderImageCorneredAnimated(data.uri)
                fileTitle.text = data.title
                fileSubTitle.text = formatFileSize(data.size ?: 0)
                checkSelectedState(data.isSelected)
            }
        }

        private fun ItemFileBinding.checkSelectedState(isSelected: Boolean) {
            selectorView.isVisible = isSelected
        }
    }

    interface FilesListener {
        fun onGalleryImageClick(data: GalleryMediaData)
    }
}

private class FilesDiffCallback : DiffUtil.ItemCallback<GalleryMediaData>() {
    override fun areItemsTheSame(oldItem: GalleryMediaData, newItem: GalleryMediaData): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: GalleryMediaData, newItem: GalleryMediaData
    ): Boolean {
        return oldItem == newItem
    }
}
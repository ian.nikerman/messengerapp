package com.messenger.features.main.chat.attachFile.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.messenger.R
import com.messenger.databinding.ItemImageGalleryBinding
import com.messenger.models.GalleryMediaData
import com.messenger.utils.showPlaceholderImage

class GalleryAdapter(
    private val listener: GalleryListener
) : ListAdapter<GalleryMediaData, RecyclerView.ViewHolder>(GalleryDiffCallback()) {

    companion object {

        private const val SCALE_DURATION = 200L

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemImageGalleryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as ViewHolder).bind(listener, item)
    }

    class ViewHolder(private val itemBinding: ItemImageGalleryBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(listener: GalleryListener, data: GalleryMediaData) {
            itemBinding.apply {
                layout.setOnClickListener { listener.onGalleryImageClick(data) }
                imageView.showPlaceholderImage(data.uri)
                checkSelectedState(data)
            }
        }

        private fun checkSelectedState(data: GalleryMediaData) {
            when {
                data.isSelected -> {
                    itemBinding.apply {
                        selectorFrame.isVisible = true
                        selectorCounter.text = data.selectCounter.toString()
                        selectorCounter.isVisible = true
                        selectorFrame.setImageResource(R.drawable.background_solid_oval)

                        imageView.animate().scaleY(0.7F).duration = SCALE_DURATION
                        imageView.animate().scaleX(0.7F).duration = SCALE_DURATION
                    }
                }

                else -> {
                    itemBinding.apply {
                        selectorFrame.isVisible = true
                        selectorCounter.isVisible = false

                        selectorFrame.setImageResource(R.drawable.background_stroke_oval)

                        imageView.animate().scaleY(1F).duration = SCALE_DURATION
                        imageView.animate().scaleX(1F).duration = SCALE_DURATION
                    }
                }
            }
        }
    }

    interface GalleryListener {
        fun onGalleryImageClick(data: GalleryMediaData)
    }
}

private class GalleryDiffCallback : DiffUtil.ItemCallback<GalleryMediaData>() {
    override fun areItemsTheSame(oldItem: GalleryMediaData, newItem: GalleryMediaData): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: GalleryMediaData, newItem: GalleryMediaData
    ): Boolean {
        return oldItem == newItem
    }
}
package com.messenger.features.main.chat.attachFile.contents

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import com.messenger.databinding.FragmentChatAttachAudioBinding
import com.messenger.features.main.chat.attachFile.AttachViewModel
import com.messenger.features.main.common.adapters.ContactsAdapter
import com.messenger.models.ContactData
import com.messenger.utils.extensions.AfterTextChangedWatcher
import com.messenger.utils.hasPermission
import com.messenger.utils.isReadStoragePermissionDenied
import com.messenger.utils.showStorageRationaleDialog
import com.messenger.utils.storagePermission
import org.koin.androidx.viewmodel.ext.android.viewModel

class FragmentAttachAudio : Fragment() {

    private var binding: FragmentChatAttachAudioBinding? = null
    private val viewModel: AttachViewModel by viewModel()
    private lateinit var audioListAdapter: ContactsAdapter

    companion object {
        private val TAG = FragmentAttachAudio::class.java.simpleName
    }

    private val audioListListener = object : ContactsAdapter.SearchListListener {
        override fun onItemClick(data: ContactData) {
//            viewModel.changeGalleryItemSelection(data)
        }
    }

    private val permissionStorageRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            when {
                granted -> checkStoragePermissions()
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatAttachAudioBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            initAudioListSheet()

            checkStoragePermissions()
        }
    }

    private fun initObservers() {
        viewModel.contentData.observe(viewLifecycleOwner) { data ->
            handleContentData(data)
        }
    }

    private fun handleContentData(data: AttachViewModel.AttachMessageData?) {
        data?.let {
            when (it) {
                is AttachViewModel.AttachMessageData.MusicData -> {
                    handleMusicData(it.audioList)
                }

                else -> {}
            }
        }
    }

    private fun FragmentChatAttachAudioBinding.initAudioListSheet() {
        recyclerView.apply {
            audioListAdapter = ContactsAdapter(audioListListener)
            adapter = audioListAdapter
        }
    }

    private fun checkStoragePermissions() {
        when {
            requireContext().hasPermission(storagePermission()) -> {
                viewModel.showAudioPickerData()
            }

            requireActivity().isReadStoragePermissionDenied() -> showStorageRationaleDialog(
                requireContext(), permissionStorageRequest
            )

            else -> permissionStorageRequest.launch(storagePermission())
        }
    }

    private fun handleMusicData(audioList: List<String>?) {
        audioList?.let { list ->
            binding?.apply {
                attachSearchInputView.hint = "Search audio files"
//                audioListAdapter.submitList(list)

                attachSearchInputView.addTextChangedListener(object : AfterTextChangedWatcher() {
                    override fun afterTextChanged(s: Editable?) {
                        viewModel.setAudioListQuery(s?.toString() ?: "")
                    }
                })
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
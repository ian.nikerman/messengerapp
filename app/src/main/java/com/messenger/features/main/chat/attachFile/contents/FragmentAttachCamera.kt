package com.messenger.features.main.chat.attachFile.contents

import android.Manifest
import android.net.Uri
import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.AspectRatio
import androidx.camera.core.Camera
import androidx.camera.core.CameraSelector
import androidx.camera.core.FocusMeteringAction
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.messenger.R
import com.messenger.databinding.FragmentChatAttachCameraBinding
import com.messenger.features.main.chat.ChatViewModel
import com.messenger.features.main.chat.attachFile.AttachViewModel
import com.messenger.features.views.EmptyDataView
import com.messenger.utils.extensions.animationCaptureIn
import com.messenger.utils.getUTCMilliseconds
import com.messenger.utils.hasPermission
import com.messenger.utils.isCameraPermissionDenied
import com.messenger.utils.showCameraRationaleDialog
import com.messenger.utils.showImage
import com.messenger.utils.showImageAnimated
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class FragmentAttachCamera : Fragment() {

    private val chatViewModel: ChatViewModel by viewModel()
    private val viewModel: AttachViewModel by viewModel()
    private var binding: FragmentChatAttachCameraBinding? = null
    private var cameraProvider: ProcessCameraProvider? = null

    companion object {

        private val TAG = FragmentAttachCamera::class.java.simpleName

        private lateinit var outputDirectory: File
        private lateinit var cameraExecutor: ExecutorService
        private var imageCapture: ImageCapture? = null
        private var preview: Preview? = null
        private var camera: Camera? = null
        private var lensFacing = CameraSelector.LENS_FACING_BACK

        private fun createFile(baseFolder: File) =
            File(baseFolder, getUTCMilliseconds().toString() + ".jpg")

    }

    private val emptyDataViewListener = object : EmptyDataView.EmptyDataViewListener {
        override fun onEmptyDataViewClick() {
            Timber.tag(TAG).d("onEmptyDataViewClick")
            permissionCameraRequest.launch(Manifest.permission.CAMERA)
        }
    }

    private val permissionCameraRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            when {
                granted -> checkCameraPermissions()
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatAttachCameraBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cameraExecutor = Executors.newSingleThreadExecutor()
        outputDirectory = getOutputDirectory()
        binding?.emptyDataView?.setListener(emptyDataViewListener)
        checkCameraPermissions()
        initUi()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            imageControlLayout.cancelImageBtn.setOnClickListener {
                onCancelImage()
            }
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.capturedImageData.observe(viewLifecycleOwner) { imagesLiveData ->
            handleCapturedImage(imagesLiveData)
        }
    }

    private fun handleCapturedImage(fileUri: Uri?) {
        if (fileUri == null) {
            onImageReset()
        }
        fileUri?.let {
            onImageHandled(it)
        }
    }

    private fun onImageHandled(fileUri: Uri) {
        binding?.apply {
            imageControlLayout.shutterBtn.isVisible = false
            imagePreview.isVisible = true
            imagePreview.showImageAnimated(fileUri)
            imageControlLayout.parentLayout.isVisible = true
            imageControlLayout.cancelImageBtn.isVisible = true
            imageControlLayout.saveButton.isVisible = true
            imageControlLayout.saveButton.setOnClickListener {
                chatViewModel.setSelectedPhoto(
                    fileUri
                )
            }
        }
    }

    private fun onImageReset() {
        binding?.apply {
            imageControlLayout.cancelImageBtn.isVisible = false
            imageControlLayout.saveButton.isVisible = false
            imagePreview.isVisible = false
            imagePreview.resetZoom()
            imagePreview.showImage(null)
        }
    }

    private fun updateCameraUi() {
        binding?.apply {
            imageControlLayout.shutterBtn.setOnClickListener {
                imageCapture?.let { imageCapture ->
                    cameraTapFocusView.animationCaptureIn()

                    val photoFile = createFile(outputDirectory)
                    val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

                    imageCapture.takePicture(
                        outputOptions,
                        cameraExecutor,
                        object : ImageCapture.OnImageSavedCallback {
                            override fun onError(exc: ImageCaptureException) {

                            }

                            override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                                val savedUri = output.savedUri ?: Uri.fromFile(photoFile)
                                viewModel.saveCameraImageBitmap(savedUri)
                            }
                        })
                }
            }
        }
    }

    private fun setupZoomAndTapToFocus() {
        binding?.apply {
            cameraTapFocusView.setOnTouchListener { view, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        val factory = cameraView.meteringPointFactory
                        val point = factory.createPoint(event.x, event.y)
                        val action = FocusMeteringAction.Builder(point, FocusMeteringAction.FLAG_AF)
                            .setAutoCancelDuration(5, TimeUnit.SECONDS).build()
                        camera?.cameraControl?.startFocusAndMetering(action)
                    }
                }
                view.performClick()
                true
            }
        }
    }

    private fun checkCameraPermissions() {
        when {
            requireContext().hasPermission(Manifest.permission.CAMERA) -> {
                binding?.emptyDataViewLayout?.isVisible = false
                setupCamera()
            }

            requireActivity().isCameraPermissionDenied() -> {
                binding?.emptyDataViewLayout?.isVisible = true
                showCameraRationaleDialog(
                    requireContext(), permissionCameraRequest
                )
            }

            else -> {
                binding?.emptyDataViewLayout?.isVisible = true
                permissionCameraRequest.launch(Manifest.permission.CAMERA)
            }
        }
    }

    private fun setupCamera() {
        Timber.tag(TAG).d("setupCamera")
        updateCameraUi()
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFuture.addListener({
            cameraProvider = cameraProviderFuture.get()
            bindCameraUseCases()
        }, ContextCompat.getMainExecutor(requireContext()))
    }

    private fun bindCameraUseCases() {
        Timber.tag(TAG).d("bindCameraUseCases")
        val cameraProvider =
            cameraProvider ?: throw IllegalStateException("Camera initialization failed.")

        val cameraSelector = CameraSelector.Builder().requireLensFacing(lensFacing).build()

        preview = Preview.Builder().build().also {
            it.setSurfaceProvider(binding?.cameraView?.surfaceProvider)
        }

        imageCapture =
            ImageCapture.Builder().setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                .setTargetAspectRatio(AspectRatio.RATIO_16_9).build()

        cameraProvider.unbindAll()

        try {
            camera = cameraProvider.bindToLifecycle(
                this, cameraSelector, preview, imageCapture
            )
            setupZoomAndTapToFocus()
        } catch (exc: Exception) {
//            Timber.tag(TAG).d( "Use case binding failed", exc)
        }
    }

    private fun onCancelImage() {
        binding?.apply {
            imageControlLayout.shutterBtn.isVisible = true
        }
        viewModel.resetImage()
    }

    private fun getOutputDirectory(): File {
        val mediaDir = requireActivity().externalMediaDirs.firstOrNull()?.let {
            File(it, getString(R.string.app_name)).apply { mkdirs() }
        }
        return if (mediaDir != null && mediaDir.exists()) mediaDir else requireActivity().filesDir
    }

    override fun onDestroyView() {
        Timber.tag(TAG).d("onDestroyView")
        cameraExecutor.shutdownNow()
        super.onDestroyView()
        binding = null
    }
}
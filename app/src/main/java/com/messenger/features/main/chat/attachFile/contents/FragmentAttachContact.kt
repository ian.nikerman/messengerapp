package com.messenger.features.main.chat.attachFile.contents

import android.Manifest
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import com.messenger.databinding.FragmentChatAttachContactsBinding
import com.messenger.features.main.chat.attachFile.AttachViewModel
import com.messenger.features.main.common.adapters.ContactsAdapter
import com.messenger.models.ContactData
import com.messenger.utils.extensions.AfterTextChangedWatcher
import com.messenger.utils.hasPermission
import com.messenger.utils.isContactsPermissionDenied
import com.messenger.utils.showContactsRationaleDialog
import org.koin.androidx.viewmodel.ext.android.viewModel

class FragmentAttachContact : Fragment() {

    private var binding: FragmentChatAttachContactsBinding? = null
    private val viewModel: AttachViewModel by viewModel()
    private lateinit var contactsAdapter: ContactsAdapter

    companion object {
        private val TAG = FragmentAttachContact::class.java.simpleName
    }

    private val permissionContactsRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            when {
                granted -> checkContactsPermissions()
            }
        }

    private val contactsListener = object : ContactsAdapter.SearchListListener {
        override fun onItemClick(data: ContactData) {
//            viewModel.changeGalleryItemSelection(data)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatAttachContactsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            initContactsSheet()

            checkContactsPermissions()
        }
    }

    private fun initObservers() {
        viewModel.contentData.observe(viewLifecycleOwner) { data ->
            handleContentData(data)
        }
    }

    private fun handleContentData(data: AttachViewModel.AttachMessageData?) {
        data?.let {
            when (it) {
                is AttachViewModel.AttachMessageData.ContactsData -> {
                    handleContactsData(it.contacts)
                }

                else -> {}
            }
        }
    }

    private fun FragmentChatAttachContactsBinding.initContactsSheet() {
        recyclerView.apply {
            contactsAdapter = ContactsAdapter(contactsListener)
            adapter = contactsAdapter
        }
    }

    private fun checkContactsPermissions() {
        when {
            requireContext().hasPermission(Manifest.permission.READ_CONTACTS) -> viewModel.showContactPickerData(
                requireContext()
            )

            requireActivity().isContactsPermissionDenied() -> showContactsRationaleDialog(
                requireContext(), permissionContactsRequest
            )

            else -> permissionContactsRequest.launch(Manifest.permission.READ_CONTACTS)
        }
    }

    private fun handleContactsData(contacts: List<ContactData>?) {
        contacts?.let { list ->
            binding?.apply {
                attachSearchInputView.hint = "Search contacts"
                contactsAdapter.submitList(list)

                attachSearchInputView.addTextChangedListener(object : AfterTextChangedWatcher() {
                    override fun afterTextChanged(s: Editable?) {
                        viewModel.setContactsQuery(s?.toString() ?: "")
                    }
                })
            }
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
package com.messenger.features.main.chat.attachFile.contents

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.messenger.R
import com.messenger.databinding.FragmentChatAttachFilesBinding
import com.messenger.features.main.chat.attachFile.AttachViewModel
import com.messenger.features.main.chat.attachFile.adapters.FilesAdapter
import com.messenger.models.GalleryMediaData
import com.messenger.utils.hasPermission
import com.messenger.utils.isReadStoragePermissionDenied
import com.messenger.utils.showStorageRationaleDialog
import com.messenger.utils.storagePermission
import org.koin.androidx.viewmodel.ext.android.viewModel

class FragmentAttachFile : Fragment() {

    private var binding: FragmentChatAttachFilesBinding? = null
    private val viewModel: AttachViewModel by viewModel()
    private lateinit var filesAdapter: FilesAdapter

    companion object {
        private val TAG = FragmentAttachFile::class.java.simpleName
    }

    private val permissionStorageRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            when {
                granted -> checkStoragePermissions()
            }
        }

    private val filesListener = object : FilesAdapter.FilesListener {
        override fun onGalleryImageClick(data: GalleryMediaData) {
            viewModel.changeFileItemSelection(data)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatAttachFilesBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            initFilesSheet()
            checkStoragePermissions()
        }
    }

    private fun initObservers() {
        viewModel.contentData.observe(viewLifecycleOwner) { data ->
            handleContentData(data)
        }
    }

    private fun handleContentData(data: AttachViewModel.AttachMessageData?) {
        data?.let {
            when (it) {
                is AttachViewModel.AttachMessageData.FilesData -> {
                    handleFilesData(it.files)
                }

                else -> {}
            }
        }
    }

    private fun FragmentChatAttachFilesBinding.initFilesSheet() {
        internalStorageItem.title.text = getString(R.string.internal_storage)
        internalStorageItem.subTitle.text = getString(R.string.browse_your_file_system)
        internalStorageItem.imageView.background = ContextCompat.getDrawable(
            this@FragmentAttachFile.requireContext(), R.drawable.ic_btn_internal_storage
        )

        appStorageItem.title.text = context?.getText(R.string.app_name)
        appStorageItem.subTitle.text = getString(R.string.browse_the_app_s_folder)
        appStorageItem.imageView.background = ContextCompat.getDrawable(
            this@FragmentAttachFile.requireContext(), R.drawable.ic_btn_app_folder
        )

        galleryStorageItem.title.text = getString(R.string.gallery)
        galleryStorageItem.subTitle.text = getString(R.string.to_send_images_without_compression)
        galleryStorageItem.imageView.background = ContextCompat.getDrawable(
            this@FragmentAttachFile.requireContext(), R.drawable.ic_btn_gallery_folder
        )

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            filesAdapter = FilesAdapter(filesListener)
            adapter = filesAdapter
        }
    }

    private fun checkStoragePermissions() {
        when {
            requireContext().hasPermission(storagePermission()) -> viewModel.showFilePickerData()

            requireActivity().isReadStoragePermissionDenied() -> showStorageRationaleDialog(
                requireContext(), permissionStorageRequest
            )

            else -> permissionStorageRequest.launch(storagePermission())
        }
    }

    private fun handleFilesData(files: List<GalleryMediaData>?) {
        files?.let {
            binding?.apply {
                filesAdapter.submitList(it)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
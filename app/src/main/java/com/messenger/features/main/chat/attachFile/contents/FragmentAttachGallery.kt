package com.messenger.features.main.chat.attachFile.contents

import android.Manifest
import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.messenger.databinding.FragmentChatAttachGalleryBinding
import com.messenger.features.main.chat.attachFile.AttachViewModel
import com.messenger.features.main.chat.attachFile.adapters.GalleryAdapter
import com.messenger.features.views.EmptyDataView
import com.messenger.models.GalleryMediaData
import com.messenger.utils.hasPermission
import com.messenger.utils.isReadStoragePermissionDenied
import com.messenger.utils.showStorageRationaleDialog
import com.messenger.utils.storagePermission
import org.koin.androidx.viewmodel.ext.android.viewModel

class FragmentAttachGallery : Fragment() {

    private var binding: FragmentChatAttachGalleryBinding? = null
    private val viewModel: AttachViewModel by viewModel()
    private lateinit var galleryAdapter: GalleryAdapter

    companion object {
        private val TAG = FragmentAttachGallery::class.java.simpleName
        private const val GALLERY_LIST_ROWS = 3
    }

    private val emptyDataViewListener = object : EmptyDataView.EmptyDataViewListener {
        override fun onEmptyDataViewClick() {
            Timber.tag(TAG).d("onEmptyDataViewClick")
            permissionStorageRequest.launch(storagePermission())
        }
    }

    private val galleryListener = object : GalleryAdapter.GalleryListener {
        override fun onGalleryImageClick(data: GalleryMediaData) {
            Timber.tag(TAG).d("onGalleryImageClick")
            viewModel.changeGalleryItemSelection(data)
        }
    }

    private val permissionStorageRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            when {
                granted -> checkStoragePermissions()
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatAttachGalleryBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()
        initObservers()
    }

    private fun initUi() {
        Timber.tag(TAG).d("initUi")
        binding?.apply {
            emptyDataView.setListener(emptyDataViewListener)
            initGallerySheet()
            checkStoragePermissions()
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.contentData.observe(viewLifecycleOwner) { data ->
            handleContentData(data)
        }
    }

    private fun handleContentData(data: AttachViewModel.AttachMessageData?) {
        data?.let {
            Timber.tag(TAG).d("handleContentData")
            when (it) {
                is AttachViewModel.AttachMessageData.GalleryData -> {
                    handleGalleryData(it.galleryList)
                }

                else -> {}
            }
        }
    }

    private fun FragmentChatAttachGalleryBinding.initGallerySheet() {
        Timber.tag(TAG).d("initGallerySheet")
        gridRecyclerView.apply {
            layoutManager = GridLayoutManager(context, GALLERY_LIST_ROWS)
            galleryAdapter = GalleryAdapter(galleryListener)
            adapter = galleryAdapter
        }
    }

    private fun checkStoragePermissions() {
        Timber.tag(TAG).d("checkStoragePermissions")
        when {
            requireContext().hasPermission(storagePermission()) -> {
                binding?.emptyDataViewLayout?.isVisible = false
                viewModel.showGalleryPickerData()
            }

            requireActivity().isReadStoragePermissionDenied() -> {
                binding?.emptyDataViewLayout?.isVisible = true
                showStorageRationaleDialog(
                    requireContext(), permissionStorageRequest
                )
            }

            else -> {
                binding?.emptyDataViewLayout?.isVisible = true
                permissionStorageRequest.launch(storagePermission())
            }
        }
    }

    private fun handleGalleryData(imagesLiveData: List<GalleryMediaData>?) {
        imagesLiveData?.let {
            Timber.tag(TAG).d("handleGalleryData")
            galleryAdapter.submitList(it)
        }
    }

    override fun onDestroyView() {
        Timber.tag(TAG).d("onDestroyView")
        super.onDestroyView()
        binding = null
    }
}
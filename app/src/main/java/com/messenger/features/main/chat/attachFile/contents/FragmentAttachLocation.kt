package com.messenger.features.main.chat.attachFile.contents

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.messenger.R
import com.messenger.databinding.FragmentChatAttachLocationBinding
import com.messenger.features.main.chat.attachFile.AttachViewModel
import com.messenger.models.DetailsType
import com.messenger.models.LocationState
import com.messenger.models.Status
import com.messenger.models.TreasureMapData
import com.messenger.utils.hasPermission
import com.messenger.utils.isLocationPermissionDenied
import com.messenger.utils.promptEnableGps
import com.messenger.utils.showLocationRationaleDialog
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class FragmentAttachLocation : Fragment(),
    OnMapReadyCallback,
    GoogleMap.OnCameraMoveStartedListener,
    GoogleMap.OnCameraIdleListener {

    private val viewModel: AttachViewModel by viewModel()
    private var binding: FragmentChatAttachLocationBinding? = null

    companion object {
        private val TAG = FragmentAttachLocation::class.java.simpleName
        private lateinit var gMap: GoogleMap
        private const val DEFAULT_ZOOM_LEVEL = 16f
    }

    private val permissionRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            when {
                granted -> viewModel.onPermissionAccepted()
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatAttachLocationBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onPause() {
        viewModel.stopLocationUpdates()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        checkLocationPermissions()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            findMeButton.setOnClickListener { onFindMeClick() }

            initLocationSheet()
            showSearchingMyLocationUi()
            showSearchingMapLocationUi()
            showMarkerLocationDetails()
        }
    }

    private fun initObservers() {
        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.mapDetailsData.collect { uiState ->
                        when (uiState.status) {
                            Status.SUCCESS -> {
                                handleDetailsData(uiState.data)
                            }

                            Status.ERROR -> {
                                showError(uiState.message)
                            }

                            else -> {}
                        }
                    }
                }
            }
        }
    }

    private fun handleDetailsData(data: List<TreasureMapData>?) {
        data?.forEach {
            when (it) {
                is TreasureMapData.ProgressData -> {}

                is TreasureMapData.LocationData -> {
                    handleLocationData(it)
                }

                is TreasureMapData.MapDetailsData -> {
                    handleMapDetailsState(it)
                }
            }
        }
    }

    private fun showError(message: String?) {
        Toast.makeText(
            requireContext(), message, Toast.LENGTH_LONG
        ).show()
    }

    private fun handleLocationData(data: TreasureMapData.LocationData) {
        when (data.locationState) {
            LocationState.LOCATION_AVAILABLE -> {
                showMeOnMap()
                if (data.showOnMap) {
                    showMapLocation(data.currentLocation)
                }
            }

            LocationState.LOCATION_UNAVAILABLE -> {
                hideMeOnMap()
                binding?.apply {
                    showDefaultMyLocationUi()
                }
            }

            LocationState.LOCATION_MANUAL -> {}
        }
    }

    private fun handleMapDetailsState(data: TreasureMapData.MapDetailsData) {
        when (data.detailsType) {
            DetailsType.SEARCHING -> {
                showSearchingMyLocationUi()
                showSearchingMapLocationUi()
            }

            DetailsType.INFO -> {
                data.detailsViewData?.let { info ->
                    showAvailableMapLocationUi()
                    showAvailableMyLocationUi()

                    binding?.apply {
                        "${info.street}, ${info.city}".also { myLocationItem.subTitle.text = it }
                    }
                }
            }
        }
    }

    private fun getTargetLatitude(): Double {
        return gMap.cameraPosition.target.latitude
    }

    private fun getTargetLongitude(): Double {
        return gMap.cameraPosition.target.longitude
    }

    private fun showMarkerLocationDetails() {
        binding?.apply {
            myLocationItem.title.text = getString(R.string.send_chosen_location)
            myLocationItem.imageView.background = ContextCompat.getDrawable(
                this@FragmentAttachLocation.requireContext(), R.drawable.ic_send_my_location
            )

            shareLocationItem.title.text = getString(R.string.share_my_live_location_for)
            shareLocationItem.subTitle.text = getString(R.string.updated_in_real_time_as_you_move)
            shareLocationItem.imageView.background = ContextCompat.getDrawable(
                this@FragmentAttachLocation.requireContext(), R.drawable.ic_share_live_location
            )
        }
    }

    private fun onFindMeClick() {
        viewModel.onFindMeClick()
        checkLocationPermissions()
    }

    private fun animateCameraZoom(currentLatLng: LatLng) {
        val cameraPosition =
            CameraPosition.Builder().target(currentLatLng).zoom(DEFAULT_ZOOM_LEVEL).build()
        gMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    private fun initLocationSheet() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this@FragmentAttachLocation)
    }

    private fun checkLocationPermissions() {
        when {

            requireContext().hasPermission(Manifest.permission.ACCESS_FINE_LOCATION) && isGpsEnabled() -> {
                viewModel.onPermissionAccepted()
            }

            requireActivity().isLocationPermissionDenied() -> showLocationRationaleDialog(
                requireContext(), permissionRequest
            )

            !requireContext().hasPermission(Manifest.permission.ACCESS_FINE_LOCATION) -> permissionRequest.launch(
                Manifest.permission.ACCESS_FINE_LOCATION
            )

            !isGpsEnabled() -> promptEnableGps(requireContext())
        }
    }

    private fun isGpsEnabled(): Boolean {
        val locationManager =
            requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private fun setUpMap(googleMap: GoogleMap?) {
        googleMap?.let {
            gMap = it
            gMap.isIndoorEnabled = true
            gMap.uiSettings.isZoomControlsEnabled = false
            gMap.uiSettings.isCompassEnabled = false
            gMap.uiSettings.isMyLocationButtonEnabled = false
            gMap.uiSettings.isRotateGesturesEnabled = false
            gMap.uiSettings.isMapToolbarEnabled = false
            gMap.setOnCameraMoveStartedListener(this)
            gMap.setOnCameraIdleListener(this)
            setNightMapMode()
        }
    }

    @SuppressLint("MissingPermission")
    private fun showMeOnMap() {
        if (requireContext().hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)
        ) {
            gMap.isMyLocationEnabled = true
        }
    }

    @SuppressLint("MissingPermission")
    private fun hideMeOnMap() {
        if (requireContext().hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)
        ) {
            gMap.isMyLocationEnabled = false
        }
    }

    private fun showMapLocation(location: Location?) {
        location?.let {
            animateCameraZoom(LatLng(it.latitude, it.longitude))
        }
    }

    private fun setNightMapMode() {
        gMap.setMapStyle(
            MapStyleOptions.loadRawResourceStyle(
                requireContext(), R.raw.map_style
            )
        )
    }

    private fun showAvailableMapLocationUi() {
        binding?.apply {
            myLocationItem.parentLayout.isClickable = true
            myLocationItem.parentLayout.alpha = 1f
        }
    }

    private fun showAvailableMyLocationUi() {
        binding?.apply {
            shareLocationItem.parentLayout.isClickable = true
            shareLocationItem.parentLayout.alpha = 1f
        }
    }

    private fun showSearchingMapLocationUi() {
        binding?.apply {
            myLocationItem.subTitle.text = getString(R.string.searching_title)
            myLocationItem.parentLayout.isClickable = true
            myLocationItem.parentLayout.alpha = 1f
        }
    }

    private fun showSearchingMyLocationUi() {
        binding?.apply {
//            shareLocationItem.subTitle.text = getString(R.string.searching_title)
            shareLocationItem.parentLayout.isClickable = true
            shareLocationItem.parentLayout.alpha = 1f
        }
    }

    private fun showDefaultMyLocationUi() {
        binding?.apply {
            shareLocationItem.subTitle.text = ""
            shareLocationItem.parentLayout.isClickable = false
            shareLocationItem.parentLayout.alpha = .4f
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        setUpMap(googleMap)
        onFindMeClick()
    }

    override fun onCameraIdle() {
        viewModel.onCameraIdle(getTargetLatitude(), getTargetLongitude())
    }

    override fun onCameraMoveStarted(reason: Int) {
        when (reason) {
            GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE -> {
                viewModel.setMapMovedByUser()
            }
        }
    }
}
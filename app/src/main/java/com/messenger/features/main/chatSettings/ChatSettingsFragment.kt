package com.messenger.features.main.chatSettings

import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.google.android.material.tabs.TabLayoutMediator
import com.messenger.R
import com.messenger.databinding.FragmentChatSettingsBinding
import com.messenger.features.main.MainViewModel
import com.messenger.features.main.chatSettings.adapters.ChatInfoAdapter
import com.messenger.features.main.chatSettings.adapters.contentData.ContentDataVPFragment
import com.messenger.features.main.common.adapters.ContentDataVPAdapter
import com.messenger.features.views.HeaderView
import com.messenger.models.ContentType
import com.messenger.utils.extensions.LiveEvent
import com.messenger.utils.extensions.ZoomOutPageTransformer
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChatSettingsFragment : Fragment() {

    private val sharedViewModel by activityViewModel<MainViewModel>()
    private val viewModel: ChatSettingsViewModel by viewModel()
    private var binding: FragmentChatSettingsBinding? = null

    companion object {

        private val TAG = ChatSettingsFragment::class.java.simpleName
        private lateinit var chatInfoAdapter: ChatInfoAdapter
        private lateinit var contentDataVPAdapter: ContentDataVPAdapter

    }

    private val headerListener = object : HeaderView.HeaderViewListener {

        override fun onMoreClick() {
            Timber.tag(TAG).d("onMoreClick")

        }

        override fun onCallClick() {
            Timber.tag(TAG).d("onCallClick")
            sharedViewModel.openCall(
                callingFragment = this@ChatSettingsFragment, roomId = handleRoomId()
            )
        }

        override fun onEditClick() {
            Timber.tag(TAG).d("onEditClick")

        }

        override fun onBackClick() {
            Timber.tag(TAG).d("onBackClick")
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
    }

    private val infoListener = object : ChatInfoAdapter.ChatInfoListener {
        override fun onItemContactClick(itemId: Long) {
            Timber.tag(TAG).d("onItemContactClick")

        }

        override fun onItemCopyClick(title: String) {
            Timber.tag(TAG).d("onItemCopyClick")

        }

        override fun onInviteLinkClick(title: String) {
            Timber.tag(TAG).d("onInviteLinkClick")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatSettingsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(requireView(), savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
        initAdapter()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            headerView.setListener(headerListener)

            chatButton.setOnClickListener {
                sharedViewModel.openChat(
                    this@ChatSettingsFragment, handleRoomId()
                )
            }
        }
    }

    private fun initAdapter() {
        Timber.tag(TAG).d("initAdapter")
        binding?.apply {
            infoRecyclerView.apply {
                chatInfoAdapter = ChatInfoAdapter(
                    infoListener
                )
                adapter = chatInfoAdapter
            }
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.chatMainData.observe(viewLifecycleOwner) { data ->
            handleChatMainData(data)
        }
        viewModel.contentData.observe(viewLifecycleOwner) { data ->
            handleVPTabsData(data)
        }
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
        viewModel.errorEvent.observe(viewLifecycleOwner) { event ->
            handleErrorEvent(event)
        }
        viewModel.getChat(roomId = handleRoomId())
    }

    private fun handleChatMainData(data: ChatSettingsViewModel.ChatInfoDetails.MainData?) {
        data?.let {
            Timber.tag(TAG).d("handleChatMainData")
            binding?.apply {
                headerView.setHeaderChatSettingsData(it)
                infoDescription.text = it.mainData.description ?: ""
                chatInfoAdapter.submitList(it.infoList)
            }
        }
    }

    private fun handleVPTabsData(data: ChatSettingsViewModel.ChatInfoDetails.ContentItemData?) {
        data?.let {
            Timber.tag(TAG).d("handleChatTabsData")
            binding?.apply {
                val fragmentList = ArrayList<Fragment>()
                it.tabs.map { it.contentType }.forEach { contentType ->
                    getContentFragment(
                        contentType, it
                    )?.let { fragment ->
                        fragmentList.add(
                            fragment
                        )
                    }
                }

                viewPager.apply {
                    contentDataVPAdapter = ContentDataVPAdapter(
                        fragmentList, childFragmentManager, lifecycle
                    )
                    setPageTransformer(ZoomOutPageTransformer())
                    adapter = contentDataVPAdapter
                    TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                        tab.text = getTabString(it.tabs[position].contentType)
                    }.attach()
                    tabLayout.getTabAt(0)?.select()
                    viewPager.currentItem = 0
                }
            }
        }
    }

    private fun getContentFragment(
        contentType: ContentType, data: ChatSettingsViewModel.ChatInfoDetails.ContentItemData
    ): Fragment? {
        return when (contentType) {
            ContentType.CONTACT -> ContentDataVPFragment(data.members)
            ContentType.MEDIA -> ContentDataVPFragment(data.media)
            ContentType.FILE -> ContentDataVPFragment(data.files)
            ContentType.LINK -> ContentDataVPFragment(data.links)
            ContentType.AUDIO -> ContentDataVPFragment(data.audios)
            ContentType.VOICE -> ContentDataVPFragment(data.voice)
            ContentType.GIF -> ContentDataVPFragment(data.gifs)
            ContentType.GROUP -> ContentDataVPFragment(data.groups)
            else -> null
        }
    }

    private fun getTabString(contentType: ContentType): String {
        return when (contentType) {
            ContentType.CONTACT -> getString(R.string.members)
            ContentType.MEDIA -> getString(R.string.media)
            ContentType.FILE -> getString(R.string.files)
            ContentType.LINK -> getString(R.string.links)
            ContentType.AUDIO -> getString(R.string.audio)
            ContentType.VOICE -> getString(R.string.voice)
            ContentType.GIF -> getString(R.string.gif_s)
            ContentType.GROUP -> getString(R.string.groups)
            else -> ""
        }
    }

    private fun handleErrorEvent(event: LiveEvent<ChatSettingsViewModel.ErrorEvent>?) {
        event?.getContentIfNotHandled()?.let {

        }
    }

    private fun handleProgressEvent(isLoading: Boolean) {
        binding?.progress?.setProgressEvent(isLoading)
    }

    private fun handleRoomId(): Long {
        val navArgs: ChatSettingsFragmentArgs by navArgs()
        return navArgs.roomId
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}

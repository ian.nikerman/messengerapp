package com.messenger.features.main.chatSettings.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.messenger.databinding.ItemInformationBinding
import com.messenger.models.ChatInfoItem
import com.messenger.models.ChatInfoType

class ChatInfoAdapter(
    private val listener: ChatInfoListener
) : ListAdapter<ChatInfoItem, RecyclerView.ViewHolder>(ChatInfoDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemInformationBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as ViewHolder).bind(listener, item)
    }

    class ViewHolder(private val itemBinding: ItemInformationBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(listener: ChatInfoListener, data: ChatInfoItem) {
            itemBinding.apply {

                when (data.type) {
                    ChatInfoType.PHONE -> {
                        data.contactData?.id?.let { id ->
                            layout.setOnClickListener { listener.onItemContactClick(id) }
                        }
                    }

                    ChatInfoType.USERNAME -> {
                        layout.setOnClickListener { listener.onItemCopyClick(data.title) }
                    }

                    ChatInfoType.INVITE_LINK -> {
                        layout.setOnClickListener { listener.onInviteLinkClick(data.title) }
                    }

                    else -> {}
                }
                title.text = data.title
                subTitle.text = data.subtitle
            }
        }
    }

    interface ChatInfoListener {
        fun onItemCopyClick(title: String)
        fun onInviteLinkClick(title: String)
        fun onItemContactClick(itemId: Long)
    }
}

private class ChatInfoDiffCallback : DiffUtil.ItemCallback<ChatInfoItem>() {
    override fun areItemsTheSame(oldItem: ChatInfoItem, newItem: ChatInfoItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: ChatInfoItem, newItem: ChatInfoItem
    ): Boolean {
        return oldItem == newItem
    }
}
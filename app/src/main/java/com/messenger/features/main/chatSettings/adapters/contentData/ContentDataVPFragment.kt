package com.messenger.features.main.chatSettings.adapters.contentData

import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.messenger.databinding.FragmentContentDataVpBinding
import com.messenger.features.main.MainViewModel
import com.messenger.models.ContentData
import com.messenger.models.ContentType
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class ContentDataVPFragment(private val data: List<ContentData>) : Fragment() {

    private val mainViewModel by activityViewModel<MainViewModel>()
    private var binding: FragmentContentDataVpBinding? = null

    companion object {
        private val TAG = ContentDataVPFragment::class.java.simpleName
        private lateinit var contentDataAdapter: ContentDataAdapter
    }

    private val contentDataListener = object : ContentDataAdapter.ContentDataAdapterListener {
        override fun onItemClick(data: ContentData) {
            Timber.tag(TAG).d("onItemClick")
            when (data.contentType) {
                ContentType.CONTACT -> mainViewModel.openChat(parentFragment, data.id)
                else -> {}
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentContentDataVpBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initAdapter()
    }

    private fun initAdapter() {
        binding?.apply {
            contentDataRecyclerView.apply {
                contentDataAdapter = ContentDataAdapter(contentDataListener)
                adapter = contentDataAdapter
            }
            contentDataAdapter.submitList(data)
        }
    }
}
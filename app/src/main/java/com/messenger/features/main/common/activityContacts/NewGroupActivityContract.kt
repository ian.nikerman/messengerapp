package com.messenger.features.main.common.activityContacts

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import com.messenger.features.main.newGroup.NewGroupActivity

class NewGroupActivityContract : ActivityResultContract<Int, Long?>() {

    override fun createIntent(context: Context, input: Int): Intent {
        return Intent(context, NewGroupActivity::class.java)
    }

    override fun parseResult(resultCode: Int, intent: Intent?): Long? {
        val data = intent?.getLongExtra(NewGroupActivity.GROUP_ID, -1)
        return if (resultCode == Activity.RESULT_OK && data != null) data
        else null
    }
}
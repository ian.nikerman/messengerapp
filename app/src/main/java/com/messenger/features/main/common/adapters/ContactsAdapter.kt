package com.messenger.features.main.common.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.messenger.databinding.ItemSearchListBinding
import com.messenger.models.ContactData
import com.messenger.utils.getColoredCircle
import com.messenger.utils.getNameInitials
import com.messenger.utils.showDrawable
import com.messenger.utils.showPlaceholderCircleImage

class ContactsAdapter(
    private val listener: SearchListListener
) : ListAdapter<ContactData, RecyclerView.ViewHolder>(SearchListDiffCallback()) {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): ViewHolder {
        val itemBinding =
            ItemSearchListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as ViewHolder).bind(
            listener, item
        )
    }

    class ViewHolder(private val itemBinding: ItemSearchListBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(
            listener: SearchListListener, data: ContactData
        ) {
            itemBinding.apply {
                layout.setOnClickListener { listener.onItemClick(data) }
                title.text = data.name
                subTitle.text = data.phoneNumber.firstOrNull() ?: ""

                checkAvatar(data)
                changeSelectedState(data.isSelected == true)
            }
        }

        private fun ItemSearchListBinding.changeSelectedState(selected: Boolean) {
            layout.isSelected = selected
            selectorView.isVisible = selected
        }

        private fun ItemSearchListBinding.checkAvatar(data: ContactData) {
            when (data.avatar) {
                null -> {
                    imageView.showDrawable(getColoredCircle(data.color))
                    initials.text = data.name.getNameInitials()
                    initials.isVisible = true
                }

                else -> {
                    imageView.showPlaceholderCircleImage(data.avatar)
                    initials.isVisible = false
                }
            }
        }
    }

    interface SearchListListener {
        fun onItemClick(data: ContactData)
    }
}

private class SearchListDiffCallback : DiffUtil.ItemCallback<ContactData>() {
    override fun areItemsTheSame(oldItem: ContactData, newItem: ContactData): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: ContactData, newItem: ContactData
    ): Boolean {
        return oldItem == newItem
    }
}
package com.messenger.features.main.contacts

import android.Manifest
import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.messenger.databinding.FragmentContactsBinding
import com.messenger.features.main.MainViewModel
import com.messenger.features.main.common.activityContacts.NewGroupActivityContract
import com.messenger.features.main.common.adapters.ContactsAdapter
import com.messenger.features.views.EmptyDataView
import com.messenger.features.views.HeaderView
import com.messenger.models.ContactData
import com.messenger.utils.Constants.Companion.REQUEST_NEW_GROUP_CONTRACT
import com.messenger.utils.closeKeyboard
import com.messenger.utils.extensions.LiveEvent
import com.messenger.utils.extensions.RecyclerViewTopScrollListener
import com.messenger.utils.hasPermission
import com.messenger.utils.isContactsPermissionDenied
import com.messenger.utils.openKeyboard
import com.messenger.utils.showContactsRationaleDialog
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ContactsFragment : Fragment() {

    private val mainViewModel by activityViewModel<MainViewModel>()
    private val viewModel: ContactsViewModel by viewModel()
    private var binding: FragmentContactsBinding? = null

    companion object {
        private val TAG = ContactsFragment::class.java.simpleName
        private lateinit var contactsAdapter: ContactsAdapter
        private lateinit var linearLayoutManager: LinearLayoutManager
    }

    private val permissionContactsRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            when {
                granted -> checkContactsPermissions()
            }
        }

    private val headerListener = object : HeaderView.HeaderViewListener {

        override fun onInputTypedText(typedText: String) {
            viewModel.querySearch(typedText)
        }

        override fun onSearchClick(inputView: EditText) {
            Timber.tag(TAG).d("onSearchClick")
            openKeyboard(requireActivity(), inputView)
        }

        override fun onBackClick() {
            Timber.tag(TAG).d("onBackClick")
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        override fun onHideInputClick(inputView: EditText) {
            Timber.tag(TAG).d("onHideInputClick")
            closeKeyboard(requireActivity(), inputView)
        }
    }

    private val contactsListener = object : ContactsAdapter.SearchListListener {
        override fun onItemClick(data: ContactData) {
            Timber.tag(TAG).d("onItemClick")
            mainViewModel.openChat(this@ContactsFragment, data.id)
        }
    }

    private val emptyDataViewListener = object : EmptyDataView.EmptyDataViewListener {
        override fun onEmptyDataViewClick() {
            Timber.tag(TAG).d("onEmptyDataViewClick")
            permissionContactsRequest.launch(Manifest.permission.READ_CONTACTS)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentContactsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onResume() {
        super.onResume()
        checkContactsPermissions()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
        initObservers()
    }

    private fun getScrollTopListener(): RecyclerViewTopScrollListener {
        return object : RecyclerViewTopScrollListener(linearLayoutManager) {

            override fun changeScrolledFromTopState(
                totalScrollRange: Int, verticalOffset: Int
            ) {
                setHeaderAlpha(totalScrollRange, verticalOffset)
            }
        }
    }

    private fun setHeaderAlpha(totalScrollRange: Int, verticalOffset: Int) {
        binding?.apply {
            val offsetVertical =
                (kotlin.math.abs(totalScrollRange) * 0.1 / verticalOffset).toFloat()
            headerLayout.alpha = offsetVertical
            when {
                offsetVertical <= 0.2 && offsetVertical != 0f -> {
                    headerLayout.isVisible = false
                }

                else -> {
                    headerLayout.isVisible = true
                }
            }
        }
    }

    private fun initUi() {
        binding?.apply {
            headerView.setListener(headerListener)

            createGroupItem.setOnClickListener {
                openNewGroupActivity.launch(REQUEST_NEW_GROUP_CONTRACT)
            }
            emptyDataView.setListener(emptyDataViewListener)
            initAdapter()
        }
    }

    private fun initObservers() {
        viewModel.emptyDataState.observe(viewLifecycleOwner) { state ->
            handleEmptyDataState(state)
        }
        viewModel.contactsData.observe(viewLifecycleOwner) { data ->
            handleContactsData(data)
        }
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
        viewModel.errorEvent.observe(viewLifecycleOwner) { event ->
            handleErrorEvent(event)
        }
    }

    private fun handleEmptyDataState(state: ContactsViewModel.EmptyDataState?) {
        state?.let {
            when (it) {
                ContactsViewModel.EmptyDataState.STATE_DATA -> binding?.emptyDataView?.isVisible =
                    false

                ContactsViewModel.EmptyDataState.STATE_DATA_EMPTY -> binding?.emptyDataView?.isVisible =
                    true
            }
        }
    }

    private fun handleProgressEvent(isLoading: Boolean?) {
        binding?.progress?.setProgressEvent(isLoading ?: false)
    }

    private fun handleErrorEvent(event: LiveEvent<ContactsViewModel.ErrorEvent>?) {
        event?.getContentIfNotHandled()?.let {
            Toast.makeText(
                requireContext(), it.name, Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun handleContactsData(data: ContactsViewModel.ContactsData?) {
        data?.let {
            contactsAdapter.submitList(it.contacts)
        }
    }

    private fun checkContactsPermissions() {
        when {
            requireContext().hasPermission(Manifest.permission.READ_CONTACTS) -> {
                binding?.emptyDataView?.isVisible = false
                viewModel.getContacts(
                    requireContext()
                )
            }

            requireActivity().isContactsPermissionDenied() -> {
                binding?.emptyDataView?.isVisible = true
                showContactsRationaleDialog(
                    requireContext(), permissionContactsRequest
                )
            }

            else -> {
                binding?.emptyDataView?.isVisible = true
                permissionContactsRequest.launch(Manifest.permission.READ_CONTACTS)
            }
        }
    }

    private fun FragmentContactsBinding.initAdapter() {
        recyclerView.apply {
            contactsAdapter = ContactsAdapter(contactsListener)
            linearLayoutManager = LinearLayoutManager(requireContext())
            layoutManager = linearLayoutManager
            adapter = contactsAdapter
            addOnScrollListener(getScrollTopListener())
        }
    }

    private val openNewGroupActivity =
        registerForActivityResult(NewGroupActivityContract()) { result ->
            Timber.tag(TAG).d("NewGroupActivityContract result: $result")
            result?.let {
                mainViewModel.openChat(ContactsFragment(), it)
            }
        }
}



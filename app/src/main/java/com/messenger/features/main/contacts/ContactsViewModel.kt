package com.messenger.features.main.contacts

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import timber.log.Timber
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.messenger.data.ContactsRepository
import com.messenger.models.ContactData
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import com.messenger.utils.hasPermission
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ContactsViewModel(private val contactsRepository: ContactsRepository) : ViewModel() {

    companion object {

        private val TAG = ContactsViewModel::class.java.simpleName
        private var contacts: List<ContactData> = ArrayList()

    }

    enum class ErrorEvent {
        ERROR_LOADING_DATA
    }

    val errorEvent = LiveEventData<ErrorEvent>()

    data class ContactsData(
        val contacts: List<ContactData>
    )

    val contactsData = MutableLiveData<ContactsData?>()

    enum class EmptyDataState {
        STATE_DATA, STATE_DATA_EMPTY
    }

    val emptyDataState = MutableLiveData<EmptyDataState>()

    val progressEvent = ProgressData()

    fun getContacts(
        context: Context
    ) {
        Timber.tag(TAG).d("getContacts")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                if (contacts.isEmpty()) {
                    if (context.hasPermission(Manifest.permission.READ_CONTACTS)
                    ) {
                        contactsData.postValue(ContactsData(contacts = contacts))
                    }
                    contacts = contactsRepository.fetchContacts(context, true)
                }
                checkDataState()
                contactsData.postValue(ContactsData(contacts = contacts))
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_DATA)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    private fun checkDataState() {
        emptyDataState.postValue(if (contacts.isEmpty()) EmptyDataState.STATE_DATA_EMPTY else EmptyDataState.STATE_DATA)
    }

    fun querySearch(typedText: String) {
        viewModelScope.launch(Dispatchers.IO) {
            contactsData.postValue(ContactsData(contacts = contacts.filter {
                it.name.contains(typedText) || it.phoneNumber.contains(typedText) || it.username.contains(
                    typedText
                )
            }))
        }
    }
}
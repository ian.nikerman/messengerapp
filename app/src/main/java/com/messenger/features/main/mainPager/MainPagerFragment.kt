package com.messenger.features.main.mainPager

import android.content.Intent
import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.messenger.R
import com.messenger.databinding.FragmentMainPagerBinding
import com.messenger.features.main.MainViewModel
import com.messenger.features.main.common.activityContacts.NewGroupActivityContract
import com.messenger.features.main.common.adapters.ContentDataVPAdapter
import com.messenger.features.main.mainPager.calls.CallsFragment
import com.messenger.features.main.mainPager.chats.ChatsFragment
import com.messenger.features.main.mainPager.contacts.VPContactsFragment
import com.messenger.features.main.mainPager.favourites.FavouritesFragment
import com.messenger.features.main.settings.SettingsActivity
import com.messenger.features.views.HeaderView
import com.messenger.models.ContactData
import com.messenger.models.MainPagerType
import com.messenger.utils.Constants.Companion.REQUEST_NEW_GROUP_CONTRACT
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainPagerFragment : Fragment() {

    private val mainViewModel by activityViewModel<MainViewModel>()
    private val mainPagerViewModel: MainPagerViewModel by viewModel()
    private var binding: FragmentMainPagerBinding? = null

    companion object {

        private val TAG = MainPagerFragment::class.java.simpleName
        private lateinit var contentDataVPAdapter: ContentDataVPAdapter
    }

    private val headerListener = object : HeaderView.HeaderViewListener {

        override fun onMyProfileClick() {
            Timber.tag(TAG).d("onMyProfileClick")
            openSettings()
        }

        override fun onSearchClick() {
            Timber.tag(TAG).d("onSearchClick")
            mainViewModel.openSearch()
        }

        override fun onCancelSelectorClick() {
            Timber.tag(TAG).d("onCancelSelectorClick")
            when (mainPagerViewModel.getCurrentPageType()) {
                MainPagerType.CONTACTS -> {
                    mainPagerViewModel.resetSelectedContactsItems()
                }

                MainPagerType.CHATS -> {
                    mainPagerViewModel.resetSelectedChatsItems()
                }

                MainPagerType.FAVOURITES -> {
                    mainPagerViewModel.resetSelectedFavouritesItems()
                }

                MainPagerType.CALLS -> {
                    mainPagerViewModel.resetSelectedCallsItems()
                }
            }
        }

        override fun onPinChangeClick(isPinShown: Boolean) {
            Timber.tag(TAG).d("onPinChangeClick")
            when (mainPagerViewModel.getCurrentPageType()) {
                MainPagerType.CHATS -> {
                    mainPagerViewModel.changePinSelectedChats(isPinShown)
                }

                else -> {}
            }
        }

        override fun onMuteChangeClick(isMuteShown: Boolean) {
            Timber.tag(TAG).d("onMuteChangeClick")
            when (mainPagerViewModel.getCurrentPageType()) {
                MainPagerType.CHATS -> {
                    mainPagerViewModel.changeMuteSelectedChats(isMuteShown)
                }

                else -> {}
            }
        }

        override fun onDeleteClick() {
            Timber.tag(TAG).d("onDeleteClick")
            when (mainPagerViewModel.getCurrentPageType()) {
                MainPagerType.CHATS -> {
                    mainPagerViewModel.deleteSelectedChats()
                }

                MainPagerType.CALLS -> {
                    mainPagerViewModel.deleteSelectedCalls()
                }

                else -> {}
            }
        }

        override fun onMoreClick() {
            Timber.tag(TAG).d("onMoreClick")
            when (mainPagerViewModel.getCurrentPageType()) {
                MainPagerType.CONTACTS -> {
                    showContactsMenuOptions()
                }

                MainPagerType.CHATS -> {
                    showChatsMenuOptions()
                }

                MainPagerType.FAVOURITES -> {
                    showFavouritesMenuOptions()
                }

                MainPagerType.CALLS -> {
                    showCallsMenuOptions()
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainPagerBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(requireView(), savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
        initAdapter()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            headerView.setListener(headerListener)
        }
    }

    private fun initAdapter() {
        binding?.apply {
            val fragmentList = ArrayList<Fragment>().apply {
                addAll(
                    listOf(
                        VPContactsFragment(), ChatsFragment(), FavouritesFragment(), CallsFragment()
                    )
                )
            }

            val tabs = listOf(
                getString(R.string.contacts), getString(R.string.chats),
                getString(R.string.favourites), getString(R.string.calls)
            )

            viewPager.apply {
                contentDataVPAdapter = ContentDataVPAdapter(
                    fragmentList, childFragmentManager, lifecycle
                )
                adapter = contentDataVPAdapter
                TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                    tab.text = tabs[position]
                }.attach()

                registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                    override fun onPageScrollStateChanged(state: Int) {
                        when (state) {
                            ViewPager2.SCROLL_STATE_IDLE -> {
                                if (MainPagerType.values()
                                        .indexOf(mainPagerViewModel.currentPage) != viewPager.currentItem
                                ) {
                                    mainPagerViewModel.setCurrentPageType(viewPager.currentItem)
                                }
                            }

                            else -> {}
                        }
                    }
                })
            }
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        mainPagerViewModel.profileData.observe(viewLifecycleOwner) { data ->
            handleProfileData(data)
        }
        mainPagerViewModel.headerDetailsData.observe(viewLifecycleOwner) { data ->
            handleHeaderDetailsData(data)
        }
        mainPagerViewModel.pageTypeData.observe(viewLifecycleOwner) { data ->
            handlePageTypeData(data)
        }
    }

    private fun handlePageTypeData(data: MainPagerType?) {
        binding?.apply {
            data?.let {
                if (MainPagerType.values()
                        .indexOf(mainPagerViewModel.currentPage) != viewPager.currentItem
                ) {
                    viewPager.currentItem = MainPagerType.values().indexOf(data)
                }
                when (it) {
                    MainPagerType.CONTACTS -> {
                        fab.apply {
                            setImageResource(R.drawable.ic_add_contact)
                            setOnClickListener { mainViewModel.openNewContact(MainPagerFragment()) }
                        }
                    }

                    MainPagerType.CHATS -> {
                        fab.apply {
                            setImageResource(R.drawable.ic_write)
                            setOnClickListener { mainViewModel.openContacts(MainPagerFragment()) }
                        }
                    }

                    MainPagerType.FAVOURITES -> {
                        fab.apply {
                            setImageResource(R.drawable.ic_person_add_alt)
                            setOnClickListener { mainViewModel.openContacts(MainPagerFragment()) }
                        }
                    }

                    MainPagerType.CALLS -> {
                        fab.apply {
                            setImageResource(R.drawable.ic_call_white)
                            setOnClickListener { mainViewModel.openContacts(MainPagerFragment()) }
                        }
                    }
                }
            }
        }
    }

    private fun handleProfileData(data: ContactData?) {
        binding?.apply {
            data?.let {
                Timber.tag(TAG).d("handleProfileData")
                headerView.setHeaderMainData(it)
            }
        }
    }

    private fun handleHeaderDetailsData(data: MainPagerViewModel.HeaderDetails?) {
        binding?.apply {
            data?.let {
                Timber.tag(TAG).d("handleHeaderDetailsData")
                when (it) {
                    is MainPagerViewModel.HeaderDetails.ChatsHeaderData -> {
                        headerView.setSelectedChatsUi(
                            it.selectedCounterData, it.isMuteShown, it.isPinShown
                        )
                        checkHeaderBehavior(it.selectedCounterData)
                    }

                    is MainPagerViewModel.HeaderDetails.CallsHeaderData -> {
                        headerView.setSelectedCallsUi(
                            it.selectedCounterData
                        )
                        checkHeaderBehavior(it.selectedCounterData)
                    }
                }
            }
        }
    }

    private fun FragmentMainPagerBinding.checkHeaderBehavior(selectedCounterData: Int) {
        when (selectedCounterData) {
            0 -> {
                changeToolbarCollapsingState(false)
                appBarLayout.setBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(), R.color.colorPrimary
                    )
                )
                requireActivity().window.statusBarColor =
                    ContextCompat.getColor(requireContext(), R.color.colorPrimary)
                requireActivity().window.navigationBarColor =
                    ContextCompat.getColor(requireContext(), R.color.colorPrimary)
            }

            else -> {
                changeToolbarCollapsingState(true)
                appBarLayout.setBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(), R.color.colorPrimaryAlternative
                    )
                )
                requireActivity().window.statusBarColor =
                    ContextCompat.getColor(requireContext(), R.color.colorPrimaryAlternative)
                requireActivity().window.navigationBarColor =
                    ContextCompat.getColor(requireContext(), R.color.colorPrimaryAlternative)
            }
        }
    }

    private fun FragmentMainPagerBinding.changeToolbarCollapsingState(isSelectedState: Boolean) {
        val headerLayoutParams = headerCollapsingLayout.layoutParams as AppBarLayout.LayoutParams
        val tabLayoutParams = tabCollapsingLayout.layoutParams as AppBarLayout.LayoutParams
        if (isSelectedState) {
            headerLayoutParams.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
            tabLayoutParams.scrollFlags =
                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
        } else {
            headerLayoutParams.scrollFlags =
                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
            tabLayoutParams.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
        }
        headerCollapsingLayout.layoutParams = headerLayoutParams
        tabCollapsingLayout.layoutParams = tabLayoutParams
    }

    private fun showContactsMenuOptions() {
        binding?.headerView?.getMoreOptionsView()?.let { view ->
            Timber.tag(TAG).d("showChatsMenuOptions")
            PopupMenu(requireContext(), view).apply {
                menuInflater.inflate(R.menu.main_contacts_menu, menu)
                setOnMenuItemClickListener {
                    when (it.itemId) {
                        R.id.menu_refresh_contacts -> {
                            mainPagerViewModel.getContacts(requireContext(), true)
                        }

                        R.id.menu_new_group -> {
                            openNewGroupActivity.launch(REQUEST_NEW_GROUP_CONTRACT)
                        }

                        R.id.menu_invite_friends -> {

                        }

                        R.id.menu_settings -> {
                            openSettings()
                        }
                    }
                    true
                }
                show()
            }
        }
    }

    private fun showChatsMenuOptions() {
        binding?.headerView?.getMoreOptionsView()?.let { view ->
            Timber.tag(TAG).d("showChatsMenuOptions")
            PopupMenu(requireContext(), view).apply {
                menuInflater.inflate(R.menu.main_chats_menu, menu)
                setOnMenuItemClickListener {
                    when (it.itemId) {
                        R.id.menu_mark_all_read -> {

                        }

                        R.id.menu_new_group -> {
                            openNewGroupActivity.launch(REQUEST_NEW_GROUP_CONTRACT)
                        }

                        R.id.menu_invite_friends -> {

                        }

                        R.id.menu_settings -> {
                            openSettings()
                        }
                    }
                    true
                }
                show()
            }
        }
    }

    private fun showFavouritesMenuOptions() {
        binding?.headerView?.getMoreOptionsView()?.let { view ->
            Timber.tag(TAG).d("showChatsMenuOptions")
            PopupMenu(requireContext(), view).apply {
                menuInflater.inflate(R.menu.main_favourites_menu, menu)
                setOnMenuItemClickListener {
                    when (it.itemId) {
                        R.id.menu_mark_all_read -> {

                        }

                        R.id.menu_new_group -> {
                            openNewGroupActivity.launch(REQUEST_NEW_GROUP_CONTRACT)
                        }

                        R.id.menu_invite_friends -> {

                        }

                        R.id.menu_settings -> {
                            openSettings()
                        }
                    }
                    true
                }
                show()
            }
        }
    }

    private fun showCallsMenuOptions() {
        binding?.headerView?.getMoreOptionsView()?.let { view ->
            Timber.tag(TAG).d("showCallsMenuOptions")
            PopupMenu(requireContext(), view).apply {
                menuInflater.inflate(R.menu.main_calls_menu, menu)
                setOnMenuItemClickListener {
                    when (it.itemId) {
                        R.id.menu_delete_all_calls -> {
                            mainPagerViewModel.deleteAllCalls()
                        }

                        R.id.menu_new_group -> {
                            openNewGroupActivity.launch(REQUEST_NEW_GROUP_CONTRACT)
                        }

                        R.id.menu_invite_friends -> {

                        }

                        R.id.menu_settings -> {
                            openSettings()
                        }
                    }
                    true
                }
                show()
            }
        }
    }

    private fun openSettings() {
        Timber.tag(TAG).d("openSettings")
        startActivity(Intent(requireActivity(), SettingsActivity::class.java))
    }

    private val openNewGroupActivity =
        registerForActivityResult(NewGroupActivityContract()) { result ->
            Timber.tag(TAG).d("NewGroupActivityContract")
            result?.let {
                mainViewModel.openChat(MainPagerFragment(), it)
            }
        }

    override fun onResume() {
        super.onResume()
        Timber.tag(TAG).d("onResume")
        mainPagerViewModel.getProfile()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Timber.tag(TAG).d("onDestroyView")
        binding = null
    }
}

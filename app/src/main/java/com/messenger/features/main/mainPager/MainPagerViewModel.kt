package com.messenger.features.main.mainPager

import android.annotation.SuppressLint
import android.content.Context
import timber.log.Timber
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.messenger.data.CallsRepository
import com.messenger.data.ChatsRepository
import com.messenger.data.ContactsRepository
import com.messenger.data.ProfileRepository
import com.messenger.models.CallData
import com.messenger.models.ContactData
import com.messenger.models.ConversationData
import com.messenger.models.MainPagerType
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainPagerViewModel(
    private val profileRepository: ProfileRepository,
    private val chatsRepository: ChatsRepository,
    private val callsRepository: CallsRepository,
    private val contactsRepository: ContactsRepository
) : ViewModel() {

    companion object {

        private val TAG = MainPagerViewModel::class.java.simpleName
        private var profile: ContactData? = null
        private var contacts: List<ContactData> = ArrayList()
        private var chats: List<ConversationData> = ArrayList()
        private var calls: List<CallData> = ArrayList()
        private var favourites: List<ContactData> = ArrayList()
        private var selectedItems: List<Long> = ArrayList()

    }

    enum class ErrorEvent {
        ERROR_FETCH_PROFILE,
        ERROR_LOADING_DATA,
        ERROR_LOADING_CONTACTS,
        ERROR_LOADING_CHATS,
        ERROR_LOADING_CALLS,
        ERROR_LOADING_FAVOURITES,
        ERROR_UPDATE_CONTACT,
        ERROR_UPDATE_CHATS,
        ERROR_UPDATE_CALLS,
        ERROR_UPDATE_FAVOURITES
    }

    val errorEvent = LiveEventData<ErrorEvent>()

    enum class EmptyDataState {
        STATE_DATA, STATE_DATA_EMPTY, STATE_SEARCH_CONTACTS_EMPTY
    }

    sealed class HeaderDetails {
        data class ChatsHeaderData(
            val selectedCounterData: Int,
            val isPinShown: Boolean? = null,
            val isMuteShown: Boolean? = null
        ) : HeaderDetails()

        data class CallsHeaderData(
            val selectedCounterData: Int
        ) : HeaderDetails()
    }

    sealed class ListData {
        data class ContactsData(
            val data: List<ContactData>, val state: EmptyDataState
        )

        data class ChatsListData(
            val data: List<ConversationData>, val state: EmptyDataState
        ) : ListData()

        data class CallsListData(
            val data: List<CallData>, val state: EmptyDataState
        ) : ListData()

        data class FavouritesListData(
            val data: List<ContactData>, val state: EmptyDataState
        ) : ListData()
    }

    var currentPage = MainPagerType.CHATS

    val headerDetailsData = MutableLiveData<HeaderDetails>()
    val profileData = MutableLiveData<ContactData>()
    val pageTypeData = MutableLiveData<MainPagerType>()
    val contactsData = MutableLiveData<ListData.ContactsData>()
    val chatsData = MutableLiveData<ListData.ChatsListData>()
    val callsData = MutableLiveData<ListData.CallsListData>()
    val favouritesData = MutableLiveData<ListData.FavouritesListData>()

    val progressEvent = ProgressData()

    fun getCurrentPageType() = currentPage

    fun setCurrentPageType(type: Int) {
        currentPage = MainPagerType.values()[type]
        pageTypeData.postValue(currentPage)
    }

    // PROFILE

    fun getProfile() {
        Timber.tag(TAG).d("getProfile")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                profile = profileRepository.fetchProfile()
                handleProfileData(profile)
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_CHATS)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    private fun handleProfileData(profile: ContactData?) {
        profile?.let {
            profileData.postValue(it)
            pageTypeData.postValue(currentPage)
        }
    }

    // CONTACTS

    fun getContacts(
        context: Context, isUpdate: Boolean = false
    ) {
        Timber.tag(TAG).d("getContacts")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                if (contacts.isEmpty() || isUpdate) {
                    contacts = contactsRepository.fetchContacts(context, isUpdate)
                }
                updateContacts()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_CONTACTS)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    fun checkFavouriteContactItem(itemId: Long) {
        Timber.tag(TAG).d("checkFavouriteContactItem")
        viewModelScope.launch(Dispatchers.IO) {
            try {
                contacts = contactsRepository.checkFavouriteContact(itemId)
                checkSelectedContactItem()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_UPDATE_CONTACT)
            }
        }
    }

    private fun updateContacts() {
        Timber.tag(TAG).d("updateContacts")
        contactsData.postValue(
            ListData.ContactsData(
                data = contacts,
                state = if (contacts.isEmpty()) EmptyDataState.STATE_DATA_EMPTY else EmptyDataState.STATE_DATA
            )
        )
    }

    fun checkSelectedContactItem(itemId: Long? = null) {
        Timber.tag(TAG).d("checkSelectedContactItem")

        selectedItems = when {
            itemId == null -> {
                selectedItems
            }

            selectedItems.contains(itemId) -> {
                emptyList()
            }

            else -> {
                listOf(itemId)
            }
        }

        val updatedContacts = contacts.map { item ->
            item.copy(
                isSelected = selectedItems.contains(
                    item.id
                )
            )
        }

        contactsData.postValue(
            ListData.ContactsData(
                data = updatedContacts, state = EmptyDataState.STATE_DATA
            )
        )
    }

    fun querySearch(typedText: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val filteredItems = contacts.filter {
                it.name.contains(typedText) || it.phoneNumber.contains(typedText) || it.username.contains(
                    typedText
                )
            }
            contactsData.postValue(
                ListData.ContactsData(
                    data = filteredItems,
                    state = if (filteredItems.isEmpty()) EmptyDataState.STATE_SEARCH_CONTACTS_EMPTY else EmptyDataState.STATE_DATA
                )
            )
        }
    }

    fun resetSelectedContactsItems() {
        selectedItems = emptyList()
    }

    // CHATS

    fun getChats() {
        Timber.tag(TAG).d("getChats")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                profile = profileRepository.fetchProfile()
                handleProfileData(profile)
                chats = chatsRepository.fetchChats()
                resetSelectedChatsItems()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_CHATS)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    fun deleteSelectedChats() {
        Timber.tag(TAG).d("deleteSelectedChats")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                chats = chatsRepository.deleteChats(selectedItems)
                resetSelectedChatsItems()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_UPDATE_CHATS)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    fun changeMuteSelectedChats(isMuteShown: Boolean) {
        Timber.tag(TAG).d("changeMuteSelectedChats")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                chats = when (isMuteShown) {
                    true -> chatsRepository.muteChats(selectedItems)
                    false -> chatsRepository.unMuteChats(selectedItems)
                }
                resetSelectedChatsItems()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_UPDATE_CHATS)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    fun changePinSelectedChats(isPinShown: Boolean) {
        Timber.tag(TAG).d("changePinSelectedChats")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                chats = when (isPinShown) {
                    true -> chatsRepository.pinChats(selectedItems)
                    false -> chatsRepository.unpinChats(selectedItems)
                }
                resetSelectedChatsItems()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_UPDATE_CHATS)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    private fun updateChats() {
        Timber.tag(TAG).d("updateChats")
        chats = chats.sortedByDescending { it.isPinned }
        chatsData.postValue(
            ListData.ChatsListData(
                data = chats,
                state = if (chats.isEmpty()) EmptyDataState.STATE_DATA_EMPTY else EmptyDataState.STATE_DATA
            )
        )
    }

    fun isItemsSelected() = selectedItems.isNotEmpty()

    fun checkSelectedChatsItem(itemId: Long) {
        Timber.tag(TAG).d("checkSelectedChatsItem")

        selectedItems = if (selectedItems.contains(itemId)) {
            selectedItems.minus(itemId)
        } else {
            selectedItems.plus(itemId)
        }

        val updatedChats = chats.map { item ->
            item.copy(
                isSelected = selectedItems.contains(
                    item.id
                )
            )
        }.sortedByDescending { it.isPinned }

        val isMuteShown = updatedChats.filter { it.isSelected ?: false }.any { !it.isMute }
        val isPinShown = updatedChats.filter { it.isSelected ?: false }.any { !it.isPinned }

        headerDetailsData.postValue(
            HeaderDetails.ChatsHeaderData(
                selectedCounterData = selectedItems.size,
                isMuteShown = isMuteShown,
                isPinShown = isPinShown
            )
        )
        chatsData.postValue(
            ListData.ChatsListData(
                data = updatedChats, state = EmptyDataState.STATE_DATA
            )
        )
    }

    fun resetSelectedChatsItems() {
        Timber.tag(TAG).d("resetSelectedChatsItems")
        selectedItems = emptyList()
        headerDetailsData.postValue(
            HeaderDetails.ChatsHeaderData(
                selectedCounterData = selectedItems.size, isMuteShown = null, isPinShown = null
            )
        )
        updateChats()
    }

    // CALLS

    fun getCalls() {
        Timber.tag(TAG).d("getCalls")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                calls = callsRepository.fetchCalls()
                resetSelectedCallsItems()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_CALLS)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    fun deleteAllCalls() {
        Timber.tag(TAG).d("deleteAllCalls")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                calls = callsRepository.removeCalls()
                resetSelectedCallsItems()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_UPDATE_CALLS)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    fun deleteSelectedCalls() {
        Timber.tag(TAG).d("deleteSelectedCalls")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                calls = callsRepository.deleteCalls(selectedItems)
                resetSelectedCallsItems()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_UPDATE_CALLS)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    fun checkSelectedCallsItem(itemId: Long) {
        Timber.tag(TAG).d("checkSelectedCallsItem")

        selectedItems = if (selectedItems.contains(itemId)) {
            selectedItems.minus(itemId)
        } else {
            selectedItems.plus(itemId)
        }

        val updatedCalls = calls.map { item ->
            item.copy(
                isSelected = selectedItems.contains(
                    item.id
                )
            )
        }

        headerDetailsData.postValue(
            HeaderDetails.CallsHeaderData(
                selectedCounterData = selectedItems.size
            )
        )
        callsData.postValue(
            ListData.CallsListData(
                data = updatedCalls, state = EmptyDataState.STATE_DATA
            )
        )
    }

    fun resetSelectedCallsItems() {
        Timber.tag(TAG).d("resetSelectedCallsItems")
        selectedItems = emptyList()
        headerDetailsData.postValue(
            HeaderDetails.CallsHeaderData(
                selectedCounterData = selectedItems.size
            )
        )
        updateCalls()
    }

    private fun updateCalls() {
        Timber.tag(TAG).d("updateCalls")
        callsData.postValue(
            ListData.CallsListData(
                data = calls,
                state = if (calls.isEmpty()) EmptyDataState.STATE_DATA_EMPTY else EmptyDataState.STATE_DATA
            )
        )
    }

    // FAVOURITES

    fun getFavourites() {
        Timber.tag(TAG).d("getFavourites")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                favourites = contactsRepository.fetchFavourites()
                resetSelectedFavouritesItems()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_FAVOURITES)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    fun deleteAllFavourites() {
        Timber.tag(TAG).d("deleteAllFavourites")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                favourites = contactsRepository.removeFavourites()
                resetSelectedFavouritesItems()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_UPDATE_FAVOURITES)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    fun deleteSelectedFavourites() {
        Timber.tag(TAG).d("deleteSelectedFavourites")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                favourites = contactsRepository.deleteFavourites(selectedItems)
                resetSelectedFavouritesItems()
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_UPDATE_FAVOURITES)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    fun checkSelectedFavouritesItem(itemId: Long) {
        Timber.tag(TAG).d("checkSelectedFavouritesItem")

        selectedItems = if (selectedItems.contains(itemId)) {
            selectedItems.minus(itemId)
        } else {
            selectedItems.plus(itemId)
        }

        val updatedFavourites = favourites.map { item ->
            item.copy(
                isSelected = selectedItems.contains(
                    item.id
                )
            )
        }

        headerDetailsData.postValue(
            HeaderDetails.CallsHeaderData(
                selectedCounterData = selectedItems.size
            )
        )
        favouritesData.postValue(
            ListData.FavouritesListData(
                data = updatedFavourites, state = EmptyDataState.STATE_DATA
            )
        )
    }

    fun resetSelectedFavouritesItems() {
        Timber.tag(TAG).d("resetSelectedFavouritesItems")
        selectedItems = emptyList()
        headerDetailsData.postValue(
            HeaderDetails.CallsHeaderData(
                selectedCounterData = selectedItems.size
            )
        )
        updateFavourites()
    }

    private fun updateFavourites() {
        Timber.tag(TAG).d("updateFavourites")
        favouritesData.postValue(
            ListData.FavouritesListData(
                data = favourites,
                state = if (favourites.isEmpty()) EmptyDataState.STATE_DATA_EMPTY else EmptyDataState.STATE_DATA
            )
        )
    }
}
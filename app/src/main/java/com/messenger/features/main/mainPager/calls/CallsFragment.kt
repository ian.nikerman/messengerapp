package com.messenger.features.main.mainPager.calls

import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.messenger.R
import com.messenger.databinding.FragmentCallsBinding
import com.messenger.features.main.MainViewModel
import com.messenger.features.main.mainPager.MainPagerViewModel
import com.messenger.features.main.mainPager.calls.adapter.CallsAdapter
import com.messenger.models.CallData
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class CallsFragment : Fragment() {

    private val mainViewModel by activityViewModel<MainViewModel>()
    private val viewModel: MainPagerViewModel by viewModels({ requireParentFragment() })
    private var binding: FragmentCallsBinding? = null

    private companion object {

        private val TAG = CallsFragment::class.java.simpleName
        private lateinit var listAdapter: CallsAdapter

    }

    private val adapterListener = object : CallsAdapter.CallsListener {
        override fun onItemCallClick(data: CallData) {
            Timber.tag(TAG).d("onItemCallClick")
            mainViewModel.openCall(parentFragment, data.conversationData.id)
        }

        override fun onItemWriteClick(data: CallData) {
            Timber.tag(TAG).d("onItemWriteClick")
            mainViewModel.openChat(parentFragment, data.conversationData.id)
        }

        override fun onItemClick(data: CallData) {
            Timber.tag(TAG).d("onItemClick")
            if (!viewModel.isItemsSelected()) {
                mainViewModel.openCallDetails(parentFragment, data.conversationData.id)
            } else {
                viewModel.checkSelectedCallsItem(data.id)
            }
        }

        override fun onItemLongClick(data: CallData) {
            Timber.tag(TAG).d("onItemLongClick")
            viewModel.checkSelectedCallsItem(data.id)
        }
    }

    private val onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            if (viewModel.isItemsSelected()) {
                viewModel.resetSelectedCallsItems()
            } else {
                mainViewModel.finishApp()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCallsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(requireView(), savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
        initAdapter()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            swipeRefresh.setColorSchemeResources(
                R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark
            )
            swipeRefresh.setOnRefreshListener {
                viewModel.resetSelectedCallsItems()
                viewModel.getCalls()
                swipeRefresh.isRefreshing = false
            }
        }
    }

    private fun initAdapter() {
        Timber.tag(TAG).d("initAdapter")
        binding?.recyclerView?.apply {
            listAdapter = CallsAdapter(
                adapterListener
            )
            adapter = listAdapter
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.callsData.observe(viewLifecycleOwner) { data ->
            handleData(data)
        }
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
        viewModel.errorEvent.observe(viewLifecycleOwner) { event ->
            handleErrorEvent(event)
        }
    }

    private fun handleData(data: MainPagerViewModel.ListData.CallsListData?) {
        data?.let {
            Timber.tag(TAG).d("handleCallsData")
            handlesList(it.data)
            handleDataState(it.state)
        }
    }

    private fun handleDataState(state: MainPagerViewModel.EmptyDataState?) {
        state?.let {
            Timber.tag(TAG).d("handleCallsDataState")
            when (it) {
                MainPagerViewModel.EmptyDataState.STATE_DATA -> binding?.emptyDataView?.isVisible =
                    false

                MainPagerViewModel.EmptyDataState.STATE_DATA_EMPTY -> binding?.emptyDataView?.isVisible =
                    true

                else -> {}
            }
        }
    }

    private fun handlesList(data: List<CallData>) {
        Timber.tag(TAG).d("handleCallsList")
        listAdapter.submitList(data)
    }

    private fun handleErrorEvent(event: LiveEvent<MainPagerViewModel.ErrorEvent>?) {
        event?.getContentIfNotHandled()?.let {
            Timber.tag(TAG).d("handleErrorEvent: ${it.name}")
        }
    }

    private fun handleProgressEvent(isLoading: Boolean) {
        binding?.progress?.setProgressEvent(isLoading)
    }

    override fun onResume() {
        super.onResume()
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner, onBackPressedCallback
        )
        viewModel.getCalls()
    }

    override fun onPause() {
        super.onPause()
        viewModel.resetSelectedCallsItems()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}

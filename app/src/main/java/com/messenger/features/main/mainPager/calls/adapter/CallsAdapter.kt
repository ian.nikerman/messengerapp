package com.messenger.features.main.mainPager.calls.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.messenger.R
import com.messenger.databinding.ItemCallBinding
import com.messenger.models.CallData
import com.messenger.models.CallStatus
import com.messenger.utils.getColoredCircle
import com.messenger.utils.getNameInitials
import com.messenger.utils.showDrawable
import com.messenger.utils.showPlaceholderCircleImage
import com.messenger.utils.utcToLocalFullFormatted

class CallsAdapter(
    private val listener: CallsListener
) : ListAdapter<CallData, RecyclerView.ViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemCallBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as ViewHolder).bind(listener, item)
    }

    class ViewHolder(private val itemBinding: ItemCallBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(listener: CallsListener, data: CallData) {
            itemBinding.apply {
                setConversationType(data)
                checkAvatar(data)
                changeSelectedState(data.isSelected == true)

                callBtn.setOnClickListener { listener.onItemCallClick(data) }
                writeBtn.setOnClickListener { listener.onItemWriteClick(data) }
                callLayout.setOnClickListener { listener.onItemClick(data) }
                callLayout.setOnLongClickListener {
                    listener.onItemLongClick(data)
                    true
                }
                callTitle.text = data.conversationData.name

                data.createdAt.let {
                    createdAt.text = utcToLocalFullFormatted(it)
                }
            }
        }

        private fun ItemCallBinding.changeSelectedState(selected: Boolean) {
            selectorView.isVisible = selected
        }

        private fun ItemCallBinding.checkAvatar(data: CallData) {
            when (data.conversationData.icon) {
                null -> {
                    imageView.showDrawable(getColoredCircle(data.conversationData.color))
                    initials.text = data.conversationData.name.getNameInitials()
                    initials.isVisible = true
                }

                else -> {
                    imageView.showPlaceholderCircleImage(data.conversationData.icon)
                    initials.isVisible = false
                }
            }
        }

        private fun ItemCallBinding.setConversationType(data: CallData) {
            when (data.callStatus) {
                CallStatus.INCOMING_CALL -> callStatus.setImageResource(R.drawable.ic_call_received)
                CallStatus.INCOMING_CALL_CANCELLED -> callStatus.setImageResource(R.drawable.ic_call_missed)
                CallStatus.INCOMING_CALL_MISSED -> callStatus.setImageResource(R.drawable.ic_call_missed)
                CallStatus.OUTGOING_CALL -> callStatus.setImageResource(R.drawable.ic_call_made)
                CallStatus.OUTGOING_CALL_CANCELLED -> callStatus.setImageResource(R.drawable.ic_call_missed_outgoing)
                CallStatus.OUTGOING_CALL_MISSED -> callStatus.setImageResource(R.drawable.ic_call_missed_outgoing)
            }
        }
    }

    interface CallsListener {
        fun onItemWriteClick(data: CallData)
        fun onItemCallClick(data: CallData)
        fun onItemClick(data: CallData)
        fun onItemLongClick(data: CallData)
    }
}

private class DiffCallback : DiffUtil.ItemCallback<CallData>() {
    override fun areItemsTheSame(oldItem: CallData, newItem: CallData): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: CallData, newItem: CallData
    ): Boolean {
        return oldItem == newItem
    }
}
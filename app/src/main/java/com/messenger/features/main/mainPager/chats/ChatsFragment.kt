package com.messenger.features.main.mainPager.chats

import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.messenger.R
import com.messenger.databinding.FragmentChatsBinding
import com.messenger.features.main.MainViewModel
import com.messenger.features.main.mainPager.MainPagerViewModel
import com.messenger.features.main.mainPager.chats.adapter.ChatsAdapter
import com.messenger.models.ConversationData
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class ChatsFragment : Fragment() {

    private val mainViewModel by activityViewModel<MainViewModel>()
    private val viewModel: MainPagerViewModel by viewModels({ requireParentFragment() })
    private var binding: FragmentChatsBinding? = null

    private companion object {

        private val TAG = ChatsFragment::class.java.simpleName
        private lateinit var listAdapter: ChatsAdapter

    }

    private val adapterListener = object : ChatsAdapter.ChatsListener {
        override fun onItemClick(itemId: Long) {
            Timber.tag(TAG).d("onItemClick")
            if (!viewModel.isItemsSelected()) {
                mainViewModel.openChat(parentFragment, itemId)
            } else {
                viewModel.checkSelectedChatsItem(itemId)
            }
        }

        override fun onItemLongClick(itemId: Long) {
            Timber.tag(TAG).d("onItemLongClick")
            viewModel.checkSelectedChatsItem(itemId)
        }
    }

    private val onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            if (viewModel.isItemsSelected()) {
                viewModel.resetSelectedChatsItems()
            } else {
                mainViewModel.finishApp()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(requireView(), savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
        initAdapter()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            swipeRefresh.setColorSchemeResources(
                R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark
            )
            swipeRefresh.setOnRefreshListener {
                viewModel.resetSelectedChatsItems()
                viewModel.getChats()
                swipeRefresh.isRefreshing = false
            }
        }
    }

    private fun initAdapter() {
        Timber.tag(TAG).d("initAdapter")
        binding?.recyclerView?.apply {
            listAdapter = ChatsAdapter(
                adapterListener
            )
            adapter = listAdapter
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.chatsData.observe(viewLifecycleOwner) { data ->
            handleChatsData(data)
        }
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
        viewModel.errorEvent.observe(viewLifecycleOwner) { event ->
            handleErrorEvent(event)
        }
    }

    private fun handleChatsData(data: MainPagerViewModel.ListData.ChatsListData?) {
        data?.let {
            Timber.tag(TAG).d("handleChatsData")
            handleChatsList(it.data)
            handleChatsDataState(it.state)
        }
    }

    private fun handleChatsDataState(state: MainPagerViewModel.EmptyDataState?) {
        state?.let {
            Timber.tag(TAG).d("handleChatsDataState")
            when (it) {
                MainPagerViewModel.EmptyDataState.STATE_DATA -> binding?.emptyDataView?.isVisible =
                    false

                MainPagerViewModel.EmptyDataState.STATE_DATA_EMPTY -> binding?.emptyDataView?.isVisible =
                    true

                else -> {}
            }
        }
    }

    private fun handleChatsList(data: List<ConversationData>) {
        Timber.tag(TAG).d("handleChatsList")
        listAdapter.submitList(data)
    }

    private fun handleErrorEvent(event: LiveEvent<MainPagerViewModel.ErrorEvent>?) {
        event?.getContentIfNotHandled()?.let {

        }
    }

    private fun handleProgressEvent(isLoading: Boolean) {
        binding?.progress?.setProgressEvent(isLoading)
    }

    override fun onResume() {
        super.onResume()
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner, onBackPressedCallback
        )
        viewModel.getChats()
    }

    override fun onPause() {
        super.onPause()
        viewModel.resetSelectedChatsItems()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}

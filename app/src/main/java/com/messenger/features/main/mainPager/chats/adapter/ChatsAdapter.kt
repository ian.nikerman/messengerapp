package com.messenger.features.main.mainPager.chats.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.messenger.R
import com.messenger.databinding.ItemChatBinding
import com.messenger.models.ConversationData
import com.messenger.models.ConversationType
import com.messenger.utils.getColoredCircle
import com.messenger.utils.getNameInitials
import com.messenger.utils.showDrawable
import com.messenger.utils.showPlaceholderCircleImage
import com.messenger.utils.utcToLocalFormatted

class ChatsAdapter(
    private val listener: ChatsListener
) : ListAdapter<ConversationData, RecyclerView.ViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemChatBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as ViewHolder).bind(listener, item)
    }

    class ViewHolder(private val itemBinding: ItemChatBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(listener: ChatsListener, data: ConversationData) {
            itemBinding.apply {
                setConversationType(data)
                checkAvatar(data)
                changeSelectedState(data.isSelected == true)

                chatLayout.setOnClickListener { listener.onItemClick(data.id) }
                chatLayout.setOnLongClickListener {
                    listener.onItemLongClick(data.id)
                    true
                }
                mutedView.isVisible = data.isMute
                pinnedView.isVisible = data.isPinned
                chatTitle.text = data.name

                data.updatedAt.let {
                    updatedAt.text = utcToLocalFormatted(it)
                }
                data.unreadMessages?.let {
                    if (data.unreadMessages != 0) {
                        unreadCount.visibility = View.VISIBLE
                    } else {
                        unreadCount.visibility = View.INVISIBLE
                    }
                    unreadCount.text = it.toString()
                }
            }
        }

        private fun ItemChatBinding.changeSelectedState(selected: Boolean) {
            selectorView.isVisible = selected
        }

        private fun ItemChatBinding.checkAvatar(data: ConversationData) {
            when (data.icon) {
                null -> {
                    imageView.showDrawable(getColoredCircle(data.color))
                    initials.text = data.name.getNameInitials()
                    initials.isVisible = true
                }

                else -> {
                    imageView.showPlaceholderCircleImage(data.icon)
                    initials.isVisible = false
                }
            }
        }

        private fun ItemChatBinding.setConversationType(data: ConversationData) {
            val type = when (data.type) {
                ConversationType.TYPE_ENCRYPTED_CONVERSATION -> chatSubTitle.context.getString(R.string.item_conversation_type_encrypted_conversation)
                ConversationType.TYPE_SECURE_GROUP -> chatSubTitle.context.getString(R.string.item_conversation_type_secure_group)
            }
            chatSubTitle.text = type
        }
    }

    interface ChatsListener {
        fun onItemClick(itemId: Long)
        fun onItemLongClick(itemId: Long)
    }
}

private class DiffCallback : DiffUtil.ItemCallback<ConversationData>() {
    override fun areItemsTheSame(oldItem: ConversationData, newItem: ConversationData): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: ConversationData, newItem: ConversationData
    ): Boolean {
        return oldItem == newItem
    }
}
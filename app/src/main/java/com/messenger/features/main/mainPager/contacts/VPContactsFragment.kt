package com.messenger.features.main.mainPager.contacts

import android.Manifest
import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.messenger.R
import com.messenger.databinding.FragmentVpContactsBinding
import com.messenger.features.main.MainViewModel
import com.messenger.features.main.common.activityContacts.NewGroupActivityContract
import com.messenger.features.main.mainPager.MainPagerViewModel
import com.messenger.features.main.mainPager.contacts.adapter.ContactsVPAdapter
import com.messenger.features.views.EmptyDataView
import com.messenger.models.ContactData
import com.messenger.utils.Constants.Companion.REQUEST_NEW_GROUP_CONTRACT
import com.messenger.utils.extensions.LiveEvent
import com.messenger.utils.extensions.RecyclerViewTopScrollListener
import com.messenger.utils.hasPermission
import com.messenger.utils.isContactsPermissionDenied
import com.messenger.utils.showContactsRationaleDialog
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import kotlin.math.abs

class VPContactsFragment : Fragment() {

    private val mainViewModel by activityViewModel<MainViewModel>()
    private val viewModel: MainPagerViewModel by viewModels({ requireParentFragment() })
    private var binding: FragmentVpContactsBinding? = null

    companion object {
        private val TAG = VPContactsFragment::class.java.simpleName
        private lateinit var listAdapter: ContactsVPAdapter
        private lateinit var linearLayoutManager: LinearLayoutManager
    }

    private val permissionContactsRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            when {
                granted -> checkContactsPermissions()
            }
        }

    private val adapterListener = object : ContactsVPAdapter.ContactsVPListener {
        override fun onItemClick(itemId: Long) {
            Timber.tag(TAG).d("onItemClick")
            viewModel.checkSelectedContactItem(itemId)
        }

        override fun onItemCallClick(itemId: Long) {
            Timber.tag(TAG).d("onItemCallClick")
            mainViewModel.openCall(parentFragment, itemId)
        }

        override fun onItemWriteClick(itemId: Long) {
            Timber.tag(TAG).d("onItemWriteClick")
            mainViewModel.openChat(parentFragment, itemId)
        }

        override fun onItemInfoClick(data: ContactData) {
            Timber.tag(TAG).d("onItemClick")
            mainViewModel.openCallDetails(parentFragment, data.id)
        }

        override fun onItemFavouriteClick(itemId: Long) {
            Timber.tag(TAG).d("onItemFavouriteClick")
            viewModel.checkFavouriteContactItem(itemId)
        }
    }

    private val emptyDataViewListener = object : EmptyDataView.EmptyDataViewListener {
        override fun onEmptyDataViewClick() {
            Timber.tag(TAG).d("onEmptyDataViewClick")
            permissionContactsRequest.launch(Manifest.permission.READ_CONTACTS)
        }
    }

    private val onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            if (viewModel.isItemsSelected()) {
                viewModel.resetSelectedContactsItems()
            } else {
                mainViewModel.finishApp()
            }
        }
    }

    private fun getScrollTopListener(): RecyclerViewTopScrollListener {
        return object : RecyclerViewTopScrollListener(linearLayoutManager) {

            override fun changeScrolledFromTopState(
                totalScrollRange: Int, verticalOffset: Int
            ) {
                setHeaderAlpha(totalScrollRange, verticalOffset)
            }
        }
    }

    private fun setHeaderAlpha(totalScrollRange: Int, verticalOffset: Int) {
        binding?.apply {
            val offsetVertical = (abs(totalScrollRange) * 0.1 / verticalOffset).toFloat()
            headerLayout.alpha = offsetVertical
            when {
                offsetVertical <= 0.2 && offsetVertical != 0f -> {
                    headerLayout.isVisible = false
                }

                else -> {
                    headerLayout.isVisible = true
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVpContactsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            emptyDataView.setListener(emptyDataViewListener)
            initAdapter()

            swipeRefresh.setColorSchemeResources(
                R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark
            )
            swipeRefresh.setOnRefreshListener {
                viewModel.resetSelectedContactsItems()
                viewModel.getContacts(requireContext(), true)
                swipeRefresh.isRefreshing = false
            }

            createGroupItem.setOnClickListener {
                openNewGroupActivity.launch(
                    REQUEST_NEW_GROUP_CONTRACT
                )
            }
        }
    }

    private fun initObservers() {
        viewModel.contactsData.observe(viewLifecycleOwner) { data ->
            handleContactsData(data)
        }
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
        viewModel.errorEvent.observe(viewLifecycleOwner) { event ->
            handleErrorEvent(event)
        }
    }

    private fun handleContactsData(data: MainPagerViewModel.ListData.ContactsData?) {
        data?.let {
            handleData(it.data)
            handleState(it.state)
        }
    }

    private fun handleData(data: List<ContactData>) {
        listAdapter.submitList(data)
    }

    private fun handleState(state: MainPagerViewModel.EmptyDataState?) {
        state?.let {
            when (it) {
                MainPagerViewModel.EmptyDataState.STATE_DATA -> {
                    binding?.emptyDataView?.isVisible = false
                    binding?.emptyDataView?.hideEmptyResults()
                }

                MainPagerViewModel.EmptyDataState.STATE_DATA_EMPTY -> {
                    binding?.emptyDataView?.hideEmptyResults()
                    binding?.emptyDataView?.isVisible = true
                }

                MainPagerViewModel.EmptyDataState.STATE_SEARCH_CONTACTS_EMPTY -> {
                    binding?.emptyDataView?.showEmptyResults()
                    binding?.emptyDataView?.isVisible = true
                }
            }
        }
    }

    private fun handleProgressEvent(isLoading: Boolean?) {
        binding?.progress?.setProgressEvent(isLoading ?: false)
    }

    private fun handleErrorEvent(event: LiveEvent<MainPagerViewModel.ErrorEvent>?) {
        event?.getContentIfNotHandled()?.let {

        }
    }

    private fun checkContactsPermissions() {
        when {
            requireContext().hasPermission(Manifest.permission.READ_CONTACTS) -> {
                binding?.emptyDataView?.isVisible = false
                viewModel.getContacts(
                    requireContext()
                )
            }

            requireActivity().isContactsPermissionDenied() -> {
                binding?.emptyDataView?.isVisible = true
                showContactsRationaleDialog(
                    requireContext(), permissionContactsRequest
                )
            }

            else -> {
                binding?.emptyDataView?.isVisible = true
                permissionContactsRequest.launch(Manifest.permission.READ_CONTACTS)
            }
        }
    }

    private fun FragmentVpContactsBinding.initAdapter() {
        recyclerView.apply {
            listAdapter = ContactsVPAdapter(adapterListener)
            linearLayoutManager = LinearLayoutManager(requireContext())
            layoutManager = linearLayoutManager
            adapter = listAdapter
            addOnScrollListener(getScrollTopListener())
        }
    }

    private val openNewGroupActivity =
        registerForActivityResult(NewGroupActivityContract()) { result ->
            Timber.tag(TAG).d("NewGroupActivityContract result: $result")
            result?.let {
                mainViewModel.openChat(VPContactsFragment(), it)
            }
        }

    override fun onResume() {
        super.onResume()
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner, onBackPressedCallback
        )
        checkContactsPermissions()
    }

    override fun onPause() {
        super.onPause()
        viewModel.resetSelectedContactsItems()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}



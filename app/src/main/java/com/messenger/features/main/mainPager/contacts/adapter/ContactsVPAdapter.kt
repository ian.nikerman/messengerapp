package com.messenger.features.main.mainPager.contacts.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.messenger.R
import com.messenger.databinding.ItemContactVpBinding
import com.messenger.models.ContactData
import com.messenger.utils.getColoredCircle
import com.messenger.utils.getNameInitials
import com.messenger.utils.showDrawable
import com.messenger.utils.showPlaceholderCircleImage

class ContactsVPAdapter(
    private val listener: ContactsVPListener
) : ListAdapter<ContactData, RecyclerView.ViewHolder>(ContactsVPDiffCallback()) {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): ViewHolder {
        val itemBinding =
            ItemContactVpBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as ViewHolder).bind(
            listener, item
        )
    }

    class ViewHolder(private val itemBinding: ItemContactVpBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(
            listener: ContactsVPListener, data: ContactData
        ) {
            itemBinding.apply {
                changeExpandableMode(false)

                checkAvatar(data)
                title.text = data.name
                subTitle.text = data.phoneNumber.firstOrNull() ?: ""

                checkFavouriteState(data.isFavourite)
                callBtn.setOnClickListener { listener.onItemCallClick(data.id) }
                infoBtn.setOnClickListener { listener.onItemInfoClick(data) }
                writeBtn.setOnClickListener { listener.onItemWriteClick(data.id) }
                favouriteBtn.setOnClickListener { listener.onItemFavouriteClick(data.id) }

                layout.setOnClickListener {
                    changeExpandableMode(!itemExpandableLayout.isVisible)
//                    listener.onItemClick(data.id)
                }
            }
        }

        private fun ItemContactVpBinding.checkFavouriteState(favourite: Boolean) {
            when (favourite) {
                true -> {
                    favouriteIcon.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null,
                        ContextCompat.getDrawable(root.context, R.drawable.ic_fave_selected),
                        null,
                        null
                    )
                }

                else -> {
                    favouriteIcon.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null,
                        ContextCompat.getDrawable(root.context, R.drawable.ic_fave_unselected),
                        null,
                        null
                    )
                }
            }
        }

        private fun ItemContactVpBinding.changeExpandableMode(selected: Boolean) {
            layout.isSelected = selected
            itemExpandableLayout.isVisible = selected
        }

        private fun ItemContactVpBinding.checkAvatar(data: ContactData) {
            when (data.avatar) {
                null -> {
                    imageView.showDrawable(getColoredCircle(data.color))
                    initials.text = data.name.getNameInitials()
                    initials.isVisible = true
                }

                else -> {
                    imageView.showPlaceholderCircleImage(data.avatar)
                    initials.isVisible = false
                }
            }
        }
    }

    interface ContactsVPListener {
        fun onItemInfoClick(data: ContactData)
        fun onItemClick(itemId: Long)
        fun onItemCallClick(itemId: Long)
        fun onItemWriteClick(itemId: Long)
        fun onItemFavouriteClick(itemId: Long)
    }
}

private class ContactsVPDiffCallback : DiffUtil.ItemCallback<ContactData>() {
    override fun areItemsTheSame(oldItem: ContactData, newItem: ContactData): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: ContactData, newItem: ContactData
    ): Boolean {
        return oldItem == newItem
    }
}
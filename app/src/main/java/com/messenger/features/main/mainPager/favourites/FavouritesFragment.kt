package com.messenger.features.main.mainPager.favourites

import android.Manifest
import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.messenger.R
import com.messenger.databinding.FragmentVpFavouritesBinding
import com.messenger.features.main.MainViewModel
import com.messenger.features.main.mainPager.MainPagerViewModel
import com.messenger.features.main.mainPager.favourites.adapter.FavouritesAdapter
import com.messenger.features.views.EmptyDataView
import com.messenger.models.ContactData
import com.messenger.utils.extensions.LiveEvent
import com.messenger.utils.hasPermission
import com.messenger.utils.isContactsPermissionDenied
import com.messenger.utils.showContactsRationaleDialog
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class FavouritesFragment : Fragment() {

    private val mainViewModel by activityViewModel<MainViewModel>()
    private val viewModel: MainPagerViewModel by viewModels({ requireParentFragment() })
    private var binding: FragmentVpFavouritesBinding? = null

    companion object {
        private val TAG = FavouritesFragment::class.java.simpleName
        private lateinit var listAdapter: FavouritesAdapter
        private const val GALLERY_LIST_ROWS = 2
    }

    private val permissionContactsRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            when {
                granted -> checkContactsPermissions()
            }
        }

    private val adapterListener = object : FavouritesAdapter.ChatsListener {
        override fun onItemClick(itemId: Long) {
            Timber.tag(TAG).d("onItemClick")
            if (!viewModel.isItemsSelected()) {
                mainViewModel.openChat(parentFragment, itemId)
            } else {
                viewModel.checkSelectedFavouritesItem(itemId)
            }
        }

        override fun onItemOptionsClick(itemId: Long) {
            Timber.tag(TAG).d("onOptionsClick")

        }

        override fun onItemFavouriteClick(itemId: Long) {
            Timber.tag(TAG).d("onFavouriteClick")

        }

        override fun onItemLongClick(itemId: Long) {
            Timber.tag(TAG).d("onItemLongClick")
            viewModel.checkSelectedFavouritesItem(itemId)
        }
    }

    private val emptyDataViewListener = object : EmptyDataView.EmptyDataViewListener {
        override fun onEmptyDataViewClick() {
            Timber.tag(TAG).d("onEmptyDataViewClick")
            permissionContactsRequest.launch(Manifest.permission.READ_CONTACTS)
        }
    }

    private val onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            if (viewModel.isItemsSelected()) {
                viewModel.resetSelectedFavouritesItems()
            } else {
                mainViewModel.finishApp()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVpFavouritesBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            emptyDataView.setListener(emptyDataViewListener)
            initAdapter()

            swipeRefresh.setColorSchemeResources(
                R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark
            )
            swipeRefresh.setOnRefreshListener {
                viewModel.resetSelectedFavouritesItems()
                viewModel.getFavourites()
                swipeRefresh.isRefreshing = false
            }
        }
    }

    private fun initObservers() {
        viewModel.favouritesData.observe(viewLifecycleOwner) { data ->
            handleData(data)
        }
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
        viewModel.errorEvent.observe(viewLifecycleOwner) { event ->
            handleErrorEvent(event)
        }
    }

    private fun handleData(data: MainPagerViewModel.ListData.FavouritesListData?) {
        data?.let {
            Timber.tag(TAG).d("handleChatsData")
            handleList(it.data)
            handleDataState(it.state)
        }
    }

    private fun handleDataState(state: MainPagerViewModel.EmptyDataState?) {
        state?.let {
            Timber.tag(TAG).d("handleChatsDataState")
            when (it) {
                MainPagerViewModel.EmptyDataState.STATE_DATA -> binding?.emptyDataView?.isVisible =
                    false

                MainPagerViewModel.EmptyDataState.STATE_DATA_EMPTY -> binding?.emptyDataView?.isVisible =
                    true

                else -> {}
            }
        }
    }

    private fun handleList(data: List<ContactData>) {
        Timber.tag(TAG).d("handleChatsList")
        listAdapter.submitList(data)
    }

    private fun handleProgressEvent(isLoading: Boolean?) {
        binding?.progress?.setProgressEvent(isLoading ?: false)
    }

    private fun handleErrorEvent(event: LiveEvent<MainPagerViewModel.ErrorEvent>?) {
        event?.getContentIfNotHandled()?.let {

        }
    }

    private fun FragmentVpFavouritesBinding.initAdapter() {
        recyclerView.apply {
            listAdapter = FavouritesAdapter(adapterListener)
            layoutManager = GridLayoutManager(context, GALLERY_LIST_ROWS)
            adapter = listAdapter
        }
    }

    private fun checkContactsPermissions() {
        when {
            requireContext().hasPermission(Manifest.permission.READ_CONTACTS) -> {
                viewModel.getFavourites()
            }

            requireActivity().isContactsPermissionDenied() -> {
                showContactsRationaleDialog(
                    requireContext(), permissionContactsRequest
                )
            }

            else -> {
                permissionContactsRequest.launch(Manifest.permission.READ_CONTACTS)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner, onBackPressedCallback
        )
        checkContactsPermissions()
    }

    override fun onPause() {
        super.onPause()
        viewModel.resetSelectedFavouritesItems()
    }

    override fun onDestroyView() {
        viewModel.resetSelectedFavouritesItems()
        super.onDestroyView()
        binding = null
    }
}



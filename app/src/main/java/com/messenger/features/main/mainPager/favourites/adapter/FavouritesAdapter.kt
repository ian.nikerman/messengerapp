package com.messenger.features.main.mainPager.favourites.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.messenger.databinding.ItemFavouriteBinding
import com.messenger.models.ContactData
import com.messenger.utils.getNameInitials

class FavouritesAdapter(
    private val listener: ChatsListener
) : ListAdapter<ContactData, RecyclerView.ViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemFavouriteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as ViewHolder).bind(listener, item)
    }

    class ViewHolder(private val itemBinding: ItemFavouriteBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(listener: ChatsListener, data: ContactData) {
            itemBinding.apply {
                itemLayout.setOnClickListener { listener.onItemClick(data.id) }
                optionsButton.setOnClickListener { listener.onItemOptionsClick(data.id) }
                favouriteButton.setOnClickListener { listener.onItemFavouriteClick(data.id) }
//                chatLayout.setOnLongClickListener {
//                    listener.onItemLongClick(data.id)
//                    true
//                }
                initials.text = data.name.getNameInitials()
                name.text = data.name
                phoneType.text = data.phoneType.name
                imageView.setBackgroundColor(data.color)
            }
        }
    }

    interface ChatsListener {
        fun onItemClick(itemId: Long)
        fun onItemOptionsClick(itemId: Long)
        fun onItemFavouriteClick(itemId: Long)
        fun onItemLongClick(itemId: Long)
    }
}

private class DiffCallback : DiffUtil.ItemCallback<ContactData>() {
    override fun areItemsTheSame(oldItem: ContactData, newItem: ContactData): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: ContactData, newItem: ContactData
    ): Boolean {
        return oldItem == newItem
    }
}
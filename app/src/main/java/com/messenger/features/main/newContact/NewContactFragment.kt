package com.messenger.features.main.newContact

import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.messenger.databinding.FragmentNewContactBinding
import com.messenger.features.main.MainViewModel
import com.messenger.features.views.HeaderView
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewContactFragment : Fragment() {

    private val sharedViewModel by activityViewModel<MainViewModel>()
    private val viewModel: NewContactViewModel by viewModel()
    private var binding: FragmentNewContactBinding? = null

    companion object {

        private val TAG = NewContactFragment::class.java.simpleName

    }

    private val headerListener = object : HeaderView.HeaderViewListener {

        override fun onBackClick() {
            Timber.tag(TAG).d("onBackClick")
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNewContactBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            headerView.setListener(headerListener)
        }
    }

    private fun initObservers() {
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
        viewModel.errorEvent.observe(viewLifecycleOwner) { event ->
            handleErrorEvent(event)
        }
    }

    private fun handleProgressEvent(isLoading: Boolean) {
        binding?.progress?.setProgressEvent(isLoading)
        binding?.confirmButton?.isEnabled = isLoading == false
    }

    private fun handleErrorEvent(event: LiveEvent<NewContactViewModel.ErrorEvent>?) {
        event?.getContentIfNotHandled()?.let {

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}

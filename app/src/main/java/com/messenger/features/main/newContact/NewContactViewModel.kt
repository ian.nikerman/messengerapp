package com.messenger.features.main.newContact

import android.Manifest
import android.content.Context
import timber.log.Timber
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.messenger.data.ContactsRepository
import com.messenger.models.ContactData
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import com.messenger.utils.hasPermission
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewContactViewModel(private val contactsRepository: ContactsRepository) : ViewModel() {

    companion object {

        private val TAG = NewContactViewModel::class.java.simpleName
        private var contacts: List<ContactData> = ArrayList()

    }

    enum class ErrorEvent {
        ERROR_LOADING_DATA
    }

    val errorEvent = LiveEventData<ErrorEvent>()

    data class ContactsData(
        val contacts: List<ContactData>
    )

    val contactsData = MutableLiveData<ContactsData?>()

    val progressEvent = ProgressData()

    fun getContacts(
        context: Context
    ) {
        Timber.tag(TAG).d("getContacts")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                if (contacts.isEmpty()) {
                    if (context.hasPermission(Manifest.permission.READ_CONTACTS)
                    ) {
                        contactsData.postValue(ContactsData(contacts = contacts))
                    }
//                    contacts = context.retrieveAllContacts()
                }
                contactsData.postValue(ContactsData(contacts = contacts))
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_DATA)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    fun querySearch(typedText: String) {
        contactsData.postValue(ContactsData(contacts = contacts.filter {
            it.name.contains(typedText) || it.phoneNumber.contains(
                typedText
            )
        }))
    }
}
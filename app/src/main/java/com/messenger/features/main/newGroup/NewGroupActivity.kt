package com.messenger.features.main.newGroup

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import timber.log.Timber
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import com.messenger.R
import com.messenger.databinding.ActivityNewGroupBinding
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewGroupActivity : AppCompatActivity() {

    private val viewModel: NewGroupViewModel by viewModel()

    companion object {
        private val TAG = NewGroupActivity::class.java.simpleName
        private lateinit var binding: ActivityNewGroupBinding
        const val GROUP_ID = "id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNewGroupBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Timber.tag(TAG).d("onCreate")
        initObservers()
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")

        viewModel.navigationEvent.observe(this) { event ->
            handleNavigationEvent(event)
        }
        viewModel.resultData.observe(this) { data ->
            handleResultData(data)
        }
        viewModel.progressEvent.observe(this) { event ->
            handleProgressEvent(event)
        }
        viewModel.errorEvent.observe(this) { event ->
            handleErrorEvent(event)
        }
    }

    private fun handleResultData(data: LiveEvent<Long?>?) {
        data?.getContentIfNotHandled()?.let {
            val intent = Intent().apply {
                putExtra(GROUP_ID, it)
            }
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    private fun handleNavigationEvent(event: LiveEvent<NavDirections?>?) {
        event?.getContentIfNotHandled()?.let {
            Timber.tag(TAG).d("handleNavigationEvent")
            try {
                findNavController(R.id.navigationContainer).navigate(it)
            } catch (ex: java.lang.Exception) {
                Timber.tag(TAG).d("handleNavigationEvent Error: ${ex.message}")
            }
        }
    }

    private fun handleProgressEvent(isLoading: Boolean?) {
        binding.progress.setProgressEvent(isLoading ?: false)
    }

    private fun handleErrorEvent(event: LiveEvent<NewGroupViewModel.ErrorEvent>?) {
        event?.getContentIfNotHandled()?.let {

        }
    }
}
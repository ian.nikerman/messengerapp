package com.messenger.features.main.newGroup

import android.net.Uri
import timber.log.Timber
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.messenger.data.ChatsRepository
import com.messenger.data.ProfileRepository
import com.messenger.features.main.newGroup.groupMembers.GroupMembersFragmentDirections
import com.messenger.models.ContactData
import com.messenger.models.GroupRequest
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewGroupViewModel(
    private val chatsRepository: ChatsRepository,
    private val profileRepository: ProfileRepository
) : ViewModel() {

    companion object {
        private val TAG = NewGroupViewModel::class.java.simpleName
    }

    enum class ErrorEvent {
        ERROR_CREATE_GROUP
    }

    val errorEvent = LiveEventData<ErrorEvent>()

    val navigationEvent = LiveEventData<NavDirections?>()

    val resultData = LiveEventData<Long?>()

    var groupData: GroupRequest? = null
    var groupDetailsData = MutableLiveData<GroupRequest?>()

    val progressEvent = ProgressData()

    init {
        groupData = (createEmptyTreasure())
    }

    private fun openGroupName() {
        Timber.tag(TAG).d("openGroupName")
        navigationEvent.postRawValue(
            GroupMembersFragmentDirections.actionGroupMembersFragmentToGroupNameFragment()
        )
    }

    fun setSelectedGroupContacts(selectedContacts: List<ContactData>) {
        groupData = groupData?.copy(members = selectedContacts)
        groupDetailsData.postValue(groupData)
        openGroupName()
    }

    fun setName(name: String, description: String, icon: Uri?) {
        groupData = groupData?.copy(
            name = name, description = description, icon = icon
        )
        groupDetailsData.postValue(groupData)
        createGroup()
    }

    private fun createGroup() {
        Timber.tag(TAG).d("createGroup")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                groupData?.let {
                    val profile = profileRepository.fetchProfile()
                    val roomId = chatsRepository.createGroup(it.copy(admin = profile))
                    resultData.postRawValue(roomId)
                }
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_CREATE_GROUP)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    private fun createEmptyTreasure(): GroupRequest {
        return GroupRequest(
            name = "", description = "", admin = null, icon = Uri.EMPTY, members = emptyList()
        )
    }
}
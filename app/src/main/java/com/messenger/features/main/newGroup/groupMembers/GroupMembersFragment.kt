package com.messenger.features.main.newGroup.groupMembers

import android.Manifest
import android.os.Bundle
import android.text.Editable
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.messenger.databinding.FragmentGroupMembersBinding
import com.messenger.features.main.common.adapters.ContactsAdapter
import com.messenger.features.main.newGroup.NewGroupViewModel
import com.messenger.features.main.newGroup.groupMembers.adapters.SelectedContactsAdapter
import com.messenger.features.views.HeaderView
import com.messenger.models.ContactData
import com.messenger.utils.extensions.AfterTextChangedWatcher
import com.messenger.utils.extensions.LiveEvent
import com.messenger.utils.hasPermission
import com.messenger.utils.isContactsPermissionDenied
import com.messenger.utils.showContactsRationaleDialog
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class GroupMembersFragment : Fragment() {

    private val sharedViewModel by activityViewModel<NewGroupViewModel>()
    private val viewModel: GroupMembersViewModel by viewModel()
    private var binding: FragmentGroupMembersBinding? = null

    companion object {
        private val TAG = GroupMembersFragment::class.java.simpleName
        private lateinit var selectedContactsAdapter: SelectedContactsAdapter
        private lateinit var contactsAdapter: ContactsAdapter
    }

    private val permissionContactsRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            when {
                granted -> checkContactsPermissions()
            }
        }

    private val headerListener = object : HeaderView.HeaderViewListener {

        override fun onBackClick() {
            Timber.tag(TAG).d("onBackClick")
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
    }

    private val selectedContactsListener =
        object : SelectedContactsAdapter.SelectedContactsListener {

            override fun onItemClick(itemId: Long) {
                Timber.tag(TAG).d("onItemClick")
                viewModel.checkSelectedItem(itemId)
            }
        }

    private val contactsListener = object : ContactsAdapter.SearchListListener {
        override fun onItemClick(data: ContactData) {
            Timber.tag(TAG).d("onItemClick")
            viewModel.checkSelectedItem(data.id)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGroupMembersBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            headerView.setListener(headerListener)

            nextButton.setOnClickListener {
                viewModel.confirmSelectedContacts()
            }

            groupMembersInputView.addTextChangedListener(object : AfterTextChangedWatcher() {
                override fun afterTextChanged(s: Editable?) {
                    viewModel.querySearch((s ?: "").toString())
                }
            })

            initSelectedContactsAdapter()
            initContactsAdapter()
            checkContactsPermissions()
        }
    }

    private fun FragmentGroupMembersBinding.initSelectedContactsAdapter() {
        val flexBoxLayoutManager = FlexboxLayoutManager(context).apply {
            flexWrap = FlexWrap.WRAP
            flexDirection = FlexDirection.ROW
            alignItems = AlignItems.STRETCH
        }

        selectedContactsRecyclerView.apply {
            layoutManager = flexBoxLayoutManager
            selectedContactsAdapter = SelectedContactsAdapter(selectedContactsListener)
            adapter = selectedContactsAdapter
        }
    }

    private fun FragmentGroupMembersBinding.initContactsAdapter() {
        contactsRecyclerView.apply {
            contactsAdapter =
                ContactsAdapter(contactsListener)
            adapter = contactsAdapter
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.contactsData.observe(viewLifecycleOwner) { data ->
            handleContactsData(data)
        }
        viewModel.selectedContactsData.observe(viewLifecycleOwner) { data ->
            handleSelectedContactsData(data)
        }
        viewModel.confirmedData.observe(viewLifecycleOwner) { data ->
            handleConfirmedData(data)
        }
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
        viewModel.errorEvent.observe(viewLifecycleOwner) { event ->
            handleErrorEvent(event)
        }
    }

    private fun handleConfirmedData(data: LiveEvent<List<ContactData>?>?) {
        data?.getContentIfNotHandled()?.let {
            if (it.isNotEmpty()) {
                sharedViewModel.setSelectedGroupContacts(it)
            }
        }
    }

    private fun handleProgressEvent(isLoading: Boolean?) {
        binding?.progress?.setProgressEvent(isLoading ?: false)
        binding?.nextButton?.isEnabled = isLoading == false
    }

    private fun handleErrorEvent(event: LiveEvent<GroupMembersViewModel.ErrorEvent>?) {
        event?.getContentIfNotHandled()?.let {

        }
    }

    private fun handleContactsData(data: GroupMembersViewModel.ContactsData?) {
        data?.let {
            contactsAdapter.submitList(it.contacts)
        }
    }

    private fun handleSelectedContactsData(data: GroupMembersViewModel.ContactsData?) {
        data?.let {
            binding?.selectedContactsRecyclerView?.isVisible = it.contacts.isNotEmpty()
            selectedContactsAdapter.submitList(it.contacts)
        }
    }

    private fun checkContactsPermissions() {
        when {
            requireContext().hasPermission(Manifest.permission.READ_CONTACTS) -> viewModel.getContacts(
                requireContext()
            )

            requireActivity().isContactsPermissionDenied() -> showContactsRationaleDialog(
                requireContext(), permissionContactsRequest
            )

            else -> permissionContactsRequest.launch(Manifest.permission.READ_CONTACTS)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}

package com.messenger.features.main.newGroup.groupMembers

import android.Manifest
import android.content.Context
import timber.log.Timber
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.messenger.data.ContactsRepository
import com.messenger.models.ContactData
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import com.messenger.utils.hasPermission
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class GroupMembersViewModel(private val contactsRepository: ContactsRepository) : ViewModel() {

    companion object {

        private val TAG = GroupMembersViewModel::class.java.simpleName
        private var allContacts: List<ContactData> = ArrayList()
        private var contacts: List<ContactData> = ArrayList()
        private var selectedItems: List<Long> = ArrayList()

    }

    enum class ErrorEvent {
        ERROR_LOADING_DATA
    }

    val errorEvent = LiveEventData<ErrorEvent>()

    data class ContactsData(
        val contacts: List<ContactData>
    )

    val contactsData = MutableLiveData<ContactsData?>()

    val selectedContactsData = MutableLiveData<ContactsData?>()
    val confirmedData = LiveEventData<List<ContactData>?>()

    val progressEvent = ProgressData()

    init {
        selectedItems = emptyList()
        contacts = emptyList()
        selectedContactsData.postValue(null)
    }

    fun getContacts(
        context: Context
    ) {
        Timber.tag(TAG).d("getContacts")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                if (allContacts.isEmpty()) {
                    if (context.hasPermission(Manifest.permission.READ_CONTACTS)
                    ) {
                        contactsData.postValue(ContactsData(contacts = allContacts))
                    }
                    allContacts = contactsRepository.fetchContacts(context, false)
                }
                contacts = allContacts
                contactsData.postValue(ContactsData(contacts = contacts))
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_DATA)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    fun querySearch(typedText: String) {
        viewModelScope.launch(Dispatchers.IO) {
            contacts = allContacts.filter {
                it.name.contains(typedText) || it.phoneNumber.contains(
                    typedText
                )
            }
            val updatedContacts = contacts.map { item ->
                item.copy(
                    isSelected = selectedItems.contains(
                        item.id
                    )
                )
            }
            contactsData.postValue(ContactsData(contacts = updatedContacts))
        }
    }

    fun checkSelectedItem(itemId: Long) {
        selectedItems = if (selectedItems.contains(itemId)) {
            selectedItems.minus(itemId)
        } else {
            selectedItems.plus(itemId)
        }

        val updatedContacts = contacts.map { item ->
            item.copy(
                isSelected = selectedItems.contains(
                    item.id
                )
            )
        }
        val selectedContactsTags = allContacts.filter { item -> selectedItems.contains(item.id) }

        contactsData.postValue(ContactsData(contacts = updatedContacts))
        selectedContactsData.postValue(ContactsData(contacts = selectedContactsTags))
    }

    fun confirmSelectedContacts() {
        val selectedContacts = allContacts.filter { item -> selectedItems.contains(item.id) }
        confirmedData.postRawValue(selectedContacts)
    }
}
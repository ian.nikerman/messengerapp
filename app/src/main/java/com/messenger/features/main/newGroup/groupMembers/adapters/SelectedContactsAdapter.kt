package com.messenger.features.main.newGroup.groupMembers.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexboxLayoutManager
import com.messenger.databinding.ItemContactTagBinding
import com.messenger.models.ContactData
import com.messenger.utils.getColoredCircle
import com.messenger.utils.getNameInitials
import com.messenger.utils.showDrawable
import com.messenger.utils.showPlaceholderCircleImage

class SelectedContactsAdapter(
    private val listener: SelectedContactsListener
) : ListAdapter<ContactData, RecyclerView.ViewHolder>(TagsDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemContactTagBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as ViewHolder).bind(listener, item)
    }

    class ViewHolder(private val itemBinding: ItemContactTagBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(listener: SelectedContactsListener, data: ContactData) {
            itemBinding.apply {
                tagItemTitle.text = data.name
                tagItemDeleteTitle.text = data.name

                tagItemBackground.setOnClickListener { showDeleteItemUi(listener, data) }

                val lp = tagItemBackground.layoutParams
                if (lp is FlexboxLayoutManager.LayoutParams) {
                    lp.flexGrow = 1f
                }

                checkAvatar(data)
            }
        }

        private fun ItemContactTagBinding.showDeleteItemUi(
            listener: SelectedContactsListener, data: ContactData
        ) {
            tagItemDeleteLayout.isVisible = true
            tagItemDeleteButton.setOnClickListener {
                tagItemDeleteLayout.isVisible = false
                listener.onItemClick(data.id)
            }
            tagItemDeleteLayout.setOnClickListener { tagItemDeleteLayout.isVisible = false }
        }

        private fun ItemContactTagBinding.checkAvatar(data: ContactData) {
            when (data.avatar) {
                null -> {
                    imageView.showDrawable(getColoredCircle(data.color))
                    initials.text = data.name.getNameInitials()
                    initials.isVisible = true
                }

                else -> {
                    imageView.showPlaceholderCircleImage(data.avatar)
                    initials.isVisible = false
                }
            }
        }
    }

    interface SelectedContactsListener {
        fun onItemClick(itemId: Long)
    }
}

private class TagsDiffCallback : DiffUtil.ItemCallback<ContactData>() {

    override fun areItemsTheSame(oldItem: ContactData, newItem: ContactData): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: ContactData, newItem: ContactData): Boolean {
        return oldItem == newItem
    }
}
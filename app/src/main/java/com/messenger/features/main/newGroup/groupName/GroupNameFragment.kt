package com.messenger.features.main.newGroup.groupName

import android.os.Bundle
import android.text.Editable
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.messenger.R
import com.messenger.databinding.FragmentGroupNameBinding
import com.messenger.features.main.common.adapters.ContactsAdapter
import com.messenger.features.main.newGroup.NewGroupViewModel
import com.messenger.features.views.HeaderView
import com.messenger.models.ContactData
import com.messenger.models.GroupRequest
import com.messenger.utils.extensions.AfterTextChangedWatcher
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class GroupNameFragment : Fragment() {

    private val sharedViewModel by activityViewModel<NewGroupViewModel>()
    private val viewModel: GroupNameViewModel by viewModel()
    private var binding: FragmentGroupNameBinding? = null

    companion object {

        private val TAG = GroupNameFragment::class.java.simpleName
        private lateinit var contactsAdapter: ContactsAdapter

    }

    private val headerListener = object : HeaderView.HeaderViewListener {

        override fun onBackClick() {
            Timber.tag(TAG).d("onBackClick")
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
    }

    private val contactsListener = object : ContactsAdapter.SearchListListener {
        override fun onItemClick(data: ContactData) {}
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGroupNameBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            headerView.setListener(headerListener)
            groupIcon.setOnClickListener {
                viewModel.openGallery()
            }
            confirmButton.setOnClickListener {
                viewModel.confirmData()
            }

            groupNameInputView.addTextChangedListener(object : AfterTextChangedWatcher() {
                override fun afterTextChanged(s: Editable?) {
                    viewModel.setGroupName(s.toString())
                }
            })

            groupDescriptionInputView.addTextChangedListener(object : AfterTextChangedWatcher() {
                override fun afterTextChanged(s: Editable?) {
                    viewModel.setGroupDescription(s.toString())
                }
            })

            initContactsAdapter()
        }
    }

    private fun FragmentGroupNameBinding.initContactsAdapter() {
        recyclerView.apply {
            contactsAdapter = ContactsAdapter(contactsListener)
            adapter = contactsAdapter
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        sharedViewModel.groupDetailsData.observe(viewLifecycleOwner) { data ->
            handleGroupData(data)
        }
        viewModel.confirmedData.observe(viewLifecycleOwner) { data ->
            handleConfirmedData(data)
        }
    }

    private fun handleGroupData(data: GroupRequest?) {
        data?.let {
            binding?.selectorCounter?.text = getMembersCounterTitle(data)
            contactsAdapter.submitList(it.members)
        }
    }

    private fun getMembersCounterTitle(data: GroupRequest) = when (data.members.size) {
        1 -> {
            String.format(getString(R.string.d_member), data.members.size)
        }

        else -> {
            String.format(getString(R.string.d_members), data.members.size)
        }
    }

    private fun handleConfirmedData(data: LiveEvent<GroupNameViewModel.GroupNameData?>?) {
        data?.getContentIfNotHandled()?.let {
            sharedViewModel.setName(it.name, it.description, it.icon)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}

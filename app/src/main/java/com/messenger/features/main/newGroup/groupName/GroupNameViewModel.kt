package com.messenger.features.main.newGroup.groupName

import android.net.Uri
import timber.log.Timber
import androidx.lifecycle.ViewModel
import com.messenger.utils.extensions.LiveEventData

class GroupNameViewModel : ViewModel() {

    companion object {

        private val TAG = GroupNameViewModel::class.java.simpleName
        private var groupName = ""
        private var groupDescription = ""
        private var groupIcon: Uri? = null

    }

    data class GroupNameData(
        val name: String, val description: String, val icon: Uri?
    )

    val confirmedData = LiveEventData<GroupNameData?>()

    init {
        confirmedData.postRawValue(null)
    }

    fun confirmData() {
        Timber.tag(TAG).d("confirmData")
        confirmedData.postRawValue(
            GroupNameData(
                name = groupName, description = groupDescription, icon = groupIcon
            )
        )
    }

    fun setGroupName(name: String) {
        groupName = name
    }

    fun setGroupIcon(icon: Uri?) {
        groupIcon = icon
    }

    fun setGroupDescription(description: String) {
        groupDescription = description
    }

    fun openGallery() {

    }
}
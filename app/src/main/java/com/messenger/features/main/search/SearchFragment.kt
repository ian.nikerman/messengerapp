package com.messenger.features.main.search

import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.messenger.R
import com.messenger.databinding.FragmentSearchBinding
import com.messenger.features.main.common.adapters.ContentDataVPAdapter
import com.messenger.features.main.search.contentData.SearchVPFragment
import com.messenger.features.views.HeaderView
import com.messenger.models.ChatSettingsTab
import com.messenger.models.ContentType
import com.messenger.utils.closeKeyboard
import com.messenger.utils.extensions.LiveEvent
import com.messenger.utils.extensions.ZoomOutPageTransformer
import com.messenger.utils.openKeyboard
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchFragment : Fragment() {

    private val viewModel: SearchViewModel by viewModel()
    private var binding: FragmentSearchBinding? = null

    companion object {

        private val TAG = SearchFragment::class.java.simpleName
        private lateinit var contentDataVPAdapter: ContentDataVPAdapter

    }

    private val headerListener = object : HeaderView.HeaderViewListener {

        override fun onHideInputClick(inputView: EditText) {
            Timber.tag(TAG).d("onHideInputClick")
            closeKeyboard(requireActivity(), inputView)
        }

        override fun onMoreClick() {
            Timber.tag(TAG).d("onMoreClick")

        }

        override fun onBackClick() {
            Timber.tag(TAG).d("onBackClick")
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        override fun onInputTypedText(typedText: String) {
            viewModel.searchByQuery(typedText)
        }

        override fun onCancelSelectorClick() {
            Timber.tag(TAG).d("onCancelSelectorClick")
//            viewModel.resetSelectedItems()
        }

        override fun onPinChangeClick(isPinShown: Boolean) {
            Timber.tag(TAG).d("onPinChangeClick")
//            viewModel.changePinSelectedChats(isPinShown)
        }

        override fun onMuteChangeClick(isMuteShown: Boolean) {
            Timber.tag(TAG).d("onMuteChangeClick")
//            viewModel.changeMuteSelectedChats(isMuteShown)
        }

        override fun onDeleteClick() {
            Timber.tag(TAG).d("onDeleteClick")
//            viewModel.deleteSelectedChats()
        }
    }

    private val onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
//            if (viewModel.isChatItemsSelected()) {
//                viewModel.resetSelectedItems()
//            } else {
//                isEnabled = false
//                requireActivity().onBackPressedDispatcher.onBackPressed()
//            }
            isEnabled = false
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
        initObservers()
    }

    private fun initUi() {
        binding?.apply {
            headerView.setListener(headerListener)
            headerView.getInputView()?.let {
                openKeyboard(requireActivity(), it)
            }
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.headerDetailsData.observe(viewLifecycleOwner) { data ->
            handleHeaderDetailsData(data)
        }
        viewModel.tabsData.observe(viewLifecycleOwner) { data ->
            handleTabsData(data)
        }
        viewModel.pageTypeData.observe(viewLifecycleOwner) { data ->
            handlePageTypeData(data)
        }
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
        viewModel.errorEvent.observe(viewLifecycleOwner) { event ->
            handleErrorEvent(event)
        }
    }

    private fun handlePageTypeData(data: Int?) {
        data?.let {
            binding?.apply {
                if (viewPager.currentItem != it) viewPager.setCurrentItem(it, false)
            }
        }
    }

    private fun handleTabsData(data: List<ChatSettingsTab>?) {
        data?.let {
            Timber.tag(TAG).d("handleTabsData")
            binding?.apply {
                val fragmentList = ArrayList<Fragment>()
                it.map { it.contentType }.forEach { contentType ->
                    fragmentList.add(
                        SearchVPFragment(
                            contentType
                        )
                    )
                }

                viewPager.apply {
                    contentDataVPAdapter = ContentDataVPAdapter(
                        fragmentList, childFragmentManager, lifecycle
                    )
                    setPageTransformer(ZoomOutPageTransformer())
                    adapter = contentDataVPAdapter
                    TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                        tab.text = getTabString(it[position].contentType)
                    }.attach()
                    registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                        override fun onPageScrollStateChanged(state: Int) {
                            when (state) {
                                ViewPager2.SCROLL_STATE_IDLE -> {
                                    viewModel.setCurrentPageType(viewPager.currentItem)
                                }

                                else -> {}
                            }
                        }
                    })
                }
            }
        }
    }

    private fun getTabString(contentType: ContentType): String {
        return when (contentType) {
            ContentType.CONTACT -> getString(R.string.members)
            ContentType.MEDIA -> getString(R.string.media)
            ContentType.FILE -> getString(R.string.files)
            ContentType.LINK -> getString(R.string.links)
            ContentType.AUDIO -> getString(R.string.audio)
            ContentType.VOICE -> getString(R.string.voice)
            ContentType.GIF -> getString(R.string.gif_s)
            ContentType.GROUP -> getString(R.string.groups)
            else -> ""
        }
    }

    private fun handleHeaderDetailsData(data: SearchViewModel.HeaderDetails.UpdatedData?) {
        data?.let {
            binding?.headerView?.setSelectedChatsUi(
                it.selectedCounterData, it.isMuteShown, it.isPinShown
            )
        }
    }

    private fun handleErrorEvent(event: LiveEvent<SearchViewModel.ErrorEvent>?) {
        event?.getContentIfNotHandled()?.let {

        }
    }

    private fun handleProgressEvent(isLoading: Boolean) {
        binding?.progress?.setProgressEvent(isLoading)
    }

    override fun onResume() {
        super.onResume()
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner, onBackPressedCallback
        )
        viewModel.getTabs()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}

package com.messenger.features.main.search

import timber.log.Timber
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.messenger.data.ChatsRepository
import com.messenger.data.ContactsRepository
import com.messenger.models.ChatSettingsTab
import com.messenger.models.ContactData
import com.messenger.models.ContentData
import com.messenger.models.ContentType
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SearchViewModel(
    private val contactsRepository: ContactsRepository, private val chatsRepository: ChatsRepository
) : ViewModel() {

    companion object {

        private val TAG = SearchViewModel::class.java.simpleName
    }

    enum class ErrorEvent {
        ERROR_LOADING_DATA
    }

    val errorEvent = LiveEventData<ErrorEvent>()
    val progressEvent = ProgressData()

    enum class EmptyDataState {
        STATE_DATA, STATE_DATA_EMPTY
    }

    sealed class HeaderDetails {
        data class UpdatedData(
            val selectedCounterData: Int,
            val isPinShown: Boolean? = null,
            val isMuteShown: Boolean? = null
        ) : HeaderDetails()
    }

    sealed class ChatInfoDetails {
        data class ContentItemData(
            val contacts: List<ContentData>,
            val groups: List<ContentData>,
            val media: List<ContentData>,
            val files: List<ContentData>,
            val links: List<ContentData>,
            val audios: List<ContentData>,
            val voice: List<ContentData>,
            val gifs: List<ContentData>
        ) : ChatInfoDetails()
    }

    private var currentPage = 0

    val pageTypeData = MutableLiveData<Int>()
    val tabsData = MutableLiveData<List<ChatSettingsTab>?>()
    val contentData = MutableLiveData<ChatInfoDetails.ContentItemData?>()
    val headerDetailsData = MutableLiveData<HeaderDetails.UpdatedData>()

    init {
        pageTypeData.postValue(currentPage)
    }

    fun getTabs() {
        Timber.tag(TAG).d("getTabs")
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val data = chatsRepository.getChatInfoTabs()
                handleTabsData(data)
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_DATA)
            }
        }
    }

    private fun handleTabsData(data: List<ChatSettingsTab>) {
        tabsData.postValue(data)
    }

    fun searchByQuery(query: String) {
        Timber.tag(TAG).d("searchByQuery")
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val contacts = contactsRepository.fetchContactsByQuery(query)
                handleData(contacts)
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_LOADING_DATA)
            }
        }
    }

    private fun handleData(contacts: List<ContactData>) {
        Timber.tag(TAG).d("handleData")
        val contactsContentData = contacts.map {
            ContentData(
                id = it.id, contactData = it, contentType = ContentType.CONTACT
            )
        }
        contentData.postValue(
            ChatInfoDetails.ContentItemData(
                contacts = contactsContentData,
                media = emptyList(),
                files = emptyList(),
                links = emptyList(),
                audios = emptyList(),
                voice = emptyList(),
                gifs = emptyList(),
                groups = emptyList(),
            )
        )
    }

    fun setCurrentPageType(type: Int) {
        Timber.tag(TAG).d("setCurrentPageType")
        currentPage = type
        pageTypeData.postValue(currentPage)
    }
}
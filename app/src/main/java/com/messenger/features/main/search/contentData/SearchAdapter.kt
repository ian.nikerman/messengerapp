package com.messenger.features.main.search.contentData

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.messenger.databinding.ItemContentDataBinding
import com.messenger.models.ContentData
import com.messenger.models.ContentType
import com.messenger.utils.getColoredCircle
import com.messenger.utils.getNameInitials
import com.messenger.utils.showDrawable
import com.messenger.utils.showPlaceholderCircleImage

class SearchAdapter(private val listener: ContentDataAdapterListener) :
    ListAdapter<ContentData, RecyclerView.ViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val itemBinding =
            ItemContentDataBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as ViewHolder).bind(
            listener,
            item
        )
    }

    class ViewHolder(private val itemBinding: ItemContentDataBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(
            listener: ContentDataAdapterListener,
            data: ContentData
        ) {
            when (data.contentType) {
                ContentType.CONTACT -> {
                    showContactUi(listener, data)
                }

                ContentType.MEDIA -> {
                    showMediaUi(listener, data)
                }

                ContentType.FILE -> {
                    showFileUi(listener, data)
                }

                ContentType.LINK -> {
                    showLinkUi(listener, data)
                }

                ContentType.AUDIO -> {
                    showAudioUi(listener, data)
                }

                ContentType.VOICE -> {
                    showVoiceUi(listener, data)
                }

                ContentType.GIF -> {
                    showGifUi(listener, data)
                }

                ContentType.GROUP -> {
                    showGroupUi(listener, data)
                }

                else -> {}
            }
        }

        private fun showContactUi(listener: ContentDataAdapterListener, data: ContentData) {
            itemBinding.itemSearchListLayout.apply {

                layout.setOnClickListener { listener.onItemClick(data) }
                data.contactData?.apply {
                    title.text = name
                    subTitle.text = phoneNumber.firstOrNull() ?: ""

                    when (avatar) {
                        null -> {
                            imageView.showDrawable(getColoredCircle(color))
                            initials.text = name.getNameInitials()
                            initials.isVisible = true
                        }

                        else -> {
                            imageView.showPlaceholderCircleImage(avatar)
                            initials.isVisible = false
                        }
                    }
                    layout.isSelected = isSelected ?: false
                    selectorView.isVisible = isSelected ?: false
                }
            }
        }

        private fun showMediaUi(listener: ContentDataAdapterListener, data: ContentData) {

        }

        private fun showFileUi(listener: ContentDataAdapterListener, data: ContentData) {

        }

        private fun showLinkUi(listener: ContentDataAdapterListener, data: ContentData) {

        }

        private fun showAudioUi(listener: ContentDataAdapterListener, data: ContentData) {

        }

        private fun showVoiceUi(listener: ContentDataAdapterListener, data: ContentData) {

        }

        private fun showGifUi(listener: ContentDataAdapterListener, data: ContentData) {

        }

        private fun showGroupUi(listener: ContentDataAdapterListener, data: ContentData) {

        }
    }

    interface ContentDataAdapterListener {
        fun onItemClick(data: ContentData)
    }

    class DiffCallback : DiffUtil.ItemCallback<ContentData>() {

        override fun areItemsTheSame(
            oldItem: ContentData,
            newItem: ContentData
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: ContentData,
            newItem: ContentData
        ): Boolean {
            return oldItem == newItem
        }
    }
}
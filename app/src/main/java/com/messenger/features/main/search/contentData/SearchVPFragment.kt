package com.messenger.features.main.search.contentData

import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.messenger.databinding.FragmentContentDataVpBinding
import com.messenger.features.main.MainViewModel
import com.messenger.features.main.search.SearchViewModel
import com.messenger.models.ContentData
import com.messenger.models.ContentType
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class SearchVPFragment(private val contentType: ContentType) : Fragment() {

    private val mainViewModel by activityViewModel<MainViewModel>()
    private val searchViewModel: SearchViewModel by viewModels({ requireParentFragment() })
    private var binding: FragmentContentDataVpBinding? = null

    companion object {

        private val TAG = SearchVPFragment::class.java.simpleName

        private lateinit var contentDataAdapter: SearchAdapter

    }

    private val contentDataListener = object : SearchAdapter.ContentDataAdapterListener {
        override fun onItemClick(data: ContentData) {
            Timber.tag(TAG).d("onItemClick")
            when (data.contentType) {
                ContentType.CONTACT -> mainViewModel.openChat(parentFragment, data.id)
                else -> {}
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentContentDataVpBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initAdapter()
        initObservers()
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        searchViewModel.contentData.observe(viewLifecycleOwner) { data ->
            handleData(data)
        }
    }

    private fun handleData(data: SearchViewModel.ChatInfoDetails.ContentItemData?) {
        data?.let {
            Timber.tag(TAG).d("handleData")
            val contentData = when (contentType) {
                ContentType.CONTACT -> it.contacts
                ContentType.GROUP -> it.groups
                ContentType.MEDIA -> it.media
                ContentType.FILE -> it.files
                ContentType.LINK -> it.links
                ContentType.AUDIO -> it.audios
                ContentType.VOICE -> it.voice
                ContentType.GIF -> it.gifs
                else -> null
            }
            contentData?.let { data ->
                contentDataAdapter.submitList(data)
                handleState(data.isEmpty())
            }
        }
    }

    private fun handleState(isShow: Boolean) {
        binding?.emptyDataView?.isVisible = isShow
    }

    private fun initAdapter() {
        Timber.tag(TAG).d("initAdapter")
        binding?.apply {
            contentDataRecyclerView.apply {
                contentDataAdapter = SearchAdapter(contentDataListener)
                adapter = contentDataAdapter
            }
        }
    }
}
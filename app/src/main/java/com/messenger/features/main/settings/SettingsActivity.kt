package com.messenger.features.main.settings

import android.os.Bundle
import timber.log.Timber
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import com.messenger.R
import com.messenger.databinding.ActivitySettingsBinding
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.viewModel

class SettingsActivity : AppCompatActivity() {

    private val viewModel: SettingsViewModel by viewModel()

    private companion object {

        private val TAG = SettingsActivity::class.java.simpleName
        private lateinit var binding: ActivitySettingsBinding

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Timber.tag(TAG).d("onCreate")
        initObservers()
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")

        viewModel.fragNavEvent.observe(this) { event ->
            handleNavigationEvent(event)
        }
    }

    private fun handleNavigationEvent(event: LiveEvent<NavDirections?>?) {
        event?.getContentIfNotHandled()?.let {
            Timber.tag(TAG).d("handleNavigationEvent:$event")
            try {
                findNavController(R.id.navigationContainer).navigate(it)
            } catch (ex: java.lang.Exception) {
                Timber.tag(TAG).d("handleNavigationEvent Error: ${ex.message}")
            }
        }
    }
}
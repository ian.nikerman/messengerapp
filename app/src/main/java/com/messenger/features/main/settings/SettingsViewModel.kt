package com.messenger.features.main.settings

import timber.log.Timber
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.messenger.features.main.settings.subSettings.profile.ProfileFragmentDirections
import com.messenger.utils.extensions.LiveEventData

class SettingsViewModel : ViewModel() {

    companion object {
        private val TAG = SettingsViewModel::class.java.simpleName
    }

    val fragNavEvent = LiveEventData<NavDirections?>()

    fun openNotificationSoundsSettings() {
        Timber.tag(TAG).d("openNotificationSoundsSettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToNotificationsSettingsFragment()
        )
    }

    fun openPrivacySettings() {
        Timber.tag(TAG).d("openPrivacySettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToSecuritySettingsFragment()
        )
    }

    fun openDataStorageSettings() {
        Timber.tag(TAG).d("openDataStorageSettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToStorageSettingsFragment()
        )
    }

    fun openChatsSettings() {
        Timber.tag(TAG).d("openChatsSettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToChatsSettingsFragment()
        )
    }

    fun openFoldersSettings() {
        Timber.tag(TAG).d("openFoldersSettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToFoldersSettingsFragment()
        )
    }

    fun openDevicesSettings() {
        Timber.tag(TAG).d("openDevicesSettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToDevicesSettingsFragment()
        )
    }

    fun openLanguageSettings() {
        Timber.tag(TAG).d("openLanguageSettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToLanguageSettingsFragment()
        )
    }

    fun openSupportHelp() {
        Timber.tag(TAG).d("openSupportHelp")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToSupportHelpFragment()
        )
    }

    fun openFAQHelp() {
        Timber.tag(TAG).d("openFAQHelp")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToFaqHelpFragment()
        )
    }

    fun openPrivacyPolicyHelp() {
        Timber.tag(TAG).d("openPrivacyPolicyHelp")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToPrivacyPolicyHelpFragment()
        )
    }
}
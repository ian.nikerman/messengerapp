package com.messenger.features.main.settings.subSettings

import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.messenger.databinding.FragmentSettingsFoldersBinding
import com.messenger.features.main.settings.subSettings.profile.ProfileViewModel
import com.messenger.features.views.HeaderView
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class FoldersSettingsFragment : Fragment() {

    private val sharedViewModel by activityViewModel<ProfileViewModel>()
    private var binding: FragmentSettingsFoldersBinding? = null

    companion object {

        private val TAG = FoldersSettingsFragment::class.java.simpleName

    }

    private val headerListener = object : HeaderView.HeaderViewListener {

        override fun onBackClick() {
            Timber.tag(TAG).d("onBackClick")
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSettingsFoldersBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
    }

    private fun initUi() {
        binding?.apply {
            headerView.setListener(headerListener)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}

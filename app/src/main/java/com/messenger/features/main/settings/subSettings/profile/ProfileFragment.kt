package com.messenger.features.main.settings.subSettings.profile

import android.content.Intent
import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.messenger.BuildConfig
import com.messenger.R
import com.messenger.databinding.FragmentSettingsBinding
import com.messenger.features.main.chatSettings.adapters.ChatInfoAdapter
import com.messenger.features.main.settings.SettingsViewModel
import com.messenger.features.onBoarding.OnBoardingActivity
import com.messenger.features.views.HeaderView
import com.messenger.utils.extensions.LiveEvent
import com.messenger.utils.showImageAnimated
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : Fragment() {

    private val settingsViewModel by activityViewModel<SettingsViewModel>()
    private val viewModel: ProfileViewModel by viewModel()
    private var binding: FragmentSettingsBinding? = null

    companion object {

        private val TAG = ProfileFragment::class.java.simpleName

        private lateinit var profileInfoAdapter: ChatInfoAdapter
    }

    private val headerListener = object : HeaderView.HeaderViewListener {

        override fun onMoreClick() {
            Timber.tag(TAG).d("onMoreClick")

        }

        override fun onBackClick() {
            Timber.tag(TAG).d("onBackClick")
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
    }

    private val infoListener = object : ChatInfoAdapter.ChatInfoListener {
        override fun onItemContactClick(itemId: Long) {
            Timber.tag(TAG).d("onItemContactClick")

        }

        override fun onItemCopyClick(title: String) {
            Timber.tag(TAG).d("onItemCopyClick")

        }

        override fun onInviteLinkClick(title: String) {
            Timber.tag(TAG).d("onInviteLinkClick")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSettingsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.tag(TAG).d("onViewCreated")
        initUi()
        initAdapter()
        initObservers()
    }

    private fun initAdapter() {
        binding?.infoRecyclerView?.apply {
            profileInfoAdapter = ChatInfoAdapter(infoListener)
            adapter = profileInfoAdapter
        }
    }

    private fun initUi() {
        binding?.apply {
            headerView.setListener(headerListener)

            notificationsAndSoundsSettings.setOnClickListener { settingsViewModel.openNotificationSoundsSettings() }
            privacySettings.setOnClickListener { settingsViewModel.openPrivacySettings() }
            dataStorageSettings.setOnClickListener { settingsViewModel.openDataStorageSettings() }
            chatsSettings.setOnClickListener { settingsViewModel.openChatsSettings() }
            foldersSettings.setOnClickListener { settingsViewModel.openFoldersSettings() }
            devicesSettings.setOnClickListener { settingsViewModel.openDevicesSettings() }
            languageSettings.setOnClickListener { settingsViewModel.openLanguageSettings() }
            supportHelp.setOnClickListener { settingsViewModel.openSupportHelp() }
            faqHelp.setOnClickListener { settingsViewModel.openFAQHelp() }
            privacyPolicyHelp.setOnClickListener { settingsViewModel.openPrivacyPolicyHelp() }

//            displayNameSetting.setListener(object : SwitchView.SwitchViewListener {
//                override fun onSwitchClick(isChecked: Boolean) {
////                    viewModel.setDisplayName(isChecked)
//                }
//            })
//            secureCallsSetting.setListener(object : SwitchView.SwitchViewListener {
//                override fun onSwitchClick(isChecked: Boolean) {
//                    viewModel.switchSecureCallsMode(isChecked)
//                }
//            })
//            semiSecureCallsSetting.setListener(object : SwitchView.SwitchViewListener {
//                override fun onSwitchClick(isChecked: Boolean) {
//                    viewModel.switchSemiSecureCallsMode(isChecked)
//                }
//            })
//            privateModeSetting.setListener(object : SwitchView.SwitchViewListener {
//                override fun onSwitchClick(isChecked: Boolean) {
//                    viewModel.switchPrivateMode(isChecked)
//                }
//            })
//            microphoneSetting.setListener(object : SwitchView.SwitchViewListener {
//                override fun onSwitchClick(isChecked: Boolean) {
//                    viewModel.switchMicrophoneAvailable(isChecked)
//                }
//            })
//            notificationSetting.setListener(object : CheckBoxView.CheckBoxViewListener {
//                override fun onCheckBoxClick(isChecked: Boolean) {
//                    viewModel.switchNotificationWindowEnabled(isChecked)
//                }
//            })
//            inRingtoneSetting.setListener(object : SwitchView.SwitchViewListener {
//                override fun onSwitchClick(isChecked: Boolean) {
//                    viewModel.switchAutoDownLoadFilesMode(isChecked)
//                }
//            })
//            outRingtoneSetting.setListener(object : SwitchView.SwitchViewListener {
//                override fun onSwitchClick(isChecked: Boolean) {
//                    viewModel.switchAutoDownLoadFilesMode(isChecked)
//                }
//            })

//            autoDownloadSetting.setListener(object : SwitchView.SwitchViewListener {
//                override fun onSwitchClick(isChecked: Boolean) {
//                    viewModel.switchAutoDownLoadFilesMode(isChecked)
//                }
//            })
//            darkThemeSetting.setListener(object : CheckBoxView.CheckBoxViewListener {
//                override fun onCheckBoxClick(isChecked: Boolean) {
//                    viewModel.switchDarkMode(isChecked)
//                }
//            })
//            tlSetting.setListener(object : SwitchView.SwitchViewListener {
//                override fun onSwitchClick(isChecked: Boolean) {
//                    viewModel.setTLMessagesExpireTime(isChecked)
//                }
//            })
//            removeTLdMessagesSetting.setListener(object : CheckBoxView.CheckBoxViewListener {
//                override fun onCheckBoxClick(isChecked: Boolean) {
//                    viewModel.switchRemoveTLMessagesMode(isChecked)
//                }
//            })
//            sendTLMessagesSetting.setListener(object : CheckBoxView.CheckBoxViewListener {
//                override fun onCheckBoxClick(isChecked: Boolean) {
//                    viewModel.switchSendTLMessagesMode(isChecked)
//                }
//            })
//            resetRegTitle.setOnClickListener {
//                viewModel.logout()
//            }
//            dataBackupTitle.setOnClickListener {
//
//            }
//            uploadLogsTitle.setOnClickListener {
//
//            }
//            alertTitle.setOnClickListener {
//
//            }
        }
    }

    private fun initObservers() {
        viewModel.chatMainData.observe(viewLifecycleOwner) { data ->
            handleProfileData(data)
        }
//        viewModel.settingsEvent.observe(viewLifecycleOwner) { event ->
//            handleSettingsEvent(event)
//        }
//        viewModel.displayName.observe(viewLifecycleOwner) { data ->
//            handleDisplayName(data)
//        }
//        viewModel.secureCallsMode.observe(viewLifecycleOwner) { data ->
//            handleSecureCallsMode(data)
//        }
//        viewModel.semiSecureCallsMode.observe(viewLifecycleOwner) { data ->
//            handleSemiSecureCallsMode(data)
//        }
//        viewModel.isPrivateMode.observe(viewLifecycleOwner) { data ->
//            handlePrivateMode(data)
//        }
//        viewModel.isMicrophoneAvailable.observe(viewLifecycleOwner) { data ->
//            handleMicrophoneAvailable(data)
//        }
//        viewModel.isNotificationWindowEnabled.observe(viewLifecycleOwner) { data ->
//            handleNotificationWindowEnabled(data)
//        }
//        viewModel.incomingRingtone.observe(viewLifecycleOwner) { data ->
//            handleIncomingRingtone(data)
//        }
//        viewModel.outGoingRingtone.observe(viewLifecycleOwner) { data ->
//            handleOutGoingRingtone(data)
//        }
//        viewModel.isAutoDownLoadFilesMode.observe(viewLifecycleOwner) { data ->
//            handleAutoDownLoadFilesMode(data)
//        }
//        viewModel.isDarkMode.observe(viewLifecycleOwner) { data ->
//            handleDarkMode(data)
//        }
//        viewModel.tlMessagesExpireTime.observe(viewLifecycleOwner) { data ->
//            handleTLMessagesExpireTime(data)
//        }
//        viewModel.isRemoveTLMessagesMode.observe(viewLifecycleOwner) { data ->
//            handleRemoveTLMessagesMode(data)
//        }
//        viewModel.isSendTLMessagesMode.observe(viewLifecycleOwner) { data ->
//            handleSendTLMessagesMode(data)
//        }

    }

    private fun handleProfileData(data: ProfileViewModel.ChatInfoDetails.MainData?) {
        data?.let {
            profileInfoAdapter.submitList(it.infoList)

            binding?.apply {
                profileName.text = it.mainData.name
                profileStatus.text = "offline"
                profileImage.showImageAnimated(it.mainData.avatar)

                appVersion.text =
                    "${getString(R.string.app_name)} for Android v.${BuildConfig.VERSION_NAME}"
            }
        }
    }

    private fun handleSettingsEvent(event: LiveEvent<ProfileViewModel.SettingsEvent>?) {
        event?.getContentIfNotHandled()?.let {
            when (it) {
                ProfileViewModel.SettingsEvent.EVENT_RESTART -> {
                    restartApp()
                }
            }
        }
    }

    private fun restartApp() {
        requireActivity().apply {
            startActivity(Intent(this, OnBoardingActivity::class.java))
            finishAffinity()
        }
    }

//    private fun handleDisplayName(data: SettingsViewModel.SettingData.UpdatedData?) {
//        data?.let {
//            binding?.apply {
//                displayNameSetting.setSubTitle(it.subtitle)
//                displayNameSetting.hideButton()
//            }
//        }
//    }
//
//    private fun handleSecureCallsMode(data: SettingsViewModel.SettingData.UpdatedData?) {
//        data?.let {
//            binding?.apply {
//                secureCallsSetting.setSubTitle(it.subtitle)
//                secureCallsSetting.setChecked(it.isChecked ?: false)
//            }
//        }
//    }
//
//    private fun handleSemiSecureCallsMode(data: SettingsViewModel.SettingData.UpdatedData?) {
//        data?.let {
//            binding?.apply {
//                semiSecureCallsSetting.setSubTitle(it.subtitle)
//                semiSecureCallsSetting.setChecked(it.isChecked ?: false)
//            }
//        }
//    }
//
//    private fun handlePrivateMode(data: SettingsViewModel.SettingData.UpdatedData?) {
//        data?.let {
//            binding?.apply {
//                privateModeSetting.setSubTitle(it.subtitle)
//                privateModeSetting.setChecked(it.isChecked ?: false)
//            }
//        }
//    }
//
//    private fun handleMicrophoneAvailable(data: SettingsViewModel.SettingData.UpdatedData?) {
//        data?.let {
//            binding?.apply {
//                microphoneSetting.setSubTitle(it.subtitle)
//                microphoneSetting.setChecked(it.isChecked ?: false)
//            }
//        }
//    }
//
//    private fun handleNotificationWindowEnabled(data: SettingsViewModel.SettingData.UpdatedData?) {
//        data?.let {
//            binding?.apply {
//                notificationSetting.setSubTitle(it.subtitle)
//                notificationSetting.setChecked(it.isChecked ?: false)
//            }
//        }
//    }
//
//    private fun handleIncomingRingtone(data: SettingsViewModel.SettingData.UpdatedData?) {
//        data?.let {
//            binding?.apply {
//                inRingtoneSetting.setSubTitle(it.subtitle)
//                inRingtoneSetting.hideButton()
//            }
//        }
//    }
//
//    private fun handleOutGoingRingtone(data: SettingsViewModel.SettingData.UpdatedData?) {
//        data?.let {
//            binding?.apply {
//                outRingtoneSetting.setSubTitle(it.subtitle)
//                outRingtoneSetting.hideButton()
//            }
//        }
//    }
//
//    private fun handleAutoDownLoadFilesMode(data: SettingsViewModel.SettingData.UpdatedData?) {
//        data?.let {
//            binding?.apply {
//                autoDownloadSetting.setSubTitle(it.subtitle)
//                autoDownloadSetting.setChecked(it.isChecked ?: false)
//            }
//        }
//    }
//
//    private fun handleDarkMode(data: SettingsViewModel.SettingData.UpdatedData?) {
//        data?.let {
//            binding?.apply {
//                darkThemeSetting.setSubTitle(it.subtitle)
//                darkThemeSetting.setChecked(it.isChecked ?: false)
//            }
//        }
//    }
//
//    private fun handleTLMessagesExpireTime(data: SettingsViewModel.SettingData.UpdatedData?) {
//        data?.let {
//            binding?.apply {
//                tlSetting.setSubTitle(it.subtitle)
//                tlSetting.hideButton()
//            }
//        }
//    }
//
//    private fun handleRemoveTLMessagesMode(data: SettingsViewModel.SettingData.UpdatedData?) {
//        data?.let {
//            binding?.apply {
//                removeTLdMessagesSetting.setSubTitle(it.subtitle)
//                removeTLdMessagesSetting.setChecked(it.isChecked ?: false)
//            }
//        }
//    }
//
//    private fun handleSendTLMessagesMode(data: SettingsViewModel.SettingData.UpdatedData?) {
//        data?.let {
//            binding?.apply {
//                sendTLMessagesSetting.setSubTitle(it.subtitle)
//                sendTLMessagesSetting.setChecked(it.isChecked ?: false)
//            }
//        }
//    }

    override fun onResume() {
        super.onResume()
        viewModel.fetchProfile()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}

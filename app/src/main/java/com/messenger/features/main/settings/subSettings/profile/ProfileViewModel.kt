package com.messenger.features.main.settings.subSettings.profile

import android.content.res.Resources
import timber.log.Timber
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.messenger.R
import com.messenger.cache.AppSettings
import com.messenger.data.ProfileRepository
import com.messenger.models.ChatInfoItem
import com.messenger.models.ChatInfoType
import com.messenger.models.ContactData
import com.messenger.network.ApiSettings
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProfileViewModel(
    private val apiSettings: ApiSettings,
    private val appSettings: AppSettings,
    private val resources: Resources,
    private val profileRepository: ProfileRepository
) : ViewModel() {

    companion object {

        private val TAG = ProfileViewModel::class.java.simpleName

    }

    enum class SettingsEvent {
        EVENT_RESTART
    }

    enum class ErrorEvent {
        ERROR_FETCH_PROFILE
    }

    val errorEvent = LiveEventData<ErrorEvent>()

    sealed class SettingData {
        data class UpdatedData(
            val subtitle: String? = null, val isChecked: Boolean? = null
        ) : SettingData()
    }

    sealed class ChatInfoDetails {
        data class MainData(
            val mainData: ContactData, val infoList: List<ChatInfoItem>?
        ) : ChatInfoDetails()
    }

    val fragNavEvent = LiveEventData<NavDirections?>()
    val chatMainData = MutableLiveData<ChatInfoDetails.MainData?>()

    val displayName = MutableLiveData<SettingData.UpdatedData>()
    val secureCallsMode = MutableLiveData<SettingData.UpdatedData>()
    val semiSecureCallsMode = MutableLiveData<SettingData.UpdatedData>()
    val isPrivateMode = MutableLiveData<SettingData.UpdatedData>()
    val isMicrophoneAvailable = MutableLiveData<SettingData.UpdatedData>()
    val isNotificationWindowEnabled = MutableLiveData<SettingData.UpdatedData>()
    val incomingRingtone = MutableLiveData<SettingData.UpdatedData>()
    val outGoingRingtone = MutableLiveData<SettingData.UpdatedData>()
    val isAutoDownLoadFilesMode = MutableLiveData<SettingData.UpdatedData>()
    val isDarkMode = MutableLiveData<SettingData.UpdatedData>()
    val tlMessagesExpireTime = MutableLiveData<SettingData.UpdatedData>()
    val isRemoveTLMessagesMode = MutableLiveData<SettingData.UpdatedData>()
    val isSendTLMessagesMode = MutableLiveData<SettingData.UpdatedData>()
    val settingsEvent = LiveEventData<SettingsEvent>()

    val progressEvent = ProgressData()

    init {
        checkDisplayName()
        checkSecureCallsMode()
        checkSemiSecureCallsMode()
        checkPrivateMode()
        checkMicrophoneAvailable()
        checkNotificationWindowEnabled()
        checkIncomingRingtone()
        checkOutGoingRingtone()
        checkAutoDownLoadFilesMode()
        checkDarkMode()
        checkTLMessagesExpireTime()
        checkRemoveTLMessagesMode()
        checkSendTLMessagesMode()
    }

    fun fetchProfile() {
        Timber.tag(TAG).d("fetchProfile")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val profile = profileRepository.fetchProfile()
                handleProfileData(profile)
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_FETCH_PROFILE)
            } finally {
                progressEvent.endProgress()
            }
        }
    }

    private fun handleProfileData(profile: ContactData?) {
        profile?.let {
            chatMainData.postValue(
                ChatInfoDetails.MainData(
                    mainData = profile, infoList = getChatUser(profile)
                )
            )
        }
    }

    private fun getChatUser(profile: ContactData): List<ChatInfoItem> = listOf(
        ChatInfoItem(
            id = 0,
            title = profile.phoneNumber.first(),
            subtitle = "Tap to change phone number",
            contactData = profile,
            type = ChatInfoType.PHONE
        ), ChatInfoItem(
            id = 1,
            title = profile.username,
            subtitle = "Username",
            contactData = profile,
            type = ChatInfoType.USERNAME
        ), ChatInfoItem(
            id = 2,
            title = profile.bio,
            subtitle = "Bio",
            contactData = profile,
            type = ChatInfoType.BIO
        )
    )

    private fun checkDisplayName() {
        val name = appSettings.displayName
        val subtitle =
            if (name.isNullOrEmpty()) resources.getString(R.string.screen_settings_display_name_default) else name
        secureCallsMode.postValue(
            SettingData.UpdatedData(
                subtitle = subtitle
            )
        )
    }

    private fun checkSecureCallsMode() {
        val isChecked = appSettings.isSecureCallsMode
        val subtitle =
            if (isChecked) resources.getString(R.string.screen_settings_secure_calls_subtitle_enabled) else resources.getString(
                R.string.screen_settings_secure_calls_subtitle_disabled
            )
        secureCallsMode.postValue(
            SettingData.UpdatedData(
                subtitle = subtitle, isChecked = isChecked
            )
        )
    }

    private fun checkSemiSecureCallsMode() {
        val isChecked = appSettings.isSemiSecureCallsMode
        val subtitle =
            if (isChecked) resources.getString(R.string.screen_settings_semi_secure_calls_subtitle_enabled) else resources.getString(
                R.string.screen_settings_semi_secure_calls_subtitle_disabled
            )
        semiSecureCallsMode.postValue(
            SettingData.UpdatedData(
                subtitle = subtitle, isChecked = isChecked
            )
        )
    }

    private fun checkPrivateMode() {
        val isChecked = appSettings.isPrivateMode
        val subtitle =
            if (isChecked) resources.getString(R.string.screen_settings_private_mode_enabled) else resources.getString(
                R.string.screen_settings_private_mode_disabled
            )
        isPrivateMode.postValue(
            SettingData.UpdatedData(
                subtitle = subtitle, isChecked = isChecked
            )
        )
    }

    private fun checkMicrophoneAvailable() {
        val isChecked = appSettings.isMicrophoneAvailable
        val subtitle =
            if (isChecked) resources.getString(R.string.screen_settings_microphone_availability_enabled) else resources.getString(
                R.string.screen_settings_microphone_availability_disabled
            )
        isMicrophoneAvailable.postValue(
            SettingData.UpdatedData(
                subtitle = subtitle, isChecked = isChecked
            )
        )
    }

    private fun checkNotificationWindowEnabled() {
        val isChecked = appSettings.isNotificationWindowEnabled
        val subtitle = resources.getString(R.string.screen_settings_notification_window)
        isNotificationWindowEnabled.postValue(
            SettingData.UpdatedData(
                subtitle = subtitle, isChecked = isChecked
            )
        )
    }

    private fun checkIncomingRingtone() {
        val ringtone = appSettings.incomingRingtone
        val subtitle = resources.getString(R.string.screen_settings_incoming_ringtone)
        incomingRingtone.postValue(
            SettingData.UpdatedData(
                subtitle = subtitle
            )
        )
    }

    private fun checkOutGoingRingtone() {
        val ringtone = appSettings.outGoingRingtone
        val subtitle = resources.getString(R.string.screen_settings_outgoing_ringtone)
        outGoingRingtone.postValue(
            SettingData.UpdatedData(
                subtitle = subtitle
            )
        )
    }

    private fun checkAutoDownLoadFilesMode() {
        val isChecked = appSettings.isAutoDownLoadFilesMode
        val subtitle =
            if (isChecked) resources.getString(R.string.screen_settings_secure_calls_subtitle_enabled) else resources.getString(
                R.string.screen_settings_secure_calls_subtitle_disabled
            )
        isAutoDownLoadFilesMode.postValue(
            SettingData.UpdatedData(
                subtitle = subtitle, isChecked = isChecked
            )
        )
    }

    private fun checkDarkMode() {
        val isChecked = appSettings.isDarkMode
        isDarkMode.postValue(
            SettingData.UpdatedData(
                isChecked = isChecked
            )
        )
    }

    private fun checkTLMessagesExpireTime() {
        val tlExpireTime = appSettings.tlMessagesExpireTime
        val subtitle = String.format(
            resources.getString(R.string.screen_settings_time_limited_messages_expire_time),
            tlExpireTime
        )
        tlMessagesExpireTime.postValue(
            SettingData.UpdatedData(
                subtitle = subtitle
            )
        )
    }

    private fun checkRemoveTLMessagesMode() {
        val isChecked = appSettings.isRemoveTLMessagesMode
        val subtitle = resources.getString(R.string.screen_settings_remove_time_limited_messages)
        isRemoveTLMessagesMode.postValue(
            SettingData.UpdatedData(
                subtitle = subtitle, isChecked = isChecked
            )
        )
    }

    private fun checkSendTLMessagesMode() {
        val isChecked = appSettings.isSendTLMessagesMode
        val subtitle = resources.getString(R.string.screen_settings_send_time_limited_messages)
        isSendTLMessagesMode.postValue(
            SettingData.UpdatedData(
                subtitle = subtitle, isChecked = isChecked
            )
        )
    }

    fun setDisplayName(name: String) {
        appSettings.displayName = name
        checkDisplayName()
    }

    fun switchSecureCallsMode(checked: Boolean) {
        appSettings.isSecureCallsMode = checked
        checkSecureCallsMode()
    }

    fun switchSemiSecureCallsMode(checked: Boolean) {
        appSettings.isSemiSecureCallsMode = checked
        checkSemiSecureCallsMode()
    }

    fun switchPrivateMode(checked: Boolean) {
        appSettings.isPrivateMode = checked
        checkPrivateMode()
    }

    fun switchMicrophoneAvailable(checked: Boolean) {
        appSettings.isMicrophoneAvailable = checked
        checkMicrophoneAvailable()
    }

    fun switchNotificationWindowEnabled(checked: Boolean) {
        appSettings.isNotificationWindowEnabled = checked
        checkNotificationWindowEnabled()
    }

    fun switchIncomingRingtone(ringtoneTitle: String) {
        appSettings.incomingRingtone = ringtoneTitle
        checkIncomingRingtone()
    }

    fun switchOutGoingRingtone(ringtoneTitle: String) {
        appSettings.outGoingRingtone = ringtoneTitle
        checkOutGoingRingtone()
    }

    fun switchAutoDownLoadFilesMode(checked: Boolean) {
        appSettings.isAutoDownLoadFilesMode = checked
        checkAutoDownLoadFilesMode()
    }

    fun switchDarkMode(checked: Boolean) {
        appSettings.isDarkMode = checked
        checkDarkMode()
    }

    fun setTLMessagesExpireTime(time: String) {
        appSettings.tlMessagesExpireTime = time
        checkTLMessagesExpireTime()
    }

    fun switchRemoveTLMessagesMode(checked: Boolean) {
        appSettings.isRemoveTLMessagesMode = checked
        checkRemoveTLMessagesMode()
    }

    fun switchSendTLMessagesMode(checked: Boolean) {
        appSettings.isSendTLMessagesMode = checked
        checkSendTLMessagesMode()
    }

    fun logout() {
        apiSettings.clearCache()
        appSettings.clearCache()
        settingsEvent.postRawValue(SettingsEvent.EVENT_RESTART)
    }


    fun openNotificationSoundsSettings() {
        Timber.tag(TAG).d("openNotificationSoundsSettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToNotificationsSettingsFragment()
        )
    }

    fun openPrivacySettings() {
        Timber.tag(TAG).d("openPrivacySettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToSecuritySettingsFragment()
        )
    }

    fun openDataStorageSettings() {
        Timber.tag(TAG).d("openDataStorageSettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToStorageSettingsFragment()
        )
    }

    fun openChatsSettings() {
        Timber.tag(TAG).d("openChatsSettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToChatsSettingsFragment()
        )
    }

    fun openFoldersSettings() {
        Timber.tag(TAG).d("openFoldersSettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToFoldersSettingsFragment()
        )
    }

    fun openDevicesSettings() {
        Timber.tag(TAG).d("openDevicesSettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToDevicesSettingsFragment()
        )
    }

    fun openLanguageSettings() {
        Timber.tag(TAG).d("openLanguageSettings")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToLanguageSettingsFragment()
        )
    }

    fun openSupportHelp() {
        Timber.tag(TAG).d("openSupportHelp")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToSupportHelpFragment()
        )
    }

    fun openFAQHelp() {
        Timber.tag(TAG).d("openFAQHelp")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToFaqHelpFragment()
        )
    }

    fun openPrivacyPolicyHelp() {
        Timber.tag(TAG).d("openPrivacyPolicyHelp")
        fragNavEvent.postRawValue(
            ProfileFragmentDirections.actionProfileFragmentToPrivacyPolicyHelpFragment()
        )
    }
}
package com.messenger.features.main.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.messenger.databinding.FragmentSplashBinding

class FragmentSplash : Fragment() {

    private var binding: FragmentSplashBinding? = null

    companion object {
        private val TAG = FragmentSplash::class.java.simpleName

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSplashBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}

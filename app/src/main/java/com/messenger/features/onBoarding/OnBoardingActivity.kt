package com.messenger.features.onBoarding

import android.content.Intent
import android.os.Bundle
import timber.log.Timber
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import com.messenger.R
import com.messenger.databinding.ActivityOnboardingBinding
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.viewModel

class OnBoardingActivity : AppCompatActivity() {

    private val viewModel: OnBoardingViewModel by viewModel()
    private lateinit var binding: ActivityOnboardingBinding

    private companion object {
        private val TAG = OnBoardingActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnboardingBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Timber.tag(TAG).d("start screen")
        initObservers()
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.navigationEvent.observe(this) { event ->
            handleNavigationEvent(event)
        }
        viewModel.serviceIntent.observe(this) { intent ->
            startServiceIntent(intent)
        }
        viewModel.setStartNavigation()
    }

    private fun handleNavigationEvent(event: LiveEvent<NavDirections>?) {
        event?.getContentIfNotHandled()?.let {
            Timber.tag(TAG).d("handleNavigationEvent")
            try {
                findNavController(R.id.navigationContainer).navigate(it)
            } catch (ex: java.lang.Exception) {
                Timber.tag(TAG).d("handleNavigationEvent Error: ${ex.message}")
            }
        }
    }

    private fun startServiceIntent(intent: LiveEvent<Intent>?) {
        intent?.getContentIfNotHandled()?.let {
            Timber.tag(TAG).d("startServiceIntent")
            startService(it)
        }
    }
}
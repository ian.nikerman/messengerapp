package com.messenger.features.onBoarding

import android.content.Intent
import timber.log.Timber
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.messenger.cache.AppSettings
import com.messenger.features.onBoarding.agreement.AgreementFragmentDirections
import com.messenger.features.onBoarding.emailCode.EnterEmailCodeFragment
import com.messenger.features.onBoarding.emailCode.EnterEmailCodeFragmentDirections
import com.messenger.features.onBoarding.emailCode.dialogs.FailedRegistrationDialog
import com.messenger.features.onBoarding.emailCode.dialogs.FailedRegistrationDialogDirections
import com.messenger.features.onBoarding.emailCode.dialogs.IncorrectRegistrationCodeDialog
import com.messenger.features.onBoarding.emailCode.dialogs.IncorrectRegistrationCodeDialogDirections
import com.messenger.features.onBoarding.enterDomain.EnterDomainFragment
import com.messenger.features.onBoarding.enterDomain.EnterDomainFragmentDirections
import com.messenger.features.onBoarding.enterEmail.EnterEmailFragmentDirections
import com.messenger.features.onBoarding.enterPhone.EnterPhoneFragmentDirections
import com.messenger.features.onBoarding.enterPhone.dialogs.ConfirmPhoneDialogDirections
import com.messenger.features.onBoarding.phoneCode.EnterPhoneCodeFragment
import com.messenger.features.onBoarding.phoneCode.EnterPhoneCodeFragmentDirections
import com.messenger.features.onBoarding.phoneCode.dialogs.FailedActivationDialog
import com.messenger.features.onBoarding.phoneCode.dialogs.FailedActivationDialogDirections
import com.messenger.features.onBoarding.phoneCode.dialogs.IncorrectCodeDialog
import com.messenger.features.onBoarding.phoneCode.dialogs.IncorrectCodeDialogDirections
import com.messenger.features.onBoarding.start.StartRegistrationFragmentDirections
import com.messenger.utils.Constants
import com.messenger.utils.extensions.LiveEventData

class OnBoardingViewModel(
    private val appSettings: AppSettings
) : ViewModel() {

    companion object {
        private val TAG = OnBoardingViewModel::class.java.simpleName
        private var phoneNumber = ""
    }

    val navigationEvent = LiveEventData<NavDirections>()
    val serviceIntent = LiveEventData<Intent>()

    fun setStartNavigation() {
        Timber.tag(TAG).d("start navigation: ${appSettings.setupStage}")
        when (appSettings.setupStage) {
            Constants.Companion.SetupStage.enter_domain.name -> navigationEvent.postRawValue(
                StartRegistrationFragmentDirections.actionStartRegistrationFragmentToEnterDomainFragment()
            )
            Constants.Companion.SetupStage.enter_email.name -> navigationEvent.postRawValue(
                StartRegistrationFragmentDirections.actionStartRegistrationFragmentToEnterEmailFragment()
            )
            Constants.Companion.SetupStage.enter_phone.name -> navigationEvent.postRawValue(
                StartRegistrationFragmentDirections.actionStartRegistrationFragmentToEnterPhoneFragment()
            )
            Constants.Companion.SetupStage.all_set.name -> navigationEvent.postRawValue(
                StartRegistrationFragmentDirections.actionStartRegistrationFragmentToAllSetFragment()
            )
            else -> appSettings.setupStage = Constants.Companion.SetupStage.start_registration.name
        }
    }

    fun openAgreementFragment() {
        Timber.tag(TAG).d("openAgreement")
        navigationEvent.postRawValue(
            StartRegistrationFragmentDirections.actionStartRegistrationFragmentToAgreementFragment()
        )
    }

    fun openEnterDomainFragment() {
        Timber.tag(TAG).d("openEnterDomain")
        appSettings.setupStage = Constants.Companion.SetupStage.enter_domain.name
        navigationEvent.postRawValue(AgreementFragmentDirections.actionAgreementFragmentToEnterDomainFragment())
    }

    fun openEnterEmailFragment(callingFragment: Fragment?) {
        Timber.tag(TAG).d("openEnterEmail")
        appSettings.setupStage = Constants.Companion.SetupStage.enter_email.name

        val action = when (callingFragment) {
            is EnterDomainFragment -> {
                EnterDomainFragmentDirections.actionEnterDomainFragmentToEnterEmailFragment()
            }
            is IncorrectRegistrationCodeDialog -> {
                IncorrectRegistrationCodeDialogDirections.actionIncorrectRegistrationDialogToEnterEmailFragment()
            }
            is FailedRegistrationDialog -> {
                FailedRegistrationDialogDirections.actionFailedRegistrationDialogToEnterEmailFragment()
            }
            else -> {
                null
            }
        }
        action?.let {
            navigationEvent.postRawValue(action)
        }
    }

    fun openEnterEmailCodeFragment() {
        Timber.tag(TAG).d("openEnterEmailCodeFragment")
        navigationEvent.postRawValue(EnterEmailFragmentDirections.actionEnterEmailFragmentToEnterEmailCodeFragment())
    }

    fun openEnterPhoneFragment(callingFragment: Fragment?) {
        Timber.tag(TAG).d("openEnterPhone")
        appSettings.setupStage = Constants.Companion.SetupStage.enter_phone.name

        val action = when (callingFragment) {
            is EnterEmailCodeFragment -> {
                EnterEmailCodeFragmentDirections.actionEnterEmailCodeFragmentToEnterPhoneFragment()
            }
            is EnterPhoneCodeFragment -> {
                EnterPhoneCodeFragmentDirections.actionEnterPhoneCodeFragmentToEnterPhoneFragment(
                )
            }
            is IncorrectCodeDialog -> {
                IncorrectCodeDialogDirections.actionIncorrectPhoneDialogToEnterPhoneFragment()
            }
            is FailedActivationDialog -> {
                FailedActivationDialogDirections.actionFailedActivationDialogToEnterPhoneFragment()
            }
            else -> {
                null
            }
        }
        action?.let {
            navigationEvent.postRawValue(action)
        }
    }

    fun openEnterPhoneCodeFragment(phone: String) {
        Timber.tag(TAG).d("openEnterPhoneCodeFragment")
        navigationEvent.postRawValue(
            ConfirmPhoneDialogDirections.actionConfirmPhoneDialogToEnterPhoneCodeFragment(
                phone
            )
        )
    }

    fun openAllSetFragment() {
        Timber.tag(TAG).d("openAllSetFragment")
        appSettings.setupStage = Constants.Companion.SetupStage.all_set.name
        navigationEvent.postRawValue(EnterPhoneCodeFragmentDirections.actionEnterPhoneCodeFragmentToAllSetFragment())
    }

    fun openRegistrationCodeIncorrectDialog() {
        Timber.tag(TAG).d("openRegistrationCodeIncorrectDialog")
        navigationEvent.postRawValue(
            EnterEmailCodeFragmentDirections.actionEnterEmailCodeFragmentToIncorrectRegistrationDialog()
        )
    }

    fun openFailedRegistrationDialog() {
        Timber.tag(TAG).d("openFailedRegistrationDialog")
        navigationEvent.postRawValue(
            EnterEmailCodeFragmentDirections.actionEnterEmailCodeFragmentToFailedRegistrationDialog()
        )
    }

    fun openActivationCodeIncorrectDialog() {
        Timber.tag(TAG).d("openActivationCodeIncorrectDialog")
        navigationEvent.postRawValue(
            EnterPhoneCodeFragmentDirections.actionEnterPhoneCodeFragmentToIncorrectPhoneDialog()
        )
    }

    fun openFailedActivationDialog() {
        Timber.tag(TAG).d("openFailedActivationDialog")
        navigationEvent.postRawValue(
            EnterPhoneCodeFragmentDirections.actionEnterPhoneCodeFragmentToFailedActivationDialog()
        )
    }

    fun openConfirmPhoneDialog(phone: String) {
        Timber.tag(TAG).d("openConfirmPhoneDialog")
        navigationEvent.postRawValue(
            EnterPhoneFragmentDirections.actionEnterPhoneFragmentToConfirmPhoneDialog(
                phone
            )
        )
    }

    fun resendSmsIntent() {
        sendSmsIntent(phoneNumber)
    }

    fun sendSmsIntent(phone: String) {
        Timber.tag(TAG).d("sendSmsIntent")
        phoneNumber = phone
        /*Intent().apply {
            component = ComponentName(
                PACKAGE_K_CORE, PACKAGE_K_CORE_MANUAL_REGISTRATION
            )

            action = ACTION_RECEIVE_SMS
            putExtra(KEY_EXTRA, phone)
            serviceIntent.postRawValue(this)
        }*/
    }
}
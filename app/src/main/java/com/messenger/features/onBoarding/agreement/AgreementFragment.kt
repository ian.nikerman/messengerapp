package com.messenger.features.onBoarding.agreement

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.messenger.databinding.FragmentAgreementBinding
import com.messenger.features.onBoarding.OnBoardingViewModel
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class AgreementFragment : Fragment() {

    private val sharedViewModel by activityViewModel<OnBoardingViewModel>()
    private val viewModel: AgreementViewModel by viewModel()
    private var binding: FragmentAgreementBinding? = null

    companion object {
        private val TAG = AgreementFragment::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.tag(TAG).d("start screen")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAgreementBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(requireView(), savedInstanceState)
        initUi()
        initObservers()
    }

    private fun initUi() {
        Timber.tag(TAG).d("initUi")
        binding?.apply {

            eulaCB.setOnClickListener { viewModel.setEulaCheckState(eulaCB.isChecked) }
            privacyPolicyCB.setOnClickListener {
                viewModel.setPrivacyPolicyCheckState(
                    privacyPolicyCB.isChecked
                )
            }
            termsCB.setOnClickListener { viewModel.setTermsCheckState(termsCB.isChecked) }

            eulaClickableTitle.setOnClickListener { viewModel.showEulaPage() }
            privacyPolicyClickableTitle.setOnClickListener { viewModel.showPrivacyPolicyPage() }
            termsClickableTitle.setOnClickListener { viewModel.showTermsPage() }

            continueBtn.setOnClickListener {
                Timber.tag(TAG).d("next click")
                sharedViewModel.openEnterDomainFragment()
            }
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")

        viewModel.nextButtonState.observe(viewLifecycleOwner) { state ->
            handleNextButtonState(state)
        }
        viewModel.webData.observe(viewLifecycleOwner) { data ->
            handleWebData(data)
        }
        viewModel.eulaData.observe(viewLifecycleOwner) { data ->
            handleEulaData(data)
        }
    }

    private fun handleEulaData(data: String?) {
        data?.let { text ->
            Timber.tag(TAG).d("handleEulaData")
            binding?.eulaWebView?.apply {
                settings
                setBackgroundColor(Color.TRANSPARENT)
                loadDataWithBaseURL(
                    null, text, "text/html", "utf-8", null
                )
            }
        }
    }

    private fun handleWebData(data: LiveEvent<String>?) {
        data?.getContentIfNotHandled()?.let { webUrl ->
            Timber.tag(TAG).d("handleWebData")
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(webUrl)))
        }
    }

    private fun handleNextButtonState(state: AgreementViewModel.NextButtonState?) {
        state?.let {
            when (it) {
                AgreementViewModel.NextButtonState.STATE_ENABLED -> {
                    binding?.continueBtn?.isEnabled = true
                    binding?.continueBtn?.alpha = 1f
                }

                AgreementViewModel.NextButtonState.STATE_DISABLED -> {
                    binding?.continueBtn?.isEnabled = false
                    binding?.continueBtn?.alpha = .5f
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
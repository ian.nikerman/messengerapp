package com.messenger.features.onBoarding.agreement

import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.messenger.R
import com.messenger.utils.extensions.LiveEventData
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream

class AgreementViewModel(private val resources: Resources) : ViewModel() {

    companion object {

        private val TAG = AgreementViewModel::class.java.simpleName
        private var isEulaAccepted = false
        private var isPrivacyPolicyAccepted = false
        private var isTermsAccepted = false

    }

    enum class NextButtonState {
        STATE_ENABLED, STATE_DISABLED
    }

    val nextButtonState = MutableLiveData<NextButtonState>()
    val webData = LiveEventData<String>()
    val eulaData = MutableLiveData<String>()

    init {
        prepareEula()
        validateAgreement()
    }

    private fun prepareEula() {
//        val eulaText = readTextFromResource()
//        eulaData.postValue(eulaText)
    }

//    private fun readTextFromResource(): String {
//        val resourceID: Int = R.raw.eula_text
//
//        val raw: InputStream = resources.openRawResource(resourceID)
//        val stream = ByteArrayOutputStream()
//        var i: Int
//        try {
//            i = raw.read()
//            while (i != -1) {
//                stream.write(i)
//                i = raw.read()
//            }
//            raw.close()
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }
//
//        return stream.toString()
//    }

    fun setEulaCheckState(checked: Boolean) {
        isEulaAccepted = checked
        validateAgreement()
    }

    fun setPrivacyPolicyCheckState(checked: Boolean) {
        isPrivacyPolicyAccepted = checked
        validateAgreement()
    }

    fun setTermsCheckState(checked: Boolean) {
        isTermsAccepted = checked
        validateAgreement()
    }

    private fun validateAgreement() {
        when {
            isEulaAccepted && isPrivacyPolicyAccepted && isTermsAccepted -> nextButtonState.postValue(
                NextButtonState.STATE_ENABLED
            )
            else -> nextButtonState.postValue(
                NextButtonState.STATE_DISABLED
            )
        }
    }

    fun showEulaPage() {
        webData.postRawValue("https://www.google.com")
    }

    fun showPrivacyPolicyPage() {
        webData.postRawValue("https://www.google.com")
    }

    fun showTermsPage() {
        webData.postRawValue("https://www.google.com")
    }
}
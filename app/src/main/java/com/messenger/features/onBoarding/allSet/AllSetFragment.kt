package com.messenger.features.onBoarding.allSet

import android.content.Intent
import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.messenger.databinding.FragmentAllSetBinding
import com.messenger.features.main.MainActivity

class AllSetFragment : Fragment() {

    private var binding: FragmentAllSetBinding? = null

    companion object {
        private val TAG = AllSetFragment::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.tag(TAG).d("start screen")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAllSetBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(requireView(), savedInstanceState)
        initUi()
    }

    private fun initUi() {
        Timber.tag(TAG).d("initUi")
        binding?.apply {
            startBtn.setOnClickListener {
                Timber.tag(TAG).d("next click")
                requireActivity().finish()
                startActivity(Intent(requireActivity(), MainActivity::class.java))
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
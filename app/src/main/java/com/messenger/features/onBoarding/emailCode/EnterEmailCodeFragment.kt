package com.messenger.features.onBoarding.emailCode

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.text.Editable
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.messenger.R
import com.messenger.databinding.FragmentEnterEmailCodeBinding
import com.messenger.features.onBoarding.OnBoardingViewModel
import com.messenger.features.views.NumPadView
import com.messenger.utils.Constants
import com.messenger.utils.Constants.Companion.USE_FOR_SMS_CODE_MAX_VALUE
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class EnterEmailCodeFragment : Fragment() {

    private val sharedViewModel by activityViewModel<OnBoardingViewModel>()
    private val viewModel: EnterEmailCodeViewModel by viewModel()
    private var binding: FragmentEnterEmailCodeBinding? = null

    companion object {
        private val TAG = EnterEmailCodeFragment::class.java.simpleName
        private lateinit var registrationBroadcastReceiver: BroadcastReceiver
    }

    private val numPadListener = object : NumPadView.NumPadViewListener {
        override fun onTypedCode(code: String) {
            viewModel.setTypedCode(code, false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEnterEmailCodeBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(requireView(), savedInstanceState)
        Timber.tag(TAG).d("start screen")
        initUi()
        initObservers()
    }

    private fun initUi() {
        Timber.tag(TAG).d("initUi")
        binding?.apply {
            numPad.setListener(numPadListener)
            numPad.setMaxNumbers(USE_FOR_SMS_CODE_MAX_VALUE)

            clearBtn.setOnClickListener {
                numPad.clearInput()
            }

            nextBtn.setOnClickListener {
                viewModel.sendCode()
            }

            registerRegistrationReceiver()
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.serviceIntent.observe(viewLifecycleOwner) { intent ->
            startServiceIntent(intent)
        }
        viewModel.confirmedPhoneData.observe(viewLifecycleOwner) { event ->
            handleNavigationEvent(event)
        }
        viewModel.nextButtonState.observe(viewLifecycleOwner) { state ->
            handleNextButtonState(state)
        }
        viewModel.clearButtonState.observe(viewLifecycleOwner) { state ->
            handleClearButtonState(state)
        }
        viewModel.inputLineState.observe(viewLifecycleOwner) { event ->
            handleInputUnderScoreState(event)
        }
        viewModel.typedCode.observe(viewLifecycleOwner) { data ->
            handleTypedCode(data)
        }
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
    }

    private fun handleNavigationEvent(event: LiveEvent<EnterEmailCodeViewModel.CodeConfirmationState>?) {
        event?.getContentIfNotHandled()?.let {
            when (it) {
                EnterEmailCodeViewModel.CodeConfirmationState.CONFIRMED -> sharedViewModel.openEnterPhoneFragment(
                    this@EnterEmailCodeFragment
                )

                EnterEmailCodeViewModel.CodeConfirmationState.FAILED -> sharedViewModel.openRegistrationCodeIncorrectDialog()
            }
        }
    }

    private fun handleNextButtonState(state: EnterEmailCodeViewModel.ButtonState?) {
        state?.let {
            when (it) {
                EnterEmailCodeViewModel.ButtonState.STATE_ENABLED -> {
                    enableButton(binding?.nextBtn)
                }

                EnterEmailCodeViewModel.ButtonState.STATE_DISABLED -> {
                    disableButton(binding?.nextBtn)
                }

                EnterEmailCodeViewModel.ButtonState.STATE_INVISIBLE -> {
                    hideButton(binding?.nextBtn)
                }
            }
        }
    }

    private fun handleClearButtonState(state: EnterEmailCodeViewModel.ButtonState?) {
        state?.let {
            when (it) {
                EnterEmailCodeViewModel.ButtonState.STATE_ENABLED -> {
                    enableButton(binding?.clearBtn)
                }

                EnterEmailCodeViewModel.ButtonState.STATE_DISABLED -> {
                    disableButton(binding?.clearBtn)
                }

                EnterEmailCodeViewModel.ButtonState.STATE_INVISIBLE -> {
                    hideButton(binding?.clearBtn)
                }
            }
        }
    }

    private fun handleInputUnderScoreState(state: EnterEmailCodeViewModel.InputLineState?) {
        state?.let {
            when (it) {
                EnterEmailCodeViewModel.InputLineState.STATE_DEFAULT -> {
                    showInputLineDefault()
                }

                EnterEmailCodeViewModel.InputLineState.STATE_CORRECT -> {
                    showInputLineCorrect()
                }

                EnterEmailCodeViewModel.InputLineState.STATE_SUCCESS -> {
                    showInputLineSuccess()
                }

                EnterEmailCodeViewModel.InputLineState.STATE_FAILED -> {
                    showInputLineFailed()
                }
            }
        }
    }

    private fun handleTypedCode(code: String?) {
        code?.let {
            binding?.codeEdittext?.text = Editable.Factory.getInstance().newEditable(it)
        }
    }

    private fun startServiceIntent(intent: LiveEvent<Intent>?) {
        intent?.getContentIfNotHandled()?.let {
            Timber.tag(TAG).d("startServiceIntent")
            requireActivity().startService(it)
            sharedViewModel.openEnterPhoneFragment(this@EnterEmailCodeFragment)
        }
    }

    private fun handleProgressEvent(isLoading: Boolean) {
        binding?.progress?.setProgressEvent(isLoading)
    }

    private fun enableButton(button: TextView?) {
        button?.isEnabled = true
        button?.alpha = 1f
        button?.isVisible = true
    }

    private fun disableButton(button: TextView?) {
        button?.isEnabled = false
        button?.alpha = .5f
        button?.isVisible = true
    }

    private fun hideButton(button: TextView?) {
        button?.isVisible = false
        button?.alpha = 1f
    }

    private fun showInputLineDefault() {
        binding?.inputUnderline?.apply {
            isVisible = false
        }
    }

    private fun showInputLineCorrect() {
        binding?.inputUnderline?.apply {
            setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.white_85
                )
            )
            isVisible = true
        }
    }

    private fun showInputLineSuccess() {
        binding?.inputUnderline?.apply {
            setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.light_green
                )
            )
            isVisible = true
        }
    }

    private fun showInputLineFailed() {
        binding?.inputUnderline?.apply {
            setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.dark_green
                )
            )
            isVisible = true
        }
    }

    override fun onDestroyView() {
        requireActivity().unregisterReceiver(registrationBroadcastReceiver)
        super.onDestroyView()
        binding = null
    }

    private fun registerRegistrationReceiver() {
        Timber.tag(TAG).d("registerRegistrationReceiver")
        registrationBroadcastReceiver = RegistrationBroadcastReceiver()
        requireActivity().registerReceiver(
            registrationBroadcastReceiver,
            IntentFilter(Constants.INTENT_FILTER_REGISTRATION)
        )
    }

    private inner class RegistrationBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            val result = intent?.getStringExtra(Constants.ARG_REG_RESULT)
            Timber.tag(TAG).d("registrationResult $result")
            result?.let {
                Timber.tag(TAG).d("RegistrationBroadcastReceiver onReceive: $it")
                when (it) {
                    Constants.Companion.RegistrationCodeErrors.success.name -> {
                        viewModel.showSuccessState()
                        sharedViewModel.openEnterPhoneFragment(this@EnterEmailCodeFragment)
                    }

                    Constants.Companion.RegistrationCodeErrors.incorrect_code.name -> {
                        viewModel.showFailedState()
                        sharedViewModel.openRegistrationCodeIncorrectDialog()
                    }

                    Constants.Companion.RegistrationCodeErrors.unknown_error.name -> {
                        viewModel.showFailedState()
                        sharedViewModel.openFailedRegistrationDialog()
                    }
                }
            }
        }
    }
}

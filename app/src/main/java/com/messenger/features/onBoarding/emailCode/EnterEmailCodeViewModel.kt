package com.messenger.features.onBoarding.emailCode

import android.content.Intent
import timber.log.Timber
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import com.messenger.utils.extensions.isValidSmsCode
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class EnterEmailCodeViewModel : ViewModel() {

    companion object {
        private val TAG = EnterEmailCodeViewModel::class.java.simpleName
        private var enteredCode = ""
    }

    enum class ButtonState {
        STATE_ENABLED, STATE_DISABLED, STATE_INVISIBLE
    }

    val nextButtonState = MutableLiveData<ButtonState>()
    val clearButtonState = MutableLiveData<ButtonState>()

    enum class InputLineState {
        STATE_DEFAULT, STATE_CORRECT, STATE_SUCCESS, STATE_FAILED
    }

    val inputLineState = MutableLiveData<InputLineState>()

    enum class CodeConfirmationState {
        CONFIRMED, FAILED
    }

    val confirmedPhoneData = LiveEventData<CodeConfirmationState>()

    val serviceIntent = LiveEventData<Intent>()
    val typedCode = MutableLiveData<String>()
    val progressEvent = ProgressData()

    init {
        validateCode()
    }

    fun setTypedCode(code: String, isAutoComplete: Boolean) {
        enteredCode = code
        validateCode()

        if (code.isEmpty()) {
            typedCode.postValue("")
        } else {
            var asterisk = ""
            repeat((code.indices).count()) {
                asterisk += "·"
            }
            typedCode.postValue(code)
            if (isAutoComplete) {
                Timber.tag(TAG).d("isAutoComplete: $isAutoComplete")
                validateEnteredSmsCode()
            }
        }
    }

    private fun validateCode() {
        when {
            enteredCode.isValidSmsCode() -> nextButtonState.postValue(ButtonState.STATE_ENABLED)
            else -> nextButtonState.postValue(ButtonState.STATE_DISABLED)
        }
        when {
            enteredCode.isNotEmpty() -> clearButtonState.postValue(ButtonState.STATE_ENABLED)
            else -> clearButtonState.postValue(ButtonState.STATE_DISABLED)
        }
        when {
            enteredCode.isValidSmsCode() -> inputLineState.postValue(
                InputLineState.STATE_CORRECT
            )

            else -> inputLineState.postValue(InputLineState.STATE_DEFAULT)
        }
    }

    private fun validateEnteredSmsCode() {
        Timber.tag(TAG).d("validateEnteredSmsCode")
        sendCode()
    }

    fun sendCode() {
        Timber.tag(TAG).d("sendCode")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                delay(2_000L)
                /*Intent().apply {
                    component = ComponentName(
                        PACKAGE_K_CORE, PACKAGE_K_CORE_MANUAL_REGISTRATION
                    )

                    action = ACTION_CHECK_REG_CODE
                    putExtra(
                        KEY_EXTRA, getFormattedCode()
                    )

                    putExtra(
                        KEY_WIZARD_CHECK_CODE, true
                    )

                    putExtra(KEY_CALL_BY, true)
                    serviceIntent.postRawValue(this)
                }*/
                delay(2_000L)
                showSuccessState()
                delay(1_000L)
                confirmedPhoneData.postRawValue(CodeConfirmationState.CONFIRMED)
                Timber.tag(TAG).d("sendCode success")
//                when {
//                    enteredCode.contains("3") -> {
//                        showSuccessState()
//                        delay(1_000L)
//                        confirmedPhoneData.postRawValue(CodeConfirmationState.CONFIRMED)
//                        Timber.tag(TAG).d("sendCode success")
//                    }
//
//                    else -> {
//                        showFailedState()
//                        delay(1_000L)
//                        confirmedPhoneData.postRawValue(CodeConfirmationState.FAILED)
//                    }
//                }
            } catch (ex: Exception) {
                Timber.tag(TAG).d("sendCode failed: $ex")
            }
        }
    }

    private fun getFormattedCode() = enteredCode.substring(0, 3) + "-" + enteredCode.substring(
        3, enteredCode.length
    )

    fun showFailedState() {
        inputLineState.postValue(
            InputLineState.STATE_FAILED
        )
        progressEvent.endProgress()
    }

    fun showSuccessState() {
        inputLineState.postValue(
            InputLineState.STATE_SUCCESS
        )
        progressEvent.endProgress()
    }
}
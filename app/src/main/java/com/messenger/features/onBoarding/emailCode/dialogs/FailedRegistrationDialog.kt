package com.messenger.features.onBoarding.emailCode.dialogs

import android.os.Bundle
import timber.log.Timber
import android.view.View
import com.messenger.R
import com.messenger.features.onBoarding.OnBoardingViewModel
import com.messenger.features.views.GeneralAlertDialog
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class FailedRegistrationDialog : GeneralAlertDialog() {

    private val setupViewModel by activityViewModel<OnBoardingViewModel>()

    companion object {
        private val TAG = FailedRegistrationDialog::class.java.simpleName
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.tag(TAG).d("start screen")
    }

    override fun onOkClick() {
        Timber.tag(TAG).d("onOkClick")
        setupViewModel.resendSmsIntent()
        dismiss()
    }

    override fun onCancelClick() {
        Timber.tag(TAG).d("onCancelClick")
        setupViewModel.openEnterEmailFragment(this@FailedRegistrationDialog)
    }

    override fun isEdiTextShown(): Int {
        return View.GONE
    }

    override fun getTitle(): String {
        return getString(R.string.dialog_failed_registration_title)
    }

    override fun getSubtitle(): String {
        return getString(R.string.dialog_failed_activation_sub_title)
    }

    override fun getTitleImage(): Int {
        return R.drawable.ic_error
    }

    override fun getCancelButtonText(): String {
        return getString(R.string.button_reenter_email)
    }

    override fun getOkButtonText(): String {
        return getString(R.string.button_try_again)
    }
}
package com.messenger.features.onBoarding.emailCode.dialogs

import android.view.View
import com.messenger.R
import com.messenger.features.onBoarding.OnBoardingViewModel
import com.messenger.features.views.GeneralAlertDialog
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class IncorrectRegistrationCodeDialog : GeneralAlertDialog() {

    private val sharedViewModel by activityViewModel<OnBoardingViewModel>()

    override fun onOkClick() {
        sharedViewModel.resendSmsIntent()
        dismiss()
    }

    override fun onCancelClick() {
        sharedViewModel.openEnterEmailFragment(this@IncorrectRegistrationCodeDialog)
    }

    override fun isEdiTextShown(): Int {
        return View.GONE
    }

    override fun getTitle(): String {
        return getString(R.string.dialog_incorrect_code_title)
    }

    override fun getSubtitle(): String {
        return getString(R.string.dialog_incorrect_code_sub_title)
    }

    override fun getTitleImage(): Int {
        return R.drawable.ic_error
    }

    override fun getCancelButtonText(): String {
        return getString(R.string.button_reenter_email)
    }

    override fun getOkButtonText(): String {
        return getString(R.string.button_try_again)
    }
}
package com.messenger.features.onBoarding.enterDomain

import android.os.Bundle
import android.text.Editable
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.messenger.R
import com.messenger.databinding.FragmentEnterDomainBinding
import com.messenger.features.onBoarding.OnBoardingViewModel
import com.messenger.utils.WizardUtils
import com.messenger.utils.extensions.AfterTextChangedWatcher
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

open class EnterDomainFragment : Fragment() {

    private val sharedViewModel by activityViewModel<OnBoardingViewModel>()
    private val viewModel: EnterDomainViewModel by viewModel()
    private var binding: FragmentEnterDomainBinding? = null

    companion object {
        private val TAG = EnterDomainFragment::class.java.simpleName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEnterDomainBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(requireView(), savedInstanceState)
        Timber.tag(TAG).d("start screen")
        initUi()
        initObservers()
    }

    private fun initUi() {
        Timber.tag(TAG).d("initUi")
        binding?.apply {
            nextBtn.setOnClickListener {
                viewModel.sendDomain()
            }
            clearBtn.setOnClickListener {
                domainEdittext.text = Editable.Factory.getInstance().newEditable("")
            }
            domainEdittext.addTextChangedListener(object : AfterTextChangedWatcher() {
                override fun afterTextChanged(s: Editable?) {
                    viewModel.setTypedDomain(s?.toString() ?: "")
                }
            })
            domainLayout.setOnClickListener {
                domainEdittext.requestFocus()
            }
            domainEdittext.requestFocus()
            WizardUtils.showKeyBoard(requireActivity())
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.uiState.observe(viewLifecycleOwner) { state ->
            handleDomainRegistrationState(state)
        }
        viewModel.confirmedDomainData.observe(viewLifecycleOwner) { event ->
            handleNavigationEvent(event)
        }
        viewModel.nextButtonState.observe(viewLifecycleOwner) { state ->
            handleNextButtonState(state)
        }
        viewModel.clearButtonState.observe(viewLifecycleOwner) { state ->
            handleClearButtonState(state)
        }
        viewModel.inputLineState.observe(viewLifecycleOwner) { event ->
            handleInputUnderScoreState(event)
        }
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
    }

    private fun handleDomainRegistrationState(state: EnterDomainViewModel.DomainRegistrationState?) {
        state?.let {
            when (it) {
                EnterDomainViewModel.DomainRegistrationState.STATE_SUCCEEDED -> {
                    showSucceededSetDomainUi()
                }

                EnterDomainViewModel.DomainRegistrationState.STATE_FAILED -> {
                    showFailedSetDomainUi()
                }

                EnterDomainViewModel.DomainRegistrationState.STATE_DEFAULT -> {
                    showDefaultSetDomainUi()
                }
            }
        }
    }

    private fun handleNextButtonState(state: EnterDomainViewModel.ButtonState?) {
        state?.let {
            when (it) {
                EnterDomainViewModel.ButtonState.STATE_ENABLED -> {
                    enableButton(binding?.nextBtn)
                }

                EnterDomainViewModel.ButtonState.STATE_DISABLED -> {
                    disableButton(binding?.nextBtn)
                }

                EnterDomainViewModel.ButtonState.STATE_INVISIBLE -> {
                    hideButton(binding?.nextBtn)
                }
            }
        }
    }

    private fun handleClearButtonState(state: EnterDomainViewModel.ButtonState?) {
        state?.let {
            when (it) {
                EnterDomainViewModel.ButtonState.STATE_ENABLED -> {
                    enableButton(binding?.clearBtn)
                }

                EnterDomainViewModel.ButtonState.STATE_DISABLED -> {
                    disableButton(binding?.clearBtn)
                }

                EnterDomainViewModel.ButtonState.STATE_INVISIBLE -> {
                    hideButton(binding?.clearBtn)
                }
            }
        }
    }

    private fun handleInputUnderScoreState(state: EnterDomainViewModel.InputLineState?) {
        state?.let {
            when (it) {
                EnterDomainViewModel.InputLineState.STATE_DEFAULT -> {
                    showInputLineDefault()
                }

                EnterDomainViewModel.InputLineState.STATE_CORRECT -> {
                    showInputLineCorrect()
                }

                EnterDomainViewModel.InputLineState.STATE_SUCCESS -> {
                    showInputLineSuccess()
                }

                EnterDomainViewModel.InputLineState.STATE_FAILED -> {
                    showInputLineFailed()
                }
            }
        }
    }

    private fun handleNavigationEvent(data: LiveEvent<String>?) {
        data?.getContentIfNotHandled()?.let {
            Timber.tag(TAG).d("handleNavigationEvent")
            WizardUtils.closeKeyBoard(requireActivity())
            sharedViewModel.openEnterEmailFragment(this@EnterDomainFragment)
        }
    }

    private fun handleProgressEvent(isLoading: Boolean) {
        binding?.progress?.setProgressEvent(isLoading)
    }

    private fun showDefaultSetDomainUi() {
        binding?.apply {
            headerIcon.setImageResource(R.drawable.ic_manage_accounts)
            editTextIconStatus.isVisible = false
            infoIcon.isVisible = true
            infoStatus.text = resources.getString(R.string.screen_enter_domain_info_default_title)
            infoStatus.setTextAppearance(R.style.InfoStatusDefaultTextStyle)
        }
    }

    private fun showSucceededSetDomainUi() {
        binding?.apply {
            headerIcon.setImageResource(R.drawable.ic_manage_accounts_success)
            editTextIconStatus.setImageResource(R.drawable.ic_status_done)
            editTextIconStatus.isVisible = true
            infoIcon.isVisible = false
            infoStatus.text = resources.getString(R.string.screen_enter_domain_info_success_title)
            infoStatus.setTextAppearance(R.style.InfoStatusSuccessTextStyle)
        }
    }

    private fun showFailedSetDomainUi() {
        binding?.apply {
            headerIcon.setImageResource(R.drawable.ic_manage_accounts_fail)
            editTextIconStatus.setImageResource(R.drawable.ic_status_failed)
            editTextIconStatus.isVisible = true
            infoIcon.isVisible = false
            infoStatus.text = resources.getString(R.string.screen_enter_domain_info_fail_title)
            infoStatus.setTextAppearance(R.style.InfoStatusFailTextStyle)
        }
    }

    private fun enableButton(button: TextView?) {
        button?.isEnabled = true
        button?.alpha = 1f
        button?.isVisible = true
    }

    private fun disableButton(button: TextView?) {
        button?.isEnabled = false
        button?.alpha = .5f
        button?.isVisible = true
    }

    private fun hideButton(button: TextView?) {
        button?.isVisible = false
        button?.alpha = 1f
    }

    private fun showInputLineDefault() {
        binding?.inputUnderline?.apply {
            isVisible = false
        }
    }

    private fun showInputLineCorrect() {
        binding?.inputUnderline?.apply {
            setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(), R.color.white_85
                )
            )
            isVisible = true
        }
    }

    private fun showInputLineSuccess() {
        binding?.inputUnderline?.apply {
            setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(), R.color.light_green
                )
            )
            isVisible = true
        }
    }

    private fun showInputLineFailed() {
        binding?.inputUnderline?.apply {
            setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(), R.color.dark_green
                )
            )
            isVisible = true
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
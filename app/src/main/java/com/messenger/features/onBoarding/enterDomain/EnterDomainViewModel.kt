package com.messenger.features.onBoarding.enterDomain

import timber.log.Timber
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import com.messenger.utils.extensions.isValidDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class EnterDomainViewModel : ViewModel() {

    companion object {

        private val TAG = EnterDomainViewModel::class.java.simpleName
        private var typedDomain = ""

    }

    enum class ButtonState {
        STATE_ENABLED, STATE_DISABLED, STATE_INVISIBLE
    }

    enum class DomainRegistrationState {
        STATE_DEFAULT, STATE_SUCCEEDED, STATE_FAILED
    }

    enum class InputLineState {
        STATE_DEFAULT, STATE_CORRECT, STATE_SUCCESS, STATE_FAILED
    }

    val nextButtonState = MutableLiveData<ButtonState>()
    val clearButtonState = MutableLiveData<ButtonState>()
    val inputLineState = MutableLiveData<InputLineState>()

    val uiState = MutableLiveData<DomainRegistrationState>()

    val progressEvent = ProgressData()
    val confirmedDomainData = LiveEventData<String>()

    init {
        validateDomain()
        uiState.postValue(DomainRegistrationState.STATE_DEFAULT)
    }

    fun setTypedDomain(domain: String) {
        typedDomain = domain
        validateDomain()
    }

    private fun validateDomain() {
        when {
            typedDomain.isValidDomain() -> nextButtonState.postValue(ButtonState.STATE_ENABLED)
            else -> nextButtonState.postValue(ButtonState.STATE_DISABLED)
        }
        when {
            typedDomain.isNotEmpty() -> clearButtonState.postValue(ButtonState.STATE_ENABLED)
            else -> clearButtonState.postValue(ButtonState.STATE_DISABLED)
        }
        when {
            typedDomain.isValidDomain() -> inputLineState.postValue(
                InputLineState.STATE_CORRECT
            )

            else -> inputLineState.postValue(InputLineState.STATE_DEFAULT)
        }
        uiState.postValue(DomainRegistrationState.STATE_DEFAULT)
    }

    fun sendDomain() {
        Timber.tag(TAG).d("sendDomain")
        hideNavigationButtons()
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
//                Intent().apply {
//                    component = ComponentName(
//                        PACKAGE_K_CORE,
//                        PACKAGE_K_CORE_MANUAL_REGISTRATION
//                    )
//
//                    action = ACTION_CHECK_EMAIL_CODE
//                    putExtra(
//                        KEY_EXTRA,
//                        getFormattedCode()
//                    )
//
//                    putExtra(
//                        KEY_WIZARD_CHECK_CODE,
//                        true
//                    )
//
//                    serviceIntent.postRawValue(this)
//                }
                delay(2_000L)
                onSendDomainSucceeded()
                delay(2_000L)
                confirmedDomainData.postRawValue("test")
                progressEvent.endProgress()
                Timber.tag(TAG).d("sendDomain success")
//                if (typedDomain == "test.com") {
//                    onSendDomainSucceeded()
//                    delay(2_000L)
//                    confirmedDomainData.postRawValue("test")
//                    progressEvent.endProgress()
//                    Timber.tag(TAG).d("sendDomain success")
//                } else {
//                    onSendDomainFailed()
//                    throw Exception("Hey, I am testing it")
//                }
            } catch (ex: Exception) {
                Timber.tag(TAG).d("sendDomain failed: $ex")
                onSendDomainFailed()
            }
        }
    }

    private fun hideNavigationButtons() {
        nextButtonState.postValue(ButtonState.STATE_INVISIBLE)
        clearButtonState.postValue(ButtonState.STATE_INVISIBLE)
    }

    private fun onSendDomainFailed() {
        nextButtonState.postValue(ButtonState.STATE_DISABLED)
        clearButtonState.postValue(ButtonState.STATE_ENABLED)
        inputLineState.postValue(InputLineState.STATE_FAILED)
        uiState.postValue(DomainRegistrationState.STATE_FAILED)
        progressEvent.endProgress()
    }

    private fun onSendDomainSucceeded() {
        inputLineState.postValue(InputLineState.STATE_SUCCESS)
        uiState.postValue(DomainRegistrationState.STATE_SUCCEEDED)
    }
}
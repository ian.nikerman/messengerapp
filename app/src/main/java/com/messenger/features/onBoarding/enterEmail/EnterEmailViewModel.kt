package com.messenger.features.onBoarding.enterEmail

import timber.log.Timber
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import com.messenger.utils.extensions.isValidEmail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class EnterEmailViewModel : ViewModel() {

    companion object {
        private val TAG = EnterEmailViewModel::class.java.simpleName
        private var typedEmail = ""
    }

    enum class ButtonState {
        STATE_ENABLED, STATE_DISABLED, STATE_INVISIBLE
    }

    enum class EmailRegistrationState {
        STATE_DEFAULT, STATE_SUCCEEDED, STATE_FAILED
    }

    enum class InputLineState {
        STATE_DEFAULT, STATE_CORRECT, STATE_SUCCESS, STATE_FAILED
    }

    val nextButtonState = MutableLiveData<ButtonState>()
    val clearButtonState = MutableLiveData<ButtonState>()
    val inputLineState = MutableLiveData<InputLineState>()

    val uiState = MutableLiveData<EmailRegistrationState>()

    val confirmedEmailData = LiveEventData<String>()
    val progressEvent = ProgressData()

    init {
        validateEmail()
        uiState.postValue(EmailRegistrationState.STATE_DEFAULT)
    }

    fun setTypedEmail(email: String) {
        typedEmail = email
        validateEmail()
    }

    private fun validateEmail() {
        when {
            typedEmail.isValidEmail() -> nextButtonState.postValue(ButtonState.STATE_ENABLED)
            else -> nextButtonState.postValue(ButtonState.STATE_DISABLED)
        }
        when {
            typedEmail.isNotEmpty() -> clearButtonState.postValue(ButtonState.STATE_ENABLED)
            else -> clearButtonState.postValue(ButtonState.STATE_DISABLED)
        }
        when {
            typedEmail.isValidEmail() -> inputLineState.postValue(
                InputLineState.STATE_CORRECT
            )

            else -> inputLineState.postValue(InputLineState.STATE_DEFAULT)
        }
        uiState.postValue(EmailRegistrationState.STATE_DEFAULT)
    }

    fun sendEmail() {
        Timber.tag(TAG).d("sendEmail")
        hideNavigationButtons()
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {

                delay(2_000L)
                if (typedEmail.contains("@")) {
                    onSendEmailSucceeded()
                    delay(2_000L)
                    confirmedEmailData.postRawValue("test")
                    Timber.tag(TAG).d("sendEmail success")
                    progressEvent.endProgress()
                } else {
                    throw Exception("Hey, I am testing it")
                }
            } catch (ex: Exception) {
                Timber.tag(TAG).d("sendEmail failed: $ex")
                onSendEmailFailed()
            }
        }
    }

    private fun hideNavigationButtons() {
        nextButtonState.postValue(ButtonState.STATE_INVISIBLE)
        clearButtonState.postValue(ButtonState.STATE_INVISIBLE)
    }

    private fun onSendEmailFailed() {
        nextButtonState.postValue(ButtonState.STATE_DISABLED)
        clearButtonState.postValue(ButtonState.STATE_ENABLED)
        inputLineState.postValue(InputLineState.STATE_FAILED)
        uiState.postValue(EmailRegistrationState.STATE_FAILED)
        progressEvent.endProgress()
    }

    private fun onSendEmailSucceeded() {
        inputLineState.postValue(InputLineState.STATE_SUCCESS)
        uiState.postValue(EmailRegistrationState.STATE_SUCCEEDED)
    }
}
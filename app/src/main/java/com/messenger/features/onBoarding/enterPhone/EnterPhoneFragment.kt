package com.messenger.features.onBoarding.enterPhone

import android.os.Bundle
import android.text.Editable
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.messenger.R
import com.messenger.databinding.FragmentEnterPhoneBinding
import com.messenger.features.onBoarding.OnBoardingViewModel
import com.messenger.features.onBoarding.enterPhone.adapters.CountryItem
import com.messenger.features.onBoarding.enterPhone.adapters.DropDownCountryAdapter
import com.messenger.utils.WizardUtils
import com.messenger.utils.extensions.AfterTextChangedWatcher
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

open class EnterPhoneFragment : Fragment() {

    private val sharedViewModel by activityViewModel<OnBoardingViewModel>()
    private val viewModel: EnterPhoneViewModel by viewModel()
    private var binding: FragmentEnterPhoneBinding? = null

    companion object {
        private val TAG = EnterPhoneFragment::class.java.simpleName
        private lateinit var adapter: DropDownCountryAdapter

    }

    private val adapterListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            viewModel.setSelectedCountry(position)
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {}
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEnterPhoneBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(requireView(), savedInstanceState)
        Timber.tag(TAG).d("start screen")
        initUi()
        initObservers()
    }

    private fun initUi() {
        Timber.tag(TAG).d("initUi")
        binding?.apply {
            nextBtn.setOnClickListener {
                viewModel.confirmPhone()
            }
            clearBtn.setOnClickListener {
                phoneEdittext.text = Editable.Factory.getInstance().newEditable("")
            }
            phoneEdittext.addTextChangedListener(object : AfterTextChangedWatcher() {
                override fun afterTextChanged(s: Editable?) {
                    viewModel.setTypedPhone(s?.toString() ?: "")
                }
            })
            phoneLayout.setOnClickListener {
                phoneEdittext.requestFocus()
                WizardUtils.showKeyBoard(requireActivity())
            }
            countriesLayout.setOnClickListener { countriesSpinner.performClick() }
            phoneEdittext.requestFocus()
            WizardUtils.showKeyBoard(requireActivity())
        }
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.selectedCodeData.observe(viewLifecycleOwner) { data ->
            handleSelectedCodeData(data)
        }
        viewModel.countriesData.observe(viewLifecycleOwner) { data ->
            handleCountriesData(data)
        }
        viewModel.confirmedPhoneData.observe(viewLifecycleOwner) { event ->
            handleNavigationEvent(event)
        }
        viewModel.nextButtonState.observe(viewLifecycleOwner) { state ->
            handleNextButtonState(state)
        }
        viewModel.clearButtonState.observe(viewLifecycleOwner) { state ->
            handleClearButtonState(state)
        }
        viewModel.inputLineState.observe(viewLifecycleOwner) { event ->
            handleInputUnderScoreState(event)
        }
        viewModel.errorEvent.observe(viewLifecycleOwner) { event ->
            handleErrorEvent(event)
        }
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
        viewModel.setupCountries(requireContext())
    }

    private fun handleErrorEvent(event: LiveEvent<EnterPhoneViewModel.ErrorEvent>?) {
        event?.getContentIfNotHandled()?.let {
            when (it) {
                EnterPhoneViewModel.ErrorEvent.ERROR_FETCH_COUNTRIES_FAILED -> Toast.makeText(
                    requireContext(), "Error on loading countries", Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun handleNextButtonState(state: EnterPhoneViewModel.ButtonState?) {
        state?.let {
            when (it) {
                EnterPhoneViewModel.ButtonState.STATE_ENABLED -> {
                    enableButton(binding?.nextBtn)
                }

                EnterPhoneViewModel.ButtonState.STATE_DISABLED -> {
                    disableButton(binding?.nextBtn)
                }

                EnterPhoneViewModel.ButtonState.STATE_INVISIBLE -> {
                    hideButton(binding?.nextBtn)
                }
            }
        }
    }

    private fun handleClearButtonState(state: EnterPhoneViewModel.ButtonState?) {
        state?.let {
            when (it) {
                EnterPhoneViewModel.ButtonState.STATE_ENABLED -> {
                    enableButton(binding?.clearBtn)
                }

                EnterPhoneViewModel.ButtonState.STATE_DISABLED -> {
                    disableButton(binding?.clearBtn)
                }

                EnterPhoneViewModel.ButtonState.STATE_INVISIBLE -> {
                    hideButton(binding?.clearBtn)
                }
            }
        }
    }

    private fun handleInputUnderScoreState(state: EnterPhoneViewModel.InputLineState?) {
        state?.let {
            when (it) {
                EnterPhoneViewModel.InputLineState.STATE_DEFAULT -> {
                    showInputLineDefault()
                }

                EnterPhoneViewModel.InputLineState.STATE_CORRECT -> {
                    showInputLineCorrect()
                }

                EnterPhoneViewModel.InputLineState.STATE_SUCCESS -> {
                    showInputLineSuccess()
                }

                EnterPhoneViewModel.InputLineState.STATE_FAILED -> {
                    showInputLineFailed()
                }
            }
        }
    }

    private fun handleSelectedCodeData(data: EnterPhoneViewModel.SelectedCodeData?) {
        data?.let {
            Timber.tag(TAG).d("handleSelectedCodeData")
            binding?.apply {
                countriesSpinner.setSelection(it.selectedCountryIndex)
                codeTxt.text = it.selectedCode
            }
        }
    }

    private fun handleNavigationEvent(data: LiveEvent<String>?) {
        data?.getContentIfNotHandled()?.let {
            Timber.tag(TAG).d("handleNavigationEvent")
            WizardUtils.closeKeyBoard(requireActivity())
            sharedViewModel.openConfirmPhoneDialog(it)
        }
    }

    private fun handleProgressEvent(isLoading: Boolean) {
        binding?.progress?.setProgressEvent(isLoading)
    }

    private fun handleCountriesData(data: ArrayList<CountryItem>?) {
        data?.let {
            Timber.tag(TAG).d("handleCountriesData: ${data.size}")
            adapter = DropDownCountryAdapter(
                requireContext(), R.layout.item_spinner_country, it
            )
            adapter.setDropDownViewResource(R.layout.item_simple_spinner_dropdown)
            binding?.apply {
                countriesSpinner.adapter = adapter
                countriesSpinner.post {
                    countriesSpinner.onItemSelectedListener = adapterListener
                }
            }
        }
    }

    private fun enableButton(button: TextView?) {
        button?.isEnabled = true
        button?.alpha = 1f
        button?.isVisible = true
    }

    private fun disableButton(button: TextView?) {
        button?.isEnabled = false
        button?.alpha = .5f
        button?.isVisible = true
    }

    private fun hideButton(button: TextView?) {
        button?.isVisible = false
        button?.alpha = 1f
    }

    private fun showInputLineDefault() {
        binding?.inputUnderline?.apply {
            isVisible = false
        }
    }

    private fun showInputLineCorrect() {
        binding?.inputUnderline?.apply {
            setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(), R.color.white_85
                )
            )
            isVisible = true
        }
    }

    private fun showInputLineSuccess() {
        binding?.inputUnderline?.apply {
            setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(), R.color.light_green
                )
            )
            isVisible = true
        }
    }

    private fun showInputLineFailed() {
        binding?.inputUnderline?.apply {
            setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(), R.color.dark_green
                )
            )
            isVisible = true
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
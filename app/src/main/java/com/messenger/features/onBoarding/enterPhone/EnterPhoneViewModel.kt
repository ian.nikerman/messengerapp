package com.messenger.features.onBoarding.enterPhone

import android.content.Context
import android.content.res.Resources
import android.telephony.TelephonyManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber
import com.messenger.features.onBoarding.enterPhone.adapters.CountryItem
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import com.messenger.utils.extensions.isValidPhone
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.Locale

class EnterPhoneViewModel(
    private val service: TelephonyManager,
    private val resources: Resources
) : ViewModel() {

    companion object {

        private val TAG = EnterPhoneViewModel::class.java.simpleName

        private var countries: ArrayList<CountryItem> = ArrayList()
        private var phoneNumber = ""
        private var countryCode = ""

    }

    enum class ErrorEvent {
        ERROR_FETCH_COUNTRIES_FAILED
    }

    enum class ButtonState {
        STATE_ENABLED,
        STATE_DISABLED,
        STATE_INVISIBLE
    }

    enum class InputLineState {
        STATE_DEFAULT,
        STATE_CORRECT,
        STATE_SUCCESS,
        STATE_FAILED
    }

    val nextButtonState = MutableLiveData<ButtonState>()
    val clearButtonState = MutableLiveData<ButtonState>()
    val inputLineState = MutableLiveData<InputLineState>()

    data class SelectedCodeData(val selectedCode: String, val selectedCountryIndex: Int)

    val selectedCodeData = MutableLiveData<SelectedCodeData>()
    val countriesData = MutableLiveData<ArrayList<CountryItem>?>()
    val errorEvent = LiveEventData<ErrorEvent>()
    val progressEvent = ProgressData()
    val confirmedPhoneData = LiveEventData<String>()

    init {
        validatePhone()
    }

    fun setSelectedCountry(position: Int) {
        Timber.tag(TAG).d("setSelectedCountry")
        val item: CountryItem =
            countries[position]
        countryCode = item.code.toString()
        selectedCodeData.postValue(SelectedCodeData("+${item.code}", position))

        validatePhone()
    }

    fun setTypedPhone(phone: String) {
        phoneNumber = phone
        validatePhone()
    }

    private fun validatePhone() {
        when {
            getPhoneNumber().isValidPhone() -> nextButtonState.postValue(ButtonState.STATE_ENABLED)
            else -> nextButtonState.postValue(ButtonState.STATE_DISABLED)
        }
        when {
            phoneNumber.isNotEmpty() && countryCode.isNotEmpty() -> clearButtonState.postValue(
                ButtonState.STATE_ENABLED
            )

            else -> clearButtonState.postValue(ButtonState.STATE_DISABLED)
        }
        when {
            phoneNumber.isNotEmpty() && countryCode.isNotEmpty() -> inputLineState.postValue(
                InputLineState.STATE_CORRECT
            )

            else -> inputLineState.postValue(InputLineState.STATE_DEFAULT)
        }
    }

    fun confirmPhone() {
        Timber.tag(TAG).d("confirmPhone")
        confirmedPhoneData.postRawValue(getPhoneNumber())
    }

    fun setupCountries(context: Context) {
        Timber.tag(TAG).d("setupCountries")
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val phoneCode = getDefaultCountryCode()
                countries = readCountriesFromSources(context)
                countriesData.postValue(countries)
                countries.firstOrNull { it.code == phoneCode }?.let {
                    val myCountryCodeIndex =
                        countries.indexOf(it)
                    setSelectedCountry(myCountryCodeIndex)
                }
                Timber.tag(TAG).d("setupCountries success")
            } catch (ex: Exception) {
                errorEvent.postRawValue(ErrorEvent.ERROR_FETCH_COUNTRIES_FAILED)
                Timber.tag(TAG).d("setupCountries failed: $ex")
            }
        }
    }

    private fun readCountriesFromSources(context: Context): ArrayList<CountryItem> {
        Timber.tag(TAG).d("readCountriesFromSources")
        countries.clear()
        var reader: BufferedReader? = null
        try {
            val countriesDoc = resources.openRawResource(
                resources.getIdentifier(
                    "countries",
                    "raw", context.applicationContext.packageName
                )
            )
            reader = BufferedReader(InputStreamReader(countriesDoc, "UTF-8"))
            var line: String?
            var i = 0
            while (reader.readLine().also { line = it } != null) {
                try {
                    val c = CountryItem(context, line, i)
                    if (!countries.contains(c)) {
                        countries.add(c)
                    }
                    i++
                } catch (e: Exception) {
                    Timber.tag(TAG).d("Failed adding country $e")
                }
            }
        } catch (e: Exception) {
            Timber.tag(TAG).d("setupCountriesSpinner error $e")
        } finally {
            if (reader != null) {
                try {
                    reader.close()
                } catch (e: IOException) {
                    Timber.tag(TAG).d("setupCountriesSpinner reader error $e")
                }
            }
        }
        return ArrayList(countries.sortedBy { it.name })
    }

    private fun getDefaultCountryCode(): Int {
        Timber.tag(TAG).d("getDefaultCountryCode")
        val countryRegion: String? = getCountryRegionFromPhone()
        return PhoneNumberUtil.getInstance().getCountryCodeForRegion(countryRegion)
    }

    private fun getPhoneNumber(): String {
        return ("+$countryCode$phoneNumber")
    }

    private fun getCountryRegionFromPhone(): String? {
        Timber.tag(TAG).d("getCountryRegionFromPhone")

        try {
            parseNumber(service.line1Number)?.uppercase(Locale.US)?.apply {
                return this
            }

        } catch (ex: SecurityException) {
            getNetworkCountryIso()?.let {
                return it
            }
            return getConfigurationLocale()

        }

        return null
    }

    private fun getNetworkCountryIso(): String? {
        return service.networkCountryIso?.uppercase(Locale.US)
    }

    private fun getConfigurationLocale(): String {
        return resources.configuration.locales.get(0).country.uppercase(Locale.US)
    }

    private fun parseNumber(paramString: String?): String? {
        Timber.tag(TAG).d("parseNumber")
        if (paramString == null) {
            return null
        }
        val numberUtil: PhoneNumberUtil = PhoneNumberUtil.getInstance()
        val result: String
        try {
            val localPhoneNumber: Phonenumber.PhoneNumber = numberUtil.parse(paramString, null)
            result = numberUtil.getRegionCodeForNumber(localPhoneNumber)
            if (result == null) {
                return null
            }
        } catch (localNumberParseException: NumberParseException) {
            return null
        }
        return result
    }
}
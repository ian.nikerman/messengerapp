package com.messenger.features.onBoarding.enterPhone.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import timber.log.Timber
import androidx.core.content.ContextCompat
import java.util.*

class CountryItem(context: Context, countryFromTxt: String?, num: Int) {

    var name: String
    var flag: Drawable? = null
    var code: Int
    var numberInList: Int

    companion object {

        private val TAG = CountryItem::class.java.simpleName

    }

    init {

        val data: Array<String> = countryFromTxt!!.split(",").toTypedArray()
        name = data[0]

        var shortName = data[1].lowercase(Locale.getDefault())
        if (shortName == "do") {
            shortName = "do1"
        }
        val id = context.resources.getIdentifier(shortName, "drawable", context.packageName)
        try {
            flag = ContextCompat.getDrawable(context, id)
        } catch (e: Exception) {
            Timber.tag(TAG).d("Flag error $e")
        }

        code = data[2].toInt()
        numberInList = num
    }
}
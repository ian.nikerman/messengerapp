package com.messenger.features.onBoarding.enterPhone.adapters

import android.content.Context
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.messenger.databinding.ItemSpinnerCountryBinding

class DropDownCountryAdapter(
    context: Context, resource: Int, countries: ArrayList<CountryItem>
) : ArrayAdapter<CountryItem>(context, resource, countries) {

    companion object {

        private val TAG = DropDownCountryAdapter::class.java.simpleName

    }

    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, parent)
    }

    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, parent)
    }

    private fun createView(position: Int, parent: ViewGroup): View {
        val item = getItem(position)
        val view = ItemSpinnerCountryBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        if (item != null) {
            try {
                view.countryName.text = item.name
                view.flagImg.background = item.flag
            } catch (e: Exception) {
                Timber.tag(TAG).d("getCountryView error $e")
            }
        }
        return view.root
    }
}
package com.messenger.features.onBoarding.enterPhone.dialogs

import android.os.Bundle
import timber.log.Timber
import android.view.View
import androidx.navigation.fragment.navArgs
import com.messenger.R
import com.messenger.features.onBoarding.OnBoardingViewModel
import com.messenger.features.views.GeneralAlertDialog
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class ConfirmPhoneDialog : GeneralAlertDialog() {

    private val sharedViewModel by activityViewModel<OnBoardingViewModel>()

    companion object {

        private val TAG = ConfirmPhoneDialog::class.java.simpleName

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.tag(TAG).d("start screen")
    }

    override fun onOkClick() {
        Timber.tag(TAG).d("onOkClick")
        val navArgs: ConfirmPhoneDialogArgs by navArgs()
        val phone = navArgs.phone
        sharedViewModel.sendSmsIntent(phone)
        sharedViewModel.openEnterPhoneCodeFragment(phone)
    }

    override fun onCancelClick() {
        Timber.tag(TAG).d("onCancelClick")
        dismiss()
    }

    override fun isEdiTextShown(): Int {
        return View.GONE
    }

    override fun getTitle(): String {
        return getString(R.string.dialog_confirm_phone_title)
    }

    override fun getSubtitle(): String {
        return getString(R.string.dialog_confirm_phone_sub_title)
    }

    override fun getTitleImage(): Int {
        return R.drawable.ic_green_keyboard
    }

    override fun getCancelButtonText(): String {
        return getString(R.string.button_edit)
    }

    override fun getOkButtonText(): String {
        return getString(R.string.button_ok)
    }
}
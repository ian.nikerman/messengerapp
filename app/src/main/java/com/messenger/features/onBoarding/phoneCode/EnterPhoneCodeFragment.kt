package com.messenger.features.onBoarding.phoneCode

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.provider.Telephony
import android.text.Editable
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.messenger.R
import com.messenger.databinding.FragmentEnterPhoneCodeBinding
import com.messenger.features.onBoarding.OnBoardingViewModel
import com.messenger.features.views.NumPadView
import com.messenger.utils.Constants
import com.messenger.utils.Constants.Companion.USE_FOR_SMS_CODE_MAX_VALUE
import com.messenger.utils.extensions.LiveEvent
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class EnterPhoneCodeFragment : Fragment() {

    private val sharedViewModel by activityViewModel<OnBoardingViewModel>()
    private val viewModel: EnterPhoneCodeViewModel by viewModel()
    private var binding: FragmentEnterPhoneCodeBinding? = null

    companion object {
        private val TAG = EnterPhoneCodeFragment::class.java.simpleName
        private const val SERVICE_KEY = "messenger"
        private const val SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED"
        private lateinit var smsReceiver: BroadcastReceiver
        private lateinit var registrationBroadcastReceiver: BroadcastReceiver
    }

    private val numPadListener = object : NumPadView.NumPadViewListener {
        override fun onTypedCode(code: String) {
            viewModel.setTypedCode(code, false)
        }

        override fun onAutoTypedCode(code: String) {
            viewModel.setTypedCode(code, true)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEnterPhoneCodeBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(requireView(), savedInstanceState)
        Timber.tag(TAG).d("start screen")
        initUi()
        initObservers()
    }

    private fun initUi() {
        Timber.tag(TAG).d("initUi")
        binding?.apply {
            numPad.setListener(numPadListener)
            numPad.setMaxNumbers(USE_FOR_SMS_CODE_MAX_VALUE)

            clearBtn.setOnClickListener {
                numPad.clearInput()
            }

            nextBtn.setOnClickListener {
                viewModel.sendCode()
            }

            resendCodeTitle.setOnClickListener {
                viewModel.confirmResendSmsCode(handlePhone())
            }

            receiveCodeByCallTitle.setOnClickListener {
                viewModel.getCodeByCallCode()
            }

            reenterPhoneTitle.setOnClickListener {
                sharedViewModel.openEnterPhoneFragment(this@EnterPhoneCodeFragment)
            }

            registerSmsReceiver()
            registerRegistrationReceiver()
        }
    }

    private fun handlePhone(): String {
        val navArgs: EnterPhoneCodeFragmentArgs by navArgs()
        return navArgs.phone
    }

    private fun initObservers() {
        Timber.tag(TAG).d("initObservers")
        viewModel.serviceIntent.observe(viewLifecycleOwner) { intent ->
            startServiceIntent(intent)
        }
        viewModel.confirmedPhoneData.observe(viewLifecycleOwner) { event ->
            handleNavigationEvent(event)
        }
        viewModel.nextButtonState.observe(viewLifecycleOwner) { state ->
            handleNextButtonState(state)
        }
        viewModel.clearButtonState.observe(viewLifecycleOwner) { state ->
            handleClearButtonState(state)
        }
        viewModel.inputLineState.observe(viewLifecycleOwner) { event ->
            handleInputUnderScoreState(event)
        }
        viewModel.typedCode.observe(viewLifecycleOwner) { data ->
            handleTypedCode(data)
        }
        viewModel.uiState.observe(viewLifecycleOwner) { state ->
            handleUiState(state)
        }
        viewModel.progressEvent.observe(viewLifecycleOwner) { event ->
            handleProgressEvent(event)
        }
    }

    private fun handleNavigationEvent(event: LiveEvent<EnterPhoneCodeViewModel.CodeConfirmationState>?) {
        event?.getContentIfNotHandled()?.let {
            when (it) {
                EnterPhoneCodeViewModel.CodeConfirmationState.CONFIRMED -> sharedViewModel.openAllSetFragment()
                EnterPhoneCodeViewModel.CodeConfirmationState.FAILED -> sharedViewModel.openActivationCodeIncorrectDialog()
            }
        }
    }

    private fun handleNextButtonState(state: EnterPhoneCodeViewModel.ButtonState?) {
        state?.let {
            when (it) {
                EnterPhoneCodeViewModel.ButtonState.STATE_ENABLED -> {
                    enableButton(binding?.nextBtn)
                }

                EnterPhoneCodeViewModel.ButtonState.STATE_DISABLED -> {
                    disableButton(binding?.nextBtn)
                }

                EnterPhoneCodeViewModel.ButtonState.STATE_INVISIBLE -> {
                    hideButton(binding?.nextBtn)
                }
            }
        }
    }

    private fun handleClearButtonState(state: EnterPhoneCodeViewModel.ButtonState?) {
        state?.let {
            when (it) {
                EnterPhoneCodeViewModel.ButtonState.STATE_ENABLED -> {
                    enableButton(binding?.clearBtn)
                }

                EnterPhoneCodeViewModel.ButtonState.STATE_DISABLED -> {
                    disableButton(binding?.clearBtn)
                }

                EnterPhoneCodeViewModel.ButtonState.STATE_INVISIBLE -> {
                    hideButton(binding?.clearBtn)
                }
            }
        }
    }

    private fun handleInputUnderScoreState(state: EnterPhoneCodeViewModel.InputLineState?) {
        state?.let {
            when (it) {
                EnterPhoneCodeViewModel.InputLineState.STATE_DEFAULT -> {
                    showInputLineDefault()
                }

                EnterPhoneCodeViewModel.InputLineState.STATE_CORRECT -> {
                    showInputLineCorrect()
                }

                EnterPhoneCodeViewModel.InputLineState.STATE_SUCCESS -> {
                    showInputLineSuccess()
                }

                EnterPhoneCodeViewModel.InputLineState.STATE_FAILED -> {
                    showInputLineFailed()
                }
            }
        }
    }

    private fun handleUiState(state: EnterPhoneCodeViewModel.UiState?) {
        state?.let {
            Timber.tag(TAG).d("handleUiState: ${state.name}")
            when (it) {
                EnterPhoneCodeViewModel.UiState.STATE_RESEND_WAIT -> {
                    binding?.apply {
                        disableButton(binding?.resendCodeTitle)
                    }
                }

                EnterPhoneCodeViewModel.UiState.STATE_RESEND -> {
                    binding?.apply {
                        enableButton(binding?.resendCodeTitle)
                    }
                }
            }
        }
    }

    private fun handleTypedCode(code: String?) {
        code?.let {
            binding?.codeEdittext?.text = Editable.Factory.getInstance().newEditable(it)
        }
    }

    private fun startServiceIntent(intent: LiveEvent<Intent>?) {
        intent?.getContentIfNotHandled()?.let {
            Timber.tag(TAG).d("startServiceIntent")
            requireActivity().startService(it)
            sharedViewModel.openAllSetFragment()
        }
    }

    private fun handleProgressEvent(isLoading: Boolean) {
        binding?.progress?.setProgressEvent(isLoading)
    }

    private fun enableButton(button: TextView?) {
        button?.isEnabled = true
        button?.alpha = 1f
        button?.isVisible = true
    }

    private fun disableButton(button: TextView?) {
        button?.isEnabled = false
        button?.alpha = .5f
        button?.isVisible = true
    }

    private fun hideButton(button: TextView?) {
        button?.isVisible = false
        button?.alpha = 1f
    }

    private fun showInputLineDefault() {
        binding?.inputUnderline?.apply {
            isVisible = false
        }
    }

    private fun showInputLineCorrect() {
        binding?.inputUnderline?.apply {
            setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(), R.color.white_85
                )
            )
            isVisible = true
        }
    }

    private fun showInputLineSuccess() {
        binding?.inputUnderline?.apply {
            setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(), R.color.light_green
                )
            )
            isVisible = true
        }
    }

    private fun showInputLineFailed() {
        binding?.inputUnderline?.apply {
            setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(), R.color.dark_green
                )
            )
            isVisible = true
        }
    }

    override fun onDestroyView() {
        requireActivity().unregisterReceiver(smsReceiver)
        requireActivity().unregisterReceiver(registrationBroadcastReceiver)
        super.onDestroyView()
        binding = null
    }

    private fun registerSmsReceiver() {
        Timber.tag(TAG).d("registerSmsReceiver")
        smsReceiver = SmsReceiver()
        requireActivity().registerReceiver(smsReceiver, IntentFilter(SMS_RECEIVED_ACTION))
    }

    private fun registerRegistrationReceiver() {
        Timber.tag(TAG).d("registerRegistrationReceiver")
        registrationBroadcastReceiver = RegistrationBroadcastReceiver()
        requireActivity().registerReceiver(
            registrationBroadcastReceiver, IntentFilter(Constants.INTENT_FILTER_REGISTRATION)
        )
    }

    private inner class SmsReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Timber.tag(TAG).d("onReceive() called with: intent = [$intent]")
            val bundle = intent.extras
            if (bundle != null) {
                Timber.tag(TAG).d("SmsReceiver onReceive data")
                val messages = Telephony.Sms.Intents.getMessagesFromIntent(intent)
                val buf = StringBuilder()
                var from: String? = null
                for (message in messages) {
                    buf.append(message.displayMessageBody)
                    from = message.displayOriginatingAddress
                }
                var smsBody = buf.toString()
                Timber.tag(TAG).d(String.format("found sms from: %s body: %s", from, smsBody))

                if (smsBody.lowercase().contains(SERVICE_KEY)) {
                    smsBody = smsBody.filter { it.isDigit() }
                    binding?.apply {
                        numPad.setAutoTypedCode(smsBody)
                    }
                }
            }
        }
    }

    private inner class RegistrationBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            val result = intent?.getStringExtra(Constants.ARG_REG_RESULT)
            Timber.tag(TAG).d("registrationResult $result")
            result?.let {
                Timber.tag(TAG).d("RegistrationBroadcastReceiver onReceive: $it")
                when (it) {
                    Constants.Companion.RegistrationCodeErrors.success.name -> {
                        viewModel.showSuccessState()
                        sharedViewModel.openAllSetFragment()
                    }

                    Constants.Companion.RegistrationCodeErrors.incorrect_code.name -> {
                        viewModel.showFailedState()
                        sharedViewModel.openActivationCodeIncorrectDialog()
                    }

                    Constants.Companion.RegistrationCodeErrors.unknown_error.name -> {
                        viewModel.showFailedState()
                        sharedViewModel.openFailedActivationDialog()
                    }
                }
            }
        }
    }
}

package com.messenger.features.onBoarding.phoneCode

import android.content.Intent
import timber.log.Timber
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.messenger.network.ApiSettings
import com.messenger.utils.extensions.LiveEventData
import com.messenger.utils.extensions.ProgressData
import com.messenger.utils.extensions.isValidSmsCode
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class EnterPhoneCodeViewModel(private val apiSettings: ApiSettings) : ViewModel() {

    companion object {
        private val TAG = EnterPhoneCodeViewModel::class.java.simpleName
        private var enteredCode = ""
        private const val TIMEOUT_DELAY_RESEND = 30_000L
    }

    enum class ErrorEvent {
        CHECK_CODE, RESEND_SMS_CODE, CODE_BY_CALL
    }

    val errorEvent = LiveEventData<ErrorEvent>()

    enum class UiState {
        STATE_RESEND_WAIT, STATE_RESEND
    }

    val uiState = MutableLiveData<UiState>()

    enum class CodeConfirmationState {
        CONFIRMED, FAILED
    }

    val confirmedPhoneData = LiveEventData<CodeConfirmationState>()

    enum class ButtonState {
        STATE_ENABLED, STATE_DISABLED, STATE_INVISIBLE
    }

    val nextButtonState = MutableLiveData<ButtonState>()
    val clearButtonState = MutableLiveData<ButtonState>()

    enum class InputLineState {
        STATE_DEFAULT, STATE_CORRECT, STATE_SUCCESS, STATE_FAILED
    }

    val inputLineState = MutableLiveData<InputLineState>()

    val serviceIntent = LiveEventData<Intent>()
    val typedCode = MutableLiveData<String>()
    val progressEvent = ProgressData()

    init {
        updateResendSmsCodeTimer()
        validateCode()
    }

    fun setTypedCode(code: String, isAutoComplete: Boolean) {
        enteredCode = code
        validateCode()

        if (code.isEmpty()) {
            typedCode.postValue("")
        } else {
            var asterisk = ""
            repeat((code.indices).count()) {
                asterisk += "·"
            }
            typedCode.postValue(code)
            if (isAutoComplete) {
                Timber.tag(TAG).d("isAutoComplete: $isAutoComplete")
                validateEnteredSmsCode()
            }
        }
    }

    private fun validateCode() {
        when {
            enteredCode.isValidSmsCode() -> nextButtonState.postValue(ButtonState.STATE_ENABLED)
            else -> nextButtonState.postValue(ButtonState.STATE_DISABLED)
        }
        when {
            enteredCode.isNotEmpty() -> clearButtonState.postValue(ButtonState.STATE_ENABLED)
            else -> clearButtonState.postValue(ButtonState.STATE_DISABLED)
        }
        when {
            enteredCode.isValidSmsCode() -> inputLineState.postValue(
                InputLineState.STATE_CORRECT
            )

            else -> inputLineState.postValue(InputLineState.STATE_DEFAULT)
        }
    }

    private fun validateEnteredSmsCode() {
        Timber.tag(TAG).d("validateEnteredSmsCode")
        sendCode()
        updateResendSmsCodeTimer()
    }

    fun confirmResendSmsCode(phone: String) {
        Timber.tag(TAG).d("confirmResendSmsCode")
        resendSmsCode(phone)
        updateResendSmsCodeTimer()
    }

    fun sendCode() {
        Timber.tag(TAG).d("sendCode")
        progressEvent.startProgress()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                delay(2_000L)
                showSuccessState()
                delay(1_000L)
                apiSettings.token = "token$$$"
                Timber.tag(TAG).d("sendCode success")
                confirmedPhoneData.postRawValue(CodeConfirmationState.CONFIRMED)
//                when {
//                    enteredCode.contains("3") -> {
//                        showSuccessState()
//                        delay(1_000L)
//                        apiSettings.token = "token$$$"
//                        Timber.tag(TAG).d("sendCode success")
//                        confirmedPhoneData.postRawValue(CodeConfirmationState.CONFIRMED)
//                    }
//
//                    else -> {
//                        showFailedState()
//                        delay(1_000L)
//                        confirmedPhoneData.postRawValue(CodeConfirmationState.FAILED)
//                    }
//                }
            } catch (ex: Exception) {
                Timber.tag(TAG).d("sendCode failed: $ex")
                errorEvent.postRawValue(ErrorEvent.CHECK_CODE)
            }
        }
    }

    private fun getFormattedCode() = enteredCode.substring(0, 3) + "-" + enteredCode.substring(
        3, enteredCode.length
    )

    private fun resendSmsCode(phone: String) {
        Timber.tag(TAG).d("resendSmsCode")
        viewModelScope.launch(Dispatchers.IO) {
            try {
                Timber.tag(TAG).d("resendSmsCode success")
            } catch (ex: Exception) {
                Timber.tag(TAG).d("resendSmsCode failed: $ex")
                errorEvent.postRawValue(ErrorEvent.RESEND_SMS_CODE)
            }
        }
    }

    fun getCodeByCallCode() {
        Timber.tag(TAG).d("getCodeByCallCode")
        viewModelScope.launch(Dispatchers.IO) {
            try {
                /*Intent().apply {
                    component = ComponentName(
                        PACKAGE_K_CORE,
                        PACKAGE_K_CORE_MANUAL_REGISTRATION
                    )

                    action = ACTION_CODE_BY_CALL_REQUEST
                    serviceIntent.postRawValue(this)
                }*/
                Timber.tag(TAG).d("getCodeByCallCode success")
            } catch (ex: Exception) {
                Timber.tag(TAG).d("getCodeByCallCode failed: $ex")
                errorEvent.postRawValue(ErrorEvent.CODE_BY_CALL)
            }
        }
    }

    private fun updateResendSmsCodeTimer() {
        Timber.tag(TAG).d("updateResendSmsCodeTimer")
        viewModelScope.launch(Dispatchers.IO) {
            try {
                uiState.postValue(UiState.STATE_RESEND_WAIT)
                delay(TIMEOUT_DELAY_RESEND)
                uiState.postValue(UiState.STATE_RESEND)
            } catch (ex: Exception) {
                Timber.tag(TAG).d("updateResendSmsCodeTimer failed: $ex")
            }
        }
    }

    fun showFailedState() {
        inputLineState.postValue(
            InputLineState.STATE_FAILED
        )
        progressEvent.endProgress()
    }

    fun showSuccessState() {
        inputLineState.postValue(
            InputLineState.STATE_SUCCESS
        )
        progressEvent.endProgress()
    }
}
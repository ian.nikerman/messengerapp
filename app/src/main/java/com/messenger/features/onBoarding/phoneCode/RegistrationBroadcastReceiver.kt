package com.messenger.features.onBoarding.phoneCode

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import timber.log.Timber
import com.messenger.utils.Constants.Companion.ARG_REG_RESULT
import com.messenger.utils.Constants.Companion.INTENT_FILTER_REGISTRATION
import com.messenger.utils.Constants.Companion.REGISTRATION_RESULT

class RegistrationBroadcastReceiver : BroadcastReceiver() {

    private companion object {

        private val TAG = RegistrationBroadcastReceiver::class.java.simpleName

    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val connectionResult = intent?.getStringExtra(REGISTRATION_RESULT)
        Timber.tag(TAG).d("registrationResult $connectionResult")

        val internalIntent = Intent(INTENT_FILTER_REGISTRATION).apply {
            putExtra(ARG_REG_RESULT, connectionResult)
        }
        context?.sendBroadcast(internalIntent)
    }
}

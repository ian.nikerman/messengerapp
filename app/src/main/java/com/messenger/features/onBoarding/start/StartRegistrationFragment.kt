package com.messenger.features.onBoarding.start

import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.messenger.databinding.FragmentStartRegistrationBinding
import com.messenger.features.onBoarding.OnBoardingViewModel
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class StartRegistrationFragment : Fragment() {

    private val sharedViewModel by activityViewModel<OnBoardingViewModel>()
    private var binding: FragmentStartRegistrationBinding? = null

    companion object {
        private val TAG = StartRegistrationFragment::class.java.simpleName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStartRegistrationBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(requireView(), savedInstanceState)
        Timber.tag(TAG).d("start screen")
        initUi()
    }

    private fun initUi() {
        Timber.tag(TAG).d("initUi")
        binding?.apply {
            startBtn.setOnClickListener {
                Timber.tag(TAG).d("next click")
                sharedViewModel.openAgreementFragment()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
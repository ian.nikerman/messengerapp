package com.messenger.features.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.messenger.R
import com.messenger.databinding.LayoutCheckboxViewBinding

class CheckBoxView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var binding: LayoutCheckboxViewBinding? = null
    private var listener: CheckBoxViewListener? = null

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_checkbox_view, this, true)
        binding = LayoutCheckboxViewBinding.bind(view)

        attrs.let {
            val attrArray = context.obtainStyledAttributes(attrs, R.styleable.CheckBoxView)
            if (attrArray.hasValue(R.styleable.CheckBoxView_checkBoxTitle)) {
                val title = attrArray.getString(R.styleable.CheckBoxView_checkBoxTitle)
                binding?.checkBoxTitle?.let {
                    it.text = title
                }
            }
            if (attrArray.hasValue(R.styleable.CheckBoxView_isLastCheckBox)) {
                val isLastScopeItem =
                    attrArray.getBoolean(R.styleable.CheckBoxView_isLastCheckBox, false)
                binding?.bottomLine?.isVisible = isLastScopeItem
            }
            attrArray.recycle()
        }

        binding?.checkBoxLayout?.setOnClickListener {
            listener?.onCheckBoxClick(!isSettingChecked())
        }
    }

    private fun isSettingChecked() = binding?.checkBoxButton?.isChecked ?: false

    fun setSubTitle(subtitle: String?) {
        when {
            subtitle.isNullOrEmpty() -> {
                binding?.checkBoxSubTitle?.visibility = View.INVISIBLE
                binding?.checkBoxSubTitle?.text = ""
            }

            else -> {
                binding?.checkBoxSubTitle?.visibility = View.VISIBLE
                binding?.checkBoxSubTitle?.text = subtitle
            }
        }
    }

    fun setChecked(isChecked: Boolean) {
        binding?.checkBoxButton?.isChecked = isChecked
    }

    fun setListener(listener: CheckBoxViewListener) {
        this.listener = listener
    }

    interface CheckBoxViewListener {
        fun onCheckBoxClick(isChecked: Boolean)
    }
}




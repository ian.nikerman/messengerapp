package com.messenger.features.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.messenger.R
import com.messenger.databinding.ViewEmptyDataBinding

class EmptyDataView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var binding: ViewEmptyDataBinding? = null
    private var listener: EmptyDataViewListener? = null

    interface EmptyDataViewListener {
        fun onEmptyDataViewClick() {}
    }

    companion object {
        private var emptyDataType = 0
        private const val CHATS = 0
        private const val CHAT = 1
        private const val CONTACTS = 2
        private const val CALLS = 3
        private const val ATTACH_CAMERA = 4
        private const val ATTACH_GALLERY = 5
        private const val SEARCH = 6
        private const val FAVOURITES = 7
    }

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.view_empty_data, this, true)
        binding = ViewEmptyDataBinding.bind(view)

        attrs.let {
            val attrArray = context.obtainStyledAttributes(attrs, R.styleable.EmptyDataView)
            if (attrArray.hasValue(R.styleable.EmptyDataView_dataType)) {
                emptyDataType = attrArray.getInt(R.styleable.EmptyDataView_dataType, -1)
                setEmptyDataType()
            }
            attrArray.recycle()
        }

        binding?.emptyViewLayout?.setOnClickListener { listener?.onEmptyDataViewClick() }
    }

    fun setListener(listener: EmptyDataViewListener) {
        this.listener = listener
    }

    private fun setEmptyDataType() {
        binding?.emptyViewTitle?.context?.apply {
            when (emptyDataType) {
                CHATS -> {
                    setIcon(R.drawable.ic_chats)
                    setTitle(getString(R.string.view_empty_data_chats_title))
                }

                CHAT -> {
                    setIcon(R.drawable.ic_message)
                    setTitle(getString(R.string.view_empty_data_messages_title))
                }

                CONTACTS -> {
                    setIcon(R.drawable.ic_contact)
                    setTitle(getString(R.string.view_empty_data_contacts_title))
                }

                CALLS -> {
                    setIcon(R.drawable.ic_call_white)
                    setTitle(getString(R.string.view_empty_data_calls_title))
                }

                ATTACH_CAMERA -> {
                    setIcon(R.drawable.ic_camera)
                    setTitle(getString(R.string.view_empty_data_camera_title))
                }

                ATTACH_GALLERY -> {
                    setIcon(R.drawable.ic_gallery)
                    setTitle(getString(R.string.view_empty_data_gallery_title))
                }

                SEARCH -> {
                    setIcon(R.drawable.ic_search)
                    setTitle(getString(R.string.view_empty_data_search_title))
                }

                FAVOURITES -> {
                    setIcon(R.drawable.ic_favourites)
                    setTitle(getString(R.string.view_empty_data_favourites_title))
                }

                else -> {}
            }
        }
    }

    fun showEmptyResults() {
        binding?.emptyViewTitle?.context?.apply {
            setIcon(R.drawable.ic_search)
            setTitle(getString(R.string.view_empty_data_search_title))
        }
    }

    fun hideEmptyResults() {
        setEmptyDataType()
    }

    private fun setTitle(text: String) {
        binding?.emptyViewTitle?.text = text
    }

    private fun setIcon(imageResource: Int) {
        binding?.emptyViewIcon?.setImageResource(imageResource)
    }
}
package com.messenger.features.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.messenger.R
import com.messenger.databinding.DialogGeneralAlertBinding

abstract class GeneralAlertDialog : DialogFragment() {

    private var binding: DialogGeneralAlertBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_FRAME, R.style.FullScreenDialogStyle)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DialogGeneralAlertBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        binding?.apply {
            dialogOk.text = getOkButtonText()
            dialogCancel.text = getCancelButtonText()
            dialogEdittext.visibility = isEdiTextShown()
            topIcon.setImageResource(getTitleImage())
            dialogTitle.text = getTitle()
            dialogSubtitle.text = getSubtitle()

            dialogOk.setOnClickListener {
                onOkClick()
            }
            dialogCancel.setOnClickListener {
                onCancelClick()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    abstract fun onOkClick()
    abstract fun onCancelClick()
    abstract fun isEdiTextShown(): Int
    abstract fun getTitle(): String
    abstract fun getSubtitle(): String
    abstract fun getTitleImage(): Int
    abstract fun getCancelButtonText(): String
    abstract fun getOkButtonText(): String
}
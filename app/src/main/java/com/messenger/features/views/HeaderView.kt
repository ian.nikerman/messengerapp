package com.messenger.features.views

import android.content.Context
import android.text.Editable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import androidx.core.view.isVisible
import com.messenger.R
import com.messenger.databinding.ViewHeaderBinding
import com.messenger.features.main.callDetails.CallDetailsViewModel
import com.messenger.features.main.chatSettings.ChatSettingsViewModel
import com.messenger.models.CategoryAttach
import com.messenger.models.ContactData
import com.messenger.models.ConversationData
import com.messenger.utils.extensions.AfterTextChangedWatcher
import com.messenger.utils.getColoredCircle
import com.messenger.utils.getNameInitials
import com.messenger.utils.showDrawable
import com.messenger.utils.showPlaceholderCircleImage

class HeaderView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var binding: ViewHeaderBinding? = null
    private var listener: HeaderViewListener? = null

    interface HeaderViewListener {
        fun onBackgroundClick() {}
        fun onMyProfileClick() {}
        fun onBackClick() {}
        fun onHideInputClick(inputView: EditText) {}
        fun onCallClick() {}
        fun onContactClick() {}
        fun onMoreClick() {}
        fun onInputTypedText(typedText: String) {}
        fun onSearchClick(inputView: EditText) {}
        fun onSearchClick() {}
        fun onEditClick() {}
        fun onCancelSelectorClick() {}
        fun onPinChangeClick(isPinShown: Boolean) {}
        fun onMuteChangeClick(isMuteShown: Boolean) {}
        fun onDeleteClick() {}
    }

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.view_header, this, true)
        binding = ViewHeaderBinding.bind(view)

        attrs.let {
            val attrArray = context.obtainStyledAttributes(attrs, R.styleable.HeaderView)

            if (attrArray.hasValue(R.styleable.HeaderView_headerType)) {
                headerType = attrArray.getInt(R.styleable.HeaderView_headerType, MAIN)
                setHeaderType()
            }
            attrArray.recycle()
        }
    }

    fun setListener(listener: HeaderViewListener) {
        this.listener = listener
    }

    private fun setHeaderType() {
        resetViewUi()
        when (headerType) {
            MAIN -> setHeaderMainUi()
            CHAT -> setHeaderChatUi()
            CHAT_SETTINGS -> setHeaderChatSettingsUi()
            SEARCH -> setHeaderSearchUi()
            CONTACTS -> setHeaderContactsUi()
            GROUP_MEMBERS -> setHeaderGroupMembersUi()
            GROUP_NAME -> setHeaderGroupNameUi()
            SETTINGS -> setHeaderSettingsUi()
            CALL -> setHeaderCallUi()
            ATTACHMENT -> setHeaderAttachmentUi()
            NEW_CONTACT -> setHeaderNewContactUi()
            CALLS -> setHeaderCallsUi()
        }
    }

    private fun resetViewUi() {
        binding?.apply {
            mainLayout.parentLayout.isVisible = false
            chatsSelectorLayout.parentLayout.isVisible = false
            callsSelectorLayout.parentLayout.isVisible = false
            chatLayout.parentLayout.isVisible = false
            chatSelectorLayout.parentLayout.isVisible = false
            titleLayout.parentLayout.isVisible = false
            inputLayout.parentLayout.isVisible = false
        }
    }

    private fun setHeaderMainUi() {
        binding?.mainLayout?.apply {
            title.text = root.context.getText(R.string.app_name)
            parentLayout.setOnClickListener { listener?.onBackgroundClick() }
            searchButton.setOnClickListener { listener?.onSearchClick() }
            optionsButton.setOnClickListener { listener?.onMoreClick() }
            parentLayout.isVisible = true
        }
    }

    private fun setHeaderChatUi() {
        binding?.chatLayout?.apply {
            optionsButton.setOnClickListener { listener?.onMoreClick() }
            contactLayout.setOnClickListener { listener?.onContactClick() }
            callButton.setOnClickListener { listener?.onCallClick() }
            backButton.setOnClickListener { listener?.onBackClick() }
            parentLayout.isVisible = true
        }
    }

    private fun setHeaderChatSettingsUi() {
        binding?.chatLayout?.apply {
            optionsButton.setOnClickListener { listener?.onMoreClick() }
            callButton.setOnClickListener { listener?.onCallClick() }
            backButton.setOnClickListener { listener?.onBackClick() }
            parentLayout.isVisible = true
        }
    }

    fun setHeaderChatSettingsData(data: ChatSettingsViewModel.ChatInfoDetails.MainData) {
        binding?.chatLayout?.apply {
            contactName.text = data.mainData.name

            when (data.mainData.icon) {
                null -> {
                    contactImage.showDrawable(getColoredCircle(data.mainData.color))
                    initials.text = data.mainData.name.getNameInitials()
                    initials.isVisible = true
                }

                else -> {
                    contactImage.showPlaceholderCircleImage(data.mainData.icon)
                    initials.isVisible = false
                }
            }
        }
    }

    fun setHeaderChatSettingsData(data: CallDetailsViewModel.ChatInfoDetails.MainData) {
        binding?.chatLayout?.apply {
            contactName.text = data.mainData.name

            when (data.mainData.icon) {
                null -> {
                    contactImage.showDrawable(getColoredCircle(data.mainData.color))
                    initials.text = data.mainData.name.getNameInitials()
                    initials.isVisible = true
                }

                else -> {
                    contactImage.showPlaceholderCircleImage(data.mainData.icon)
                    initials.isVisible = false
                }
            }
        }
    }

    fun setHeaderChatData(data: ConversationData) {
        binding?.chatLayout?.apply {
            contactName.text = data.name

            when (data.icon) {
                null -> {
                    contactImage.showDrawable(getColoredCircle(data.color))
                    initials.text = data.name.getNameInitials()
                    initials.isVisible = true
                }

                else -> {
                    contactImage.showPlaceholderCircleImage(data.icon)
                    initials.isVisible = false
                }
            }
        }
    }

    fun setHeaderMainData(data: ContactData) {
        binding?.mainLayout?.apply {

            when (data.avatar) {
                null -> {
                    profileButton.showDrawable(getColoredCircle(data.color))
                    initials.text = data.name.getNameInitials()
                    initials.isVisible = true
                }

                else -> {
                    profileButton.showPlaceholderCircleImage(data.avatar)
                    initials.isVisible = false
                }
            }

            profileButton.setOnClickListener {
                listener?.onMyProfileClick()
            }
        }
    }

    fun setHeaderChatAttachData(data: CategoryAttach) {
        binding?.titleLayout?.apply {
            title.text = data.headerTitle
            parentLayout.isVisible = true
        }
    }

    fun getMoreOptionsView(): View? {
        return binding?.mainLayout?.optionsButton
    }

    private fun setHeaderSearchUi() {
        binding?.inputLayout?.apply {
            listener?.onSearchClick(
                searchInputView
            )
            backButton.setOnClickListener {
                listener?.onHideInputClick(searchInputView)
                listener?.onBackClick()
            }
            clearButton.setOnClickListener {
                searchInputView.text = Editable.Factory().newEditable("")
            }
            searchInputView.addTextChangedListener(object : AfterTextChangedWatcher() {
                override fun afterTextChanged(s: Editable?) {
                    clearButton.isVisible = !s.isNullOrEmpty()
                    listener?.onInputTypedText(s.toString())
                }
            })
            searchInputView.requestFocus()
            parentLayout.isVisible = true
        }
    }

    private fun setHeaderContactsUi() {
        binding?.titleLayout?.apply {
            title.text = root.context.getText(R.string.title_contacts)
            backButton.setOnClickListener { listener?.onBackClick() }
            searchButton.setOnClickListener {
                resetViewUi()
                showSearchInput()
            }
            searchButton.isVisible = true
            parentLayout.isVisible = true
        }
    }

    private fun setHeaderGroupMembersUi() {
        binding?.titleLayout?.apply {
            title.text = root.context.getText(R.string.title_group_members)
            backButton.setOnClickListener { listener?.onBackClick() }
            parentLayout.isVisible = true
        }
    }

    private fun setHeaderGroupNameUi() {
        binding?.titleLayout?.apply {
            title.text = root.context.getText(R.string.title_group_name)
            backButton.setOnClickListener { listener?.onBackClick() }
            parentLayout.isVisible = true
        }
    }

    private fun setHeaderSettingsUi() {
        binding?.titleLayout?.apply {
            title.text = root.context.getText(R.string.title_settings)
            backButton.setOnClickListener { listener?.onBackClick() }
            parentLayout.isVisible = true
        }
    }

    private fun setHeaderCallUi() {
        binding?.titleLayout?.apply {
            backButton.setOnClickListener { listener?.onBackClick() }
            parentLayout.isVisible = true
            background.isVisible = false
        }
    }

    private fun setHeaderAttachmentUi() {
        binding?.titleLayout?.apply {
            backButton.setOnClickListener { listener?.onBackClick() }
            parentLayout.isVisible = true
        }
    }

    private fun setHeaderNewContactUi() {
        binding?.titleLayout?.apply {
            title.text = root.context.getText(R.string.title_new_contact)
            backButton.setOnClickListener { listener?.onBackClick() }
            parentLayout.isVisible = true
        }
    }

    private fun setHeaderCallsUi() {
        binding?.titleLayout?.apply {
            title.text = root.context.getText(R.string.title_calls)
            backButton.setOnClickListener { listener?.onBackClick() }
            parentLayout.isVisible = true
        }
    }

    private fun showSearchInput() {
        binding?.inputLayout?.apply {
            backButton.setOnClickListener {
                listener?.onHideInputClick(searchInputView)
                setHeaderType()
            }
            clearButton.setOnClickListener {
                searchInputView.text = Editable.Factory().newEditable("")
            }
            searchInputView.addTextChangedListener(object : AfterTextChangedWatcher() {
                override fun afterTextChanged(s: Editable?) {
                    clearButton.isVisible = !s.isNullOrEmpty()
                    listener?.onInputTypedText(s.toString())
                }
            })
            searchInputView.requestFocus()
            listener?.onSearchClick(searchInputView)
            parentLayout.isVisible = true
        }
    }

    fun setSelectedChatsUi(
        counter: Int, isMuteShown: Boolean? = null, isPinShown: Boolean? = null
    ) {
        binding?.chatsSelectorLayout?.apply {
            when (counter) {
                COUNTER_DEFAULT -> {
                    resetViewUi()
                    setHeaderMainUi()
                }

                else -> {
                    resetViewUi()

                    isPinShown?.let {
                        pinButton.setOnClickListener {
                            listener?.onPinChangeClick(isPinShown)
                        }
                    }
                    isMuteShown?.let {
                        muteButton.setOnClickListener {
                            listener?.onMuteChangeClick(isMuteShown)
                        }
                    }

                    when (isPinShown) {
                        true -> pinButton.setImageResource(R.drawable.ic_pin)
                        false -> pinButton.setImageResource(R.drawable.ic_unpin)
                        else -> pinButton.isVisible = false
                    }

                    when (isMuteShown) {
                        true -> muteButton.setImageResource(R.drawable.ic_mute)
                        false -> muteButton.setImageResource(R.drawable.ic_unmute)
                        else -> muteButton.isVisible = false
                    }

                    selectorCounter.text = "$counter"
                    cancelButton.setOnClickListener { listener?.onCancelSelectorClick() }
                    deleteButton.setOnClickListener { listener?.onDeleteClick() }
                    optionsButton.setOnClickListener { listener?.onMoreClick() }
                    parentLayout.isVisible = true
                }
            }
        }
    }

    fun setSelectedCallsUi(
        counter: Int
    ) {
        binding?.callsSelectorLayout?.apply {
            when (counter) {
                COUNTER_DEFAULT -> {
                    resetViewUi()
                    setHeaderMainUi()
                }

                else -> {
                    resetViewUi()

                    selectorCounter.text = "$counter"
                    cancelButton.setOnClickListener { listener?.onCancelSelectorClick() }
                    deleteButton.setOnClickListener { listener?.onDeleteClick() }
                    optionsButton.setOnClickListener { listener?.onMoreClick() }
                    parentLayout.isVisible = true
                }
            }
        }
    }

    fun getInputView(): EditText? {
        return binding?.inputLayout?.searchInputView
    }

    companion object {
        private var headerType = 0
        private const val MAIN = 0
        private const val CHAT = 1
        private const val CHAT_SETTINGS = 2
        private const val SEARCH = 3
        private const val CONTACTS = 4
        private const val GROUP_MEMBERS = 5
        private const val GROUP_NAME = 6
        private const val SETTINGS = 7
        private const val CALL = 8
        private const val ATTACHMENT = 9
        private const val NEW_CONTACT = 10
        private const val CALLS = 11

        private const val COUNTER_DEFAULT = 0
    }
}


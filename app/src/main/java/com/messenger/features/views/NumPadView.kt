package com.messenger.features.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View.OnClickListener
import androidx.constraintlayout.widget.ConstraintLayout
import com.messenger.R
import com.messenger.databinding.ViewNumpadBinding
import com.messenger.utils.Constants.Companion.USE_FOR_PIN_CODE_MAX_VALUE

class NumPadView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var binding: ViewNumpadBinding? = null
    private var typedNumbers = ""

    interface NumPadViewListener {
        fun onTypedCode(code: String)
        fun onAutoTypedCode(code: String) {}
    }

    companion object {

        private var MAX_VALUE = USE_FOR_PIN_CODE_MAX_VALUE
    }

    private var listener: NumPadViewListener? = null

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.view_numpad, this, true)
        binding = ViewNumpadBinding.bind(view)

        binding?.apply {
            numpadBtn1.setOnClickListener { addNumber(numpadBtn1.text.toString()) }
            numpadBtn2.setOnClickListener { addNumber(numpadBtn2.text.toString()) }
            numpadBtn3.setOnClickListener { addNumber(numpadBtn3.text.toString()) }
            numpadBtn4.setOnClickListener { addNumber(numpadBtn4.text.toString()) }
            numpadBtn5.setOnClickListener { addNumber(numpadBtn5.text.toString()) }
            numpadBtn6.setOnClickListener { addNumber(numpadBtn6.text.toString()) }
            numpadBtn7.setOnClickListener { addNumber(numpadBtn7.text.toString()) }
            numpadBtn8.setOnClickListener { addNumber(numpadBtn8.text.toString()) }
            numpadBtn9.setOnClickListener { addNumber(numpadBtn9.text.toString()) }
            numpadBtn0.setOnClickListener { addNumber(numpadBtn0.text.toString()) }

            numpadBtnBackspace.setOnClickListener(OnClickListener {
                if (typedNumbers.isEmpty()) {
                    return@OnClickListener
                }
                typedNumbers = typedNumbers.substring(0, typedNumbers.length - 1)
                listener?.onTypedCode(typedNumbers)
            })
            numpadBtnBackspace.setOnLongClickListener {
                clearInput()
                return@setOnLongClickListener true
            }
        }
    }

    private fun addNumber(text: String?) {
        if (typedNumbers.length < MAX_VALUE) {
            typedNumbers += text
            listener?.onTypedCode(typedNumbers)
        }
    }

    fun clearInput() {
        typedNumbers = ""
        listener?.onTypedCode(typedNumbers)
    }

    fun setAutoTypedCode(code: String) {
        typedNumbers = code
        listener?.onAutoTypedCode(typedNumbers)
    }

    fun getText(): String {
        return typedNumbers
    }

    fun setListener(listener: NumPadViewListener? = null) {
        this.listener = listener
    }

    fun setMaxNumbers(maxValue: Int) {
        MAX_VALUE = maxValue
    }
}
package com.messenger.features.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.messenger.R
import com.messenger.databinding.LayoutSwitchViewBinding

class SwitchView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var binding: LayoutSwitchViewBinding? = null
    private var listener: SwitchViewListener? = null

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_switch_view, this, true)
        binding = LayoutSwitchViewBinding.bind(view)

        attrs.let {
            val attrArray = context.obtainStyledAttributes(attrs, R.styleable.SwitchView)
            if (attrArray.hasValue(R.styleable.SwitchView_switchTitle)) {
                val title = attrArray.getString(R.styleable.SwitchView_switchTitle)
                binding?.switchTitle?.let {
                    it.text = title
                }
            }
            if (attrArray.hasValue(R.styleable.SwitchView_isLastSwitch)) {
                val isLastScopeItem =
                    attrArray.getBoolean(R.styleable.SwitchView_isLastSwitch, false)
                binding?.bottomLine?.isVisible = isLastScopeItem
            }
            attrArray.recycle()
        }

        binding?.switchLayout?.setOnClickListener {
            listener?.onSwitchClick(!isSettingChecked())
        }
    }

    private fun isSettingChecked() = binding?.switchButton?.isChecked ?: false

    fun setSubTitle(subtitle: String?) {
        when {
            subtitle.isNullOrEmpty() -> {
                binding?.switchSubTitle?.visibility = View.INVISIBLE
                binding?.switchSubTitle?.text = ""
            }

            else -> {
                binding?.switchSubTitle?.visibility = View.VISIBLE
                binding?.switchSubTitle?.text = subtitle
            }
        }
    }

    fun setChecked(isChecked: Boolean) {
        binding?.switchButton?.isChecked = isChecked
    }

    fun setListener(listener: SwitchViewListener) {
        this.listener = listener
    }

    fun hideButton() {
        binding?.switchButton?.isVisible = false
    }

    interface SwitchViewListener {
        fun onSwitchClick(isChecked: Boolean)
    }
}




package com.messenger.features.views.progress

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.messenger.R

class ProgressBar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val paint = Paint()

    init {
        paint.color = ContextCompat.getColor(context, R.color.dark_green)
        paint.isAntiAlias = true
    }

    override fun onDraw(canvas: Canvas) {
        val x1 = (left + paddingLeft).toFloat()
        val x2 = (right - paddingRight).toFloat()
        val y1 = (top + paddingTop).toFloat()
        val y2 = (bottom - paddingBottom).toFloat()
        val w = x2 - x1
        val d = w / 120f
        val interpolation = interpolation
        var f1: Float = interpolation * PART - PART
        loop@ while (true) {
            val f2: Float = f1 + PART
            var dx1 = if (f1 >= 0) (f1 * f1 - SHIFT) * w else 0f
            var dx2: Float = (f2 * f2 - SHIFT) * w - d
            if (dx1 < x1) {
                dx1 = x1
            }
            if (dx2 > x2) {
                dx2 = x2
            }
            if (dx1 >= x2) {
                break@loop
            }
            if (dx2 >= x1) {
                canvas.drawRect(dx1, y1, dx2, y2, paint)
            }
            f1 += PART
        }
        invalidate()
    }

    private val interpolation: Float
        get() = (System.currentTimeMillis() % 500L).toFloat() / 500f

    companion object {
        private val TAG = ProgressBar::class.java.simpleName
        private const val PART = 0.25f
        private const val SHIFT = 0.05f
    }
}








package com.messenger.features.views.progress

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.messenger.R
import com.messenger.databinding.ViewSetupAccountBinding

class ProgressSetupView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var binding: ViewSetupAccountBinding? = null

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.view_setup_account, this, true)
        binding = ViewSetupAccountBinding.bind(view)
    }

    fun setProgressEvent(isLoading: Boolean) {
        binding?.progressLayout?.isVisible = isLoading
    }
}
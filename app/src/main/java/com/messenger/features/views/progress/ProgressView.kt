package com.messenger.features.views.progress

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.messenger.R
import com.messenger.databinding.LayoutProgressViewBinding

class ProgressView @JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    ConstraintLayout(context, attrs, defStyleAttr) {

    private var binding: LayoutProgressViewBinding? = null

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_progress_view, this, true)
        binding = LayoutProgressViewBinding.bind(view)
    }

    fun setProgressEvent(isLoading: Boolean) {
        binding?.progressLayout?.isVisible = isLoading
    }
}








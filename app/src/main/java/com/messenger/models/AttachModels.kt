package com.messenger.models

import android.location.Location
import android.net.Uri
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FineLocation(
    val latitude: Double, val longitude: Double, val altitude: Double?
) : Parcelable

data class CategoryAttach(
    val id: Long,
    val name: String,
    val headerTitle: String,
    val type: CategoryAttachType,
    val icon: Int,
    val isSelected: Boolean? = false
)

data class GalleryMediaData(
    val id: Long,
    val uri: Uri,
    val title: String?,
    val size: Long?,
    val duration: Long?,
    val isSelected: Boolean,
    val selectCounter: Int?,
    val type: GalleryDataType
)

enum class GalleryDataType {
    IMAGE, VIDEO, FILE
}

enum class CategoryAttachType {
    CAMERA, GALLERY, FILE, LOCATION, CONTACT, MUSIC
}

data class DetailsViewData(
    val location: Location, val city: String, val street: String
)

sealed class TreasureMapData {
    data class ProgressData(
        val showProgress: Boolean
    ) : TreasureMapData()

    data class LocationData(
        val locationState: LocationState,
        val showOnMap: Boolean,
        val currentLocation: Location? = null
    ) : TreasureMapData()

    data class MapDetailsData(
        val detailsType: DetailsType, val detailsViewData: DetailsViewData?
    ) : TreasureMapData()
}

enum class LocationState {
    LOCATION_AVAILABLE, LOCATION_UNAVAILABLE, LOCATION_MANUAL
}

enum class DetailsType {
    SEARCHING, INFO
}
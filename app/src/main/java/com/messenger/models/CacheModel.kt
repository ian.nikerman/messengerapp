package com.messenger.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CacheProfile(
    val profile: ContactData
) : Parcelable

@Parcelize
data class CacheContacts(
    val contacts: List<ContactData>
) : Parcelable

@Parcelize
data class CacheChatsDetails(
    val chats: List<ConversationData>
) : Parcelable

@Parcelize
data class CacheChats(
    val chats: Map<Long, List<ChatMessage>>
) : Parcelable

@Parcelize
data class CacheCalls(
    val calls: List<CallData>
) : Parcelable


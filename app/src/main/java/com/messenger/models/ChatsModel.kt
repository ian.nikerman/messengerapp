package com.messenger.models

import android.net.Uri
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ContactData(
    val id: Long,
    val name: String,
    val phoneType: PhoneType,
    val username: String,
    val bio: String,
    val isFavourite: Boolean,
    val phoneNumber: List<String>,
    val avatar: Uri?,
    val isSelected: Boolean? = false,
    val isMe: Boolean? = false,
    val color: Int
) : Parcelable

@Parcelize
data class ConversationData(
    val id: Long,
    val name: String,
    val description: String?,
    val inviteLink: String,
    val icon: Uri?,
    val color: Int,
    val admin: ContactData?,
    val members: List<ContactData>,
    val type: ConversationType,
    val unreadMessages: Int?,
    val updatedAt: Long,
    val isMute: Boolean,
    val isPinned: Boolean,
    val isSelected: Boolean? = false
) : Parcelable

@Parcelize
data class ChatMessage(
    val id: Long,
    val message: String,
    val senderType: SenderType,
    val createdAt: Long,
    val type: MessageStatus,
    val messageData: MessageData?,
    val isSelected: Boolean? = false
) : Parcelable

@Parcelize
data class CallData(
    val id: Long,
    val conversationData: ConversationData,
    val createdAt: Long,
    val duration: Long,
    val callStatus: CallStatus,
    val isSelected: Boolean? = false
) : Parcelable

@Parcelize
data class MessageData(
    val id: Long,
    val contentType: ContentType,
    val fileUrl: String? = null,
    val previewUrl: Long? = null,
    val fileStatus: FileStatus? = null,
    val callData: CallData? = null
) : Parcelable

@Parcelize
data class ContentData(
    val id: Long,
    val contentType: ContentType,
    val contactData: ContactData? = null,
    val messageData: MessageData? = null
) : Parcelable

@Parcelize
data class GroupRequest(
    val name: String,
    val description: String,
    val icon: Uri?,
    val admin: ContactData?,
    val members: List<ContactData>
) : Parcelable

@Parcelize
data class ChatInfoItem(
    val id: Int,
    val title: String,
    val subtitle: String,
    val contactData: ContactData?,
    val type: ChatInfoType
) : Parcelable

@Parcelize
data class ChatSettingsTab(
    val id: Int, val contentType: ContentType, val isSelected: Boolean
) : Parcelable

enum class ChatInfoType {
    PHONE, USERNAME, INVITE_LINK, BIO
}

enum class SenderType {
    ME, CONTACT, DATE
}

enum class PhoneType {
    MOBILE, HOME
}

enum class ContentType {
    CONTACT, GROUP, MEDIA, FILE, LINK, AUDIO, VOICE, GIF, CALL
}

enum class FileStatus {
    SENT, RECEIVED, READ, FAILED, LOADING
}

enum class MessageStatus {
    SENDING, SENT, RECEIVED, READ, FAILED, LOADING, UI
}

enum class CallStatus {
    INCOMING_CALL, INCOMING_CALL_CANCELLED, INCOMING_CALL_MISSED, OUTGOING_CALL, OUTGOING_CALL_CANCELLED, OUTGOING_CALL_MISSED
}

enum class ConnectionStatus {
    CONNECTING, CONNECTED
}

enum class ConversationType {
    TYPE_ENCRYPTED_CONVERSATION, TYPE_SECURE_GROUP
}

enum class MainPagerType {
    CONTACTS, CHATS, FAVOURITES, CALLS
}
package com.messenger.models

data class ServerResponse<T>(
    var code: Int = 0, var message: String? = null, var data: T? = null
)

data class ResponseState<out T>(val status: Status, val data: T?, val message: String?) {

    companion object {

        fun <T> onEmpty(): ResponseState<T> {
            return ResponseState(Status.EMPTY, null, "")
        }

        fun <T> success(data: T?): ResponseState<T> {
            return ResponseState(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String): ResponseState<T> {
            return ResponseState(Status.ERROR, null, msg)
        }

        fun <T> loading(): ResponseState<T> {
            return ResponseState(Status.LOADING, null, null)
        }
    }
}

enum class Status {
    EMPTY, SUCCESS, ERROR, LOADING
}
package com.messenger.network

interface ApiService {

//    @POST(SERVER_MESSAGE_SEND)
//    fun sendMessage(@Body message: ChatMessage): ServerResponse<SendMessageResult>
//
//    @GET(SERVER_MESSAGE_RECEIVE)
//    fun receiveMessages(): ServerResponse<Array<XmppMessageBase>>
//
//    @POST(SERVER_ACK_RECEIVE)
//    fun ackReceivedMessages(@Body receivedIds: List<Long>): ServerResponse<AckResult>
//
//    @GET(SERVER_MESSAGE_DELIVERED)
//    fun getDeliveryAcks(): ServerResponse<Array<MessageDeliveryAck>>
//
//    @POST(SERVER_ACK_DELIVERED)
//    fun sendAckedMessageIds(@Body ackedMessagesIds: List<Long>): ServerResponse<AckResult>
//
//    @POST(SERVER_ACK_MESSAGES_SEEN)
//    fun seenAckReceiver(@Body seenIds: List<Long>): ServerResponse<AckResult>
//
//    @GET(SERVER_GET_MESSAGES_SEEN)
//    fun seenAckSender(): ServerResponse<Array<SeenAckSender>>
//
//    @POST(SERVER_SEND_SEEN_SEEN_MESSAGES)
//    fun sendSenderSeenSeenMessages(@Body ackedMessagesIdsS: List<Long>): ServerResponse<AckResult>
//
//    @POST(SERVER_GROUP_SEND)
//    fun sendGroupMessage(@Body message: GroupOperationMessage): ServerResponse<ResponseBody>

    companion object {

        private const val SERVER_MESSAGE_SEND = "v2/Messages/send" // sender sends the messages
        private const val SERVER_MESSAGE_RECEIVE =
            "v2/Messages/receive" // receiver asks for new messages after receiving push notification
        private const val SERVER_ACK_RECEIVE =
            "v2/Messages/receiveAck" // receiver confirms that he has received and saved in db messages with provided ids
        private const val SERVER_MESSAGE_DELIVERED =
            "v2/Messages/delivered" // sender requests delivered and acked messages after receiving push notification
        private const val SERVER_ACK_DELIVERED =
            "Messages/deliveredAck" /* sender confirms that he has been acknowledged that the messages with provided ids had been received and saved by receiver */
        private const val SERVER_ACK_MESSAGES_SEEN =
            "v2/Messages/seenAckReceiver" // receiver acknowledges server that he has read messages with provided ids
        private const val SERVER_GET_MESSAGES_SEEN =
            "v2/Messages/seenAckSender" // sender requests seen messages after receiving push notification
        private const val SERVER_SEND_SEEN_SEEN_MESSAGES =
            "v2/Messages/SenderSeenSeen" /* sender confirms that he has been acknowledged that the messages with provided ids had been seen by receiver */
        private const val SERVER_GROUP_SEND = "Messages/groupOperation"

    }
}


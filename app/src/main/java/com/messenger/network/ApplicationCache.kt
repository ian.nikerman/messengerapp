package com.messenger.network

import android.content.SharedPreferences

class ApplicationCache(
    private val sharedPreferences: SharedPreferences
) {

    var profile: String?
        get() = sharedPreferences.getString(TAG_PROFILE, null)
        set(profile) = sharedPreferences.edit().putString(TAG_PROFILE, profile).apply()

    var contacts: String?
        get() = sharedPreferences.getString(TAG_CONTACTS, null)
        set(contacts) = sharedPreferences.edit().putString(TAG_CONTACTS, contacts).apply()

    var chatsDetails: String?
        get() = sharedPreferences.getString(TAG_CHATS_DETAILS, null)
        set(chatsDetails) = sharedPreferences.edit().putString(TAG_CHATS_DETAILS, chatsDetails)
            .apply()

    var chats: String?
        get() = sharedPreferences.getString(TAG_CHATS, null)
        set(chats) = sharedPreferences.edit().putString(TAG_CHATS, chats).apply()

    var calls: String?
        get() = sharedPreferences.getString(TAG_CALLS, null)
        set(calls) = sharedPreferences.edit().putString(TAG_CALLS, calls).apply()

    companion object {
        private const val TAG_PROFILE = "profile"
        private const val TAG_CONTACTS = "contacts"
        private const val TAG_CHATS_DETAILS = "chats_details"
        private const val TAG_CHATS = "chats"
        private const val TAG_CALLS = "calls"
    }
}

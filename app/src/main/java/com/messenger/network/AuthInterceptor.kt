package com.messenger.network

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.util.concurrent.locks.ReentrantReadWriteLock

class AuthInterceptor(private val apiSettings: ApiSettings) : Interceptor {

    private var storedAuthToken: String? = null

    private val lock = ReentrantReadWriteLock(true)

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val authToken: String?

        var request = chain.request()

        lock.readLock().lock()
        try {
            authToken = getStoredAuthToken()
        } finally {
            lock.readLock().unlock()
        }

        request = addTokenToRequest(request, authToken)
        val response = chain.proceed(request)

        if (!response.header("X-Auth-Token").isNullOrEmpty()) {
            val newToken = response.header("X-Auth-Token") as String
            if (newToken.isNotEmpty()) {
                updateToken(newToken)
            }
        }
        return response
    }

    private fun updateToken(newToken: String) {
        storedAuthToken = newToken
        apiSettings.token = newToken
    }

    private fun getStoredAuthToken(): String? {
        return apiSettings.token
    }

    private fun addTokenToRequest(request: Request, authToken: String?): Request {
        var updatedRequest = request
        if (authToken != null) {
            val requestBuilder =
                request.newBuilder().addHeader("Authorization", "Bearer $authToken")
            updatedRequest = requestBuilder.build()
        }
        return updatedRequest
    }
}

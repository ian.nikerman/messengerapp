package com.messenger.utils

class Constants {

    companion object {

        const val PREFS_KEY = "MessengerSharedPrefs"
        const val BASE_SERVER_URL = ""
        const val SCROLL_START_POS = 0

        const val TIMEOUT = 2000L
        const val TIMEOUT_1 = 1000L
        const val ALPHA_MAX = 1.0f
        const val ALPHA_MIN = 0.0f
        const val RESULT_DEFAULT = -1
        const val RESULT_WIFI_CONNECTED = 1
        const val RESULT_WIFI_FAILED = 2

        const val USE_FOR_SMS_CODE_MAX_VALUE = 6
        const val USE_FOR_PIN_CODE_MAX_VALUE = 4

        const val ARG_REG_RESULT = "ARG_REG_RESULT"
        const val INTENT_FILTER_REGISTRATION = "INTENT_FILTER_REGISTRATION"
        const val INTENT_FILTER_WIFI_CONNECTION = "INTENT_FILTER_WIFI_CONNECTION"
        const val INTENT_FILTER_EMAIL_VALIDATION = "INTENT_FILTER_EMAIL_VALIDATION"
        const val ARG_WIFI_RESULT = "ARG_WIFI_RESULT"
        const val ARG_KEY_ERROR = "ARG_KEY_ERROR"
        const val ACTION_RECEIVE_SMS = "action_receive_sms"
        const val ACTION_CHECK_REG_CODE = "action_check_reg_code"
        const val ACTION_CODE_BY_CALL_REQUEST = "code_by_call_request"
        const val KEY_CALL_BY = "call_by"
        const val KEY_EXTRA = "extra"
        const val WIFI_CONNECT_RESULT: String = "WIFI_CONNECT_RESULT"
        const val REGISTRATION_RESULT: String = "REGISTRATION_RESULT"
        const val ACTION_SET_USER_CONFIG = "com.kcore.config.set"
        const val KEY_WIZARD_SEMI_SECURE = "wizard_semi_secure"
        const val KEY_WIZARD_CHECK_CODE = "wizard_check_code"

        const val REQUEST_NEW_GROUP_CONTRACT = 1

        const val EMAIL_REGEX = "\\S+@[\\w_.-]*\\.[\\w_.-]*"

        enum class SetupStage {
            start_registration, enter_domain, enter_email, enter_phone, all_set
        }

        enum class RegistrationCodeErrors {
            success, incorrect_code, unknown_error
        }

    }
}

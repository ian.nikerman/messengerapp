package com.messenger.utils

import android.content.Context
import com.messenger.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

fun getFullFormatDate(context: Context, inputDate: String): String {

    val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    formatter.timeZone = TimeZone.getTimeZone("UTC")
    val date: Date?
    try {
        date = formatter.parse(inputDate)
    } catch (e: ParseException) {
        e.printStackTrace()
        return inputDate
    }

    var formattedDate = ""

    var formattedTime = ""
    val timeFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
    date?.let {
        formattedTime = timeFormat.format(it)
    }

    val dateWithTimeFormat = SimpleDateFormat("yyyy MMMM d, HH:mm", Locale.getDefault())
    date?.let {
        formattedDate = dateWithTimeFormat.format(it)
    }

    when {
        isDateNow(date) -> {
            return String.format(
                context.getString(R.string.date_format_just_now_title), formattedTime
            )
        }
        isDateTomorrow(date) -> {
            return String.format(
                context.getString(R.string.date_format_tomorrow_title), formattedTime
            )
        }
        isDateToday(date) -> {
            return String.format(
                context.getString(R.string.date_format_today_title), formattedTime
            )
        }
        isDateYesterday(date) -> {
            return String.format(
                context.getString(R.string.date_format_yesterday_title), formattedTime
            )
        }
        !isSameYear(date) -> {
            return formattedDate
        }
        else -> {
            val currentYearDateFormat = SimpleDateFormat("MMMM d, HH:mm", Locale.getDefault())
            date?.let {
                formattedDate = currentYearDateFormat.format(it)
            }
            return formattedDate
        }
    }
}

fun getChatDateFormatDate(context: Context, inputDate: Long): String {

    val localTimestamp = utcToLocalTimestamp(inputDate)

    val date = Calendar.getInstance()
    date.timeInMillis = localTimestamp

    var formattedDate = ""

    val dateWithTimeFormat = SimpleDateFormat("yyyy MMMM d", Locale.getDefault())
    date.let {
        formattedDate = dateWithTimeFormat.format(it.time)
    }

    when {
        isDateToday(date.time) -> {
            return context.getString(R.string.date_format_today_simple_title)
        }
        isDateYesterday(date.time) -> {
            return context.getString(R.string.date_format_yesterday_simple_title)
        }
        !isSameYear(date.time) -> {
            return formattedDate
        }
        else -> {
            val currentYearDateFormat = SimpleDateFormat("MMMM d, EEE", Locale.getDefault())
            date.time.let {
                formattedDate = currentYearDateFormat.format(it)
            }
            return formattedDate
        }
    }
}

private fun isDateTomorrow(date: Date): Boolean {
    val now = Calendar.getInstance()
    now.add(Calendar.DATE, +1)
    val inputDate = Calendar.getInstance()
    inputDate.timeInMillis = date.time
    return now.get(Calendar.DATE) == inputDate.get(Calendar.DATE) && now.get(Calendar.YEAR) == inputDate.get(
        Calendar.YEAR
    )
}

private fun isDateNow(date: Date): Boolean {
    val now = Calendar.getInstance()
    val inputDate = Calendar.getInstance()
    inputDate.timeInMillis = date.time
    return now.get(Calendar.DATE) == inputDate.get(Calendar.DATE) && now.get(Calendar.HOUR_OF_DAY) == inputDate.get(
        Calendar.HOUR_OF_DAY
    ) && now.get(Calendar.MINUTE) == inputDate.get(Calendar.MINUTE) && now.get(Calendar.YEAR) == inputDate.get(
        Calendar.YEAR
    )
}

private fun isDateToday(date: Date): Boolean {
    val now = Calendar.getInstance()
    val inputDate = Calendar.getInstance()
    inputDate.timeInMillis = date.time
    return now.get(Calendar.DATE) == inputDate.get(Calendar.DATE) && now.get(Calendar.YEAR) == inputDate.get(
        Calendar.YEAR
    )
}

private fun isDateToday(time: Long): Boolean {
    val now = Calendar.getInstance()
    val inputDate = Calendar.getInstance()
    inputDate.timeInMillis = time
    return now.get(Calendar.DATE) == inputDate.get(Calendar.DATE) && now.get(Calendar.YEAR) == inputDate.get(
        Calendar.YEAR
    )
}

private fun isDateYesterday(time: Long): Boolean {
    val now = Calendar.getInstance()
    val inputDate = Calendar.getInstance()
    inputDate.timeInMillis = time
    return now.get(Calendar.DATE) - inputDate.get(Calendar.DATE) == 1
}

private fun isDateYesterday(date: Date): Boolean {
    val now = Calendar.getInstance()
    val inputDate = Calendar.getInstance()
    inputDate.timeInMillis = date.time
    return now.get(Calendar.DATE) - inputDate.get(Calendar.DATE) == 1
}

private fun isSameWeek(time: Long): Boolean {
    val now = Calendar.getInstance()
    val inputDate = Calendar.getInstance()
    inputDate.timeInMillis = time
    return now.get(Calendar.WEEK_OF_YEAR) == inputDate.get(Calendar.WEEK_OF_YEAR) && now.get(
        Calendar.YEAR
    ) == inputDate.get(Calendar.YEAR)
}

private fun isSameYear(date: Date): Boolean {
    val now = Calendar.getInstance()
    val inputDate = Calendar.getInstance()
    inputDate.timeInMillis = date.time
    return now.get(Calendar.YEAR) == inputDate.get(Calendar.YEAR)
}

fun isSameDay(date1: Long, date2: Long): Boolean {
    val cal1: Calendar = Calendar.getInstance()
    val cal2 = Calendar.getInstance()
    cal1.timeInMillis = utcToLocalTimestamp(date1)
    cal2.timeInMillis = utcToLocalTimestamp(date2)
    return cal1.get(Calendar.DAY_OF_YEAR) == cal2[Calendar.DAY_OF_YEAR] && cal1.get(Calendar.YEAR) == cal2[Calendar.YEAR]
}

private fun isSameYear(time: Long): Boolean {
    val now = Calendar.getInstance()
    val inputDate = Calendar.getInstance()
    inputDate.timeInMillis = time
    return now.get(Calendar.YEAR) == inputDate.get(Calendar.YEAR)
}

private fun getDateFromMilliseconds(time: Long): String {
    val simpleDateFormat = SimpleDateFormat("MMMM d, yyyy, HH:mm", Locale.getDefault())
    return simpleDateFormat.format(time)
}

private fun getDateShortFromMilliseconds(time: Long): String {
    val simpleDateFormat = SimpleDateFormat("MM/dd/yy, HH:mm", Locale.getDefault())
    return simpleDateFormat.format(time)
}

private fun getDayOfWeekFromMilliseconds(time: Long): String {
    val simpleDateFormat = SimpleDateFormat("EEE", Locale.getDefault())
    return simpleDateFormat.format(time)
}

private fun getTimeFromMilliseconds(time: Long): String {
    val simpleDateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
    return simpleDateFormat.format(time)
}

private fun getDateOfThisYearFromMilliseconds(time: Long): String {
    val simpleDateFormat = SimpleDateFormat("MMM d", Locale.getDefault())
    return simpleDateFormat.format(time)
}

private fun getDateOfYearFromMilliseconds(time: Long): String {
    val simpleDateFormat = SimpleDateFormat("MM/dd/yy", Locale.getDefault())
    return simpleDateFormat.format(time)
}

fun utcToLocalFullFormatted(time: Long): String {
    val tz = TimeZone.getTimeZone("UTC")
    val cal = GregorianCalendar.getInstance(tz)
    val offsetInMillis = tz.getOffset(cal.timeInMillis)
    val localTime = when {
        offsetInMillis >= 0 -> time + offsetInMillis
        else -> time - offsetInMillis
    }
    val formattedTime = when {
        isDateToday(localTime) -> {
            "Today at ${getTimeFromMilliseconds(localTime)}"
        }
        isDateYesterday(localTime) -> {
            "Yesterday at ${getTimeFromMilliseconds(localTime)}"
        }
        isSameYear(localTime) -> {
            "${getDateOfThisYearFromMilliseconds(localTime)} at ${getTimeFromMilliseconds(localTime)}"
        }
        else -> "${getDateOfYearFromMilliseconds(localTime)} at ${getTimeFromMilliseconds(localTime)}"
    }
    return formattedTime
}

fun utcToLocalFormatted(time: Long): String {
    val tz = TimeZone.getTimeZone("UTC")
    val cal = GregorianCalendar.getInstance(tz)
    val offsetInMillis = tz.getOffset(cal.timeInMillis)
    val localTime = when {
        offsetInMillis >= 0 -> time + offsetInMillis
        else -> time - offsetInMillis
    }
    val formattedTime = when {
        isDateToday(localTime) -> {
            getTimeFromMilliseconds(localTime)
        }
        isSameWeek(localTime) -> {
            getDayOfWeekFromMilliseconds(localTime)
        }
        isSameYear(localTime) -> {
            getDateOfThisYearFromMilliseconds(localTime)
        }
        else -> getDateOfYearFromMilliseconds(localTime)
    }
    return formattedTime
}

fun utcToTimeFormatted(time: Long): String {
    val tz = TimeZone.getTimeZone("UTC")
    val cal = GregorianCalendar.getInstance(tz)
    val offsetInMillis = tz.getOffset(cal.timeInMillis)
    val localTime = when {
        offsetInMillis >= 0 -> time + offsetInMillis
        else -> time - offsetInMillis
    }
    return getTimeFromMilliseconds(localTime)
}

fun utcToLocalTimestamp(time: Long): Long {
    val tz = TimeZone.getTimeZone("UTC")
    val cal = GregorianCalendar.getInstance(tz)
    val offsetInMillis = tz.getOffset(cal.timeInMillis)
    return if (offsetInMillis >= 0) {
        time + offsetInMillis
    } else {
        time - offsetInMillis
    }
}

fun getDurationFromMilliseconds(time: Long): String {
    val hours: Long = TimeUnit.MILLISECONDS.toHours(time)
    val minutes: Long = TimeUnit.MILLISECONDS.toMinutes(time)
    val seconds: Long = TimeUnit.MILLISECONDS.toSeconds(time)

    when {
        hours != 0L -> return String.format("%s hours %s minutes", hours, minutes)
        minutes != 0L -> return String.format("%s minutes", minutes)
        seconds != 0L -> return String.format("%s seconds", seconds)
    }
    return ""
}

fun getUTCMilliseconds(): Long {
    return System.currentTimeMillis()
}
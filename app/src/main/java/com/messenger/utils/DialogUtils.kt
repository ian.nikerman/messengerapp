package com.messenger.utils

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.provider.Settings
import androidx.activity.result.ActivityResultLauncher
import androidx.appcompat.app.AlertDialog
import com.messenger.R

/**
 * Ask the user if they want to enable GPS, and if so, show them system settings
 */
fun promptEnableGps(context: Context) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(context)
    builder.setMessage(context.getString(R.string.enable_gps_message)).setPositiveButton(
        context.getString(R.string.enable_gps_positive_button)
    ) { _: DialogInterface?, _: Int ->
        val intent = Intent(
            Settings.ACTION_LOCATION_SOURCE_SETTINGS
        )
        context.startActivity(intent)
    }.setNegativeButton(
        context.getString(R.string.enable_gps_negative_button)
    ) { _: DialogInterface?, _: Int ->

    }
    builder.create().show()
}

/**
 * Ask the user if they want to confirm fine location permissions
 */
fun showLocationRationaleDialog(
    context: Context, permissionRequest: ActivityResultLauncher<String>
) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(context)
    builder.setMessage(context.getString(R.string.alert_dialog_permission_location_message))
        .setPositiveButton(context.getString(R.string.alert_dialog_permission_positive_button)) { dialog, _ ->
            permissionRequest.launch(Manifest.permission.ACCESS_FINE_LOCATION)
            dialog.dismiss()
        }
        .setNegativeButton(context.getString(R.string.alert_dialog_permission_negative_button)) { dialog, _ ->
            dialog.dismiss()
        }
    builder.create().show()
}


/**
 * Ask the user if they want to confirm camera permissions
 */
fun showCameraRationaleDialog(context: Context, permissionRequest: ActivityResultLauncher<String>) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(context)
    builder.setMessage(context.getString(R.string.alert_dialog_permission_camera_message))
        .setPositiveButton(context.getString(R.string.alert_dialog_permission_positive_button)) { dialog, _ ->
            permissionRequest.launch(Manifest.permission.CAMERA)
            dialog.dismiss()
        }
        .setNegativeButton(context.getString(R.string.alert_dialog_permission_negative_button)) { dialog, _ ->
            dialog.dismiss()
        }
    builder.create().show()
}

/**
 * Ask the user if they want to confirm storage permissions
 */
fun showStorageRationaleDialog(
    context: Context, permissionRequest: ActivityResultLauncher<String>
) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(context)
    builder.setMessage(context.getString(R.string.alert_dialog_permission_read_storage_message))
        .setPositiveButton(context.getString(R.string.alert_dialog_permission_positive_button)) { dialog, _ ->
            permissionRequest.launch(storagePermission())
            dialog.dismiss()
        }
        .setNegativeButton(context.getString(R.string.alert_dialog_permission_negative_button)) { dialog, _ ->
            dialog.dismiss()
        }
    builder.create().show()
}

/**
 * Ask the user if they want to confirm contacts permissions
 */
fun showContactsRationaleDialog(
    context: Context, permissionRequest: ActivityResultLauncher<String>
) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(context)
    builder.setMessage(context.getString(R.string.alert_dialog_permission_read_contacts_message))
        .setPositiveButton(context.getString(R.string.alert_dialog_permission_positive_button)) { dialog, _ ->
            permissionRequest.launch(storagePermission())
            dialog.dismiss()
        }
        .setNegativeButton(context.getString(R.string.alert_dialog_permission_negative_button)) { dialog, _ ->
            dialog.dismiss()
        }
    builder.create().show()
}
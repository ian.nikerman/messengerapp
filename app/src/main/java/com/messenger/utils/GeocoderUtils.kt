package com.messenger.utils

import android.content.Context
import android.location.Address
import android.location.Geocoder
import java.io.IOException

class GeocoderUtils {

    companion object {

        fun getCity(
            context: Context,
            latitude: Double? = COORDINATE_DEFAULT,
            longitude: Double? = COORDINATE_DEFAULT
        ): String {
            try {
                val geoCoder = Geocoder(context)
                val addresses = geoCoder.getFromLocation(
                    latitude ?: COORDINATE_DEFAULT, longitude ?: COORDINATE_DEFAULT, 1
                )
                if (!addresses.isNullOrEmpty()) {
                    val returnAddress = addresses[0]
                    var cityAddress =
                        "${getLocality(returnAddress)}${getSubAdminArea(returnAddress)}${
                            getAdminArea(
                                returnAddress
                            )
                        }${getCountryName(returnAddress)}"
                    if (cityAddress.startsWith(",")) {
                        cityAddress = cityAddress.substringAfter(",")
                    }
                    return when {
                        cityAddress.isNotEmpty() -> cityAddress
                        else -> {
                            getFeatureName(returnAddress)
                        }
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return ""
        }

        fun getStreet(
            context: Context,
            latitude: Double? = COORDINATE_DEFAULT,
            longitude: Double? = COORDINATE_DEFAULT
        ): String {
            val geoCoder = Geocoder(context)

            try {
                val addresses = geoCoder.getFromLocation(
                    latitude ?: COORDINATE_DEFAULT, longitude ?: COORDINATE_DEFAULT, 1
                )
                if (!addresses.isNullOrEmpty()) {
                    val returnAddress = addresses[0]
                    val streetName = returnAddress.thoroughfare ?: ""
                    val homeNumber = returnAddress.subThoroughfare ?: ""
                    return "$streetName $homeNumber"
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return ""
        }

        private const val COORDINATE_DEFAULT = 0.000000

        private fun getLocality(returnAddress: Address) =
            if (!returnAddress.locality.isNullOrEmpty()) returnAddress.locality else ""

        private fun getFeatureName(returnAddress: Address) =
            if (!returnAddress.featureName.isNullOrEmpty()) returnAddress.featureName else ""

        private fun getSubAdminArea(returnAddress: Address) =
            if (!returnAddress.subAdminArea.isNullOrEmpty()) ", ${returnAddress.subAdminArea}" else ""

        private fun getAdminArea(returnAddress: Address) =
            if (!returnAddress.adminArea.isNullOrEmpty()) ", ${returnAddress.adminArea}" else ""

        private fun getCountryName(returnAddress: Address) =
            if (!returnAddress.countryName.isNullOrEmpty()) ", ${returnAddress.countryName}" else ""
    }
}
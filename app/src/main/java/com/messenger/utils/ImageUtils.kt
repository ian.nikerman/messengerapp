package com.messenger.utils

import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.messenger.R

fun ImageView.showImage(uri: Uri?) {
    Glide.with(this.context).load(uri).into(this)
}

fun ImageView.showImageAnimated(uri: Uri?) {
    Glide.with(this.context).load(uri).transition(DrawableTransitionOptions.withCrossFade())
        .into(this)
}

fun ImageView.showPlaceholderImage(uri: Uri?) {
    Glide.with(this.context).load(uri).placeholder(R.drawable.placeholder_image).into(this)
}

fun ImageView.showPlaceholderImageCorneredAnimated(uri: Uri?) {
    Glide.with(this.context).load(uri).apply(RequestOptions.bitmapTransform(RoundedCorners(14)))
        .transition(DrawableTransitionOptions.withCrossFade())
        .placeholder(R.drawable.placeholder_image_cornered).into(this)
}


fun ImageView.showPlaceholderCircleImage(uri: Uri?) {
    Glide.with(this.context).load(uri).apply(RequestOptions().circleCrop())
        .placeholder(R.drawable.background_placeholder_oval).into(this)
}

fun ImageView.showPlaceholderCircleImage(drawable: Int) {
    Glide.with(this.context).load(ContextCompat.getDrawable(this.context, drawable))
        .placeholder(R.drawable.background_placeholder_oval).into(this)
}

fun ImageView.showDrawable(drawable: GradientDrawable?) {
    Glide.with(this.context).load(drawable).placeholder(R.drawable.background_placeholder_oval)
        .into(this)
}

fun ImageView.resetImage(uri: Uri?) {
    Glide.with(this.context).load(uri).into(this)
}
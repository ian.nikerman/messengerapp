package com.messenger.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

fun closeKeyboard(context: Context, focusView: View) {
    val inputManager: InputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputManager.hideSoftInputFromWindow(
        focusView.windowToken, InputMethodManager.RESULT_UNCHANGED_SHOWN
    )
}

fun openKeyboard(context: Context, focusView: View) {
    val inputManager: InputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputManager.showSoftInput(focusView, InputMethodManager.SHOW_FORCED)
}
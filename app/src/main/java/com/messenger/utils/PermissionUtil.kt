package com.messenger.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat

/**
 * Helper function to simplify permission checks/requests.
 */
fun Context.hasPermission(permission: String): Boolean {
    return ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
}

fun Activity.isLocationPermissionDenied(): Boolean {
    return ActivityCompat.shouldShowRequestPermissionRationale(
        this, Manifest.permission.ACCESS_FINE_LOCATION
    )
}

fun Activity.isCameraPermissionDenied(): Boolean {
    return ActivityCompat.shouldShowRequestPermissionRationale(
        this, Manifest.permission.CAMERA
    )
}

fun Activity.isContactsPermissionDenied(): Boolean {
    return ActivityCompat.shouldShowRequestPermissionRationale(
        this, Manifest.permission.READ_CONTACTS
    )
}

fun Activity.isReadStoragePermissionDenied(): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) ActivityCompat.shouldShowRequestPermissionRationale(
        this, Manifest.permission.READ_MEDIA_IMAGES
    ) else ActivityCompat.shouldShowRequestPermissionRationale(
        this, Manifest.permission.READ_EXTERNAL_STORAGE
    )
}

fun storagePermission() = when {
    Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU -> Manifest.permission.READ_MEDIA_IMAGES
    else -> Manifest.permission.READ_EXTERNAL_STORAGE
}
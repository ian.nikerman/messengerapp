package com.messenger.utils

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import kotlin.random.Random

var mColors = listOf(
    "F94144",
    "F3722C",
    "F8961E",
    "F9C74F",
    "90BE6D",
    "43AA8B",
    "C879FF",
    "FFB7FF",
    "3BF4FB",
    "CAFF8A",
    "AAF683",
    "FFD97D",
    "01BEFE",
    "FEE440",
    "00BBF9",
    "00F5D4",
    "90BE6D",
    "EA698B",
    "FFDE05",
    "93E01F",
    "19A3FE",
    "901DF5",
    "fd2ea3",
    "fe6fc5",
    "fcadd8",
    "05eeff",
    "ff0019",
    "fe42d6",
    "05eeff",
    "f4f231",
    "D55D92",
    "FF0F77"
)

private fun getRandomNumber(): Int {
    return Random.nextInt(mColors.size - 1)
}

fun getRandomColor(): Int {
    return Color.parseColor("#" + mColors[getRandomNumber()])
}

fun getColoredCircle(color: Int?): GradientDrawable {
    val shape = GradientDrawable()
    shape.shape = GradientDrawable.OVAL
    shape.setColor(color ?: getRandomColor())
    return shape
}
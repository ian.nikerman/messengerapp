package com.messenger.utils

import java.text.DecimalFormat

fun String.getNameInitials() = if (this.contains(" ")) {
    "${this.substring(0, 1)}${
        this.substringAfter(" ").substring(0, 1)
    }".uppercase()
} else {
    this.substring(0, 1).uppercase()
}

fun formatFileSize(size: Long): String {
    var hrSize: String? = null
    val b = size.toDouble()
    val k = size / 1024.0
    val m = size / 1024.0 / 1024.0
    val g = size / 1024.0 / 1024.0 / 1024.0
    val t = size / 1024.0 / 1024.0 / 1024.0 / 1024.0
    val dec = DecimalFormat("0.00")
    hrSize = when {
        t > 1 -> {
            dec.format(t) + " TB"
        }

        g > 1 -> {
            dec.format(g) + " GB"
        }

        m > 1 -> {
            dec.format(m) + " MB"
        }

        k > 1 -> {
            dec.format(k) + " KB"
        }

        else -> {
            dec.format(b) + " Bytes"
        }
    }
    return hrSize
}
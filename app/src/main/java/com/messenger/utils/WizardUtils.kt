package com.messenger.utils

import android.content.Context
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.FragmentActivity

class WizardUtils {
    companion object {

        fun closeKeyBoard(activity: FragmentActivity) {
            val manager =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            manager.hideSoftInputFromWindow(
                activity.currentFocus?.windowToken, InputMethodManager.HIDE_IMPLICIT_ONLY
            )
        }

        fun showKeyBoard(activity: FragmentActivity?) {
            val manager =
                activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            manager.showSoftInput(
                activity.currentFocus, InputMethodManager.SHOW_IMPLICIT
            )
        }
    }
}
package com.messenger.utils.extensions

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.os.SystemClock
import android.text.Spannable
import android.text.SpannableString
import android.text.TextDirectionHeuristics
import android.text.style.ReplacementSpan
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import kotlin.math.pow
import kotlin.random.Random

class AnimatedTextView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {

    private val frameDuration: Long = 40L
    private val animationCallback = Runnable { animationCallbackImpl() }

    private fun spans() =
        (text as? SpannableString)?.getSpans(0, text.length, CharAnimationSpan::class.java)

    private fun animating(): Boolean {
        return spans()?.firstOrNull { !it.animationFinished() }?.let {
            true
        } ?: false
    }

    private fun animationCallbackImpl() {
        invalidate()
        if (animating()) {
            postDelayed(animationCallback, frameDuration)
        } /*else {
            removeSpans()
        }*/
    }

    private fun startTextAnimation() {
        post {
            animationCallback.run()
        }
    }

    private fun stopTextAnimation() {
        removeCallbacks(animationCallback)
        invalidate()
    }

    private fun removeSpans() {
        (text as? Spannable)?.let { spannable ->
            spans()?.forEach {
                spannable.removeSpan(it)
            }
            super.setText(spannable, BufferType.SPANNABLE)
        }
    }

    // This function should be tuned and some custom properties should be extracted
    // Meanwhile there are magic numbers here =(
    private fun getRandomDuration() = 100L + Random.nextInt(2).toDouble().pow(2.0).toInt() * 100

    override fun setText(text: CharSequence?, type: BufferType?) {
        if (!isInEditMode && !text.isNullOrEmpty() && this.text.toString() != text.toString()) {
            textDirection =
                if (TextDirectionHeuristics.FIRSTSTRONG_LTR.isRtl(text, 0, text.length)) {
                    View.TEXT_DIRECTION_RTL
                } else {
                    View.TEXT_DIRECTION_LTR
                }
            stopTextAnimation()
            val spannable = SpannableString(text)
            for (i in text.indices) {
                val span = CharAnimationSpan(getRandomDuration(), frameDuration)
                spannable.setSpan(span, i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
            super.setText(spannable, BufferType.SPANNABLE)

            startTextAnimation()
        } else {
            super.setText(text, type)
        }
    }
}

private class CharAnimationSpan(val animationDuration: Long, val animationStep: Long) :
    ReplacementSpan() {

    companion object {
        private val specialChars = "!@#$%^&*()_+<>?}{}".toCharArray().toList()
        private val digitsPool: List<Char> = ('0'..'9').toList()
        private val lowerCaseLettersPool: List<Char> = ('a'..'z').toList() //+ specialChars
        private val uppercaseCaseLettersPool: List<Char> = ('A'..'Z').toList() //+ specialChars
    }

    private var originalWidth: Float = 0f
    private var replacementWidth: Float = 0f
    private var replacementChar = ' '

    private val charFirstUpdated = SystemClock.elapsedRealtime()
    private var charLastUpdated: Long = 0

    private fun updateReplacementCharIfNeeded(symbol: Char): Boolean {
        if (animationFinished()) {
            replacementChar = symbol
            return true
        }
        if (SystemClock.elapsedRealtime() - charLastUpdated > animationStep) {
            replacementChar = generateRandomChar(symbol)
            return true
        }
        return false
    }

    fun animationFinished() = SystemClock.elapsedRealtime() - charFirstUpdated > animationDuration

    override fun getSize(
        paint: Paint, text: CharSequence, start: Int, end: Int, fm: Paint.FontMetricsInt?
    ): Int {
        originalWidth = paint.measureText(text, start, end)

        val metrics = paint.fontMetricsInt
        if (fm != null) {
            fm.top = metrics.top
            fm.ascent = metrics.ascent
            fm.descent = metrics.descent
            fm.bottom = metrics.bottom
        }

        return originalWidth.toInt()
    }

    override fun draw(
        canvas: Canvas,
        text: CharSequence,
        start: Int,
        end: Int,
        x: Float,
        top: Int,
        y: Int,
        bottom: Int,
        paint: Paint
    ) {
        if (updateReplacementCharIfNeeded(text[start])) {
            replacementWidth = paint.measureText(replacementChar.toString())
        }

        canvas.drawText(
            replacementChar.toString(),
            x + (originalWidth - replacementWidth) / 2,
            y.toFloat(),
            paint
        )
    }

    private fun generateRandomChar(symbol: Char): Char {
        return when {
            symbol.isDigit() -> digitsPool.random()
            symbol.isLowerCase() -> lowerCaseLettersPool.random()
            symbol.isUpperCase() -> uppercaseCaseLettersPool.random()
            symbol.isSurrogate() -> specialChars.random()
            else -> symbol
        }
    }
}

private fun List<Char>.random() = this[Random.nextInt(this.count())]


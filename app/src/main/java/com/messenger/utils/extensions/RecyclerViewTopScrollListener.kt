package com.messenger.utils.extensions

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class RecyclerViewTopScrollListener(private val mLayoutManager: LinearLayoutManager) :
    RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val firstCompletelyVisibleItem = mLayoutManager.findFirstCompletelyVisibleItemPosition()
        val firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition()

        val scrolledHeight = recyclerView.computeVerticalScrollOffset()
        val firstItemHeight = mLayoutManager.getChildAt(firstCompletelyVisibleItem)?.height ?: 100

        if (scrolledHeight < firstItemHeight && firstVisibleItem == 0) {
            changeScrolledFromTopState(firstItemHeight, scrolledHeight)
        } else {
            changeScrolledFromTopState(1, 1)
        }
    }

    internal abstract fun changeScrolledFromTopState(totalScrollRange: Int, verticalOffset: Int)
}

package com.messenger.utils.extensions

import android.util.Patterns
import com.messenger.utils.Constants.Companion.USE_FOR_SMS_CODE_MAX_VALUE

fun CharSequence?.isValidPhone() = !isNullOrEmpty() && Patterns.PHONE.matcher(this).matches()

fun CharSequence?.isValidEmail() =
    !isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun CharSequence?.isValidDomain() = !isNullOrEmpty() && Patterns.DOMAIN_NAME.matcher(this).matches()

fun CharSequence?.isValidSmsCode() = !isNullOrEmpty() && this.length == USE_FOR_SMS_CODE_MAX_VALUE
package com.messenger.utils.extensions

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.graphics.Color
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.messenger.R
import java.util.*

private const val VIEW_ANIMATION_FADE_SCALE_IN = 150L
private const val VIEW_ANIMATION_FADE_SCALE_OUT = 100L
private const val VIEW_ANIMATION_FADE_IN = 50L
private const val VIEW_ANIMATION_FADE_OUT = 150L
private const val VIEW_ANIMATION_DURATION_IN = 150L
private const val VIEW_ANIMATION_DURATION_OUT = 100L

fun View.animationUpIn() {
    if (!this.isVisible) {
        this.alpha = .3f
        this.translationY = 70f
        this.isVisible = true
        val view = this
        view.animate().translationY(0f).alpha(1f).setDuration(VIEW_ANIMATION_DURATION_IN)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    view.isVisible = true
                }
            })
    }
}

fun View.animationDownOut() {
    if (this.isVisible) {
        this.alpha = 1f
        this.translationY = 0f
        val view = this
        view.animate().setInterpolator(AccelerateDecelerateInterpolator()).translationY(900f)
            .alpha(.3f).setDuration(VIEW_ANIMATION_DURATION_OUT)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    view.isVisible = false
                }
            })
    }
}

fun View.animationFadeIn() {
    if (!this.isVisible) {
        val view = this
        this.alpha = 0f
        this.isVisible = true
        this.animate().setInterpolator(AccelerateDecelerateInterpolator()).alpha(1f)
            .setDuration(VIEW_ANIMATION_FADE_SCALE_IN)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    view.isVisible = true
                }
            })
    }

}

fun View.animationFadeOut() {
    if (this.isVisible) {
        val view = this
        this.alpha = 1f
        this.animate().setInterpolator(AccelerateInterpolator()).alpha(0f)
            .setDuration(VIEW_ANIMATION_FADE_SCALE_OUT)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    view.isVisible = false
                }
            })
    }
}

fun View.animationCaptureIn() {
    val view = this
    view.alpha = 0f
    view.background = ContextCompat.getDrawable(view.context, R.color.white_85)
    view.animate().setInterpolator(AccelerateInterpolator()).alpha(1f)
        .setDuration(VIEW_ANIMATION_FADE_IN).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                view.animationCaptureOut()
            }
        })
}

fun View.animationCaptureOut() {
    val view = this
    view.animate().setInterpolator(AccelerateDecelerateInterpolator()).alpha(0f)
        .setDuration(VIEW_ANIMATION_FADE_OUT).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                view.background = null
                view.alpha = 1f
            }
        })
}

fun View.animationMarkerUp() {
    val view = this
    view.animate().translationY(0f).duration = VIEW_ANIMATION_DURATION_IN
}

fun getRandColor(): Int {
    val rand = Random()
    val r: Int = rand.nextInt(255)
    val g: Int = rand.nextInt(255)
    val b: Int = rand.nextInt(255)
    return Color.rgb(r, g, b)
}

fun View.animationMarkerDown() {
    animate().translationY(-6f).duration = VIEW_ANIMATION_DURATION_OUT
}

fun View.switchViewTo(viewIn: View) {
    this.animationDownOut()
    viewIn.animationUpIn()
}
